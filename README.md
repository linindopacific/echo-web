# Project Echo Application

## What is Echo?
Echo is ....


## Setup

- Copy `env` to `.env` and tailor for your app, specifically the baseURL, apiKey and any database settings.
- Install vendor package with composer. Run it with `composer install`. Updating the installed packages just change `install` with `update`.
- Install asset package with yarn. Run it with `yarn install --modules-folder public/node_modules` (Make sure the node_modules folder inside the public folder). Updating the installed packages just change `install` with `upgrade`.
- Migration Table. `php spark migrate -all` then `php spark migrate`.
- Data Seed. `php spark db:seed AppSeeder`.
- Fake Data Seed (optional). Import fake data; users, services, service_categories, service_reviews. `php spark db:seed SampleSeeder`.

## Server Requirements

Composer

- Just follow the instruction [here](https://getcomposer.org/download/).

Node.js

- Version 10 or higher is required. Required for installing yarn package. Just follow the instruction [here](https://nodejs.org/en/download/).

Yarn

- Version 1 is required. Just follow the instruction [here](https://classic.yarnpkg.com/en/docs/install)

PHP version 7.3 or higher is required, with the following extensions installed:

- [intl](http://php.net/manual/en/intl.requirements.php)
- [libcurl](http://php.net/manual/en/curl.requirements.php) if you plan to use the HTTP\CURLRequest library

Additionally, make sure that the following extensions are enabled in your PHP:

- json (enabled by default - don't turn it off)
- [mbstring](http://php.net/manual/en/mbstring.installation.php)
- [mysqlnd](http://php.net/manual/en/mysqlnd.install.php)
- xml (enabled by default - don't turn it off)