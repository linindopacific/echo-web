<?php namespace App\Libraries;

/*
 * File: XenditLibrary.php
 * Project: echo
 * File Created: Thursday, 27th May 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Wednesday, 7th July 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\I18n\Time;
use Xendit\Xendit;
use Xendit\EWallets;
use Xendit\Retail;
use Xendit\VirtualAccounts;

class XenditLibrary
{
    /**
     * Currency used for the transaction in ISO4217 format - IDR, PHP
     *
     * @var string
     */
    protected $currencyCode = "IDR";

     /**
     * An unique ID.
     *
     * @var string
     */
    protected $id;

    /**
     * An order ID.
     *
     * @var string
     */
    protected $externalId;

    /**
     * Customer name.
     *
     * @var string
     */
    protected $customerName;

    /**
     * Customer phone no.
     *
     * @var string
     */
    protected $customerPhoneNumber;

    /**
     * Payment category.
     *
     * @var string
     */
    protected $category;

    /**
     * Payment channel
     *
     * @var string
     */
    protected $channel;

    /**
     *  The virtual account number you want to assign. And this is what a user will need to tell Retail Outlets cashier or show via barcode page.
     *
     * @var string
     */
    protected $accountNumber;

    /**
     * The specific amount that the virtual account will expect.
     *
     * @var integer
     */
    protected $expectedAmount;

    /**
     * The time when the virtual account will be expired.
     *
     * @var \Datetime
     */
    protected $expiredAt;

    /**
     * Flag which indicates whether redirection is required for end user to complete payment
     * When True, merchants should redirect the end user to the url given in the actions field.
     * When False, there is no need for redirection for payment process to continue
     *
     * @var boolean
     */
    protected $is_redirect_required = false;

    /**
     * eWallet issuer generated URL for web checkout on devices with a stand-alone screen
     *
     * @var string
     */
    protected $desktop_web_checkout_url;

    /**
     * eWallet issuer generated URL for web checkout on mobile devices
     *
     * @var string
     */
    protected $mobile_web_checkout_url;

    /**
     * eWallet issuer generated URL for deeplink checkout on mobile devices (jumps directly into eWallet app for payment confirmation)
     *
     * @var string
     */
    protected $mobile_deeplink_checkout_url;

    /**
     * eWallet issuer generated qr string for checkout usually on devices with a stand-alone screen
     *
     * @var string
     */
    protected $qr_checkout_string;

    /**
     * 	Status of charge request
     *
     * @var string
     */
    protected $status = 'PENDING';

    public function __construct()
    {
        Xendit::setApiKey(env("XND_SECRET_KEY"));
    }

    public function setToExpire(array $params)
    {
        if ( $params["payment_category"] == "VIRTUAL_ACCOUNT" )
        {
            $id = $params["id"];
            $myTime = new Time("now");
            $datetime = new \DateTime($myTime->toDateTimeString());
            $updateParams = ["expiration_date" => $datetime->format(\DateTime::ATOM)];

            try {
                $updateVA = VirtualAccounts::update($id, $updateParams);
            } catch (\Exception $e) {
                log_message("error", $e->getMessage());
            }
        }
    }

    public function inquiry(string $category, array $params)
    {
        $params["order_no"] = str_replace("/", "-", $params["order_no"]);

        $myTime = new Time("now");
        $myTime = $myTime->addHours(2);
        $datetime = new \DateTime($myTime->toDateTimeString());
        $params["expired_at"] = $datetime->format(\DateTime::ATOM); // Updated ISO8601

        $this->externalId = $params["order_no"];
        $this->category = $category;
        $this->channel = $params["payment_channel"];
        $this->customerName = $params["customer_name"];
        $this->customerPhoneNumber = $params["customer_phone_no"];
        $this->expectedAmount = (int)$params["total_amount"];
        $this->expiredAt = $params["expired_at"];

        if ( $category == "VIRTUAL_ACCOUNT" )
        {
            return $this->virtual_account($params);
        }
        elseif ( $category == "RETAIL_OUTLET" ) {
            return $this->retail_outlet();
        }
        elseif ( $category == "EWALLET" ) {
            # code...
            $newChannel = "ID_" . $params["payment_channel"];
            $this->channel = $newChannel;

            return $this->ewallet($params);
        }
    }

    private function virtual_account(array $params)
    {
        $data = [
            "external_id" => $this->externalId,
            "bank_code" => $this->channel,
            "name" => $this->customerName,
            "is_closed" => true,
            "expected_amount" => $this->expectedAmount,
            "expiration_date" => $this->expiredAt,
            "is_single_use" => true
        ];

        if ($data["bank_code"] == "BRI")
        {
            $data["description"] = $params["description"];
        }

        try
        {
            $inquiry = VirtualAccounts::create($data);
            $this->id = $inquiry["id"];
            $this->accountNumber = $inquiry["account_number"];

            return ["status" => true, "data" => $this->onSuccess()];
        }
        catch (\Exception $e)
        {
            return ["status" => false, "message" => $e->getMessage()];
        }
    }

    private function retail_outlet()
    {
        $data = [
            "external_id" => $this->externalId,
            "retail_outlet_name" => $this->channel,
            "name" => $this->customerName,
            "expected_amount" => $this->expectedAmount,
            "expiration_date" => $this->expiredAt,
            "is_single_use" => true
        ];

        try
        {
            $inquiry = Retail::create($data);
            $this->id = $inquiry["id"];
            $this->accountNumber = $inquiry["payment_code"];

            return ["status" => true, "data" => $this->onSuccess()];
        }
        catch (\Exception $e)
        {
            return ["status" => false, "message" => $e->getMessage()];
        }
    }

    private function ewallet(array $params)
    {
        $data = [
            "reference_id" => $this->externalId,
            "currency" => $this->currencyCode,
            "amount" => $this->expectedAmount,
            "checkout_method" => "ONE_TIME_PAYMENT",
            "channel_code" => $this->channel,
            "channel_properties" => [
                "mobile_number" => $this->customerPhoneNumber,
                "success_redirect_url" => site_url("services/order-list")
            ],
            "basket" => [
                [
                    "reference_id" => $params["service_id"],
                    "name" => $params["service_name"],
                    "category" => $params["service_category"],
                    "currency" => $this->currencyCode,
                    "price" => $this->expectedAmount,
                    "quantity" => 1,
                    "type" => "SERVICE"
                ]
            ]
        ];

        try
        {
            $inquiry = EWallets::createEWalletCharge($data);
            $this->id = $inquiry["id"];
            $this->status = $inquiry["status"];
            $this->is_redirect_required = $inquiry["is_redirect_required"];

            $actions = $inquiry["actions"];
            if ( $actions <> NULL )
            {
                $this->desktop_web_checkout_url = $actions["desktop_web_checkout_url"];
                $this->mobile_web_checkout_url = $actions["mobile_web_checkout_url"];
                $this->mobile_deeplink_checkout_url = $actions["mobile_deeplink_checkout_url"];
                $this->qr_checkout_string = $actions["qr_checkout_string"];
            }

            $voided_at = $inquiry["voided_at"];
            if ( $voided_at <> NULL)
            {
                $time = Time::parse($voided_at, "UTC");
                $time2 = $time->setTimezone("Asia/Jakarta");
                $this->expiredAt = $time2->toDateTimeString();
            }

            return ["status" => true, "data" => $this->onSuccess()];
        }
        catch (\Exception $e)
        {
            return ["status" => false, "message" => $e->getMessage()];
        }
    }

    private function onSuccess()
    {
        return [
            "id" => $this->id,
            "external_id" => $this->externalId,
            "payment_code" => $this->accountNumber,
            "name" => $this->customerName,
            "amount" => $this->expectedAmount,
            "expired_at" => $this->expiredAt,
            "status" => $this->status,
            "is_redirect_required" => $this->is_redirect_required,
            "desktop_web_checkout_url" => $this->desktop_web_checkout_url,
            "mobile_web_checkout_url" => $this->mobile_web_checkout_url,
            "mobile_deeplink_checkout_url" => $this->mobile_deeplink_checkout_url,
            "qr_checkout_string" => $this->qr_checkout_string
        ];
    }
}