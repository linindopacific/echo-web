<?php namespace App\Libraries;

/*
 * File: NotificationLibrary.php
 * Project: echo
 * File Created: Monday, 21st December 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Tuesday, 27th April 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use App\Entities\Notifications;
use App\Entities\NotificationChange;
use App\Entities\NotificationObject;
use App\Models\NotificationChangeModel;
use App\Models\NotificationEntityModel;
use App\Models\NotificationObjectModel;
use App\Models\NotificationsModel;

class NotificationLibrary
{
    /**
     * Get Entity Reverse
     *
     * @param string $name
     * @return array
     */
    private function entityReverse(string $name) : array
    {
        switch ($name) {
            case 'status-react':
                $names = ['status-react-like'];
                break;
            case 'relation-cancel':
                $names = ['relation-invite'];
                break;
            case 'relation-reject':
                $names = ['relation-invite'];
                break;
            default:
                $names = [];
                break;
        }

        if ( count($names) > 0 )
        {
            $model = new NotificationEntityModel();
            $entityTypeIds = $model->asArray()->select('id')->whereIn('name', $names)->findAll();

            return $entityTypeIds;
        }
        else
        {
            return [];
        }
    }

    /**
     * Get Entity Type of Id
     *
     * @param string $name
     * @return integer|null
     *
     */

    private function entityTypeId(string $name)
    {
        $model = new NotificationEntityModel();
        $entity = $model->where('name', $name)->first();

        return ( empty($entity) ) ? null : $entity->id;
    }

    private function getEntity(string $name) : int
    {
        switch ($name) {
            case 'create_post':
                $type = 1;
                break;
            case 'update_post':
                $type = 2;
                break;
            case 'delete_post':
                $type = 3;
                break;
            case 'like_post':
                $type = 4;
                break;
            case 'unlike_post':
                $type = 5;
                break;
            case 'create_service':
                $type = 6;
                break;
            case 'update_service':
                $type = 7;
                break;
            case 'delete_service':
                $type = 8;
                break;
            case 'create_review':
                $type = 9;
                break;
            case 'update_review':
                $type = 10;
                break;
            case 'delete_review':
                $type = 11;
                break;
            case 'create_order':
                $type = 12;
                break;
            case 'create_payment':
                $type = 13;
                break;
            case 'fulfill_order':
                $type = 14;
                break;
            case 'finish_order':
                $type = 15;
                break;
            case 'take_payment':
                $type = 16;
                break;
            default:
                $type = 0;
                break;
        }

        return $type;
    }

    /**
     * Save Notification Object
     *
     * @param string $nameType
     * @param integer $id
     * @param integer $userId
     * @return void
     */
    function saveObject(string $nameType, int $id, int $userId)
    {
        $entityTypeId = $this->entityTypeId($nameType);

        if ( $entityTypeId <> NULL )
        {
            // check if correction type
            $entityTypeIds0 = $this->entityReverse($nameType);
            if ( count($entityTypeIds0) == 0 )
            {
                $objectModel = new NotificationObjectModel();
                $object = new NotificationObject();
                $object->entity_type_id = $entityTypeId;
                $object->entity_id = $id;
                $object->status = 1;

                if ( ! $objectModel->save($object) )
                {
                    log_message('error', 'Unable to save: ' . implode(' ', $objectModel->errors()));
                }
                else
                {
                    $objectId = $objectModel->getInsertID();
                    if ( ! $this->saveChanges($objectId, $userId) )
                    {
                        return 0;
                    }
                    else
                    {
                        return $objectId;
                    }
                }
            }
            else
            {
                for ($i=0; $i < count($entityTypeIds0); $i++)
                {
                    $entityTypeId0 = $entityTypeIds0[$i]['id'];
                    if ( in_array($nameType, ['relation-cancel', 'relation-reject']) )
                        $userId = 0;

                    if ( $entityTypeId0 <> 0 )
                    {
                        $objectModel = new NotificationObjectModel();
                        $object = $objectModel->get_object_from_activity($entityTypeId0, $id, $userId);
                        $ids = [];
                        foreach ( $object as $row )
                        {
                            $ids[] = $row->id;
                        }

                        if ( !$objectModel->delete($ids) )
                        {
                            log_message('error', 'Unable to delete ' . implode($objectModel->errors()));
                        }
                    }
                }

                return 0;
            }
        }
        else
        {
            log_message('error', 'Notification entity of {name} was not found', ['name' => $nameType]);
            return 0;
        }
    }

    /**
     * Save the notification changes
     *
     * @param integer $objectId
     * @param integer $userId
     * @return bool
     */
    function saveChanges(int $objectId, int $userId) : bool
    {
        $changeModel = new NotificationChangeModel();
        $change = new NotificationChange();
        $change->notification_object_id = $objectId;
        $change->actor_id = $userId;

        if ( ! $changeModel->save($change) )
        {
            log_message('error', 'Unable to save: ' . implode(' ', $changeModel->errors()));
            return false;
        }
        else
        {
            return true;
        }
    }

    /**
     * Save notification and broadcast with pusher beams
     *
     * @param integer $objectId
     * @param array $userIds
     * @return void
     */
    function notifyUsers(int $objectId, array $userIds)
    {
        if ($objectId == 0)
            return;

        $notifyModel = new NotificationsModel();

        for ($i=0; $i < count($userIds); $i++) {
            $notify = new Notifications();
            $notify->notification_object_id = $objectId;
            $notify->notifier_id = $userIds[$i];
            $notify->status = 0;

            if ( ! $notifyModel->save($notify) )
            {
                log_message('error', 'Unable to save: ' . implode(' ', $notifyModel->errors()));
                return false;
            }
            else
            {
                $notifyId = $notifyModel->getInsertID();
                $notifyModel->beamsNotification($notifyId);
            }
        }

        return true;
    }
}