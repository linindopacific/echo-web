<?php namespace App\Authentication;

use \Config\Services;

class LocalAuthenticator extends \Myth\Auth\Authentication\LocalAuthenticator
{
    /**
     * Attempts to validate the credentials and log a user in.
     *
     * @param array $credentials
     * @param bool  $remember Should we remember the user (if enabled)
     *
     * @return bool
     */
    public function attempt(array $credentials, bool $remember = null): bool
    {
        $this->user = $this->validate($credentials, true);

        if (empty($this->user))
        {
            // Always record a login attempt, whether success or not.
            $ipAddress = Services::request()->getIPAddress();
            $this->recordLoginAttempt($credentials['email'] ?? $credentials['username'], $ipAddress, $this->user->id ?? null, false);

            $this->user = null;
            return false;
        }

        if ($this->user->isBanned())
        {
            // Always record a login attempt, whether success or not.
            $ipAddress = Services::request()->getIPAddress();
            $this->recordLoginAttempt($credentials['email'] ?? $credentials['username'], $ipAddress, $this->user->id ?? null, false);

            $this->error = lang('Auth.userIsBanned');

            $this->user = null;
            return false;
        }

        if (! $this->user->isActivated())
        {
            // Always record a login attempt, whether success or not.
            $ipAddress = Services::request()->getIPAddress();
            $this->recordLoginAttempt($credentials['email'] ?? $credentials['username'], $ipAddress, $this->user->id ?? null, false);

            $param = http_build_query([
                'login' => urlencode($credentials['email'] ?? $credentials['username'])
            ]);

            $this->error = lang('Auth.notActivated') .' '. anchor(route_to('resend-activate-account').'?'.$param, lang('Auth.activationResend'));

            $this->user = null;
            return false;
        }

        return $this->login($this->user, $remember);
    }

    /**
     * Record a login attempt
     *
     * @param string      $email
     * @param string|null $ipAddress
     * @param int|null    $userID
     *
     * @param bool        $success
     *
     * @return bool|int|string
     */
    public function recordLoginAttempt(string $email, string $ipAddress=null, int $userID=null, bool $success)
    {
        $loginModel = new \App\Models\LoginModel();
        return $loginModel->insert([
            'ip_address' => $ipAddress,
            'email' => $email,
            'user_id' => $userID,
            'date' => date('Y-m-d H:i:s'),
            'success' => (int)$success
        ]);
    }
}
