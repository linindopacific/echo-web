<?php namespace App\Authentication\Activators;

use Config\Email;
use CodeIgniter\Entity\Entity;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;

/**
 * Class EmailActivator
 *
 * Sends an activation email to user.
 *
 * @package App\Authentication\Activators
 */
class EmailActivator extends \Myth\Auth\Authentication\Activators\BaseActivator implements \Myth\Auth\Authentication\Activators\ActivatorInterface
{
    /**
     * @var string
     */
    protected $error;

    /**
     * Sends an activation email
     *
     * @param User $user
     *
     * @return mixed
     */
    public function send(Entity $user = null): bool
    {
        $config = new Email();
        $email = new PHPMailer(true);

        $settings = $this->getActivatorSettings();

        // Mailer configs
        $email->isSMTP();
        $email->SMTPDebug = SMTP::DEBUG_OFF ;
        $email->Host = $config->SMTPHost;
        $email->SMTPAuth = true;
        $email->Username = $config->SMTPUser;
        $email->Password = $config->SMTPPass;
        $email->SMTPSecure = $config->SMTPCrypto;
        $email->Port = $config->SMTPPort;
        $email->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );

        // Recipients
        $email->setFrom($settings->fromEmail ?? $config->fromEmail, $settings->fromName ?? $config->fromName);
        $email->addAddress($user->email);
        //$email->addReplyTo('dpratama@group.linindo.com', 'Information');

        // bounces will be sent to
        $email->Sender = 'dpratama@group.linindo.com';

        // Content
        $email->isHTML(true);
        $email->Subject = lang('Auth.activationSubject');
        $body = view($this->config->views['emailActivation'], ['hash' => $user->activate_hash]);
        $email->Body = $body;
        $email->AltBody = strip_tags($body);

        // Send the message, check for errors
        if (! $email->send())
        {
            $this->error = lang('Auth.errorSendingActivation', [$user->email]);
            log_message("error", "Activation email could not be sent to {email} - {message}", ["email" => $user->email, "message" => $email->ErrorInfo]);

            return false;
        }

	    log_message("info", "Activation email has been sent to {email}", ["email" => $user->email]);
        return true;
    }

    /**
     * Returns the error string that should be displayed to the user.
     *
     * @return string
     */
    public function error(): string
    {
        return $this->error ?? '';
    }

}