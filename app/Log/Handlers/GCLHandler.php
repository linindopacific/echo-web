<?php namespace App\Log\Handlers;

/*
 * File: GCLHandler.php
 * Project: echo
 * File Created: Tuesday, 13th July 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Tuesday, 13th July 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Log\Handlers\BaseHandler;
use Google\Cloud\Logging\LoggingClient;

class GCLHandler extends BaseHandler
{
    /**
     * Channel name
     *
     * @var string
     */
	protected $channelName;

    /**
     * Google service account key file
     *
     * @var string
     */
    protected $keyFilePath;

    //--------------------------------------------------------------------

	/**
	 * Constructor
	 *
	 * @param array $config
	 */
	public function __construct(array $config = [])
	{
		parent::__construct($config);

		$this->channelName = empty(env('GOOGLE_LOG_NAME')) ? 'echo' : env('GOOGLE_LOG_NAME');

		$this->keyFilePath = env('GOOGLE_SERVICE_ACCOUNT');
	}

	//--------------------------------------------------------------------

	/**
	 * Handles logging the message.
	 * If the handler returns false, then execution of handlers
	 * will stop. Any handlers that have not run, yet, will not
	 * be run.
	 *
	 * @param string $level
	 * @param string $message
	 *
	 * @return boolean
	 * @throws Exception
	 */
	public function handle($level, $message): bool
	{
        try {
            $logging = new LoggingClient([
                'keyFilePath' => FCPATH . $this->keyFilePath
            ]);

            $psrLogger = $logging->psrLogger($this->channelName);
            $psrLogger->log($level, $message);

            return true;
        } catch (\Throwable $th) {
            return false;
        }
	}

	//--------------------------------------------------------------------
}