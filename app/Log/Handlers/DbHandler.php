<?php namespace App\Log\Handlers;

/*
 * File: DbHandler.php
 * Project: echo
 * File Created: Tuesday, 13th July 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Tuesday, 13th July 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Log\Handlers\BaseHandler;

class DbHandler extends BaseHandler
{
    /**
     * Channel name
     *
     * @var string
     */
	protected $channelName;

    /**
     * Database connection
     *
     * @var \Config\Database
     */
    protected $db;

    /**
     * Table name
     *
     * @var string
     */
    protected $table;

    /**
     * Unix timestamp
     *
     * @var int
     */
    protected $time;

    //--------------------------------------------------------------------

	/**
	 * Constructor
	 *
	 * @param array $config
	 */
	public function __construct(array $config = [])
	{
		parent::__construct($config);

        $this->db = \Config\Database::connect();
        $this->channelName = $config['channelName'];
        $this->table = $config['tableName'];
        $this->time = time();

        if ( !$this->db->tableExists($this->table) )
        {
            $sql = "CREATE TABLE IF NOT EXISTS " . $this->table . " (`channel` VARCHAR(255), `level` INTEGER, `message` LONGTEXT, `time` INTEGER UNSIGNED)";
            $this->db->query($sql);
        }
	}

	//--------------------------------------------------------------------

	/**
	 * Handles logging the message.
	 * If the handler returns false, then execution of handlers
	 * will stop. Any handlers that have not run, yet, will not
	 * be run.
	 *
	 * @param string $level
	 * @param string $message
	 *
	 * @return boolean
	 * @throws Exception
	 */
	public function handle($level, $message): bool
	{
        $sql = "INSERT INTO " . $this->table . " (`channel`, `level`, `message`, `time`) VALUES (?, ?, ?, ?)";

        try {
            $this->db->query($sql, [$this->channelName, $level, $message, $this->time]);
            $this->db->close();
            return true;
        } catch (\Throwable $th) {
            return false;
        }
	}

	//--------------------------------------------------------------------
}