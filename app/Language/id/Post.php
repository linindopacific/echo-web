<?php

return [
    'errorDelete' => 'Terjadi kesalahan saat menghapus Pos.',
    'successDelete' => 'Pos berhasil dihapus',
    'unableToDelete' => 'Anda tidak dapat menghapus Pos ini.',
];