<?php

/*
 * File: Payment.php
 * Project: echo
 * File Created: Friday, 28th May 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Wednesday, 16th June 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

return [
    'va' => [
        'bca' => [
            'internal' => [
                'atm' => [
                    'Masukkan kartu ATM dan PIN BCA Anda',
                    'Di menu utama, pilih "Transaksi Lainnya". Pilih "Transfer". Pilih "Ke BCA Virtual Account"',
                    'Masukkan nomor Virtual Account',
                    'Pastikan data Virtual Account Anda benar, kemudian masukkan angka yang perlu Anda bayarkan, kemudian pilih "Benar"',
                    'Cek dan perhatikan konfirmasi pembayaran dari layar ATM, jika sudah benar pilih "Ya", atau pilih "Tidak" jika data di layar masih salah',
                    'Transaksi Anda sudah selesai. Pilih "Tidak" untuk tidak melanjutkan transaksi lain',
                    'Setelah transaksi pembayaran Anda selesai, invoice ini akan diperbarui secara otomatis. Ini bisa memakan waktu hingga 5 menit'
                ],
                'mbanking' => [
                    'Buka Aplikasi BCA Mobile',
                    'Pilih "m-BCA", kemudian pilih "m-Transfer"',
                    'Pilih "BCA Virtual Account"',
                    'Masukkan nomor Virtual Account, lalu pilih "OK"',
                    'Klik tombol "Send" yang berada di sudut kanan atas aplikasi untuk melakukan transfer',
                    'Klik "OK" untuk melanjutkan pembayaran',
                    'Masukkan PIN Anda untuk meng-otorisasi transaksi',
                    'Setelah transaksi pembayaran Anda selesai, invoice ini akan diperbarui secara otomatis. Ini bisa memakan waktu hingga 5 menit'
                ],
                'ibanking' => [
                    'Login ke KlikBCA Individual',
                    'Pilih "Transfer", kemudian pilih "Transfer ke BCA Virtual Account"',
                    'Masukkan nomor Virtual Account',
                    'Pilih "Lanjutkan" untuk melanjutkan pembayaran',
                    'Masukkan "RESPON KEYBCA APPLI 1" yang muncul pada Token BCA Anda, lalu klik tombol "Kirim"',
                    'Transaksi Anda sudah selesai',
                    'Setelah transaksi pembayaran Anda selesai, invoice ini akan diperbarui secara otomatis. Ini bisa memakan waktu hingga 5 menit'
                ]
            ],
            'interbank' => [

            ]
        ],
        'bni' => [
            'internal' => [
                'atm' => [
                    'Masukkan Kartu Anda',
                    'Pilih Bahasa',
                    'Masukkan PIN ATM Anda',
                    'Pilih "Menu Lainnya"',
                    'Pilih "Transfer"',
                    'Pilih Jenis rekening yang akan Anda gunakan (contoh: "Dari Rekening Tabungan")',
                    'Pilih "Virtual Account Billing"',
                    'Masukkan nomor Virtual Account Anda (contoh: 88089999XXXXXX)',
                    'Konfirmasi, apabila telah sesuai, lanjutkan transaksi',
                    'Transaksi Anda telah selesai'
                ],
                'mbanking' => [
                    'Akses BNI Mobile Banking dari handphone kemudian masukkan user ID dan password',
                    'Pilih menu "Transfer"',
                    'Pilih menu "Virtual Account Billing" kemudian pilih rekening debet',
                    'Masukkan nomor Virtual Account Anda (contoh: 88089999XXXXXX) pada menu "input baru"',
                    'Konfirmasi transaksi Anda',
                    'Masukkan Password Transaksi',
                    'Pembayaran Anda Telah Berhasil'
                ],
                'ibanking' => [
                    'Ketik alamat https://ibank.bni.co.id kemudian klik "Enter"',
                    'Masukkan User ID dan Password',
                    'Pilih menu "Transfer"',
                    'Pilih "Virtual Account Billing"',
                    'Virtual Account Anda (contoh: 88089999XXXXXX) yang hendak dibayarkan. Lalu pilih rekening debet yang akan digunakan. Kemudian tekan "lanjut"',
                    'Periksa ulang mengenai transaksi yang Anda ingin lakukan',
                    'Masukkan Kode Otentikasi Token',
                    'Pembayaran Anda berhasil'
                ]
            ],
            'interbank' => [

            ]
        ],
        'bri' => [
            'internal' => [
                'atm' => [
                    'Pilih menu Transaksi Lain',
                    'Pilih menu Lainnya',
                    'Pilih menu Pembayaran',
                    'Pilih menu Lainnya',
                    'Pilih menu BRIVA',
                    'Masukkan Nomor BRI Virtual Account (Contoh: 26215-xxxxxxxxxxxx), lalu tekan "Benar"',
                    'Konfirmasi pembayaran, tekan "Ya" bila sesuai'
                ],
                'mbanking' => [
                    'Log in ke Mobile Banking',
                    'Pilih menu Pembayaran',
                    'Pilih menu BRIVA',
                    'Masukkan nomor BRI Virtual Account dan jumlah pembayaran',
                    'Masukkan nomor PIN Anda',
                    'Tekan "OK" untuk melanjutkan transaksi',
                    'Transaksi berhasil',
                    'SMS konfirmasi akan masuk ke nomor telepon Anda'
                ],
                'ibanking' => [
                    'Masukan User ID dan Password',
                    'Pilih menu Pembayaran',
                    'Pilih menu BRIVA',
                    'Pilih rekening Pembayar',
                    'Masukkan Nomor Virtual Account BRI Anda (Contoh: 26215-xxxxxxxxxxxx)',
                    'Masukkan nominal yang akan dibayar',
                    'Masukkan password dan Mtoken Anda'
                ]
            ],
            'interbank' => [

            ]
        ],
        'bss' => [
            'internal' => [
                'atm' => [
                    'Masukkan kartu ATM dan PIN Anda',
                    'Pilih menu TRANSAKSI LAINNYA → TRANSFER → TRANSFER KE BANK SAHABAT SAMPOERNA',
                    'Masukkan 16 digit nomor rekening virtual account',
                    'Masukkan nominal pembayaran',
                    'Cek kembali transaksi Anda, lalu pilih YA untuk melanjutkan',
                    'Pembayaran melalui ATM sukses dilakukan'
                ],
                'mbanking' => [
                    'Login akun Mobile Banking pada handphone Anda',
                    'Pilih menu TRANSFER DANA → TRANSFER KE ANTAR BANK. Masukkan kode BANK SAHABAT SAMPOERNA yaitu 523 → Pilih YA',
                    'Masukkan 16 digit nomor rekening virtual account',
                    'Masukkan nominal pembayaran',
                    'Input token M-Banking Anda',
                    'Pembayaran melalui M-Banking sukses dilakukan'
                ],
                'ibanking' => [
                    'Login akun Internet Banking pada handphone Anda',
                    'Pilih menu TRANSFER DANA → TRANSFER KE REKENING BANK SAHABAT SAMPOERNA',
                    'Masukkan 16 digit nomor rekening virtual account',
                    'Masukkan nominal pembayaran',
                    'Input token i-Banking Anda',
                    'Pembayaran melalui i-Banking sukses dilakukan'
                ]
            ],
            'interbank' => [

            ]
        ],
        'mandiri' => [
            'internal' => [
                '88608' => [
                    'atm' => [
                        'Masukkan kartu ATM dan pilih "Bahasa Indonesia"',
                        'Ketik nomor PIN dan tekan BENAR',
                        'Pilih menu "BAYAR/BELI"',
                        'Pilih menu "MULTI PAYMENT"',
                        'Ketik kode perusahaan, yaitu "88608" atau Xendit, tekan "BENAR"',
                        'Masukkan nomor Virtual Account',
                        'Isi NOMINAL, kemudian tekan "BENAR"',
                        'Muncul konfirmasi data customer. Pilih Nomor 1 sesuai tagihan yang akan dibayar, kemudian tekan "YA"',
                        'Muncul konfirmasi pembayaran. Tekan "YA" untuk melakukan pembayaran',
                        'Bukti pembayaran dalam bentuk struk agar disimpan sebagai bukti pembayaran yang sah dari Bank Mandiri',
                        'Transaksi Anda sudah selesai'
                    ],
                    'mbanking' => [
                        'Log in Mobile Banking Mandiri Online (Install Mandiri Online by PT. Bank Mandiri (Persero Tbk.)',
                        'Klik "Icon Menu" di sebelah kiri atas',
                        'Pilih menu "Pembayaran"',
                        'Pilih "Buat Pembayaran Baru"',
                        'Pilih "Multi Payment"',
                        'Pilih Penyedia Jasa "Xendit 88608"',
                        'Pilih "No. Virtual"',
                        'Masukkan no virtual account dengan kode perusahaan "88608" lalu pilih "Tambah Sebagai Nomor Baru"',
                        'Masukkan "Nominal" lalu pilih "Konfirmasi"',
                        'Pilih "Lanjut"',
                        'Muncul tampilan konfirmasi pembayaran',
                        'Scroll ke bawah di tampilan konfirmasi pembayaran lalu pilih "Konfirmasi"',
                        'Masukkan "PIN" dan transaksi telah selesai'
                    ],
                    'ibanking' => [
                        'Kunjungi website Mandiri Internet Banking: https://ibank.bankmandiri.co.id/retail3/',
                        'Login dengan memasukkan USER ID dan PIN',
                        'Pilih "Pembayaran"',
                        'Pilih "Multi Payment"',
                        'Pilih "No Rekening Anda"',
                        'Pilih Penyedia Jasa "Xendit 88608"',
                        'Pilih "No Virtual Account"',
                        'Masukkan nomor Virtual Account Anda',
                        'Masuk ke halaman konfirmasi 1',
                        'Apabila benar/sesuai, klik tombol tagihan TOTAL, kemudian klik "Lanjutkan"',
                        'Masuk ke halaman konfirmasi 2',
                        'Masukkan Challenge Code yang dikirimkan ke Token Internet Banking Anda, kemudian klik "Kirim"',
                        'Anda akan masuk ke halaman konfirmasi jika pembayaran telah selesai'
                    ]
                ],
                '88908' => [
                    'atm' => [
                        'Masukkan kartu ATM dan pilih "Bahasa Indonesia"',
                        'Ketik nomor PIN dan tekan BENAR',
                        'Pilih menu "BAYAR/BELI"',
                        'Pilih menu "MULTI PAYMENT"',
                        'Ketik kode perusahaan, yaitu "88908" atau Xendit, tekan "BENAR"',
                        'Masukkan nomor Virtual Account',
                        'Isi NOMINAL, kemudian tekan "BENAR"',
                        'Muncul konfirmasi data customer. Pilih Nomor 1 sesuai tagihan yang akan dibayar, kemudian tekan "YA"',
                        'Muncul konfirmasi pembayaran. Tekan "YA" untuk melakukan pembayaran',
                        'Bukti pembayaran dalam bentuk struk agar disimpan sebagai bukti pembayaran yang sah dari Bank Mandiri',
                        'Transaksi Anda sudah selesai'
                    ],
                    'mbanking' => [
                        'Log in Mobile Banking Mandiri Online (Install Mandiri Online by PT. Bank Mandiri (Persero Tbk.)',
                        'Klik "Icon Menu" di sebelah kiri atas',
                        'Pilih menu "Pembayaran"',
                        'Pilih "Buat Pembayaran Baru"',
                        'Pilih "Multi Payment"',
                        'Pilih Penyedia Jasa "Xendit 88908"',
                        'Pilih "No. Virtual"',
                        'Masukkan no virtual account dengan kode perusahaan "88908" lalu pilih "Tambah Sebagai Nomor Baru"',
                        'Masukkan "Nominal" lalu pilih "Konfirmasi"',
                        'Pilih "Lanjut"',
                        'Muncul tampilan konfirmasi pembayaran',
                        'Scroll ke bawah di tampilan konfirmasi pembayaran lalu pilih "Konfirmasi"',
                        'Masukkan "PIN" dan transaksi telah selesai'
                    ],
                    'ibanking' => [
                        'Kunjungi website Mandiri Internet Banking: https://ibank.bankmandiri.co.id/retail3/',
                        'Login dengan memasukkan USER ID dan PIN',
                        'Pilih "Pembayaran"',
                        'Pilih "Multi Payment"',
                        'Pilih "No Rekening Anda"',
                        'Pilih Penyedia Jasa "Xendit 88908"',
                        'Pilih "No Virtual Account"',
                        'Masukkan nomor Virtual Account Anda',
                        'Masuk ke halaman konfirmasi 1',
                        'Apabila benar/sesuai, klik tombol tagihan TOTAL, kemudian klik "Lanjutkan"',
                        'Masuk ke halaman konfirmasi 2',
                        'Masukkan Challenge Code yang dikirimkan ke Token Internet Banking Anda, kemudian klik "Kirim"',
                        'Anda akan masuk ke halaman konfirmasi jika pembayaran telah selesai'
                    ]
                ]
            ],
            'interbank' => [

            ]
        ],
        'permata' => [
            'internal' => [
                'atm' => [
                    'Pada menu utama, pilih transaksi lain',
                    'Pilih Pembayaran Transfer',
                    'Pilih pembayaran lainnya',
                    'Pilih pembayaran Virtual Account',
                    'Masukkan Nomor Virtual Account Anda',
                    'Pada halaman konfirmasi, akan muncul nominal yang dibayarkan, nomor, dan nama merchant, lanjutkan jika sudah sesuai',
                    'Pilih sumber pembayaran Anda dan lanjutkan',
                    'Transaksi anda selesai',
                    'Ketika transaksi anda sudah selesai, invoice anda akan diupdate secara otomatis. Ini mungkin memakan waktu hingga 5 menit'
                ],
                'mbanking' => [
                    'Buka Permata Mobile X dan Login',
                    'Pilih Pay "Pay Bills"/ "Pembayaran Tagihan"',
                    'Pilih "Virtual Account"',
                    'Masukkan Nomor Virtual Account Anda',
                    'Detail pembayaran Anda akan muncul di layar',
                    'Nominal yang ditagihkan akan muncul di layar. Pilih sumber pembayaran',
                    'Konfirmasi transaksi Anda',
                    'Masukkan kode response token Anda',
                    'Transaksi anda telah selesai',
                    'Ketika transaksi anda sudah selesai, invoice anda akan diupdate secara otomatis. Ini mungkin memakan waktu hingga 5 menit'
                ],
                'ibanking' => [
                    'Buka situs https://new.permatanet.com dan Login',
                    'Pilih menu "Pembayaran"',
                    'Pilih menu "Pembayaran Tagihan"',
                    'Pilih "Virtual Account"',
                    'Pilih sumber pembayaran',
                    'Pilih menu "Masukkan Daftar Tagihan Baru"',
                    'Masukan nomor Virtual Account Anda',
                    'Konfirmasi transaksi Anda',
                    'Masukkan SMS token response',
                    'Pembayaran Anda berhasil',
                    'Ketika transaksi anda sudah selesai, invoice anda akan diupdate secara otomatis. Ini mungkin memakan waktu hingga 5 menit'
                ]
            ],
            'interbank' => [

            ]
        ]
    ],
];