<?php

return [
   'helpCenter'          => 'Pusat Bantuan',
   'about'               => 'Tentang',
   'privacyPolicy'       => 'Kebijakan Privasi',
   'communityGuidelines' => 'Pedoman Komunitas',
   'cookiesPolicy'       => 'Kebijakan Cookie',
   'career'              => 'Karir',
   'language'            => 'Bahasa',
   'copyright'           => 'Hak Cipta',
   'copyrightPolicy'     => 'Kebijakan Hak Cipta',
];
