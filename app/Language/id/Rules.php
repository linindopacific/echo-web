<?php

return [
    'post_content' => 'Konten Pos',
    'post_visibility' => 'Visibilitas Pos',
    'no_changes' => 'Anda tidak membuat perubahan apa pun.',
    'unauthorized' => 'Anda tidak memiliki izin.',

    'service_id' => 'ID Layanan',
    'service_name' => 'Nama Layanan',
    'service_coverage' => 'Cakupan Layanan',
    'service_description' => 'Deskripsi Layanan',
    'service_category' => 'Kategori Layanan',
    'service_price' => 'Harga Layanan',
    'service_price_per' => 'Harga per Layanan',
    'service_featured' => 'Layanan Unggulan',
    'service_pinned' => 'Layanan Disematkan',
    'service_active' => 'Status Layanan',

    'order_id' => 'ID Pesanan',
    'order_no' => 'No. Pesanan',
    'order_provider_name' => 'Nama Penyedia',
    'order_provider_address' => 'Alamat Penyedia',
    'order_provider_city' => 'Kota/Kab. Penyedia',
    'order_provider_zipcode' => 'Kode Pos Penyedia',
    'order_provider_phone' => 'No. Telepon Penyedia',
    'order_customer_name' => 'Nama Pelanggan',
    'order_customer_address' => 'Alamat Pelanggan',
    'order_customer_city' => 'Kota/Kab. Pelanggan',
    'order_customer_zipcode' => 'Kode Pos Pelanggan',
    'order_customer_phone' => 'No. Telepon Pelanggan',
    'order_created_date' => 'Tanggal Pemesanan',
    'order_request_date' => 'Tanggal Permintaan',
    'order_item' => 'Barang',
    'order_description' => 'Deskripsi',
    'order_quantity' => 'Jumlah',
    'order_unit_price' => 'Harga',
    'order_unit_discount' => 'Diskon',
    'order_amount' => 'Harga Total',
    'order_fees' => 'Biaya Pasar',
    'order_remarks' => 'Pesan Keterangan',
    'order_referal_code' => 'Kode Referensi',
    'order_fulfilled' => 'Terpenuhi',
    'order_status' => 'Status Pesanan',
    'order_error' => 'Gagal membuat pesanan',

    'fulfillment_id' => 'ID Pemenuhan',
    'review_content' => 'Konten Ulasan',
    'review_rating' => 'Tingkat Ulasan',

    'payment_fees' => 'Biaya Layanan',

    'xnd' => [
        'id' => 'ID Unik',
        'external_id' => 'ID External',
        'amount' => 'Biaya',
        'category' => 'Kategori',
        'channel' => 'Kanal Pembayaran',
        'code' => 'Kode/Nomor',
        'qr_string' => 'QR String',
        'payment_id' => 'ID Pembayaran',
        'expired_at' => 'Kadaluarsa Pada',
        'transaction_at' => 'Transaksi Pada'
    ],

];