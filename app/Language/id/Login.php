<?php

return [
   'signIn'         => 'Masuk',
   'signUp'         => 'Daftar',
   'rememberMe'     => 'Ingat saya',
   'forgotPassword' => 'Lupa Sandi?',
   'username'       => 'Nama Pengguna',
   'password'       => 'Sandi',
   'repeatPassword' => 'Ulangi Sandi',
   'viaSso'          => 'Masuk dengan Akun Media Sosial',
   'viaSsoGoogle'    => 'Masuk dengan Google',
   'viaSsoMicrosoft'  => 'Masuk dengan Microsoft',
   'viaSsoFacebook'  => 'Masuk dengan Facebook',
   'viaSsoTwitter'   => 'Masuk dengan Twitter',
   'tos'            => 'Ya, saya memahami dan menyetujui Syarat & Ketentuan yang berlaku.',
   'email'          => 'Surel',
   'fullName'       => 'Nama Lengkap',
   'country'        => 'Negara',
];
