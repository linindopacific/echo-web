<?php

return [
    'comment-create' => 'mengomentari postinganmu:',
    'fulfillment-create' => 'menyelesaikan permintaan Anda:',
    'fulfillment-complete' => 'mengakhiri pesanan:',
    'payment-create' => 'meminta layanan Anda:',
    'relation-approve' => 'menerima permintaan pertemanan Anda',
    'relation-invite' => 'mengirimi Anda permintaan pertemanan',
    'review-create' => 'memberikan review di',
    'status-create' => 'membagikan postingnya:',
    'status-react-like' => 'menyukai postinganmu di',
];