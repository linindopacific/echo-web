<?php

return [
   'signIn'         => 'Sign in',
   'signUp'         => 'Sign up',
   'rememberMe'     => 'Remember me',
   'forgotPassword' => 'Forgot Password?',
   'username'       => 'Username',
   'password'       => 'Password',
   'repeatPassword' => 'Repeat Password',
   'viaSso'          => 'Login via Social Account',
   'viaSsoGoogle'    => 'Login via Google',
   'viaSsoMicrosoft'  => 'Login via Microsoft',
   'viaSsoFacebook'  => 'Login via Facebook',
   'viaSsoTwitter'   => 'Login via Twitter',
   'tos'            => 'Yes, I understand and agree to the Terms & Conditions.',
   'email'          => 'Email',
   'fullName'       => 'Full Name',
   'country'        => 'Country',
];
