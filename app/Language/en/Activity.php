<?php

return [
    'comment-create' => 'commented on your post:',
    'fulfillment-create' => 'complete your request:',
    'fulfillment-complete' => 'end order from:',
    'payment-create' => 'requested your service:',
    'relation-approve' => 'accept your friend request',
    'relation-invite' => 'sent you a friend request',
    'review-create' => 'leave a review on',
    'status-create' => 'share a post:',
    'status-react-like' => 'likes on your post',
];