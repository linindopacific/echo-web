<?php

return [
   'helpCenter'          => 'Help Center',
   'about'               => 'About',
   'privacyPolicy'       => 'Privacy Policy',
   'communityGuidelines' => 'Community Guidelines',
   'cookiesPolicy'       => 'Cookie Policy',
   'career'              => 'Career',
   'language'            => 'Language',
   'copyright'           => 'Copyright',
   'copyrightPolicy'     => 'Copyright Policy',
];
