<?php

return [
    'errorDelete' => 'There was an error deleting the Post.',
    'successDelete' => 'Post successfully deleted',
    'unableToDelete' => 'You cannot delete this post.',
];