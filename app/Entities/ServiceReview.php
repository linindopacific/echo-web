<?php namespace App\Entities;

/*
 * File: ServiceReview.php
 * Project: echo
 * File Created: Wednesday, 17th March 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Wednesday, 17th November 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use App\Models\UserProfileModel;
use CodeIgniter\Entity\Entity;

class ServiceReview extends Entity
{
    protected $dates = [
        "created_at",
        "updated_at",
        "deleted_at"
    ];

    public function getUser()
    {
        $userProfileModel = new UserProfileModel();

        return $userProfileModel->getProfile($this->attributes["user_id"]);
    }
}