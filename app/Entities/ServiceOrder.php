<?php namespace App\Entities;

/*
 * File: ServiceOrder.php
 * Project: echo
 * File Created: Wednesday, 25th November 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 15th November 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use App\Models\ServiceFulfillmentModel;
use App\Models\ServiceInvoiceModel;
use App\Models\ServiceReviewModel;
use App\Models\UserModel;
use CodeIgniter\Entity\Entity;
use CodeIgniter\HTTP\URI;

class ServiceOrder extends Entity
{
    protected $dates = [
        "request_date",
        "created_at",
        "updated_at",
        "deleted_at"
    ];

    protected $casts = [
        "created_at" => "datetime",
        "request_date" => "datetime",
        "quantity" => "integer",
        "unit_price" => "integer",
        "unit_discount" => "integer",
        "amount" => "integer",
        "market_fee" => "integer",
        "has_review" => "boolean",
        "has_fulfilled" => "boolean"
    ];

    public function getOrderStatus()
    {
        $fulfillmentModel = new ServiceFulfillmentModel();
        $fulfillment = $fulfillmentModel->where("order_id", $this->attributes["id"])->first();

        if (! empty($fulfillment))
        {
            if ( $fulfillment->completed )
            {
                return "COMPLETED";
            }
            else
            {
                return "FULFILLED";
            }
        }
        else
        {
            $invoice = $this->getInvoice();
            return ( $invoice->status === "PAID" ) ? "PROCESS" : "-";
        }
    }

    public function getInvoice()
    {
        $invoiceModel = new ServiceInvoiceModel();
        return $invoiceModel->where("order_id", $this->attributes["id"])->first();
    }

    public function getPaymentInstruction()
    {
        $invoice = $this->getInvoice();

        if (! empty($invoice))
        {
            $uri = new URI(site_url("services/payment"));
            $redirectURL = $uri->setQuery("id=$invoice->id");
            return anchor($redirectURL, "PENDING", ["target"=>"_blank"]);
        }
        else
        {
            return "PENDING";
        }
    }

    public function getFulfill()
    {
        $fulfillmentModel = new ServiceFulfillmentModel();
        return $fulfillmentModel->where("order_id", $this->attributes["id"])->first();
    }

    public function getHasReview()
    {
        $fulfillment = $this->getFulfill();

        if (! empty($fulfillment))
        {
            $reviewModel = new ServiceReviewModel();
            $review = $reviewModel->where("fulfillment_id", $fulfillment->id)->first();

            return ( !empty($review) ) ? true : false;
        }

        return false;
    }

    public function getHasFulfilled()
    {
        $fulfillment = $this->getFulfill();

        return (! empty($fulfillment)) ? true : false;
    }

    public function getSellToFullAddress()
    {
        $address = $this->attributes["sell_to_address"];
        $city = strtolower($this->attributes["sell_to_city"]);

        return $address . ", " . ucwords($city);
    }

    public function getSellToEmailAddress()
    {
        $userModel = new UserModel();
        $user = $userModel->find($this->attributes["user_id"]);

        return $user->email;
    }

}