<?php namespace App\Entities;

/*
 * File: UserRelation.php
 * Project: echo
 * File Created: Thursday, 26th November 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 15th November 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Entity\Entity;

class UserRelation extends Entity
{
    protected $dates = [
        "created_at",
        "updated_at",
        "deleted_at"
    ];

    public function getStatusStr()
    {
        switch ($this->attributes["status"]) {
            case 0:
                $status = "Pending";
                break;
            case 1:
                $status = "Accepted";
                break;
            case 2:
                $status = "Declined";
                break;
            case 3:
                $status = "Blocked";
                break;
            default:
                $status = "";
                break;
        }

        return $status;
    }

}