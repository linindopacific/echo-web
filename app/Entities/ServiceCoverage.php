<?php namespace App\Entities;

/*
 * File: ServiceCoverage.php
 * Project: echo
 * File Created: Monday, 9th November 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 15th November 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use App\Models\RegRegencyModel;
use CodeIgniter\Entity\Entity;

class ServiceCoverage extends Entity
{
    protected $dates = [
        "created_at",
        "updated_at",
        "deleted_at"
    ];

    public function getName()
    {
        $regencyModel = new RegRegencyModel();
        $regency = $regencyModel->find($this->attributes["regency_id"]);

        return $regency->name;
    }
}