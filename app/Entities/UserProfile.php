<?php namespace App\Entities;

/*
 * File: UserProfile.php
 * Project: echo
 * File Created: Thursday, 27th August 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 15th November 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use App\Models\UploadModel;
use App\Models\UserModel;
use App\Models\UserRelationModel;
use CodeIgniter\Entity\Entity;
use CodeIgniter\I18n\Time;

class UserProfile extends Entity
{
	protected $dates = [
        "created_at",
        "updated_at",
        "deleted_at"
    ];

	protected $casts = [
        "is_visible" => "boolean",
        "is_complete" => "boolean"
    ];

    public function getIsComplete()
    {
        if (empty($this->getDisplayName()) || empty($this->getFullAddress()) || empty($this->attributes["city"]) || empty($this->attributes["region"]) || empty($this->attributes["phone_number"])) {
			return false;
		}

        return true;
    }

    public function getFirstName()
    {
        $this->attributes["first_name"] =  $this->attributes["first_name"] ?? "";

        return $this->attributes["first_name"];
    }

    public function getMiddleName()
    {
        $this->attributes["middle_name"] =  $this->attributes["middle_name"] ?? "";

        return $this->attributes["middle_name"];
    }

    public function getLastName()
    {
        $this->attributes["last_name"] =  $this->attributes["last_name"] ?? "";

        return $this->attributes["last_name"];
    }

    public function getGender()
    {
        $this->attributes["gender"] =  $this->attributes["gender"] ?? "";

        return $this->attributes["gender"];
    }

    public function getAddress1()
    {
        $this->attributes["address_1"] =  $this->attributes["address_1"] ?? "";

        return $this->attributes["address_1"];
    }

    public function getAddress2()
    {
        $this->attributes["address_2"] =  $this->attributes["address_2"] ?? "";

        return $this->attributes["address_2"];
    }

    public function getFullAddress()
    {
        return trim($this->attributes["address_1"] . " " . $this->attributes["address_2"]);
    }

    public function getFullAddressCityRegion()
    {
        return $this->getFullAddress() . ", " . $this->getCity() . ", " . $this->getRegion();
    }

    public function getCity()
    {
        $this->attributes["city"] =  $this->attributes["city"] ?? "";

        return $this->attributes["city"];
    }

    public function getRegion()
    {
        $this->attributes["region"] =  $this->attributes["region"] ?? "";

        return $this->attributes["region"];
    }

    public function getZipCode()
    {
        $this->attributes["zip_code"] =  $this->attributes["zip_code"] ?? "";

        return $this->attributes["zip_code"];
    }

    public function getPhoneNumber()
    {
        $phoneNumber =  $this->attributes["phone_number"] ?? "";

        if ( $phoneNumber <> "" )
        {
            if ( substr($phoneNumber, 0, 1) == "0" )
            {
                $phoneNumber = substr_replace($phoneNumber, "+62", 0, 1);
            }
        }

        return $phoneNumber;
    }

    public function getBithdate()
    {
        $this->attributes["birthdate"] =  $this->attributes["birthdate"] ?? "";

        return $this->attributes["birthdate"];
    }

    public function getDisplayPicture()
    {
        $uploadModel = new UploadModel();

        if ( is_null($this->attributes["display_picture"]) )
        {
            $displayName = $this->getDisplayName();
            $this->attributes["display_picture"] = "https://ui-avatars.com/api/?name= " . $displayName . " &bold=true";
        }

        $imageFile = $uploadModel->find($this->attributes["display_picture"]);
        if (! empty($imageFile))
        {
            $imgUrl = base_url($imageFile->file_path ."/" . $imageFile->file_name);
            $this->attributes["display_picture"] = $imgUrl;
        }

        return $this->attributes["display_picture"];
    }

    public function getDisplayName()
    {
        $displayName = "User";

        $userModel = new UserModel();
        $user = $userModel->find($this->attributes["user_id"]);
        if (! empty($user))
        {
            $displayName = ($user->display_name == "") ? explode("@", $user->email)[0] : $user->display_name;
        }

        return $displayName;
    }

    public function getEmail()
    {
        $emailAddress = "";

        $userModel = new UserModel();
        $user = $userModel->find($this->attributes["user_id"]);
        if (! empty($user))
        {
            $emailAddress = $user->email;
        }

        return $emailAddress;
    }

    public function getJoined()
    {
        $joined = "";

        $userModel = new UserModel();
        $user = $userModel->find($this->attributes["user_id"]);
        if (! empty($user))
        {
            $time = Time::parse($user->created_at);
            $joined = $time->humanize();
        }

        return $joined;
    }

    public function getConnected()
    {
        $userRelationModel = new UserRelationModel();

        return $userRelationModel->hasConnectedWith($this->attributes["user_id"]);
    }

    public function getConnection()
    {
        $userRelationModel = new UserRelationModel();

        return $userRelationModel->statusConnectionWith($this->attributes["user_id"]);
    }

    public function getTotalConnections()
    {
        $userRelationModel = new UserRelationModel();

        $query = $userRelationModel->getConnectionList($this->attributes["user_id"]);
        return count($query);
    }

    public function getTotalPendingRequests()
    {
        $userRelationModel = new UserRelationModel();

        $query = $userRelationModel->getRequestConnectionList($this->attributes["user_id"]);
        return count($query);
    }

    public function setCreatedAt(string $dateString)
    {
        $this->attributes["created_at"] = new Time($dateString, "UTC");

        return $this;
    }

    public function getCreatedAt(string $format = "Y-m-d H:i:s")
    {
        // Convert to CodeIgniter\I18n\Time object
        $this->attributes["created_at"] = $this->mutateDate($this->attributes["created_at"]);

        $timezone = $this->timezone ?? app_timezone();

        $this->attributes["created_at"]->setTimezone($timezone);

        return $this->attributes["created_at"]->format($format);
    }

    public function getUrl()
    {
        return site_url("profile/" . $this->attributes["username"]);
    }
}