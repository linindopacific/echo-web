<?php namespace App\Entities;

/*
 * File: RegRegency.php
 * Project: echo
 * File Created: Friday, 28th August 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 28th June 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Entity\Entity;

class RegRegency extends Entity
{
	protected $dates = [];

	protected $casts = [];

    // public function getName()
    // {
    // 	// replace the string of KAB. to KABUPATEN
    // 	if (strpos($this->attributes['name'], 'KAB.') !== false) {
	// 	    $this->attributes['name'] = str_replace("KAB.","KABUPATEN", $this->attributes['name']);
	// 	}

    //     return $this->attributes['name'];
    // }
}