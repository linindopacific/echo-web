<?php namespace App\Entities;

/*
 * File: ServiceCategory.php
 * Project: echo
 * File Created: Friday, 28th August 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 15th November 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Entity\Entity;
use CodeIgniter\I18n\Time;

class ServiceCategory extends Entity
{
	protected $dates = [
        "created_at",
        "updated_at",
        "deleted_at"
    ];

	protected $casts = [];

	public function setCreatedAt(string $dateString)
    {
        $this->attributes["created_at"] = new Time($dateString, "UTC");

        return $this;
    }

    public function getCreatedAt(string $format = "Y-m-d H:i:s")
    {
        // Convert to CodeIgniter\I18n\Time object
        $this->attributes["created_at"] = $this->mutateDate($this->attributes["created_at"]);

        $timezone = $this->timezone ?? app_timezone();

        $this->attributes["created_at"]->setTimezone($timezone);

        return $this->attributes["created_at"]->format($format);
    }
}