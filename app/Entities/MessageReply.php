<?php namespace App\Entities;

/*
 * File: MessageReply.php
 * Project: echo
 * File Created: Thursday, 10th December 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 15th November 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use App\Models\UserProfileModel;
use CodeIgniter\Entity\Entity;
use CodeIgniter\I18n\Time;

class MessageReply extends Entity
{
    protected $dates = [
        "created_at",
        "updated_at",
        "deleted_at"
    ];

    public function getSenderPicture()
    {
        $userProfileModel = new UserProfileModel();
        $profile = $userProfileModel->getProfile($this->attributes["user_id"]);

        return $profile->display_picture;
    }

    public function getSenderName()
    {
        $userProfileModel = new UserProfileModel();
        $profile = $userProfileModel->getProfile($this->attributes["user_id"]);

        return $profile->display_name;
    }

    public function getTimeString()
    {
        $time = Time::parse($this->attributes["created_at"]);
        return date("H:i", $time->getTimestamp());
    }

    public function getDateString()
    {
        $time = Time::parse($this->attributes["created_at"]);
        return date("M d", $time->getTimestamp());
    }
}