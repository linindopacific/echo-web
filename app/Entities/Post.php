<?php namespace App\Entities;

/*
 * File: Post.php
 * Project: echo
 * File Created: Monday, 26th October 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 15th November 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use App\Models\UserProfileModel;
use App\Models\PostCommentModel;
use App\Models\PostReactionModel;
use CodeIgniter\Entity\Entity;
use CodeIgniter\I18n\Time;

class Post extends Entity
{
    protected $dates = [
        "created_at",
        "updated_at",
        "deleted_at"
    ];

    protected $casts = [];

    public function isReacted()
    {
        $postReactionModel = new PostReactionModel();
        $postReaction = $postReactionModel->where("post_id", $this->attributes["id"])
                                          ->where("user_id", user_id())
                                          ->first();

        return (! empty($postReaction)) ? True : False;
    }

    public function getProfile()
    {
        $userProfileModel = new UserProfileModel();
        return $userProfileModel->getProfile($this->attributes["user_id"]);
    }

    public function getSince()
    {
        $time = Time::parse($this->attributes["created_at"]);
        $since = $time->humanize();

        return $since;
    }

    public function getComments()
    {
        $postCommentModel = new PostCommentModel();
        return $postCommentModel->where("post_id", $this->attributes["id"])
                                ->where("parent_id", null)
                                ->orderBy("created_at", "DESC")
                                ->findAll();
    }

    public function getIsOwner() : bool
    {
        return ($this->attributes["user_id"] === user_id()) ? true : false;
    }

}