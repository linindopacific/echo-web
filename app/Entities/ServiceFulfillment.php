<?php namespace App\Entities;

/*
 * File: ServiceFulfillment.php
 * Project: echo
 * File Created: Monday, 18th January 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 15th November 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use App\Models\ServiceModel;
use App\Models\ServiceOrderModel;
use CodeIgniter\Entity\Entity;

class ServiceFulfillment extends Entity
{
    protected $dates = [
        "arrived_at",
        "created_at",
        "updated_at",
        "deleted_at"
    ];

    protected $casts = [
        "completed" => "boolean"
    ];

    public function getService()
    {
        $serviceOrder = $this->getOrder();
        if (! empty($serviceOrder))
        {
            $serviceModel = new ServiceModel();
            return $serviceModel->find($serviceOrder->service_id);
        }
        else
        {
            return null;
        }
    }

    public function getOrder()
    {
        $serviceOrderModel = new ServiceOrderModel();
        return $serviceOrderModel->find($this->attributes["order_id"]);
    }

}