<?php namespace App\Entities;

/*
 * File: PhoneAuth.php
 * Project: echo
 * File Created: Friday, 16th July 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 15th November 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Entity\Entity;

class PhoneAuth extends Entity
{
	protected $dates   = [
		"created_at",
		"updated_at",
		"deleted_at",
	];

	protected $casts   = [
		"created_at" => "datetime",
	];

	protected $datamap = [
        "sessionInfo" => "session_info",
    ];

}