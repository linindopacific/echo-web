<?php namespace App\Entities;

/*
 * File: Service.php
 * Project: echo
 * File Created: Thursday, 3rd September 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Wednesday, 17th November 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use App\Models\ServiceCategoryModel;
use App\Models\ServiceCoverageModel;
use App\Models\ServiceOrderModel;
use App\Models\ServiceReviewModel;
use App\Models\UserProfileModel;
use CodeIgniter\Entity\Entity;
use CodeIgniter\I18n\Time;

class Service extends Entity
{
    protected $dates = [
        "created_at",
        "updated_at",
        "deleted_at"
    ];

	protected $casts = [
        "is_coverage" => "boolean",
        "featured" => "boolean",
        "pinned" => "boolean",
        "active" => "boolean"
    ];

    public function getIsCoverage()
    {
        $userProfileModel = new UserProfileModel();
        $userProfile = $userProfileModel->getProfile();

        $coverageModel = new ServiceCoverageModel();
        $isCoverage = $coverageModel->where("service_id", $this->attributes["id"])
                                    ->where("regency_id", function(\CodeIgniter\Database\BaseBuilder $builder) use($userProfile) {
                                        return $builder->select("id")->from(TBL_REG_REGENCIES)->where("name", $userProfile->city);
                                    })
                                    ->findAll();
        if ( empty($isCoverage) )
        {
            return false;
        }

        return true;
    }

    public function getIsPinned()
    {
        return ($this->attributes["pinned"] == "1") ? true : false;
    }

    public function getIsOwnerProfileComplete()
    {
        // minimal display name, full address, phone number terisi jika ingin muncul di marketplace
        $userProfileModel = new UserProfileModel();
        $userProfile = $userProfileModel->getProfile($this->attributes["user_id"]);

        if (! $userProfile->is_complete)
        {
            return false;
        }

        return true;
    }

    public function getIsOwner()
    {
        return ($this->attributes["user_id"] === user_id()) ? true : false;
    }

    public function getOwner()
    {
        $userProfileModel = new UserProfileModel();
        $userProfile = $userProfileModel->getProfile($this->attributes["user_id"]);

        return $userProfile->display_name;
    }

    public function getOwnerEmail()
    {
        $userProfileModel = new UserProfileModel();
        $userProfile = $userProfileModel->getProfile($this->attributes["user_id"]);

        return $userProfile->email;
    }

    public function getOwnerFirstName()
    {
        $userProfileModel = new UserProfileModel();
        $userProfile = $userProfileModel->getProfile($this->attributes["user_id"]);

        return $userProfile->firstname;
    }

    public function getOwnerLastName()
    {
        $userProfileModel = new UserProfileModel();
        $userProfile = $userProfileModel->getProfile($this->attributes["user_id"]);

        return $userProfile->lastname;
    }

    public function getOwnerAddress()
    {
        $userProfileModel = new UserProfileModel();
        $userProfile = $userProfileModel->getProfile($this->attributes["user_id"]);

        return $userProfile->full_address;
    }

    public function getOwnerCity()
    {
        $userProfileModel = new UserProfileModel();
        $userProfile = $userProfileModel->getProfile($this->attributes["user_id"]);

        return $userProfile->city;
    }

    public function getOwnerContact()
    {
        $userProfileModel = new UserProfileModel();
        $userProfile = $userProfileModel->getProfile($this->attributes["user_id"]);

        return $userProfile->phone_number;
    }

    public function getCategory()
    {
        $categoryModel = new ServiceCategoryModel();
        $category = $categoryModel->find($this->attributes["category_id"]);

        return $category->name;
    }

    public function getCoverageIds()
    {
        $results = [];

        $coverageModel = new ServiceCoverageModel();
        $coverages = $coverageModel->where("service_id", $this->attributes["id"])->findAll();
        if ( !empty($coverages) )
        {
            for ($i=0; $i < count($coverages); $i++)
            {
                array_push($results, $coverages[$i]->regency_id);
            }
        }

        return $results;
    }

    public function getCoverages()
    {
        $results = "";

        $coverageModel = new ServiceCoverageModel();
        $coverages = $coverageModel->where("service_id", $this->attributes["id"])->findAll();
        if ( !empty($coverages) )
        {
            for ($i=0; $i < count($coverages); $i++)
            {
                if ($i === count($coverages)-1)
                {
                    $results.= $coverages[$i]->name;
                }
                else
                {
                    $results.= $coverages[$i]->name . ", ";
                }
            }
        }

        return $results;
    }

    public function getUpdated()
    {
        $time = Time::parse($this->attributes["updated_at"]);

        return strtolower($time->humanize());
    }

    public function getSince()
    {
        $time = Time::parse($this->attributes["created_at"]);

        return strtolower($time->getYear());
    }

    public function getImages()
    {
        // create new DOMDocument
        $doc = new \DOMDocument("1.0", "UTF-8");
        // set error level
        $internalErrors = libxml_use_internal_errors(true);
        $doc->loadHTML($this->attributes["description"]);
        // Restore error level
        libxml_use_internal_errors($internalErrors);
        $imageTags = $doc->getElementsByTagName("img");

        $results = array();
        if (count($imageTags) > 0)
        {
            foreach($imageTags as $tag)
            {
                $results[] = $tag->getAttribute("src");
            }
        }
        else
        {
            $results[] = "https://via.placeholder.com/300/09f/fff.png?text=" . $this->attributes["name"];
        }

        return $results;
    }

    public function getCardContent()
    {
        return preg_replace("/<img[^>]+\>/i", "", $this->attributes["description"]);
    }

    public function getCompletes()
    {
        $serviceOrderModel = new ServiceOrderModel();

        return $serviceOrderModel->count_completes($this->attributes["id"]);
    }

    public function getNetAmount()
    {
        $serviceOrderModel = new ServiceOrderModel();
        $serviceOrder = $serviceOrderModel->get_earnings(user_id());

        $amount = 0;
        foreach ($serviceOrder as $row)
        {
            $tmp_amount = $row->amount - ($row->amount*$row->market_fee/100);
            $amount += $tmp_amount;
        }

        return $amount;
    }

    public function getRatings()
    {
        $serviceReviewModel = new ServiceReviewModel();

        return $serviceReviewModel->count_ratings($this->attributes["id"]);
    }

    public function getReviews()
    {
        $serviceReviewModel = new ServiceReviewModel();

        return $serviceReviewModel->get_reviews($this->attributes["id"]);
    }

}