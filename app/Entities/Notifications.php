<?php namespace App\Entities;

/*
 * File: Notifications.php
 * Project: echo
 * File Created: Monday, 21st December 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 15th November 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Entity\Entity;

class Notifications extends Entity
{
    protected $dates = [
        "read_on",
        "created_at",
        "updated_at",
        "deleted_at"
    ];

}