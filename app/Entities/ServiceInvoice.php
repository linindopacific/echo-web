<?php namespace App\Entities;

/*
 * File: ServiceInvoice.php
 * Project: echo
 * File Created: Wednesday, 30th December 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 15th November 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use App\Models\ServiceModel;
use App\Models\ServiceOrderModel;
use App\Models\UserModel;
use CodeIgniter\Entity\Entity;
use CodeIgniter\I18n\Time;

class ServiceInvoice extends Entity
{
    protected $dates = [
        "expired_at",
        "transaction_at",
        "created_at",
        "updated_at",
        "deleted_at"
    ];

    protected $casts = [
        "expired_at" => "datetime",
        "transaction_at" => "datetime",
        "amount" => "integer",
        "qr_string" => "string",
        "is_paid" => "boolean",
        "is_expired" => "boolean"
    ];

    public function getService()
    {
        $serviceOrder = $this->getOrder();
        if (! empty($serviceOrder))
        {
            $serviceModel = new ServiceModel();
            return $serviceModel->find($serviceOrder->service_id);
        }
        else
        {
            return null;
        }
    }

    public function getSellerEmail()
    {
        $service = $this->getService();
        if (! empty($service))
        {
            $userId = $service->user_id;

            $userModel = new UserModel();
            $user = $userModel->find($userId);
            return (! empty($user)) ? $user->email : "";
        }
        else
        {
            return "";
        }
    }

    public function getOrder()
    {
        $serviceOrderModel = new ServiceOrderModel();
        return $serviceOrderModel->find($this->attributes["order_id"]);
    }

    public function getCustomerEmail()
    {
        $serviceOrder = $this->getOrder();
        if (! empty($serviceOrder))
        {
            $userId = $serviceOrder->user_id;

            $userModel = new UserModel();
            $user = $userModel->find($userId);
            return (! empty($user)) ? $user->email : "";
        }
        else
        {
            return "";
        }
    }

    public function getIsOwner()
    {
        $serviceOrder = $this->getOrder();
        if (! empty($serviceOrder))
        {
            return ($serviceOrder->user_id === user_id()) ? true : false;
        }
        else
        {
            return false;
        }
    }

    public function getIsPaid()
    {
        if (! empty($this->attributes["transaction_at"]))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function getIsExpired()
    {
        $currentDateTime = Time::now();
        $invoiceDueDateTime = Time::parse($this->attributes["expired_at"]);

        if ( $invoiceDueDateTime->isAfter($currentDateTime) )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function getPaymentChannelLogo()
    {
        $channel = strtolower($this->attributes["payment_channel"]);
        return base_url("assets/images/gateway/xnd/logo-" . $channel . ".png");
    }

    public function getStatus()
    {
        if ( $this->getIsPaid() )
        {
            return "PAID";
        }
        else
        {
            $currentDateTime = Time::now();
            $invoiceDueDateTime = Time::parse($this->attributes["expired_at"]);

            if ( $currentDateTime->isAfter($invoiceDueDateTime) )
            {
                return "EXPIRED";
            }
            else
            {
                return "PENDING";
            }
        }
    }

}