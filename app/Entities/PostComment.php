<?php namespace App\Entities;

/*
 * File: PostComment.php
 * Project: echo
 * File Created: Friday, 18th December 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 15th November 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use App\Models\UserProfileModel;
use CodeIgniter\Entity\Entity;
use CodeIgniter\I18n\Time;

class PostComment extends Entity
{
    protected $dates = [
        "created_at",
        "updated_at",
        "deleted_at"
    ];

    protected $casts = [];

    public function getProfile()
    {
        $userProfileModel = new UserProfileModel();
        return $userProfileModel->getProfile($this->attributes["user_id"]);
    }

    public function getSince()
    {
        $time = Time::parse($this->attributes["created_at"]);
        $since = $time->humanize();

        return $since;
    }
}