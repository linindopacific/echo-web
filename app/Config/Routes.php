<?php namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('HomeController');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'HomeController::index');
$routes->get('myIP', 'TestController::myIP');
$routes->get('test', 'TestController::test');
$routes->get('pusher-test', 'PusherController::test');
$routes->get('payment/test', 'PaymentController::test');
$routes->get('payment/simulate', 'PaymentController::simulate');
$routes->post('payment/simulate', 'PaymentController::attemptSimulate');
$routes->get('payment/testmail', 'PaymentController::testMail');

$routes->group('', ['namespace' => 'App\Controllers'], function ($routes) {
	$routes->get('beams-auth', 'PusherController::beamsAuth');
	$routes->post('pusher-auth', 'PusherController::auth');

    // Login/out
	$routes->get('login', 'AuthController::login', ['as' => 'login']);
	$routes->post('login', 'AuthController::attemptLogin');
	$routes->get('logout', 'AuthController::logout');
	$routes->post('callback', 'AuthController::callback', ['as' => 'callback']);

	// Registration
	$routes->get('register', 'AuthController::register', ['as' => 'register']);
	$routes->post('register', 'AuthController::attemptRegister');

	// Activation
	$routes->get('activate-account', 'AuthController::activateAccount', ['as' => 'activate-account']);
	$routes->get('resend-activate-account', 'AuthController::resendActivateAccount', ['as' => 'resend-activate-account']);

	// Forgot/Resets
	$routes->get('forgot', 'AuthController::forgotPassword', ['as' => 'forgot']);
	$routes->post('forgot', 'AuthController::attemptForgot');
	$routes->get('reset-password', 'AuthController::resetPassword', ['as' => 'reset-password']);
	$routes->post('reset-password', 'AuthController::attemptReset');

	// Account
	$routes->get('account/settings', 'AccountController::settings');
	$routes->post('account/settings', 'AccountController::attemptSettings');

	$routes->get('getProvince', 'RegionController::getProvince');
	$routes->get('getRegency', 'RegionController::getRegency');

	// Profile
	$routes->get('profile', 'ProfileController::userLookup');
	$routes->post('profile/me', 'ProfileController::me');
	$routes->post('profile/about', 'ProfileController::about');
	$routes->post('profile/connection', 'ProfileController::connection');
	$routes->get('profile/connection_status', 'ProfileController::connectionStatus');
	$routes->get('profile/(:segment)', 'ProfileController::userLookup');
	$routes->post('profile/ajax_search', 'ProfileController::ajax_search');
	$routes->post('profile/search', 'ProfileController::search');
	$routes->post('profile/action', 'ProfileController::attemptAction');

	// Service
	$routes->get('services', 'ServiceController::index');
	$routes->get('services/(:num)', 'ServiceController::serviceLookup');
	$routes->post('services/mine', 'ServiceController::mine', ['filter' => 'ajax']);
	$routes->post('services/published', 'ServiceController::mine_published', ['filter' => 'ajax']);
	$routes->post('services/draft', 'ServiceController::mine_draft', ['filter' => 'ajax']);
	$routes->post('services/recommended', 'ServiceController::mine_recommended', ['filter' => 'ajax']);
	$routes->get('services/store', 'ServiceController::store', ['filter' => 'ajax']);
	$routes->post('services/store', 'ServiceController::attemptStore', ['filter' => 'ajax']);
	$routes->match(['get', 'post'], 'services/marketplace', 'ServiceController::marketplace', ['filter' => 'ajax']);
	$routes->match(['get', 'post'], 'services/recommendation', 'ServiceController::recommendation', ['filter' => 'ajax']);

	// Service Order
	$routes->get('services/order-detail', 'ServiceOrderController::orderLookup', ['filter' => 'ajax']);
	$routes->get('services/order', 'ServiceOrderController::serviceLookup');
	$routes->post('services/orders', 'ServiceOrderController::orders', ['filter' => 'ajax']);
	$routes->match(['get', 'post'], 'services/purchase-orders', 'ServiceOrderController::purchaseOrders', ['filter' => 'ajax']);
	$routes->match(['get', 'post'], 'services/sales-orders', 'ServiceOrderController::salesOrders', ['filter' => 'ajax']);
	$routes->get('services/order-list', 'ServiceOrderController::ordersLookup');
	$routes->post('services/checkout', 'ServiceOrderController::attemptOrder');

	$routes->get('services/payment', 'PaymentController::instruction');

	$routes->post('services/deliver-order', 'ServiceFulfillmentController::attemptFulfill', ['filter' => 'ajax']);
	$routes->post('services/confirm-order', 'ServiceFulfillmentController::attemptConfirm', ['filter' => 'ajax']);

	$routes->get('services/order-review', 'ServiceReviewController::orderReview');
	$routes->post('services/order-review', 'ServiceReviewController::attemptReview');
	$routes->get('services/order-reviews', 'ServiceReviewController::orderReviews');

	// Post
	$routes->post('post/timeline', 'PostController::getTimeline');
	$routes->get('post/store', 'PostController::store');
	$routes->post('post/store', 'PostController::attemptStore');
	$routes->post('post/action', 'PostController::attemptAction');

	// Comment
	$routes->post('comment/store', 'PostCommentController::attemptStore');

	// Message
	$routes->get('messages/t/(:segment)', 'MessageController::threadLookup');
	$routes->post('messages/threads', 'MessageController::getThread');
	$routes->post('messages/store', 'MessageController::attemptStore');

	// Notification
	$routes->get('getNotification', 'NotificationController::getNotification');

	// Upload
	$routes->post('upload/store', 'UploadController::attemptStore');
	$routes->post('upload/delete', 'UploadController::attemptDelete');
});

$routes->group('api', ['namespace' => 'App\Controllers'], function ($routes) {
	// Payment channels
	$routes->get('payment/channels', 'PaymentController::getChannels');
});

$routes->group('callback', ['namespace' => 'App\Controllers'], function ($routes) {
	// Payment channels
	$routes->post('xnd/ro_paid', 'PaymentController::ro_paid');
	$routes->post('xnd/fva_paid', 'PaymentController::fva_paid');
	$routes->post('xnd/fva_created', 'PaymentController::fva_created');
	$routes->post('xnd/ewallet_status', 'PaymentController::ewallet_status');
});

$routes->cli('tools/orders-create', 'ToolsController::ordersCreate');
$routes->cli('tools/orders-fulfilled', 'ToolsController::ordersFulfilled');

/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
