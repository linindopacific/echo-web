<?php

//--------------------------------------------------------------------
// App Namespace
//--------------------------------------------------------------------
// This defines the default Namespace that is used throughout
// CodeIgniter to refer to the Application directory. Change
// this constant to change the namespace that all application
// classes should use.
//
// NOTE: changing this will require manually modifying the
// existing namespaces of App\* namespaced-classes.
//
defined('APP_NAMESPACE') || define('APP_NAMESPACE', 'App');

/*
|--------------------------------------------------------------------------
| Composer Path
|--------------------------------------------------------------------------
|
| The path that Composer's autoload file is expected to live. By default,
| the vendor folder is in the Root directory, but you can customize that here.
*/
defined('COMPOSER_PATH') || define('COMPOSER_PATH', ROOTPATH . 'vendor/autoload.php');

/*
|--------------------------------------------------------------------------
| Timing Constants
|--------------------------------------------------------------------------
|
| Provide simple ways to work with the myriad of PHP functions that
| require information to be in seconds.
*/
defined('SECOND') || define('SECOND', 1);
defined('MINUTE') || define('MINUTE', 60);
defined('HOUR')   || define('HOUR', 3600);
defined('DAY')    || define('DAY', 86400);
defined('WEEK')   || define('WEEK', 604800);
defined('MONTH')  || define('MONTH', 2592000);
defined('YEAR')   || define('YEAR', 31536000);
defined('DECADE') || define('DECADE', 315360000);

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        || define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          || define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         || define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   || define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  || define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') || define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     || define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       || define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      || define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      || define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

define('PP_HOUR', 'hour');
define('PP_DAY', 'day');
define('PP_EVENT', 'event');

define('TBL_AUTH_GROUPS', 'auth_groups');
define('TBL_AUTH_LOGINS', 'auth_logins');
define('TBL_AUTH_LOGINS_IP', 'auth_logins_ip');
define('TBL_AUTH_PHONE_VERIFY', 'auth_phone_verify');
define('TBL_MESSAGES', 'messages');
define('TBL_MESSAGE_REPLIES', 'message_replies');
define('TBL_NOTIFICATIONS', 'notifications');
define('TBL_NOTIFICATION_CHANGE', 'notification_change');
define('TBL_NOTIFICATION_ENTITY', 'notification_entity');
define('TBL_NOTIFICATION_OBJECT', 'notification_object');
define('TBL_POSTS', 'posts');
define('TBL_POST_COMMENTS', 'post_comments');
define('TBL_POST_REACTIONS', 'post_reaction');
define('TBL_POST_VISIBILITIES', 'post_visibilities');
define('TBL_REACTIONS', 'reactions');
define('TBL_REG_PROVINCES', 'reg_provinces');
define('TBL_REG_REGENCIES', 'reg_regencies');
define('TBL_REG_DISTRICTS', 'reg_districts');
define('TBL_REG_VILLAGES', 'reg_villages');
define('TBL_SERVICES', 'services');
define('TBL_SERVICE_CATEGORIES', 'service_categories');
define('TBL_SERVICE_COVERAGES', 'service_coverages');
define('TBL_SERVICE_FULFILLMENTS', 'service_fulfillment');
define('TBL_SERVICE_ORDERS', 'service_orders');
define('TBL_SERVICE_INVOICES', 'service_invoices'); // depreceated, changed to xnd
define('TBL_SERVICE_INVOICES_XND', 'service_invoices_xnd');
define('TBL_SERVICE_REVIEWS', 'service_reviews');
define('TBL_SERVICE_TRANSACTIONS', 'service_transactions');
define('TBL_UPLOADS', 'uploads');
define('TBL_USERS', 'users');
define('TBL_USER_PROFILES', 'user_profile');
define('TBL_USER_RELATION', 'user_relation');