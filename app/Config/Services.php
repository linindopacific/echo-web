<?php namespace Config;

use App\Libraries\NotificationLibrary;
use App\Log\CloudLogger;
use CodeIgniter\Config\Services as CoreServices;
use PHPMailer\PHPMailer\PHPMailer;
use Pusher\Pusher;
use Pusher\PushNotifications\PushNotifications;

/**
 * Services Configuration file.
 *
 * Services are simply other classes/libraries that the system uses
 * to do its job. This is used by CodeIgniter to allow the core of the
 * framework to be swapped out easily without affecting the usage within
 * the rest of your application.
 *
 * This file holds any application-specific services, or service overrides
 * that you might need. An example has been included with the general
 * method format you should use for your service methods. For more examples,
 * see the core Services file at system/Config/Services.php.
 */
class Services extends CoreServices
{

	//    public static function example($getShared = true)
	//    {
	//        if ($getShared)
	//        {
	//            return static::getSharedInstance('example');
	//        }
	//
	//        return new \CodeIgniter\Example();
	//    }

	//--------------------------------------------------------------------

	public static function ipstack(bool $getShared = true)
	{
		if ($getShared)
        {
            return self::getSharedInstance("ipstack");
        }

		$request = \Config\Services::request();
        $ipAddress = $request->getIPAddress();

		if ( $ipAddress == '::1' ) {
			return ['status' => 'error', 'message' => 'localhost'];
		}

		$cacheName = "ip_$ipAddress";
		if ( ! $data = cache($cacheName) )
		{
			$client = \Config\Services::curlrequest();
			$response = $client->request('GET', 'http://api.ipstack.com/' . $ipAddress, ['query' => [
				'access_key' => env('IPSTACK_KEY'),
				'fields' => 'ip,country_code,country_name,region_name,city,zip,latitude,longitude,connection.isp,location.country_flag,location.country_flag_emoji_unicode,location.calling_code,location.languages',
				'output' => 'json',
				'language' => 'en'
			]]);

			$body = $response->getBody();
			if (strpos($response->getHeader('content-type'), 'application/json') !== false)
			{
				$body = json_decode($body, true);

				if ( !isset($body['success']) )
				{
					$data = $body;
					cache()->save($cacheName, $data, 3600);
				}
				else
				{
					log_message('error', 'IPSTACK {code} {type} - {info}', ['code' => $body['error']['code'], 'type' => $body['error']['type'], 'info' => $body['error']['info']]);
					return ['status' => 'error', 'message' => $body['error']['info']];
				}
			}
		}

		return ['status' => 'success', 'data' => $data];
	}

	//--------------------------------------------------------------------

	/**
	 * The commands utility for running and working with activity.
	 *
	 * @param boolean $getShared
	 *
	 * @return NotificationLibrary
	 */
	public static function activity(bool $getShared = true)
	{
		if ($getShared)
		{
			return static::getSharedInstance('activity');
		}

		return new NotificationLibrary();
	}

	//--------------------------------------------------------------------

	/**
	 * The commands utility for running and working with Pusher Beam
	 *
	 * @param boolean $getShared
	 * @return PushNotifications
	 */
	public static function pusher_beam(bool $getShared = true)
	{
		if ($getShared)
		{
			return static::getSharedInstance('pusher_beam');
		}

		return new PushNotifications([
            'instanceId' => env('PUSHER_BEAMS_ID'),
            'secretKey' => env('PUSHER_BEAMS_SECRET'),
        ]);
	}

	//--------------------------------------------------------------------

	/**
	 * The commands utility for running and working with Pusher Channel
	 *
	 * @param boolean $getShared
	 * @return Pusher
	 */
	public static function pusher_channel(bool $getShared = true)
	{
		if ($getShared)
		{
			return static::getSharedInstance('pusher_channel');
		}

		$options = array(
            'cluster' => env('PUSHER_CLUSTER'),
            'useTLS' => true,
			'encrypted' => true,
			'encryption_master_key_base64' => env('PUSHER_ENCRYPTION_MASTER_KEY')
        );

		return new Pusher(
            env('PUSHER_KEY'),
            env('PUSHER_SECRET'),
            env('PUSHER_APPID'),
            $options
        );
	}

	//--------------------------------------------------------------------

	public static function phpmailer(bool $getShared = true)
	{
		if ($getShared)
		{
			return static::getSharedInstance('phpmailer');
		}

		$mail = new PHPMailer(true);
		$mail->IsSMTP();
		$mail->SMTPAuth = true;
		$mail->Host = env('email.SMTPHost');
		$mail->Username = env('email.SMTPUser');
		$mail->Password = env('email.SMTPPass');
		$mail->SMTPSecure = env('email.SMTPCrypto');
    	$mail->Port = env('email.SMTPPort');
		$mail->CharSet = 'utf-8';

		$mail->setLanguage('id');
		$mail->setFrom(env('email.fromEmail'), env('email.fromName'));
		$mail->addBCC(env('ADMIN_EMAIL'));

		return $mail;
	}
}
