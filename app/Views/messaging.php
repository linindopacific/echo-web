<?= $this->extend($viewLayout) ?>

<?= $this->section('pageStyles') ?>
<?= link_tag(base_url('assets/css/style_chat.css')); ?>
<?= $this->endSection() ?>

<?= $this->section('main') ?>
<div class="container">
	<div class="row pt-3 justify-content-center">
		<div class="col-lg-12">
            <div class="messaging">
                <div class="inbox_msg">
                    <div class="mesgs">
                        <div id="msg_box" class="msg_history"></div>
                        <div class="type_msg">
                            <div class="input_msg_write">
                                <form id="form" id="chat_form">
                                    <input id="chat_input" type="text" class="write_msg" placeholder="Type a message" disabled />
                                    <button class="msg_send_btn" type="submit"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('pageScripts') ?>
<?= script_tag('https://cdnjs.cloudflare.com/ajax/libs/socket.io/4.0.0/socket.io.min.js'); ?>
<script type="text/javascript">
    $(document).ready(function() {
        const encryptedChannel = pusher.subscribe("private-encrypted-user" + currentUserId);
        encryptedChannel.bind("pusher:subscription_succeeded", () => {});

        var socket = io.connect('https://ws.linindo.com',
            {
				reconnection: true,
				reconnectionAttempts: Infinity,
				reconnectionDelay: 1000,
				reconnectionDelayMax: 5000,
				randomizationFactor: 0.5,
				timeout: 20000,
				autoConnect: true,
				transports: ['websocket'],
				transportOptions: {},
			}
        );
        var msg_box = $('#msg_box');
        var input = $('#chat_input');

        get_messages(<?= $profile->user_id ?>);

        socket.on("connect", () => {
            console.log(socket.id);
            console.log(socket.connected); // true
            socket.emit('join', {id: '<?= user_id() ?>'});
            input.attr('disabled', false);
        });

        socket.on("disconnect", () => {
            console.log(socket.id); // undefined
            console.log(socket.connected); // false
            input.attr('disabled', true);
        });

        // rejoin if there's a disconnect
        socket.on('reconnect', () => {
            socket.emit('join', {id: '<?= user_id() ?>'});
        })

        socket.on("private-message", (data) => {
            append_message('in', data)
        });

        $('form').submit(function(e){
            e.preventDefault();
            var data = {type: 'private-message', content: input.val(), from: '<?= user_id() ?>', to: '<?= $profile->user_id ?>'};
            if(input.val().length < 1) {
                alert('Cannot be blank');
            } else {
                append_message('out', data)
            }
            return false;
        });

        function append_message(type, data)
        {
            $.ajaxq ('MyQueue', {
                url: siteUrl + '/messages/store',
                type: 'post',
                data: {content: data.content, from: data.from, to: data.to, timestamp: data.timestamp, type: type},
                dataType: 'json',
                success: function(response) {
                    if (response.status == 'success') {
                        if (type == 'out') {
                            socket.emit('notification', data);
                            input.val('');
                        }
                        msg_box.append(response.data);
                    }
                    else if (response.status == 'error') {
                        if (response.message == 'session_expired') {
                            alert('Session expired, please re-login.');
                            window.location.replace(siteUrl);
                        } else {
                            alert(response.message);
                        }
                    }
                },
            });
        }

        function get_messages(userId)
        {
            $.ajaxq ('MyQueue', {
                url: siteUrl + '/messages/threads',
                type: 'post',
                data: {to: userId},
                dataType: 'json',
                success: function(response) {
                    if (response.status == 'success') {
                        msg_box.append(response.data);
                        scroll_to_bottom();
                    }
                    else if (response.status == 'error') {
                        if (response.message == 'session_expired') {
                            alert('Session expired, please re-login.');
                            window.location.replace(siteUrl);
                        } else {
                            alert(response.message);
                        }
                    }
                },
            });
        }

        function scroll_to_bottom()
        {
            if ($(".msg_history").find(".msg").length > 0){
                $('.msg_history').animate({
                    scrollTop: $('.msg_history .msg:last-child').position().top
                }, 'fast');
            }
        }
    });
</script>
<?= $this->endSection() ?>