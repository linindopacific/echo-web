<?= $this->extend($viewLayout) ?>

<?= $this->section('pageStyles') ?>
<?= link_tag('https://cdn.jsdelivr.net/npm/placeholder-loading/dist/css/placeholder-loading.min.css'); ?>
<?= link_tag("https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.18/dist/css/bootstrap-select.min.css"); ?>
<?= link_tag(base_url('assets/css/style_profile.css')); ?>
<?= link_tag(base_url('assets/css/style_connection.css')); ?>
<?= link_tag(base_url('assets/css/style_comment.css')); ?>
<?= $this->endSection() ?>

<?= $this->section('main') ?>
<div class="container-fluid" style="width:90%;">
    <div class="img" style="background-image: linear-gradient(150deg, rgba(63, 174, 255, .3)15%, rgba(63, 174, 255, .3)70%, rgba(63, 174, 255, .3)94%), url(https://bootdey.com/img/Content/flores-amarillas-wallpaper.jpeg);height: 200px;background-size: cover;"></div>
    <div class="card social-prof">
        <div class="card-body">
            <div class="wrapper">
                <img src="<?= $profile->display_picture; ?>" alt="" class="user-profile">
                <h3><?= $profile->display_name; ?></h3>
                <p><?= $profile->job; ?></p>
            </div>
            <hr class="garis">
            <div class="row">
                <div class="col-lg-8 col-sm-12 py-1">
                    <ul class="nav nav-pills justify-content-left s-nav" id="pills-tab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <a href="<?= site_url('profile/me'); ?>" class="nav-link show active" id="pills-beranda-tab" data-toggle="pill" data-target="#pills-beranda" role="tab" aria-controls="pills-beranda" aria-selected="true">Timeline</a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a href="<?= site_url('profile/about'); ?>" class="nav-link" id="pills-tentang-tab" data-toggle="pill" data-target="#pills-tentang" role="tab" aria-controls="pills-tentang" aria-selected="false">About</a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a href="<?= site_url('profile/connection'); ?>" class="nav-link" id="pills-koneksi-tab" data-toggle="pill" data-target="#pills-koneksi" role="tab" aria-controls="pills-koneksi" aria-selected="false">Connections</a>
                        </li>
                    </ul>
                    <ul class="nav nav-pills justify-content-left s-nav" id="pills-tab" role="tablist"></ul>
                </div>
                <div id="connection_status" class="col-lg-4 col-sm-12"></div>
            </div>
        </div>
    </div>
    <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade show active" id="pills-beranda" role="tabpanel" aria-labelledby="pills-beranda-tab"></div>
        <div class="tab-pane fade" id="pills-tentang" role="tabpanel" aria-labelledby="pills-tentang-tab"></div>
        <div class="tab-pane fade" id="pills-koneksi" role="tabpanel" aria-labelledby="pills-koneksi-tab"></div>
    </div>
    <div class="backTop"><i class="fas fa-arrow-circle-up fa-2x"></i></div>
</div>
<?= $this->endSection() ?>

<?= $this->section('pageScripts') ?>
<?= script_tag("https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.18/dist/js/bootstrap-select.min.js"); ?>
<script type="text/javascript">
    $(document).ready(function() {
        // default tab
        var $this = $('#pills-tab a[id="pills-beranda-tab"]'),
            loadurl = $this.attr('href'),
            targ = $this.attr('data-target');

        $.ajaxq ('MyQueue', {
            url: loadurl,
            type: 'post',
            data: {'uid': '<?= $profile->username; ?>'},
            dataType: 'json',
            beforeSend: function() {
            },
            success: function(response) {
                $(targ).html(response.data);
            },
            error: function(xhr, textStatus, errorThrown) {
                var errors = xhr.responseJSON;
                if (xhr.status == 401) {
                    alert("Error: " + errorThrown);
                    window.location.replace(siteUrl);
                } else {
                    alert("Error: " + errors.message);
                }
            }
        });

        $('#pills-tab a').on('shown.bs.tab', function (e) {
            e.target // newly activated tab
            e.relatedTarget // previous active tab
            var $this = $(this),
                loadurl = $this.attr('href'),
                targ = $this.attr('data-target'),
                prevTarg = e.relatedTarget.id.slice(0,-4);

            $.ajaxq ('MyQueue', {
                url: loadurl,
                type: 'post',
                data: {'uid': '<?= $profile->username; ?>'},
                dataType: 'json',
                beforeSend: function() {
                    $('#' + prevTarg).html('');
                },
                complete: function() {
                },
                success: function(response) {
                    $(targ).html(response.data);
                },
                error: function(xhr, textStatus, errorThrown) {
                    var errors = xhr.responseJSON;
                    if (xhr.status == 401) {
                        alert("Error: " + errorThrown);
                        window.location.replace(siteUrl);
                    } else {
                        alert("Error: " + errors.message);
                    }
                }
            });
        });

        connection_status();

        privateChannel.bind('relation', function(data) {
            connection_status();
        });
    });
</script>
<?= $this->endSection() ?>