<?php if($isMe) { ?>
    <button class="btn float-right" type="button" id="gedf-drop11" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fa fa-ellipsis-h"></i>
    </button>
    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="gedf-drop11"></div>
    <a href="<?= site_url('account/settings'); ?>" class="btn btn-primary pull-left float-right mr-2">Edit Profile</a>
<?php } else { ?>
    <div>
        <button class="btn float-right" type="button" id="gedf-drop11" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-ellipsis-h"></i>
        </button>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="gedf-drop11">
            <?php if ($profile->connected): ?>
                <a class="dropdown-item act-profile" href="#" data-act="relation-remove" data-id="<?= $profile->id; ?>"><i class="fas fa-user-minus"></i> Remove connection</a>
            <?php endif; ?>
            <a class="dropdown-item" href="#"><i class="fas fa-exclamation-circle"></i> Block this account</a>
            <a class="dropdown-item" href="#"><i class="fas fa-flag"></i> Report this account</a>
        </div>
    </div>
    <?php if ($profile->connected) { ?>
        <button class="btn btn-primary pull-left float-right mr-2 act-profile" data-act="private-message" data-id="<?= $profile->id; ?>" data-toggle="tooltip" data-placement="top" title="Message">
            <i class="fab fa-facebook-messenger"></i> Message
        </button>
    <?php } else { ?>
        <button class="btn btn-primary pull-left float-right mr-2 act-profile" data-act="private-message" data-id="<?= $profile->id; ?>" data-toggle="tooltip" data-placement="top" title="Message">
            <i class="fab fa-facebook-messenger"></i>
        </button>
        <?php if (!empty($profile->connection)) { ?>
            <?php if ($profile->connection->status === '0') { ?>
                <?php if ($profile->connection->action_user_id === user_id()) { ?>
                    <button class="btn btn-primary pull-left float-right mr-2 act-profile" data-act="relation-cancel" data-id="<?= $profile->id; ?>">
                        Cancel Request
                    </button>
                <?php } else { ?>
                    <div>
                        <button class="btn btn-primary pull-left float-right mr-2" type="button" id="gedf-drop12" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-user-check"></i> Respond
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="gedf-drop12">
                            <a class="dropdown-item act-profile" href="#" data-act="relation-approve" data-id="<?= $profile->id; ?>">Confirm</a>
                            <a class="dropdown-item act-profile" href="#" data-act="relation-reject" data-id="<?= $profile->id; ?>">Delete Request</a>
                        </div>
                    </div>
                <?php } ?>
            <?php } elseif ($profile->connection->status === '2') { ?>
                <button class="btn btn-primary pull-left float-right mr-2 act-profile" data-act="relation-invite" data-id="<?= $profile->id; ?>">
                    <i class="fas fa-user-plus"></i> Connect
                </button>
            <?php } ?>
        <?php } else { ?>
            <button class="btn btn-primary pull-left float-right mr-2 act-profile" data-act="relation-invite" data-id="<?= $profile->id; ?>">
                <i class="fas fa-user-plus"></i> Connect
            </button>
        <?php } ?>
    <?php } ?>
<?php } ?>

<script type="text/javascript">
    toggle_profile();
</script>