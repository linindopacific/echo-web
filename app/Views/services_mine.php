<div class="row">
    <div class="col-lg-3 col-md-12 col-sm-12 m-2 px-0"></div>
    <div class="col-lg-8 col-md-12 col-sm-12 mb-1 pl-0">
        <div class="d-flex justify-content-end">
            <button type="button" class="btn btn-outline-info p-2 service-modal"><i class="fas fa-toolbox"></i> Add a new service</button>
        </div>
    </div>
</div>
<div class="row mt-3">
    <div class="col-md-3 col-sm-12">
        <div class="nav flex-column nav-pills" id="v-service-tab" role="tablist" aria-orientation="vertical">
            <a href="<?= site_url('services/published'); ?>" class="nav-link show active" id="v-service-published-tab" data-toggle="pill" data-target="#v-service-published" role="tab" aria-controls="v-service-published" aria-selected="true">Published</a>
            <a href="<?= site_url('services/draft'); ?>" class="nav-link" id="v-service-draft-tab" data-toggle="pill" data-target="#v-service-draft" role="tab" aria-controls="v-service-draft" aria-selected="false">Draft</a>
            <a href="<?= site_url('services/recommended'); ?>" class="nav-link" id="v-service-recommended-tab" data-toggle="pill" data-target="#v-service-recommended" role="tab" aria-controls="v-service-recommended" aria-selected="false">Recommended</a>
        </div>
    </div>
    <div class="col-md-9 col-sm-12">
        <div class="tab-content" id="v-pills-tabContent">
            <div class="tab-pane fade show active" id="v-service-published" role="tabpanel" aria-labelledby="v-service-published-tab"></div>
            <div class="tab-pane fade" id="v-service-draft" role="tabpanel" aria-labelledby="v-service-draft-tab"></div>
            <div class="tab-pane fade" id="v-service-recommended" role="tabpanel" aria-labelledby="v-service-recommended-tab"></div>
        </div>
    </div>
</div>

<div class="modal fade" id="serviceModal" tabindex="-1" role="dialog" aria-labelledby="serviceModalLabel" aria-hidden="true"></div>

<script type="text/javascript">
    // default tab
    var $this = $('#v-service-tab a[id="v-service-published-tab"]'),
        loadurl = $this.attr('href'),
        targ = $this.attr('data-target');

    $.post(loadurl, function(data) {
        $(targ).html(data);
    });

    $('#v-service-tab a').on('shown.bs.tab', function (e) {
        e.target // newly activated tab
        e.relatedTarget // previous active tab
        var $this = $(this),
            loadurl = $this.attr('href'),
            targ = $this.attr('data-target');

        $.post(loadurl, function(data) {
            $(targ).html(data);
        });
    });
</script>