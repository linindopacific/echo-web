<?php $faker = \Faker\Factory::create(); ?>

<div class="row">
    <div class="col-lg-3 col-md-12 col-sm-12 m-2 px-0">
        <?= $this->include('services_search_filter') ?>
    </div>
    <div class="col-lg-8 col-md-12 col-sm-12 m-2">
        <div class="row">
            <div class="col-6">
                <!-- <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0" style="background-color: #fff;">
                    <li class="breadcrumb-item"><a href="#">Categories</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Events</li>
                    </ol>
                </nav> -->
            </div>
            <div class="col-6">
                <div class="form-group col-md-4 float-right">
                    <select class="form-control selectpicker" multiple data-max-options="1" title="Friend Circle...">
                        <option value="1">Circle 1</option>
                        <option value="2">Circle 2</option>
                        <option value="3">Circle 3</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row row-cols-1 row-cols-md-4">
            <?php foreach ($services as $row): ?>
                <?php if ($row->is_owner_profile_complete): ?>
                <div class="col mb-4 px-2">
                    <div class="card shadow-sm h-100">
                        <img src="<?= $row->images[0]; ?>" class="card-img-top" alt="<?= $row->name; ?>" loading="lazy">
                        <div class="card-body pb-1">
                            <div class="d-flex bd-highlight">
                                <div class="flex-grow-1 bd-highlight">
                                    <h5 class="card-title">
                                        <?= anchor(site_url('services/' . $row->id), $row->name, []); ?>
                                    </h5>
                                </div>
                            </div>
                            <p class="text-muted"><?= $row->owner; ?> <small>&nbsp;</small></p>
                            <p class="text-danger font-weight-bold">Rp <?= $numberLib->singkat_angka($row->price); ?>/<?= $row->price_per; ?></p>
                            <ul class="list-group text-muted mb-3">
                                <li class="list-group-item px-0 py-0 border-0">
                                    <span class="score">
                                        <div class="score-wrap">
                                            <span class="market-stars-active" style="width:<?= ($row->ratings/5)*100 ?>%">
                                                <i class="fas fa-star" aria-hidden="true"></i>
                                                <i class="fas fa-star" aria-hidden="true"></i>
                                                <i class="fas fa-star" aria-hidden="true"></i>
                                                <i class="fas fa-star" aria-hidden="true"></i>
                                                <i class="fas fa-star" aria-hidden="true"></i>
                                            </span>
                                            <span class="market-stars-inactive">
                                                <i class="far fa-star" aria-hidden="true"></i>
                                                <i class="far fa-star" aria-hidden="true"></i>
                                                <i class="far fa-star" aria-hidden="true"></i>
                                                <i class="far fa-star" aria-hidden="true"></i>
                                                <i class="far fa-star" aria-hidden="true"></i>
                                            </span>
                                        </div>
                                    </span>
                                </li>
                                <li class="list-group-item px-0 py-0 border-0">
                                    <span class="market-coverage"><i class="fas fa-map-marker-alt" aria-hidden="true"></i></span> <?= ucwords(strtolower($row->coverages)); ?>
                                </li>
                                <li class="list-group-item px-0 py-0 border-0">
                                    <span class="market-complete"><i class="fas fa-check-circle" aria-hidden="true"></i></span> <?= $faker->numberBetween($min=10, $max=100) ?> Completed
                                </li>
                                <li class="list-group-item px-0 py-0 border-0">
                                    <span class="market-recommended"><i class="fas fa-thumbs-up" aria-hidden="true"></i></span> (<?= $faker->numberBetween($min=10, $max=50) ?>) Recommended
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
        <!-- end row -->
        <?php $pager->setPath('services/marketplace', 'marketplace'); ?>
        <?= $pager->makeLinks($page, $perPage, $total, 'echo_services', 0); ?>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        var valCategories = [],
            valLocations = [],
            valStars = [],
            valPricePers = [],
            url = siteUrl + '/services/marketplace',
            target = $('#pills-marketplace'),
            data = {'categories':valCategories, 'locations':valLocations, 'stars': valStars, 'pricePers': valPricePers};

        $('#filter_clear').on('click', function(e) {
            e.preventDefault();
            data.categories = [];
            data.locations = [];
            data.stars = [];
            data.pricePers = [];

            find_services(url, data, target);
        });

        $('.pagination .page-item a').on('click', function (e) {
            e.preventDefault();
            var url = $(this).attr('data-target');
            find_services(url, data, target);
        });

        // filter --category
        <?php if (! empty($filter_categories)): ?>
            <?php for ($i=0; $i < count($filter_categories); $i++): ?>
                valCategories.push('<?= $filter_categories[$i] ?>');
            <?php endfor; ?>
            $('#filterCategories').selectpicker('val', valCategories);
        <?php endif; ?>

        $('#filterCategories').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
            e.preventDefault();
            var valSelected = $('#filterCategories option').eq(clickedIndex).val();
            if (isSelected) {
                valCategories.push(valSelected);
            } else {
                valCategories = $.grep(valCategories, function(value) {
                    return value != valSelected;
                });
            }

            data.categories = valCategories;
            find_services(url, data, target);
        });

        // filter --location
        <?php if (! empty($filter_locations)): ?>
            <?php for ($i=0; $i < count($filter_locations); $i++): ?>
                valLocations.push('<?= $filter_locations[$i] ?>');
            <?php endfor; ?>
            $('#filterLocations').selectpicker('val', valLocations);
        <?php endif; ?>

        $('#filterLocations').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
            e.preventDefault();
            var valSelected = $('#filterLocations option').eq(clickedIndex).val();
            if (isSelected) {
                valLocations.push(valSelected);
            } else {
                valLocations = $.grep(valLocations, function(value) {
                    return value != valSelected;
                });
            }

            data.locations = valLocations;
            find_services(url, data, target);
        });

        // filter --stars
        <?php if (! empty($filter_stars)): ?>
            <?php for ($i=0; $i < count($filter_stars); $i++): ?>
                <?php $_id = '#filterStar_' . $filter_stars[$i]; ?>
                $('<?= $_id ?>').prop("checked", true);
                valStars.push(<?= $filter_stars[$i] ?>);
            <?php endfor; ?>
        <?php endif; ?>

        $('.filterStars').change(function(e) {
            e.preventDefault();
            var $this = $(this),
                isChecked = $this.is(':checked'),
                valStar = $this.attr("data-star");

            if (isChecked) {
                valStars.push(valStar);
            } else {
                valStars = $.grep(valStars, function(value) {
                    return value != valStar;
                });
            }

            data.stars = valStars;
            find_services(url, data, target);
        });

        // filter --price per
        <?php if (! empty($filter_pricePers)): ?>
            <?php for ($i=0; $i < count($filter_pricePers); $i++): ?>
                valPricePers.push('<?= $filter_pricePers[$i] ?>');
            <?php endfor; ?>
            $('#filterPricePers').selectpicker('val', valPricePers);
        <?php endif; ?>

        $('#filterPricePers').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
            var valSelected = $('#filterPricePers option').eq(clickedIndex).val();
            if (isSelected) {
                valPricePers.push(valSelected);
            } else {
                valPricePers = $.grep(valPricePers, function(value) {
                    return value != valSelected;
                });
            }

            data.pricePers = valPricePers;
            find_services(url, data, target);
        });
    });
</script>