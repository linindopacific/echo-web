<div class="card-body">
    <ul class="nav bg-light nav-pills rounded nav-fill mb-3" id="pills-tab" role="tablist">
        <li class="nav-item" role="presentation">
            <a class="nav-link active" data-toggle="pill" href="#nav-tab-va">
            <i class="fa fa-university"></i>  Virtual Account</a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link" data-toggle="pill" href="#nav-tab-ew">
            <i class="fa fa-wallet"></i>  e-Wallets</a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link" data-toggle="pill" href="#nav-tab-ro">
            <i class="fa fa-store"></i>  Retail Outlets</a>
        </li>
    </ul>

    <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade show active" id="nav-tab-va" data-id="VIRTUAL_ACCOUNT">
            <?php for ($i=0; $i < count($data['VIRTUAL_ACCOUNT']); $i++): ?>
                <div class="form-check pb-2">
                    <label class="form-check-label">
                        <input class="form-check-input" type="radio" name="payment_channel" value="<?= $data['VIRTUAL_ACCOUNT'][$i]['code'] ?>">
                        <?= img($data['VIRTUAL_ACCOUNT'][$i]['image'], false, ['width' => '50%']); ?>
                    </label>
                </div>
            <?php endfor; ?>
        </div> <!-- tab-pane.// -->
        <div class="tab-pane" id="nav-tab-ew" data-id="EWALLET">
            <?php for ($i=0; $i < count($data['EWALLET']); $i++): ?>
                <div class="form-check pb-2">
                    <label class="form-check-label">
                        <input class="form-check-input" type="radio" name="payment_channel" value="<?= $data['EWALLET'][$i]['code'] ?>">
                        <?= img($data['EWALLET'][$i]['image'], false, ['width' => '50%']); ?>
                    </label>
                </div>
            <?php endfor; ?>
        </div> <!-- tab-pane.// -->
        <div class="tab-pane" id="nav-tab-ro" data-id="RETAIL_OUTLET">
            <?php for ($i=0; $i < count($data['RETAIL_OUTLET']); $i++): ?>
                <div class="form-check pb-2">
                    <label class="form-check-label">
                        <input class="form-check-input" type="radio" name="payment_channel" value="<?= $data['RETAIL_OUTLET'][$i]['code'] ?>">
                        <?= img($data['RETAIL_OUTLET'][$i]['image'], false, ['width' => '50%']); ?>
                    </label>
                </div>
            <?php endfor; ?>
        </div> <!-- tab-pane.// -->
    </div> <!-- tab-content .// -->
</div> <!-- card-body.// -->
<input id="payment-category" type="hidden" name="payment_category" value="" />

<script type="text/javascript">
    $(document).ready(function() {

        $("input[name='payment_channel']").change(function(e){
            var category = $(this).closest('.tab-pane').attr('data-id');
            console.log( category );
            $("#payment-category").val(category);
        });

    });

</script>