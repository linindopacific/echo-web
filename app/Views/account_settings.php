<?= $this->extend($viewLayout) ?>

<?= $this->section('pageStyles') ?>
<?= link_tag("https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.18/dist/css/bootstrap-select.min.css"); ?>
<?= link_tag(base_url('node_modules/@chenfengyuan/datepicker/dist/datepicker.min.css')); ?>
<?= link_tag(base_url('assets/css/style_profile1.css')); ?>
<?= $this->endSection() ?>

<?= $this->section('main') ?>
<div class="container-fluid" style="width:90%;">

	<div class="row justify-content-center">
		<div class="col-lg-12">
			<?= \Config\Services::validation()->listErrors(); ?>
			<?= view('App\Template\_message_block') ?>
			<?= form_open('account/settings', ['autocomplete' => 'off', 'enctype' => 'multipart/form-data']); ?>
			<?= csrf_field() ?>
			<div class="row">
				<div class="col-lg-3">
					<div class="profile-card-4 z-depth-3">
						<div class="card">
							<div class="card-body text-center bg-primary rounded-top">
								<div class="user-box picture-container">
									<div class="picture">
										<img src="<?= user_profile()->display_picture; ?>" alt="user avatar" class="picture-src" id="wizardPicturePreview">
										<input type="file" id="wizard-picture" accept="image/x-png,image/jpeg" name="avatar">
									</div>
								</div>
								<h5 class="mb-1 text-white"><?= user_profile()->first_name.' '.user_profile()->middle_name.' '.user_profile()->last_name?></h5>
								<h6 class="text-light"><?= user_profile()->job ?></h6>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-8">
					<div class="card z-depth-3">
						<div class="card-body">
							<div class="tab-pane" id="edit">
								<div class="form-group row">
									<div class="form-group col-md-4">
										<label class="col-form-label form-control-label" for="inputFirstName">First Name</label>
										<input type="text" class="form-control" id="inputFirstName" name="first_name" value="<?= set_value('first_name', user_profile()->first_name ?? ""); ?>" autofocus="">
									</div>
									<div class="form-group col-md-4">
										<label class="col-form-label form-control-label" for="inputMiddleName">Middle Name</label>
										<input type="text" class="form-control" id="inputMiddleName" name="middle_name" value="<?= set_value('middle_name', user_profile()->middle_name ?? ""); ?>">
									</div>
									<div class="form-group col-md-4">
										<label class="col-form-label form-control-label" for="inputLastName">Last Name</label>
										<input type="text" class="form-control" id="inputLastName" name="last_name" value="<?= set_value('last_name', user_profile()->last_name ?? ""); ?>">
									</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label form-control-label">Birthday Date</label>
									<div class="col-lg-9">
										<div class="input-group" id="birthdate">
											<input type="text" class="form-control" id="inputBirthdate" name="birthdate" value="<?= set_value('birthdate', user_profile()->birthdate ?? ""); ?>">
											<div class="input-group-addon">
												<span class="glyphicon glyphicon-th"></span>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label form-control-label">Gender</label>
									<div class="col-lg-9">
										<select id="inputGender" class="form-control selectpicker" name="gender" data-live-search="true" data-selected-text-format="count > 2" data-size="5">
											<?php
											if(user_profile()->gender == NULL)
											{
												echo "<option disabled selected>Pilih Gender...</option>" ;

											}
											else
											{
												echo "<option selected value='".user_profile()->gender."'>".ucwords(user_profile()->gender)."</option>";
											} ?>
											<option value="male"> Male</option>
											<option value="female"> Female</option>
											<option value="other"> Other</option>
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label form-control-label">Mobile Phone</label>
									<div class="col-lg-9">
										<input class="form-control" type="text" id="inputPhoneNumber" name="phone_number" value="<?= set_value('phone_number', user_profile()->phone_number ?? ""); ?>">
									</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label form-control-label">Job</label>
									<div class="col-lg-9">
										<input class="form-control" type="text" id="inputJob" name="job" value="<?= set_value('job', user_profile()->job ?? ""); ?>">
									</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label form-control-label">Industry</label>
									<div class="col-lg-9">
										<input class="form-control" type="text" id="inputIndustry" name="industry" value="<?= set_value('industry', user_profile()->industry ?? ""); ?>">
									</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label form-control-label">Hobby</label>
									<div class="col-lg-9">
										<input class="form-control" type="text" id="inputHobby" name="hobby" value="<?= set_value('hobby', user_profile()->hobby ?? ""); ?>">
									</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label form-control-label">Address</label>
									<div class="col-lg-9">
										<input class="form-control" type="text" id="inputAddress" name="address_1" value="<?= set_value('address_1', user_profile()->address_1 ?? ""); ?>">
									</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label form-control-label"></label>
									<div class="col-lg-9">
										<input class="form-control" type="text" id="inputAddress2" name="address_2" value="<?= set_value('address_2', user_profile()->address_2 ?? ""); ?>">
									</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label form-control-label">Provinsi</label>
									<div class="col-lg-9">
										<select id="inputState" class="form-control selectpicker" name="region" data-live-search="true" data-selected-text-format="count > 2" data-size="5">
											<option disabled selected>Pilih Provinsi...</option>
										</select>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label form-control-label">Kota / Kabupaten</label>
									<div class="col-lg-9">
										<select id="inputCity" class="form-control selectpicker" name="city" data-live-search="true" data-selected-text-format="count > 2" data-size="5">
											<option disabled selected>Pilih Kota/Kabupaten...</option>
										</select>
									</div>
								</div>
								<!-- <div class="form-group col-md-2">
									<label for="inputZip">Kode Pos</label>
									<input type="text" class="form-control" id="inputZip" name="zip_code" value="<?= set_value('zip_code', user_profile()->zip_code ?? ""); ?>">
								</div> -->
								<div class="form-group row">
									<label class="col-lg-3 col-form-label form-control-label">About me</label>
									<div class="col-lg-9">
										<textarea class="form-control" id="inputAbout" name="about"><?= set_value('about', user_profile()->about ?? ""); ?></textarea>
									</div>
								</div>
								<div class="form-group row">
									<label class="col-lg-3 col-form-label form-control-label"></label>
									<div class="col-lg-9">
										<input type="submit" class="btn btn-primary" value="Save Changes">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?= form_close(); ?>
		</div>
	</div>
</div>
<?= $this->endSection() ?>

<?= $this->section('pageScripts') ?>
<?= script_tag("https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.18/dist/js/bootstrap-select.min.js"); ?>
<?= script_tag(base_url('node_modules/@chenfengyuan/datepicker/dist/datepicker.min.js')); ?>
<script type="text/javascript">
	$(document).ready(function() {
		var siteUrl = '<?=site_url();?>';

		$('#inputBirthdate').datepicker({
		    format: "yyyy-mm-dd",
		    autoHide: true,
		    startDate: new Date(<?= date('Y')-60 ?>, 0, 1),
			endDate: new Date(<?= date('Y')-17 ?>, 11, 31)
		});

		// check if user doesn't have a password
		firebase.auth().onAuthStateChanged(function(user) {
			if (user) {
				// User is signed in.
				var email = user.email;

				firebase.auth().fetchSignInMethodsForEmail(email).then(function(methods) {
					found = false;
					methods.forEach(function(method) {
						console.log(method);
						if (method === 'password') {
							found = true;
						}
					});

					if (!found) {
						console.log(email + " doesn't have a password.");
					}
				});
				// ...
			} else {
				// User is signed out.
				// ...
			}
		});

		$.ajaxq ('MyQueue', {
	        url: siteUrl + 'getProvince',
	        type: 'get',
	        dataType: 'json',
	        success: function(data) {
	        	var formoption = '<option disabled selected>Pilih Provinsi...</option>',
	        		selectedVal = '<?=user_profile()->region ?? ""?>';

				$.each(data, function(v) {
					var val = data[v];
					formoption += '<option value="' + val.name + '">' + val.name + '</option>';
				});

				$('#inputState').html(formoption);
				$('#inputState').selectpicker('refresh');
				$('#inputState').selectpicker('val', selectedVal);
            }
	    });

	    $('#inputState').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
	    	if (isSelected === true) {
	    		$.ajaxq ('MyQueue', {
			        url: siteUrl + 'getRegency',
			        type: 'get',
			        data: {'province':this.value},
			        dataType: 'json',
			        success: function(data) {
			        	var formoption = '<option disabled selected>Pilih Kota/Kabupaten...</option>',
			        		selectedVal = '<?=user_profile()->city ?? ""?>';

						$.each(data, function(v) {
							var val = data[v];
							formoption += '<option value="' + val.name + '">' + val.name + '</option>';
						});

						$('#inputCity').html(formoption);
						$('#inputCity').selectpicker('refresh');
						$('#inputCity').selectpicker('val', selectedVal);
		            }
			    });
	    	} else { // form update, selected option exist
	    		var selectedVal = '<?=user_profile()->region ?? ""?>';
		    	if (selectedVal != '') {
		    		$.ajaxq ('MyQueue', {
				        url: siteUrl + 'getRegency',
				        type: 'get',
				        data: {'province':selectedVal},
				        dataType: 'json',
				        success: function(data) {
				        	var formoption = '<option disabled selected>Pilih Kota/Kabupaten...</option>',
				        		selectedVal = '<?=user_profile()->city ?? ""?>';

							$.each(data, function(v) {
								var val = data[v];
								formoption += '<option value="' + val.name + '">' + val.name + '</option>';
							});

							$('#inputCity').html(formoption);
							$('#inputCity').selectpicker('refresh');
							$('#inputCity').selectpicker('val', selectedVal);
			            }
				    });
		    	}
	    	}
		});

		/* form avatar */
		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				reader.onload = function (e) {
					$('#wizardPicturePreview').attr('src', e.target.result);
				}
				reader.readAsDataURL(input.files[0]);
			}
		}
		$("#wizard-picture").change(function(){
			readURL(this);
		});

	});
</script>
<?= $this->endSection() ?>