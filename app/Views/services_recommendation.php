<?php $faker = \Faker\Factory::create(); ?>

<div class="row">
    <div class="col-lg-3 col-md-12 col-sm-12 m-2 px-0">
        <?= $this->include('services_search_filter') ?>
    </div>
    <div class="col-lg-8 col-md-12 col-sm-12 m-2">
        <div class="row">
            <div class="col-6">
                <!-- <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0" style="background-color: #fff;">
                    <li class="breadcrumb-item"><a href="#">Categories</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Events</li>
                    </ol>
                </nav> -->
            </div>
            <div class="col-6">
                <div class="form-group col-md-4 float-right">
                    <select class="form-control selectpicker" multiple data-max-options="1" title="Friend Circle...">
                        <option value="1">Circle 1</option>
                        <option value="2">Circle 2</option>
                        <option value="3">Circle 3</option>
                    </select>
                </div>
            </div>
        </div>
        <!-- start row -->
        <?php foreach ($services as $row): ?>
            <?php if ($row->is_owner_profile_complete): ?>
            <div class="row border-bottom">
                <div class="col-sm-8 my-3">
                    <div class="row">
                        <div class="col-sm-4">
                            ​<div class="card border-0 w-100">
                                <img src="<?= $row->images[0]; ?>" class="card-img-top" alt="" loading="lazy">
                                <div class="card-body">
                                    <div class="d-flex justify-content-center">
                                        <div class="px-1 py-1"><a href="#" class="btn btn-primary">Message</a></div>
                                        <div class="px-1 py-1"><a href="#" class="btn btn-secondary">Schedule</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-8 px-0">
                            <div class="d-flex ml-0 mr-2 mt-2 flex-wrap bd-highlight">
                                <div class="px-2 flex-grow-1 bd-highlight">
                                    <span class="font-weight-bold">
                                        <h5>
                                            <?= anchor(site_url('services/' . $row->id), $row->name, []); ?>
                                        </h5>
                                    </span>
                                </div>
                                <div class="px-2 bd-highlight">
                                    <span class="font-weight-bold text-danger">Rp <?= $numberLib->singkat_angka($row->price); ?>/<?= $row->price_per; ?></span>
                                </div>
                            </div>
                            <div class="d-flex ml-0 mr-2 mb-3 flex-wrap bd-highlight">
                                <div class="px-2 bd-highlight">
                                    <span class="text-muted"><?= $row->owner; ?></span>
                                </div>
                            </div>
                            <div class="d-flex ml-0 mr-3 my-3 flex-wrap flex-row bd-highlight text-muted">
                                <div class="px-2 bd-highlight order-1">
                                    <span class="market-coverage"><i class="fas fa-map-marker-alt" aria-hidden="true"></i></span> <?= ucwords(strtolower($row->coverages)); ?>
                                </div>
                                <div class="px-2 bd-highlight order-2">
                                    <span class="market-complete"><i class="fas fa-check-circle" aria-hidden="true"></i></span> <?= $faker->numberBetween($min=10, $max=100) ?> Completed
                                </div>
                                <div class="px-2 bd-highlight order-3">
                                    <span class="score">
                                        <div class="score-wrap">
                                            <span class="market-stars-active" style="width:<?= $faker->numberBetween($min=10, $max=100) ?>%">
                                                <i class="fas fa-star" aria-hidden="true"></i>
                                                <i class="fas fa-star" aria-hidden="true"></i>
                                                <i class="fas fa-star" aria-hidden="true"></i>
                                                <i class="fas fa-star" aria-hidden="true"></i>
                                                <i class="fas fa-star" aria-hidden="true"></i>
                                            </span>
                                            <span class="market-stars-inactive">
                                                <i class="far fa-star" aria-hidden="true"></i>
                                                <i class="far fa-star" aria-hidden="true"></i>
                                                <i class="far fa-star" aria-hidden="true"></i>
                                                <i class="far fa-star" aria-hidden="true"></i>
                                                <i class="far fa-star" aria-hidden="true"></i>
                                            </span>
                                        </div>
                                    </span>
                                </div>
                                <div class="px-2 bd-highlight order-4">
                                    <span class="market-recommended"><i class="fas fa-thumbs-up" aria-hidden="true"></i></span> (<?= $faker->numberBetween($min=10, $max=50) ?>) Recommended
                                </div>
                                <div class="px-2 bd-highlight order-5">
                                    <span class="market-since"><i class="fas fa-calendar" aria-hidden="true"></i></span> Since 2020
                                </div>
                            </div>
                            <div class="d-flex ml-0 mr-3 py-2 justify-content-start">
                                <?php if (count($row->images) > 1): ?>
                                    <?php for ($i=1; $i < 5; $i++): ?>
                                        <?php if (isset($row->images[$i])): ?>
                                            <a href="<?= $row->images[$i] ?>" data-toggle="lightbox" data-gallery="hidden-images" data-type="image" class="col-sm-3 px-1">
                                                <img src="<?= $row->images[$i] ?>" class="img-fluid rounded" loading="lazy">
                                            </a>
                                        <?php endif; ?>
                                    <?php endfor; ?>
                                <?php endif; ?>
                            </div>
                            <!-- elements not showing, use data-remote -->
                            <?php if (count($row->images) > 5): ?>
                                <?php for ($i=5; $i < count($row->images); $i++): ?>
                                    <div data-toggle="lightbox" data-gallery="hidden-images" data-type="image" data-remote="<?= $row->images[$i] ?>" loading="lazy"></div>
                                <?php endfor; ?>
                            <?php endif; ?>
                            <div class="d-flex ml-0 mr-3 mt-2" style="text-align: justify;">
                                <span class="text-muted">
                                    <?= character_limiter(strip_tags($row->card_content), 80); ?>
                                    <?= anchor(site_url('services/' . $row->id), 'Read more', ['class' => 'text-decoration-none']); ?>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 my-4 mr-0 border-left">
                    <div class="row mb-4">
                        <div class="col">
                            <p class="font-weight-bold">Connection:</p>
                            <div class="d-flex align-items-center">
                                <img src="assets/images/1st.jpg" width="48" height="48" class="rounded-circle" alt="">
                                <div class="px-2 text-muted">
                                    <span class="">Link: Direct</span><br>
                                    <span class="">Group: <a href="#" class="text-decoration-none">High School Buddies</a></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <p class="font-weight-bold">Recommended By:</p>
                            <div class="d-flex align-items-center mb-2 ml-4">
                                <img src="assets/images/user-blank.png" width="48" height="48" class="rounded-circle" alt="">
                                <div class="px-2 text-muted">
                                    <span class="font-weight-bold">Eddy Lee</span><br>
                                </div>
                            </div>
                            <div class="d-flex align-items-center mb-2 ml-4">
                                <img src="assets/images/user-blank.png" width="48" height="48" class="rounded-circle" alt="">
                                <div class="px-2 text-muted">
                                    <span class="font-weight-bold">Lina Nasaur</span><br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <?php endif; ?>
        <?php endforeach; ?>
        <?php $pager->setPath('services/recommendation', 'recommendation'); ?>
        <?= $pager->makeLinks($page, $perPage, $total, 'echo_services', 0); ?>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        var valCategories = [],
            valLocations = [],
            valStars = [],
            valPricePers = [],
            url = siteUrl + '/services/recommendation',
            target = $('#pills-recommendation'),
            data = {'categories':valCategories, 'locations':valLocations, 'stars': valStars, 'pricePers': valPricePers};

        $('#filter_clear').on('click', function(e) {
            e.preventDefault();
            data.categories = [];
            data.locations = [];
            data.stars = [];
            data.pricePers = [];

            find_services(url, data, target);
        });

        $('.pagination .page-item a').on('click', function (e) {
            e.preventDefault();
            var url = $(this).attr('data-target');
            find_services(url, data, target);
        });

        // filter --category
        <?php if ( !empty($filter_categories) ): ?>
            <?php for ($i=0; $i < count($filter_categories); $i++): ?>
                valCategories.push('<?= $filter_categories[$i] ?>');
            <?php endfor; ?>
            $('#filterCategories').selectpicker('val', valCategories);
        <?php endif; ?>

        $('#filterCategories').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
            e.preventDefault();
            var valSelected = $('#filterCategories option').eq(clickedIndex).val();
            if (isSelected) {
                valCategories.push(valSelected);
            } else {
                valCategories = $.grep(valCategories, function(value) {
                    return value != valSelected;
                });
            }

            data.categories = valCategories;
            find_services(url, data, target);
        });

        // filter --location
        <?php if ( !empty($filter_locations) ): ?>
            <?php for ($i=0; $i < count($filter_locations); $i++): ?>
                valLocations.push('<?= $filter_locations[$i] ?>');
            <?php endfor; ?>
            $('#filterLocations').selectpicker('val', valLocations);
        <?php endif; ?>

        $('#filterLocations').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
            e.preventDefault();
            var valSelected = $('#filterLocations option').eq(clickedIndex).val();
            if (isSelected) {
                valLocations.push(valSelected);
            } else {
                valLocations = $.grep(valLocations, function(value) {
                    return value != valSelected;
                });
            }

            data.locations = valLocations;
            find_services(url, data, target);
        });

        // filter --stars
        <?php if ( !empty($filter_stars) ): ?>
            <?php for ($i=0; $i < count($filter_stars); $i++): ?>
                <?php $_id = '#filterStar_' . $filter_stars[$i]; ?>
                $('<?= $_id ?>').prop("checked", true);
                valStars.push(<?= $filter_stars[$i] ?>);
            <?php endfor; ?>
        <?php endif; ?>

        $('.filterStars').change(function(e) {
            e.preventDefault();
            var $this = $(this),
                isChecked = $this.is(':checked'),
                valStar = $this.attr("data-star");

            if (isChecked) {
                valStars.push(valStar);
            } else {
                valStars = $.grep(valStars, function(value) {
                    return value != valStar;
                });
            }

            data.stars = valStars;
            find_services(url, data, target);
        });

        // filter --price
        <?php if ( !empty($filter_pricePers) ): ?>
            <?php for ($i=0; $i < count($filter_pricePers); $i++): ?>
                valPricePers.push('<?= $filter_pricePers[$i] ?>');
            <?php endfor; ?>
            $('#filterPricePers').selectpicker('val', valPricePers);
        <?php endif; ?>

        $('#filterPricePers').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
            e.preventDefault();
            var valSelected = $('#filterPricePers option').eq(clickedIndex).val();
            if (isSelected) {
                valPricePers.push(valSelected);
            } else {
                valPricePers = $.grep(valPricePers, function(value) {
                    return value != valSelected;
                });
            }

            data.pricePers = valPricePers;
            find_services(url, data, target);
        });
    });
</script>