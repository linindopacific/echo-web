<?php $faker = \Faker\Factory::create(); ?>

<div class="row">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body info-card social-about">
                <h2 class="text-blue">About</h2>
                <h4><strong>About Me</strong></h4>
                <p><?= $profile->about; ?></p>
                <h4><strong>Location</strong></h4>
                <?= $profile->address_1; ?><br>
                <?= $profile->address_2; ?><br>
                <p><?= ucwords($profile->city); ?>, <?= ucwords($profile->region); ?></p>
                <h4 class="mb-3"><strong>Personal Info</strong></h4>
                <div class="row">
                    <div class="col-6">
                        <div class="social-info">
                            <i class="fas fa-birthday-cake mr-2"></i>
                            <span><?= $profile->birthdate; ?></span>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="social-info">
                            <i class="fas fas fa-users mr-2"></i>
                            <span><?= ucwords($profile->gender); ?></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="social-info">
                            <i class="fas fas fa-mobile mr-2"></i>
                            <span><?= $profile->phone_number; ?></span>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="social-info">
                            <i class="fas fas fa-envelope mr-2"></i>
                            <span><?= $profile->email; ?></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="social-info">
                            <i class="fas fas fa-briefcase mr-2"></i>
                            <span><?= $profile->job; ?></span>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="social-info">
                            <i class="fas fas fa-industry mr-2"></i>
                            <span><?= $profile->industry; ?></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="social-info">
                            <i class="fas fas fa-life-ring mr-2"></i>
                            <span><?= $profile->hobby; ?></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="card info-card">
            <div class="card-body">
                <h2 class="text-blue">Reviews</h2>
                <div class="row">
                    <div class="col-lg-6">
                        <h4>HTML5</h4>
                        <?php $stars = $faker->numberBetween($min=2, $max=5); $noStars = 5-$stars; ?>
                        <div class="mb-3">
                            <?php for ($i=0; $i < $stars; $i++): ?>
                                <i class="fas fa-star"></i>
                            <?php endfor; ?>
                            <?php for ($i=0; $i < $noStars; $i++): ?>
                                <i class="far fa-star"></i>
                            <?php endfor; ?>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <h4>Bootstrap Framework</h4>
                        <?php $stars = $faker->numberBetween($min=2, $max=5); $noStars = 5-$stars; ?>
                        <div class="mb-3">
                            <?php for ($i=0; $i < $stars; $i++): ?>
                                <i class="fas fa-star"></i>
                            <?php endfor; ?>
                            <?php for ($i=0; $i < $noStars; $i++): ?>
                                <i class="far fa-star"></i>
                            <?php endfor; ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <h4>Vanilla Javascript</h4>
                        <?php $stars = $faker->numberBetween($min=2, $max=5); $noStars = 5-$stars; ?>
                        <div class="mb-3">
                            <?php for ($i=0; $i < $stars; $i++): ?>
                                <i class="fas fa-star"></i>
                            <?php endfor; ?>
                            <?php for ($i=0; $i < $noStars; $i++): ?>
                                <i class="far fa-star"></i>
                            <?php endfor; ?>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <h4>Angular Js</h4>
                        <?php $stars = $faker->numberBetween($min=2, $max=5); $noStars = 5-$stars; ?>
                        <div class="mb-3">
                            <?php for ($i=0; $i < $stars; $i++): ?>
                                <i class="fas fa-star"></i>
                            <?php endfor; ?>
                            <?php for ($i=0; $i < $noStars; $i++): ?>
                                <i class="far fa-star"></i>
                            <?php endfor; ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <h4>Adobe Photoshop</h4>
                        <?php $stars = $faker->numberBetween($min=2, $max=5); $noStars = 5-$stars; ?>
                        <div class="mb-3">
                            <?php for ($i=0; $i < $stars; $i++): ?>
                                <i class="fas fa-star"></i>
                            <?php endfor; ?>
                            <?php for ($i=0; $i < $noStars; $i++): ?>
                                <i class="far fa-star"></i>
                            <?php endfor; ?>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <h4>Adobe Illustrator</h4>
                        <?php $stars = $faker->numberBetween($min=2, $max=5); $noStars = 5-$stars; ?>
                        <div class="mb-3">
                            <?php for ($i=0; $i < $stars; $i++): ?>
                                <i class="fas fa-star"></i>
                            <?php endfor; ?>
                            <?php for ($i=0; $i < $noStars; $i++): ?>
                                <i class="far fa-star"></i>
                            <?php endfor; ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <h4>SASS/SCSS</h4>
                        <?php $stars = $faker->numberBetween($min=2, $max=5); $noStars = 5-$stars; ?>
                        <div class="mb-3">
                            <?php for ($i=0; $i < $stars; $i++): ?>
                                <i class="fas fa-star"></i>
                            <?php endfor; ?>
                            <?php for ($i=0; $i < $noStars; $i++): ?>
                                <i class="far fa-star"></i>
                            <?php endfor; ?>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <h4>WORDPRESS</h4>
                        <?php $stars = $faker->numberBetween($min=2, $max=5); $noStars = 5-$stars; ?>
                        <div class="mb-3">
                            <?php for ($i=0; $i < $stars; $i++): ?>
                                <i class="fas fa-star"></i>
                            <?php endfor; ?>
                            <?php for ($i=0; $i < $noStars; $i++): ?>
                                <i class="far fa-star"></i>
                            <?php endfor; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>