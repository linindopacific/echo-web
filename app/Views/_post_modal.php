<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="postModalLabel"><?= (!$isNew) ? 'Update Post' : 'New Post'; ?></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?= form_open(route_to('post/store'), ['id' => $formId]); ?>
        <?= (!$isNew) ? form_hidden('id', $post->id) : ''; ?>
        <div class="modal-body">
            <div class="form-group">
                <textarea class="form-control" id="post-content" name="content"><?= (!$isNew) ? $post->content : ''; ?></textarea>
            </div>
            <div class="form-group text-right">
                <span id="maxContentPost"></span>
            </div>
        </div>
        <div class="modal-footer">
            <div class="dropdown">
                <select class="selectpicker" id="post_visibility" name="visibility" data-width="fit">
                    <?php foreach ($postVisibility as $row): ?>
                        <?php
                            switch ($row['name']) {
                                case 'connection':
                                    $icon = "<i class='fas fa-users'></i> &nbsp;";
                                    break;
                                case 'public':
                                    $icon = "<i class='fas fa-globe'></i> &nbsp;";
                                    break;
                                case 'private':
                                    $icon = "<i class='fas fa-lock'></i> &nbsp;";
                                    break;
                                default:
                                    $icon = "";
                                    break;
                            }
                        ?>
                        <?php $selected = (!$isNew) ? (($row['id'] === $post->visibility) ? 'selected' : '') : (($row['name'] === 'connection') ? 'selected' : '');?>
                        <option value="<?= $row['id'] ?>" data-content="<?= $icon . $row['description']; ?>" <?= $selected ?>><?= $row['description']; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <button id="share-post" type="button" class="btn btn-primary">
                <i class="fas fa-share-square"></i> <span class="align-middle"> Post</span>
            </button>
        </div>
        <?= form_close(); ?>
    </div>
</div>