<div class="row">
	<div class="col-lg-3">
		<div class="card">
			<ul class="list-group list-group-flush">
				<li class="list-group-item">
					<div class="h6 text-muted">Connection</div>
					<div class="h5"><?= count($relations); ?></div>
				</li>
			</ul>
		</div>
	</div>
	<div class="col-lg-9 gedf-main">
		<!--- \\\\\\\Post-->
		<div class="card social-timeline-card">
			<div class="card-body">
				<?= ($profile->total_pending_requests === 0) ? "No" : $profile->total_pending_requests; ?>  pending connection requests
			</div>
		</div>
		<!-- Post /////-->
		<!--- \\\\\\\Post-->
		<div class="card social-timeline-card">
			<div class="card-body">
				<div class="people-nearby">
					<?php foreach ($relations as $relation): ?>
					<div class="nearby-user">
						<div class="row">
							<div class="col-md-3 col-sm-3 profile-photo">
								<img src="<?= $relation->display_picture; ?>" alt="user" class="profile-photo-lg">
							</div>
							<div class="col-md-6 col-sm-6">
								<h5><?= anchor(site_url('profile/' . $relation->username),$relation->display_name, ['class' => 'profile-link']); ?></h5>
								<p style="margin-bottom: 0;"><?= $relation->job; ?></p>
								<p class="text-muted" style="font-size: smaller;">Connected <?= $relation->joined; ?></p>
							</div>
							<div class="col-md-3 col-sm-3">
								<button class="btn btn-primary float-right pull-left act-profile" data-act="send_message" data-id="<?= $relation->id; ?>" data-toggle="tooltip" data-placement="top" title="Message">
									<i class="fab fa-facebook-messenger"></i> Message
								</button>
								<button class="btn btn-link float-right dropdown-toggle" type="button" id="gedf-drop11" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									<i class="fa fa-ellipsis-h"></i>
								</button>
								<div class="dropdown-menu dropdown-menu-right" aria-labelledby="gedf-drop11">
									<a class="dropdown-item act-profile" href="#" data-act="connect_remove" data-id="<?= $relation->id; ?>"><i class="fas fa-user-minus"></i> Remove connection</a>
								</div>
							</div>
						</div>
					</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$('[data-toggle="tooltip"]').tooltip();
</script>