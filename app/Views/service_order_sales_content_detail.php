<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title"><?= $data->order_no; ?></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col">
                    <dl>
                        <dt>Total Bayar</dt>
                        <dd><?= number_to_currency($data->invoice->amount, "IDR", "id"); ?></dd>
                        <dt>Metode Pembayaran</dt>
                        <dd><?= $data->invoice->payment_channel; ?></dd>
                        <dt>Status Bayar</dt>
                        <dd><?= ($data->invoice->status === "PENDING") ? $data->payment_instruction : $data->invoice->status; ?></dd>
                        <dt>Nama Pemesan</dt>
                        <dd><?= $data->sell_to_name; ?></dd>
                        <dt>Nama Item</dt>
                        <dd><?= $data->item; ?></dd>
                        <dt>Tanggal Pemesanan</dt>
                        <dd><?= $data->created_at->toLocalizedString("d MMM yyyy, H:mm OOOO"); ?></dd>
                        <dt>Status Pesanan</dt>
                        <dd><?= $data->order_status; ?></dd>
                        <dt>Ekspektasi Pengerjaan</dt>
                        <dd><?= $data->request_date->toLocalizedString("d MMM yyyy, H:mm OOOO"); ?></dd>
                        <dt>Lokasi Pengerjaan</dt>
                        <dd><?= $data->sell_to_full_address; ?></dd>
                    </dl>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <?php if ($data->invoice->status === "PAID" && !in_array($data->order_status, ["FULFILLED", "COMPLETED"]) ): ?>
            <button id="deliver-order" type="button" class="btn btn-primary" data-id="<?= $id; ?>">
                <i class="fa fa-check-square-o"></i> Selesaikan Pesanan
            </button>
            <?php endif; ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $("#deliver-order").on("click", function(e) {
            e.preventDefault();
            var id = $(this).attr("data-id");
            $.ajaxq ("MyQueue", {
                url: siteUrl + "/services/deliver-order",
                type: "post",
                data: {order_id: id},
                dataType: "json",
                beforeSend: function() {
                    $("#deliver-order").html('<i class="fas fa-spinner fa-pulse"></i> Memproses');
                    $("#deliver-order").attr("disabled", true);
                },
                complete:function() {
                    $("#deliver-order").html('<i class="fa fa-check-square-o"></i> Selesaikan Pesanan');
                    $("#deliver-order").attr("disabled", false);
                },
                success: function(response) {
                    $("#serviceOrderModal").modal("hide");
                    window.location.href = siteUrl + "/services/order-list";
                },
                error: function(xhr, textStatus, errorThrown) {
                    var res = xhr.responseJSON;
                    if (xhr.status == 401) {
                        alert("Error: " + errorThrown)
                        window.location.replace(siteUrl);
                    } else {
                        alert("Error: " + res.error);
                    }
                }
            });
        });
    });
</script>