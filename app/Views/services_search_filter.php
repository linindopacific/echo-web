<div class="row justify-content-between font-weight-bold border-top mx-0 py-2">
    <div class="col-auto mr-auto">
        Filters
    </div>
    <div class="col-auto">
        <?php if ( !empty($filter_categories) || !empty($filter_locations) || !empty($filter_stars) || !empty($filter_pricePers) ): ?>
        <span id="filter_clear"><small>[clear]</small></span>
        <?php endif; ?>
    </div>
</div>
<div class="font-weight-bold border-top mx-0 py-2">
    <ul class="list-group list-group-flush">
        <li class="list-group-item border-0 py-1">
            <div class="row mb-1">
                <div class="col">
                    Category
                </div>
            </div>
            <div class="row ml-1 mt-3 font-weight-normal">
                <div class="col-lg-8 col-md-12 col-sm-12">
                    <div class="form-group">
                        <select id="filterCategories" class="form-control selectpicker" multiple data-live-search="true" data-selected-text-format="count > 2" data-size="8">
                            <?php
                                foreach ($service_category as $r1):
                                    if (!empty($r1['subcategory'])) {
                                        echo '<optgroup label="' . $r1['name'] . '">';
                                        foreach ($r1['subcategory'] as $r2) {
                                            echo '<option value="' . $r2['id'] . '">' . $r2['name'] . '</option>';
                                        }
                                        echo '</optgroup>';
                                    } else {
                                        echo '<option value="' . $r1['id'] . '">' . $r1['name'] . '</option>';
                                    }
                                endforeach;
                            ?>
                        </select>
                    </div>
                </div>
            </div>
        </li>
        <li class="list-group-item border-0 py-1">
            <div class="row mb-1">
                <div class="col">
                    Location
                </div>
            </div>
            <div class="row ml-1 mt-3 font-weight-normal">
                <div class="col-lg-8 col-md-12 col-sm-12">
                    <div class="form-group">
                        <select id="filterLocations" class="form-control selectpicker" multiple data-live-search="true" data-selected-text-format="count > 2" data-size="8">
                            <?php
                                $optgroup='';
                                foreach ($province_regency as $key1 => $value1):
                                    foreach ($province_regency[$key1] as $key2 => $value2):
                                        if ($optgroup != $key1) {
                                            if ($optgroup != '') echo '</optgroup>';
                                            echo '<optgroup label="' . $key1 . '">';
                                        } else {
                                            echo '<option value="' . $key2 . '">' . $value2 . '</option>';
                                        }
                                        $optgroup = $key1;
                                    endforeach;
                                endforeach;
                            ?>
                        </select>
                    </div>
                </div>
            </div>
        </li>
        <li class="list-group-item border-0 py-1">
            <div class="row mb-1">
                <div class="col">
                    Average Review Rating
                </div>
            </div>
            <div class="form-group row ml-4 mb-1 font-weight-normal" style="line-height: normal;">
                <div class="form-check">
                    <input class="form-check-input filterStars" type="checkbox" id="filterStar_5" data-star="5">
                    <label class="form-check-label" for="filterStar_5">
                        <span class="score">
                            <div class="score-wrap">
                                <span class="market-stars-active" style="width:100%">
                                    <i class="fas fa-star fa-sm" aria-hidden="true"></i>
                                    <i class="fas fa-star fa-sm" aria-hidden="true"></i>
                                    <i class="fas fa-star fa-sm" aria-hidden="true"></i>
                                    <i class="fas fa-star fa-sm" aria-hidden="true"></i>
                                    <i class="fas fa-star fa-sm" aria-hidden="true"></i>
                                </span>
                                <span class="market-stars-inactive">
                                    <i class="far fa-star fa-sm" aria-hidden="true"></i>
                                    <i class="far fa-star fa-sm" aria-hidden="true"></i>
                                    <i class="far fa-star fa-sm" aria-hidden="true"></i>
                                    <i class="far fa-star fa-sm" aria-hidden="true"></i>
                                    <i class="far fa-star fa-sm" aria-hidden="true"></i>
                                </span>
                            </div>
                        </span>
                    </label>
                </div>
            </div>
            <div class="form-group row ml-4 mb-1 font-weight-normal" style="line-height: normal;">
                <div class="form-check">
                    <input class="form-check-input filterStars" type="checkbox" id="filterStar_4" data-star="4">
                    <label class="form-check-label" for="filterStar_4">
                        <span class="score">
                            <div class="score-wrap">
                                <span class="market-stars-active" style="width:80%">
                                    <i class="fas fa-star fa-sm" aria-hidden="true"></i>
                                    <i class="fas fa-star fa-sm" aria-hidden="true"></i>
                                    <i class="fas fa-star fa-sm" aria-hidden="true"></i>
                                    <i class="fas fa-star fa-sm" aria-hidden="true"></i>
                                    <i class="fas fa-star fa-sm" aria-hidden="true"></i>
                                </span>
                                <span class="market-stars-inactive">
                                    <i class="far fa-star fa-sm" aria-hidden="true"></i>
                                    <i class="far fa-star fa-sm" aria-hidden="true"></i>
                                    <i class="far fa-star fa-sm" aria-hidden="true"></i>
                                    <i class="far fa-star fa-sm" aria-hidden="true"></i>
                                    <i class="far fa-star fa-sm" aria-hidden="true"></i>
                                </span>
                            </div>
                        </span>
                    </label>
                </div>
            </div>
            <div class="form-group row ml-4 mb-1 font-weight-normal" style="line-height: normal;">
                <div class="form-check">
                    <input class="form-check-input filterStars" type="checkbox" id="filterStar_3" data-star="3">
                    <label class="form-check-label" for="filterStar_3">
                        <span class="score">
                            <div class="score-wrap">
                                <span class="market-stars-active" style="width:60%">
                                    <i class="fas fa-star fa-sm" aria-hidden="true"></i>
                                    <i class="fas fa-star fa-sm" aria-hidden="true"></i>
                                    <i class="fas fa-star fa-sm" aria-hidden="true"></i>
                                    <i class="fas fa-star fa-sm" aria-hidden="true"></i>
                                    <i class="fas fa-star fa-sm" aria-hidden="true"></i>
                                </span>
                                <span class="market-stars-inactive">
                                    <i class="far fa-star fa-sm" aria-hidden="true"></i>
                                    <i class="far fa-star fa-sm" aria-hidden="true"></i>
                                    <i class="far fa-star fa-sm" aria-hidden="true"></i>
                                    <i class="far fa-star fa-sm" aria-hidden="true"></i>
                                    <i class="far fa-star fa-sm" aria-hidden="true"></i>
                                </span>
                            </div>
                        </span>
                    </label>
                </div>
            </div>
            <div class="form-group row ml-4 mb-1 font-weight-normal" style="line-height: normal;">
                <div class="form-check">
                    <input class="form-check-input filterStars" type="checkbox" id="filterStar_2" data-star="2">
                    <label class="form-check-label" for="filterStar_2">
                        <span class="score">
                            <div class="score-wrap">
                                <span class="market-stars-active" style="width:40%">
                                    <i class="fas fa-star fa-sm" aria-hidden="true"></i>
                                    <i class="fas fa-star fa-sm" aria-hidden="true"></i>
                                    <i class="fas fa-star fa-sm" aria-hidden="true"></i>
                                    <i class="fas fa-star fa-sm" aria-hidden="true"></i>
                                    <i class="fas fa-star fa-sm" aria-hidden="true"></i>
                                </span>
                                <span class="market-stars-inactive">
                                    <i class="far fa-star fa-sm" aria-hidden="true"></i>
                                    <i class="far fa-star fa-sm" aria-hidden="true"></i>
                                    <i class="far fa-star fa-sm" aria-hidden="true"></i>
                                    <i class="far fa-star fa-sm" aria-hidden="true"></i>
                                    <i class="far fa-star fa-sm" aria-hidden="true"></i>
                                </span>
                            </div>
                        </span>
                    </label>
                </div>
            </div>
            <div class="form-group row ml-4 mb-1 font-weight-normal" style="line-height: normal;">
                <div class="form-check">
                    <input class="form-check-input filterStars" type="checkbox" id="filterStar_1" data-star="1">
                    <label class="form-check-label" for="filterStar_1">
                        <span class="score">
                            <div class="score-wrap">
                                <span class="market-stars-active" style="width:20%">
                                    <i class="fas fa-star fa-sm" aria-hidden="true"></i>
                                    <i class="fas fa-star fa-sm" aria-hidden="true"></i>
                                    <i class="fas fa-star fa-sm" aria-hidden="true"></i>
                                    <i class="fas fa-star fa-sm" aria-hidden="true"></i>
                                    <i class="fas fa-star fa-sm" aria-hidden="true"></i>
                                </span>
                                <span class="market-stars-inactive">
                                    <i class="far fa-star fa-sm" aria-hidden="true"></i>
                                    <i class="far fa-star fa-sm" aria-hidden="true"></i>
                                    <i class="far fa-star fa-sm" aria-hidden="true"></i>
                                    <i class="far fa-star fa-sm" aria-hidden="true"></i>
                                    <i class="far fa-star fa-sm" aria-hidden="true"></i>
                                </span>
                            </div>
                        </span>
                    </label>
                </div>
            </div>
        </li>
        <li class="list-group-item border-0 py-1">
            <div class="row mb-1">
                <div class="col">
                    Price
                </div>
            </div>
            <div class="row ml-1 mt-3 font-weight-normal">
                <div class="col-lg-8 col-md-12 col-sm-12">
                    <div class="form-group">
                        <select id="filterPricePers" class="form-control selectpicker" multiple>
                            <option value="hour">Per Hour</option>
                            <option value="day">Per Day</option>
                            <option value="event">Per Event</option>
                        </select>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.selectpicker').selectpicker();
    });
</script>