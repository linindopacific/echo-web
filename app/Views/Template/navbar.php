<div id="dekstop" class="d-none d-sm-none d-md-block d-lg-block d-xl-block">
    <nav class="navbar navbar-expand-md navbar-light fixed-top bg-white shadow-sm">
        <a class="navbar-brand" href="<?= base_url(); ?>">
            <img src="<?= base_url('assets/images/bootstrap-solid.svg'); ?>" width="30" height="30" class="d-inline-block align-top" alt="">
            Navbar
        </a>
        <div class="ml-0 d-none d-lg-block d-xl-block">
            <?= form_open(route_to('profile/search'), ['id' => 'myForm', 'class' => 'form-inline ml-2']); ?>
                <input id="s-people" class="form-control mr-sm-2" type="search" name="phrase" placeholder="Find People..." aria-label="Search">
            <?= form_close(); ?>
        </div>
        <div class="mx-auto">
            <ul class="navbar-nav">
                <li class="nav-item mx-3 <?= $active_home ?? ""; ?>">
                    <a class="nav-link" href="<?= base_url(); ?>"><i class="material-icons md-36">home</i> <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item mx-3 <?= $active_service ?? ""; ?>">
                    <a class="nav-link" href="<?= site_url('services'); ?>"><i class="material-icons md-36">construction</i></a>
                </li>
                <li class="nav-item mx-3 <?= $active_people ?? ""; ?>">
                    <a class="nav-link" href="#"><i class="material-icons md-36">people</i></a>
                </li>
                <li class="nav-item mx-3 <?= $active_orders ?? ""; ?>">
                    <a class="nav-link" href="<?= site_url('services/order-list'); ?>"><i class="material-icons md-36">payments</i></a>
                </li>
            </ul>
        </div>
        <div class="mr-0">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item ml-3 mr-1 <?= $active_profile ?? ""; ?>">
                    <a class="nav-link" href="<?= site_url('profile'); ?>">
                        <img src="<?= user_profile()->display_picture; ?>" width="30" height="30" class="rounded-circle" alt="">
                        <span class="hidden-xs"><?= user_profile()->first_name; ?></span>
                    </a>
                </li>
                <li class="nav-item dropdown mx-1">
                    <button class="btn btn-secondary" type="button" id="dropdownNotification" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-bell"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right py-0" aria-labelledby="dropdownNotification"></div>
                </li>
                <li class="nav-item dropdown mx-1">
                    <button class="btn btn-secondary" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-gear"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                        <a class="dropdown-item <?= $active_account_settings ?? ""; ?>" href="<?= site_url('account/settings'); ?>">Pengaturan Akun</a>
                        <a class="dropdown-item" href="<?= site_url('logout'); ?>">Keluar</a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</div>
<div id="mobile" class="d-block d-sm-block d-md-none d-lg-none d-xl-none">
    <nav class="navbar navbar-expand-lg navbar-light fixed-top bg-white shadow-sm">
        <a class="navbar-brand" href="#">
            <img src="<?= base_url('assets/images/bootstrap-solid.svg'); ?>" width="30" height="30" class="d-inline-block align-top" alt="">
            Navbar M
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavM" aria-controls="navbarNavM" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavM">
            <ul class="navbar-nav">
                <li class="nav-item <?= $active_home ?? ""; ?>">
                    <a class="nav-link" href="<?= base_url(); ?>">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item <?= $active_service ?? ""; ?>">
                    <a class="nav-link" href="<?= site_url('services'); ?>">Services</a>
                </li>
                <li class="nav-item <?= $active_people ?? ""; ?>">
                    <a class="nav-link" href="#">People</a>
                </li>
                <li class="nav-item <?= $active_orders ?? ""; ?>">
                    <a class="nav-link" href="<?= site_url('services/order-list'); ?>">Transactions</a>
                </li>
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" id="dropdownMenu2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img src="<?= user_profile()->display_picture; ?>" width="30" height="30" class="rounded-circle" alt="">
                        <span class="hidden-xs"><?= user_profile()->first_name; ?></span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                        <a class="dropdown-item <?= $active_profile ?? ""; ?>" href="<?= site_url('profile'); ?>">Profil</a>
                        <a class="dropdown-item <?= $active_account_settings ?? ""; ?>" href="<?= site_url('account/settings'); ?>">Pengaturan Akun</a>
                        <a class="dropdown-item" href="<?= site_url('logout'); ?>">Keluar</a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</div>