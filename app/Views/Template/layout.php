<?php

/*
 * File: layout.php
 * Project: echo
 * File Created: Monday, 24th August 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Tuesday, 16th November 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

?>

<?= doctype(); ?>
<html lang="id">
<head>
    <title><?= isset($title) ? $title . ' - ' . getenv('APP_NAME') : getenv('APP_NAME'); ?></title>
    <meta charset="utf-8">
    <meta name="author" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <?= link_tag(base_url('favicon.ico'), 'shortcut icon', 'image/ico'); ?>
    <!-- STYLES -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootswatch@4.6.1/dist/cerulean/bootstrap.min.css" integrity="sha256-RW/ojwB9sS6DzIfFEaDiahxVPN/MupUlrbKrKVkgA9M=" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/pace-js@1.2.4/themes/blue/pace-theme-minimal.css" integrity="sha256-Phb65CA8UtNIOfnRXDSUB0eOaknq+Muq1+JY8Bm3zK4=" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/easy-autocomplete@1.3.5/dist/easy-autocomplete.min.css" integrity="sha256-fARYVJfhP7LIqNnfUtpnbujW34NsfC4OJbtc37rK2rs=" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/easy-autocomplete@1.3.5/dist/easy-autocomplete.themes.min.css" integrity="sha256-kK9BInVvQN0PQuuyW9VX2I2/K4jfEtWFf/dnyi2C0tQ=" crossorigin="anonymous">
    <?= link_tag(base_url('assets/css/style.css')); ?>
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <?= $this->renderSection('pageStyles') ?>
</head>
<body class="<?= isset($bodyClass) ? $bodyClass : ''; ?>">
	<?= $this->include('App\Views\Template\navbar') ?>
	<main role="main" style="margin-top: 80px;">
    <?= $this->renderSection('main') ?>
	</main>	<!-- main end -->
    <!-- SCRIPTS -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha256-fgLAgv7fyCGopR/gBNq2iW3ZKIdqIcyshnUULC4vex8=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/easy-autocomplete@1.3.5/dist/jquery.easy-autocomplete.min.js" integrity="sha256-aS5HnZXPFUnMTBhNEiZ+fKMsekyUqwm30faj/Qh/gIA=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/pace-js@1.2.4/pace.min.js" integrity="sha256-gqd7YTjg/BtfqWSwsJOvndl0Bxc8gFImLEkXQT8+qj0=" crossorigin="anonymous"></script>
    <?= script_tag("https://cdn.jsdelivr.net/npm/ajaxq@0.0.7/ajaxq.min.js"); ?>
    <?= script_tag('https://kit.fontawesome.com/fde69c8c4d.js'); ?>
    <?= script_tag('https://www.gstatic.com/firebasejs/8.6.8/firebase-app.js'); ?>
    <?= script_tag('https://www.gstatic.com/firebasejs/8.6.8/firebase-auth.js'); ?>
    <?= script_tag('https://www.gstatic.com/firebasejs/8.6.8/firebase-messaging.js'); ?>
	<?= script_tag('https://www.gstatic.com/firebasejs/8.6.8/firebase-analytics.js'); ?>
	<?= script_tag('https://js.pusher.com/beams/1.0/push-notifications-cdn.js'); ?>
	<?= script_tag('https://js.pusher.com/7.0/pusher-with-encryption.min.js'); ?>
    <?= script_tag(base_url('assets/js/script.js')); ?>
    <!-- Firebase & Pusher project configuration -->
	<script type="text/javascript">
		const firebaseConfig = {
			apiKey: "<?= getenv('firebase.apiKey'); ?>",
			authDomain: "<?= getenv('firebase.projectId'); ?>.firebaseapp.com",
			databaseURL: "https://<?= getenv('firebase.projectId'); ?>.firebaseio.com",
			projectId: "<?= getenv('firebase.projectId'); ?>",
			storageBucket: "<?= getenv('firebase.projectId'); ?>.appspot.com",
			messagingSenderId: "<?= getenv('firebase.senderId'); ?>",
			appId: "<?= getenv('firebase.appId'); ?>",
			measurementId: "G-<?= getenv('firebase.measurementId') ?>"
		};
		// Initialize Firebase
		firebase.initializeApp(firebaseConfig);

        const beamsClient = new PusherPushNotifications.Client({
            instanceId: "<?= getenv('PUSHER_BEAMS_ID') ?>",
        });
        Pusher.logToConsole = true;
        const pusher = new Pusher("<?= getenv('PUSHER_KEY') ?>", {
            cluster: "<?= getenv('PUSHER_CLUSTER') ?>",
            authEndpoint: siteUrl + "/pusher-auth"
        });
        pusher.connection.bind("error", function (error) {
            console.error("connection error", error);
        });
	</script>
    <?php if ( ! empty(user()) ) { ?>
        <script type="text/javascript">
            const currentUserId = "<?= user_id() ?>";
            const privateChannel = pusher.subscribe("private-user" + currentUserId);
            const presenceChannel = pusher.subscribe("presence-echo");
            const beamsTokenProvider = new PusherPushNotifications.TokenProvider({
                url: siteUrl + "/beams-auth",
            });
            presenceChannel.bind("pusher:subscription_succeeded", () => {
                var me = presenceChannel.members.me;
                var userId = me.id;
                var userInfo = me.info;

                console.log(presenceChannel.members.count);
            });
            presenceChannel.bind("pusher:member_added", () => {
                console.log(presenceChannel.members.count);
            });
            presenceChannel.bind("pusher:member_removed", () => {
                console.log(presenceChannel.members.count);
            });
            presenceChannel.bind("pusher:subscription_error", (error) => {
                var { status } = error;
                if (status == 408 || status == 503) {
                    // Retry?
                    console.log(error);
                }
            });

            beamsClient
                .getUserId()
                .then((userId) => {
                    if (userId !== currentUserId) {
                        beamsClient
                            .start()
                            .then(() => beamsClient.setUserId(currentUserId, beamsTokenProvider))
                            .catch(console.error);
                    }
                })
                .catch(console.error);
        </script>
    <?php } else { ?>
        <script type="text/javascript">
            beamsClient.stop().catch(console.error);
        </script>
    <?php } ?>
    <?= $this->renderSection('pageScripts') ?>
</body>
</html>