<?= $this->extend($viewLayout) ?>

<?= $this->section('pageStyles') ?>
<?= link_tag(base_url('node_modules/jquery-datetimepicker/build/jquery.datetimepicker.min.css')); ?>
<style>
    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    /* Firefox */
    input[type=number] {
        -moz-appearance: textfield;
    }

    [type=radio] {
        position: absolute;
        opacity: 0;
        width: 0;
        height: 0;
    }
    /* IMAGE STYLES */
    [type=radio] + img {
        cursor: pointer;
    }
    /* CHECKED STYLES */
    [type=radio]:checked + img {
        outline: 2px solid #f00;
    }
</style>
<?= $this->endSection() ?>

<?= $this->section('main') ?>
<div class="container">
    <div class="row pt-3 justify-content-center">
        <div class="col">
            <div class="row">
                <div class="col-md-12">
                    <?php
                        if( ! empty(session()->getFlashdata('error')) ){  ?>
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <strong>Error!</strong> <?php echo session()->getFlashdata('error'); ?>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        <?php }
                    ?>
                </div>
            </div>
            <div id="container_row" class="row">
                <div class="col-md-4 order-md-2 mb-4">
                    <h4 class="d-flex justify-content-between align-items-center mb-3">
                        <span class="text-muted">Your cart</span>
                        <span class="badge badge-secondary badge-pill"></span>
                    </h4>
                    <ul class="list-group mb-3">
                        <li class="list-group-item d-flex justify-content-between lh-condensed">
                            <div>
                                <h6 class="my-0"><?= $service->name; ?></h6>
                                <small class="text-muted"><?= word_limiter(strip_tags($service->card_content), 6); ?></small>
                            </div>
                            <span class="text-muted amount-price" data-amount="<?= $service->price; ?>"><?= number_to_currency($service->price, 'IDR', 'id'); ?></span>
                        </li>
                        <li class="list-group-item d-flex justify-content-between">
                            <span>Total (IDR)</span>
                            <strong>Rp. <span id="amount-price-total" data-amount="0"></span></strong>
                        </li>
                    </ul>
                </div>

                <div class="col-md-8 order-md-1">
                    <h4 class="mb-3">Schedule Request</h4>
                    <?= form_open(site_url('services/checkout'), ['id' => 'fm_checkout', 'class' => '', 'autocomplete' => 'off']) ?>
                        <input id="fm_amount" type="hidden" name="total_amount" value="<?= $service->price; ?>"/>
                        <input type="hidden" name="item_amount" value="<?= $service->price; ?>"/>
                        <input type="hidden" name="item_name" value="<?= $service->name; ?>"/>
                        <input type="hidden" name="item_id" value="<?= $service->id; ?>"/>
                        <div class="mb-3">
                            <label for="datetimepicker">Expected Date & Time</label> <br>
                            <input type="text" class="" id="datetimepicker" name="request_datetime">
                            <div id="feedback_datetimepicker" class="invalid-feedback">
                                Please enter your expect date and time.
                            </div>
                        </div>

                        <div class="mb-3">
                            <label for="address">Address</label>
                            <input type="text" class="form-control" id="address" name="address" placeholder="Jl. Diponegoro" value="<?= user_profile()->full_address; ?>" readonly required>
                            <div class="invalid-feedback">
                                Please enter your shipping address.
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-5 mb-3">
                                <label for="inputState">Provinsi</label>
                                <select class="custom-select d-block w-100" id="inputState" disabled required>
                                    <option value="<?= user_profile()->region; ?>"><?= user_profile()->region; ?></option>
                                </select>
                                <div class="invalid-feedback">
                                    Please select a valid province.
                                </div>
                                <input type="hidden" name="region" value="<?= user_profile()->region; ?>" />
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="inputCity">Kota / Kabupaten</label>
                                <select class="custom-select d-block w-100" id="inputCity" disabled required>
                                    <option value="<?= user_profile()->city; ?>"><?= user_profile()->city; ?></option>
                                </select>
                                <div class="invalid-feedback">
                                    Please provide a valid city.
                                </div>
                                <input type="hidden" name="city" value="<?= user_profile()->city; ?>" />
                            </div>
                        </div>

                        <hr class="mb-4">

                        <h4 class="mb-3">Payment</h4>
                        <span>Once you confirm payment, you will be unable to change payment method.</span>

                        <article id="payment-channels" class="card border-0"></article> <!-- card.// -->

                        <hr class="mb-4">
                        <?= form_submit('submit', 'Continue to checkout', ['class' => 'btn btn-primary btn-lg btn-block']) ?>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('pageScripts') ?>
<?= script_tag(base_url('node_modules/jquery-datetimepicker/build/jquery.datetimepicker.full.min.js')); ?>
<script type="text/javascript">
    $(document).ready(function() {
        var siteUrl = '<?=site_url();?>';
        var minDate = new Date(),
            minDate = minDate.setDate(minDate.getDate() + 1);

        var dtpicker_onselectdate = function( ct ){
            // picker date
            var _pick_date_y = ct.getFullYear();
            var _pick_date_m = ct.getMonth() + 1;
            var _pick_date_m = ((''+_pick_date_m).length<2 ? '0' : '') + _pick_date_m;
            var _pick_date_d = ((''+ct.getDate()).length<2 ? '0' : '') + ct.getDate();
            var pick_date = _pick_date_y + '-' + _pick_date_m + '-' + _pick_date_d;
            // current date
            var d = new Date();
            var month = d.getMonth()+1;
            var day = d.getDate();
            var current_date = d.getFullYear() + '-' +
                ((''+month).length<2 ? '0' : '') + month + '-' +
                ((''+day).length<2 ? '0' : '') + day;

            if( pick_date == current_date ) {
                this.setOptions({
                    minTime: 0
                });
            }
            else {
                this.setOptions({
                    minTime: '8:00'
                });
            }
        };

        $.datetimepicker.setLocale('id');
        $('#datetimepicker').datetimepicker({
            format:'Y-m-d H:i',
            inline: true,
            minDate: minDate,
            defaultDate: minDate,
            minTime: '08:00',
            maxTime: '21:00',
            onSelectDate: dtpicker_onselectdate,
            allowTimes: []
        });

        co_calc();

        $("#fm_checkout").submit(function(e) {
            if ($("#datetimepicker").val() === "")
            {
                $("#feedback_datetimepicker").show();
                e.preventDefault();
            }
            else if($("input[name='payment_channel']").is(":checked") === false)
            {
                alert("Please select a payment method");
                e.preventDefault();
            }
        });

        $.ajax ({
            url: siteUrl + 'api/payment/channels',
            type: 'get',
            global: false
        })
        .done(function(response) {
            $('#payment-channels').html(response.data);
        });
    });

    function co_calc()
    {
        var sum = 0;
        $('.amount-price').each(function(){
            sum += parseFloat($(this).attr('data-amount'));
        });
        $('#fm_amount').val(sum);
        $('#amount-price-total').attr('data-amount', sum);
        $('#amount-price-total').text(numberWithCommas(sum));
    }
</script>
<?= $this->endSection() ?>