<div class="card-columns">
	<?php foreach ($my_drafted_services as $row): ?>
		<div class="card mb-3 mr-5" style="max-width: 540px;">
			<div class="row no-gutters">
				<div class="col-md-4">
					<img src="<?= $row->images[0]; ?>" class="card-img" loading="lazy">
				</div>
				<div class="col-md-8">
					<div class="card-body">
						<h5 class="card-title">
							<?= $row->name; ?>
							<a class="float-right service-modal" href="#" data-id="<?= $row->id; ?>"><i class="far fa-edit fa-xs"></i></a>
						</h5>
						<p class="card-text"><?= character_limiter(strip_tags($row->card_content), 80); ?></p>
						<p class="card-text"><small class="text-muted">Last updated <?= $row->updated; ?></small></p>
					</div>
				</div>
			</div>
		</div>
	<?php endforeach; ?>
</div>

<script type="text/javascript">
    toggle_my_services();
</script>