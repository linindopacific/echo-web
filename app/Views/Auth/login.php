<?php
/*
 * File: login.php
 * Project: echo
 * File Created: Wednesday, 19th August 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Tuesday, 22nd September 2020
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */
?>
<?= $this->extend($viewLayout) ?>

<?= $this->section('pageStyles') ?>
<?= link_tag(base_url('assets/css/ww_style.css')) ?>
<?= link_tag(base_url('assets/css/responsive.css')) ?>
<?= $this->endSection() ?>

<?= $this->section('main') ?>
<div class="sign-in-page">
	<div class="signin-popup">
		<div class="signin-pop">
			<div class="row">
				<div class="col-lg-6">
					<div class="cmp-info">
						<div class="cm-logo">
							<?= img(base_url('assets/images/sample-logo.png'), true) ?>
							<p>Laboris ipsum quis pariatur eiusmod. Et dolore amet eu ullamco occaecat id excepteur ut nulla enim commodo laboris minim. Voluptate commodo est amet labore cupidatat dolor esse. Adipisicing sunt laboris enim irure. </p>
						</div><!--cm-logo end-->
						<?= img(base_url('assets/images/cm-main-img.png'), true) ?>
					</div><!--cmp-info end-->
				</div>
				<div class="col-lg-6">
					<div class="login-sec">
						<ul class="sign-control">
							<li class="current"><a href="<?= base_url('login') ?>" title=""><?= lang('Auth.signIn') ?></a></li>
							<?php if ($allowRegistration) : ?>
							<li><a href="<?= base_url('register') ?>" title=""><?= lang('Auth.register') ?></a></li>
							<?php endif; ?>
						</ul>
						<div class="sign_in_sec current" id="tab-1">
							<h3><?=lang('Auth.signIn')?></h3>
							<?= view('App\Auth\_message_block') ?>
							<form action="<?= base_url('login') ?>" method="post">
								<?= csrf_field() ?>
								<div class="row">
									<div class="col-lg-12 no-pdd">
										<div class="sn-field">
											<?php if ($validFields === ['email']): ?>
											<input id="email" type="email" name="login" class="<?= (session('errors.login')) ? 'is-invalid' : '' ?>" placeholder="<?= lang('Auth.email') ?>">
											<?php else: ?>
											<input id="username" type="text" name="login" class="<?= (session('errors.login')) ? 'is-invalid' : '' ?>" placeholder="<?= lang('Auth.emailOrUsername') ?>">
											<?php endif; ?>
											<i class="la la-envelope"></i>
										</div><!--sn-field end-->
									</div>
									<div class="col-lg-12 no-pdd">
										<div class="sn-field">
											<input id="password" type="password" name="password" class="<?= (session('errors.password')) ? 'is-invalid' : '' ?>" placeholder="<?= lang('Auth.password') ?>">
											<i class="la la-lock"></i>
										</div>
									</div>
									<div class="col-lg-12 no-pdd">
										<div class="checky-sec">
											<?php if ($allowRemembering): ?>
											<div class="fgt-sec">
												<input type="checkbox" name="remember" id="c1" <?= (old('remember')) ? 'checked' : '' ?>>
												<label for="c1">
													<span></span>
												</label>
												<small><?= lang('Auth.rememberMe') ?></small>
											</div><!--fgt-sec end-->
											<?php endif; ?>
											<?php if ($activeResetter): ?>
											<a href="<?= base_url('forgot') ?>" title=""><?= lang('Auth.forgotYourPassword') ?></a>
											<?php endif; ?>
										</div>
									</div>
									<div class="col-lg-12 no-pdd">
										<button type="submit" value="submit" id="quickstart-sign-in"><?= lang('Auth.loginAction') ?></button>
									</div>
								</div>
							</form>
							<div class="login-resources">
								<h4><?= lang('Login.viaSso') ?></h4>
								<ul>
									<li><a href="#" title="" class="gl sso" id="google.com"><i class="fa fa-google"></i><?= lang('Login.viaSsoGoogle') ?></a></li>
									<li><a href="#" title="" class="mc sso" id="microsoft.com"><i class="fa fa-windows"></i><?= lang('Login.viaSsoMicrosoft') ?></a></li>
									<li><a href="#" title="" class="fb sso" id="facebook.com"><i class="fa fa-facebook"></i><?= lang('Login.viaSsoFacebook') ?></a></li>
									<li><a href="#" title="" class="tw sso" id="twitter.com"><i class="fa fa-twitter"></i><?= lang('Login.viaSsoTwitter') ?></a></li>
								</ul>
							</div><!--login-resources end-->
						</div><!--sign_in_sec end-->
					</div><!--login-sec end-->
				</div>
			</div>
		</div><!--signin-pop end-->
	</div><!--signin-popup end-->
	<div class="footy-sec">
		<div class="container">
			<ul>
				<li><?= anchor(current_url(), lang('General.helpCenter'), ['title' => '']) ?></li>
				<li><?= anchor(current_url(), lang('General.about'), ['title' => '']) ?></li>
				<li><?= anchor(current_url(), lang('General.privacyPolicy'), ['title' => '']) ?></li>
				<li><?= anchor(current_url(), lang('General.communityGuidelines'), ['title' => '']) ?></li>
				<li><?= anchor(current_url(), lang('General.cookiesPolicy'), ['title' => '']) ?></li>
				<li><?= anchor(current_url(), lang('General.career'), ['title' => '']) ?></li>
				<li><?= anchor(current_url(), lang('General.language'), ['title' => '']) ?></li>
				<li><?= anchor(current_url(), lang('General.copyrightPolicy'), ['title' => '']) ?></li>
			</ul>
			<p><?= img(base_url('assets/images/copy-icon.png'), true); ?><?= lang('General.copyright') ?> 2020</p>
		</div>
	</div><!--footy-sec end-->
</div><!--sign-in-page end-->
<?= $this->endSection() ?>

<?= $this->section('pageScripts') ?>
<?= script_tag(base_url('assets/js/ww_script.js')) ?>
<?= view('App\Views\Auth\script') ?>
<script type="text/javascript">
	$(document).ready(function(){
		toggleSignIn();
		toggleSso();
	});
</script>
<?= $this->endSection() ?>
