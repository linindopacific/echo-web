<?php
/*
 * File: reset.php
 * Project: echo
 * File Created: Wednesday, 19th August 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Wednesday, 2nd September 2020
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com>)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */
?>
<?= $this->extend($viewLayout) ?>

<?= $this->section('pageStyles') ?>
<?= link_tag(base_url('assets/css/ww_style.css')) ?>
<?= link_tag(base_url('assets/css/responsive.css')) ?>
<?= $this->endSection() ?>

<?= $this->section('main') ?>
<div class="sign-in-page">
    <div class="signin-popup">
        <div class="signin-pop">
            <div class="row">
                <div class="col-lg-6">
                    <div class="cmp-info">
                        <div class="cm-logo">
                            <?= img(base_url('assets/images/sample-logo.png'), true) ?>
                            <p>Laboris ipsum quis pariatur eiusmod. Et dolore amet eu ullamco occaecat id excepteur ut nulla enim commodo laboris minim. Voluptate commodo est amet labore cupidatat dolor esse. Adipisicing sunt laboris enim irure. </p>
                        </div><!--cm-logo end-->
                        <?= img(base_url('assets/images/cm-main-img.png'), true) ?>
                    </div><!--cmp-info end-->
                </div>
                <div class="col-lg-6">
                    <div class="login-sec">
                        <ul class="sign-control">
                            <li><a href="<?= base_url('login') ?>" title=""><?= lang('Auth.signIn') ?></a></li>
                            <?php if ($allowRegistration) : ?>
                            <li><a href="<?= base_url('register') ?>" title=""><?= lang('Auth.register') ?></a></li>
                            <?php endif; ?>
                        </ul>
                        <div class="sign_in_sec current" id="tab-1">
                            <h3><?= lang('Auth.resetYourPassword') ?></h3>
                            <?= view('App\Auth\_message_block') ?>
                            <p><?= lang('Auth.enterCodeEmailPassword') ?></p><br>
                            <form action="<?= base_url('reset-password') ?>" method="post">
                                <?= csrf_field() ?>
                                <div class="row">
                                    <div class="col-lg-12 no-pdd">
                                        <div class="sn-field">
                                            <input type="text" name="token" class="<?= (session('errors.token')) ? 'is-invalid' : '' ?>" placeholder="<?= lang('Auth.token') ?>" value="<?= old('token', $token ?? '') ?>">
                                            <i class="la la-expeditedssl"></i>
                                        </div><!--sn-field end-->
                                    </div>
                                    <div class="col-lg-12 no-pdd">
                                        <div class="sn-field">
                                            <input type="email" name="email" class="<?= (session('errors.email')) ? 'is-invalid' : '' ?>" placeholder="<?= lang('Auth.emailAddress') ?>" value="<?= old('email') ?>">
                                            <i class="la la-envelope"></i>
                                        </div><!--sn-field end-->
                                    </div>
                                    <div class="col-lg-12 no-pdd">
                                        <div class="sn-field">
                                            <input type="password" name="password" class="<?= (session('errors.password')) ? 'is-invalid' : '' ?>" placeholder="<?= lang('Auth.newPassword') ?>">
                                            <i class="la la-lock"></i>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 no-pdd">
                                        <div class="sn-field">
                                            <input type="password" name="pass_confirm" class="<?= (session('errors.pass_confirm')) ? 'is-invalid' : '' ?>" placeholder="<?= lang('Auth.newPasswordRepeat') ?>">
                                            <i class="la la-lock"></i>
                                        </div>
                                    </div>
                                    <div class="col-lg-12 no-pdd">
                                        <button type="submit" value="submit"><?= lang('Auth.resetPassword') ?></button>
                                    </div>
                                </div>
                            </form>
                        </div><!--sign_in_sec end-->
                    </div><!--login-sec end-->
                </div>
            </div>
        </div><!--signin-pop end-->
    </div><!--signin-popup end-->
    <div class="footy-sec">
        <div class="container">
            <ul>
                <li><?= anchor(current_url(), lang('General.helpCenter'), ['title' => '']) ?></li>
                <li><?= anchor(current_url(), lang('General.about'), ['title' => '']) ?></li>
                <li><?= anchor(current_url(), lang('General.privacyPolicy'), ['title' => '']) ?></li>
                <li><?= anchor(current_url(), lang('General.communityGuidelines'), ['title' => '']) ?></li>
                <li><?= anchor(current_url(), lang('General.cookiesPolicy'), ['title' => '']) ?></li>
                <li><?= anchor(current_url(), lang('General.career'), ['title' => '']) ?></li>
                <li><?= anchor(current_url(), lang('General.language'), ['title' => '']) ?></li>
                <li><?= anchor(current_url(), lang('General.copyrightPolicy'), ['title' => '']) ?></li>
            </ul>
            <p><?= img(base_url('assets/images/copy-icon.png'), true); ?><?= lang('General.copyright') ?> 2020</p>
        </div>
    </div><!--footy-sec end-->
</div><!--sign-in-page end-->
<?= $this->endSection() ?>

<?= $this->section('pageScripts') ?>
<?= script_tag(base_url('assets/js/ww_script.js')) ?>
<?= view('App\Views\Auth\script') ?>
<?= $this->endSection() ?>
