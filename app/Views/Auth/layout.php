<?php
/*
 * File: layout.php
 * Project: echo
 * File Created: Wednesday, 19th August 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Tuesday, 16th November 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */
?>
<?= doctype(); ?>
<html lang="id">
<head>
	<title><?= isset($title) ? $title : getenv('APP_NAME'); ?></title>
	<meta charset="UTF-8">
	<meta name="author" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<?= link_tag(base_url('favicon.ico'), 'shortcut icon', 'image/ico'); ?>
	<!-- STYLES -->
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootswatch@4.6.1/dist/cerulean/bootstrap.min.css" integrity="sha256-RW/ojwB9sS6DzIfFEaDiahxVPN/MupUlrbKrKVkgA9M=" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/animate.css@4.1.1/animate.min.css" integrity="sha256-X7rrn44l1+AUO65h1LGALBbOc5C5bOstSYsNlv9MhT8=" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/line-awesome@1.3.0/dist/line-awesome/css/line-awesome.min.css" integrity="sha256-zmGhjPCE8VADeYNABEZD8ymsX5AEWsstnneDaL15mFQ=" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" integrity="sha256-3h45mwconzsKjTUULjY+EoEkoRhXcOIU4l5YAw2tSOU=" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css" integrity="sha256-etrwgFLGpqD4oNAFW08ZH9Bzif5ByXK2lXNHKy7LQGo=" crossorigin="anonymous">
	<?= $this->renderSection('pageStyles') ?>
</head>

<body class="<?= isset($bodyClass) ? $bodyClass : ''; ?>">
	<div class="wrapper">
		<?= $this->renderSection('main') ?>
	</div> <!--theme-layout end-->
	<!-- SCRIPTS -->
	<script src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha256-fgLAgv7fyCGopR/gBNq2iW3ZKIdqIcyshnUULC4vex8=" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js" integrity="sha256-DHF4zGyjT7GOMPBwpeehwoey18z8uiz98G4PRu2lV0A=" crossorigin="anonymous"></script>
	<?= script_tag("https://cdn.jsdelivr.net/npm/ajaxq@0.0.7/ajaxq.min.js"); ?>
	<?= script_tag('https://www.gstatic.com/firebasejs/8.6.8/firebase-app.js'); ?>
	<?= script_tag('https://www.gstatic.com/firebasejs/8.6.8/firebase-auth.js'); ?>
	<?= script_tag('https://www.gstatic.com/firebasejs/8.6.8/firebase-analytics.js'); ?>
	<!-- Firebase project configuration -->
	<script type="text/javascript">
		const firebaseConfig = {
			apiKey: "<?= getenv('firebase.apiKey'); ?>",
			authDomain: "<?= getenv('firebase.projectId'); ?>.firebaseapp.com",
			databaseURL: "https://<?= getenv('firebase.projectId'); ?>.firebaseio.com",
			projectId: "<?= getenv('firebase.projectId'); ?>",
			storageBucket: "<?= getenv('firebase.projectId'); ?>.appspot.com",
			messagingSenderId: "<?= getenv('firebase.senderId'); ?>",
			appId: "<?= getenv('firebase.appId'); ?>",
			measurementId: "G-<?= getenv('firebase.measurementId') ?>"
		};
		// Initialize Firebase
		firebase.initializeApp(firebaseConfig);
	</script>
	<?= script_tag('https://kit.fontawesome.com/fde69c8c4d.js'); ?>
	<?= $this->renderSection('pageScripts') ?>
</body>
</html>
