<?php

/*
 * File: script.php
 * Project: echo
 * File Created: Friday, 21st August 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Thursday, 22nd April 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

?>
<script type="text/javascript">
	const auth = firebase.auth();

	function getProviderForProviderId(providerId)
	{
		switch (providerId) {
			case 'google.com':
				var provider = new firebase.auth.GoogleAuthProvider();
				provider.addScope('https://www.googleapis.com/auth/plus.login');
				break;
			case 'microsoft.com':
				var provider = new firebase.auth.OAuthProvider('microsoft.com');
				provider.addScope('User.Read');
				break;
			case 'facebook.com':
				var provider = new firebase.auth.FacebookAuthProvider();
				provider.addScope('email');
				break;
			case 'twitter.com':
				var provider = new firebase.auth.TwitterAuthProvider();
				break;
		}

		return provider;
	}

	/**
	 * Handles the password reset button press.
	 */
	function sendPasswordReset() {
		$('#quickstart-password-reset').on('click', function (event) {
			event.preventDefault();
			var email = $('#email');
			if (email.val().length < 4) {
                alert('Please enter an email address.');
                return;
            }
			// [START sendpasswordemail]
			auth.sendPasswordResetEmail(email.val()).then(function() {
				// Password Reset Email Sent!
				// [START_EXCLUDE]
				alert('Password Reset Email Sent!');
				// [END_EXCLUDE]
			}).catch(function(error) {
				// Handle Errors here.
				var errorCode = error.code;
				var errorMessage = error.message;
				// [START_EXCLUDE]
				if (errorCode == 'auth/invalid-email') {
					alert(errorMessage);
				} else if (errorCode == 'auth/user-not-found') {
					alert(errorMessage);
				}
				console.log(error);
				// [END_EXCLUDE]
			});
			// [END sendpasswordemail];
		});
    }

    function toggleSso() {
	    $('.sso').on('click', function (event) {
	    	event.preventDefault();
	        if (auth.currentUser) {
	            // [START signout]
	            auth.signOut();
	            // [END signout]
	        } else {
	        	var provider = getProviderForProviderId(event.target.id);
				// [START signin]
				auth.signInWithRedirect(provider);
				// [END signin]
	        }

	        $('#quickstart-sign-up').prop('disabled', true);
	    });

	    // Result from Redirect auth flow.
		// [START getidptoken]
		auth.getRedirectResult()
		.then(function(result) {
			if (result.credential) {
				var token = result.credential.accessToken;
				var secret = result.credential.secret;
				// [START_EXCLUDE]
				//document.getElementById('quickstart-oauthtoken').textContent = token;
				//document.getElementById('quickstart-oauthsecret').textContent = secret;
			} else {
				//document.getElementById('quickstart-oauthtoken').textContent = 'null';
				//document.getElementById('quickstart-oauthsecret').textContent = 'null';
				// [END_EXCLUDE]
			}
			// The signed-in user info.
			var user = result.user;
			var pendingCred = sessionStorage.getItem("prendingCred");
			if (pendingCred != null) {
				// As we have access to the pending credential, we can directly call the link method.
				result.user.linkAndRetrieveDataWithCredential(pendingCred).then(function(usercred) {
					// Successfully linked to the existing Firebase user.
					console.log(usercred);
					successAuthCallback(result);
				});
			}
			if (user != null) {
				successAuthCallback(result);
			}
		})
		.catch(function(error) {
			// Handle Errors here.
			var errorCode = error.code;
			var errorMessage = error.message;
			// The email of the user's account used.
			var email = error.email;
			// The firebase.auth.AuthCredential type that was used.
			var credential = error.credential;
			console.log(error);
			// [START_EXCLUDE]
			if (errorCode === 'auth/account-exists-with-different-credential') {
				alert('You have already signed up with a different auth provider for that email.');
				// If you are using multiple auth providers on your app you should handle linking
				// the user's accounts here.
				auth.fetchSignInMethodsForEmail(email).then(function(methods) {
					methods.forEach(function(method) {
						if (method != 'password') {
							var provider = getProviderForProviderId(method);
							// At this point, you should let the user know that they already has an account
							// but with a different provider, and let them validate the fact they want to
							// sign in with this provider.
							// Sign in to provider. Note: browsers usually block popup triggered asynchronously,
							// so in real scenario you should ask the user to click on a "continue" button
							// that will trigger the signInWithPopup.
							sessionStorage.setItem("pendingCred", credential);
							auth.signInWithRedirect(provider);
							// Remember that the user may have signed in with an account that has a different email
							// address than the first one. This can happen as Firebase doesn't control the provider's
							// sign in flow and the user is free to login using whichever account they own.
						}
					});
				});
			} else if (errorCode === 'auth/operation-not-allowed') {
				alert('The identity provider configuration is not found.');
			} else {
				console.error(error);
				alert(errorMessage);
			}
			// [END_EXCLUDE]
		});
		// [END getidptoken]
	}

	/**
	 * Handles the sign in button press.
	 */
	function toggleSignIn() {
	    $('#quickstart-sign-in').on('click', function (event) {
	    	event.preventDefault();
	        if (auth.currentUser) {
	            // [START signout]
	            auth.signOut();
	            // [END signout]
	        } else {
	            var email = $('#email'),
	            	password = $('#password');

	            if (email.val().length < 4) {
	                alert('Please enter an email address.');
	                return;
	            }
	            if (password.val().length < 4) {
	                alert('Please enter a password.');
	                return;
	            }
	            // Sign in with email and pass.
	            // [START authwithemail]
	            auth.signInWithEmailAndPassword(email.val(), password.val()).then(function(result) {
	            	successAuthCallback(result);
	            }).catch(function(error) {
	                // Handle Errors here.
	                var errorCode = error.code;
	                var errorMessage = error.message;
	                // [START_EXCLUDE]
	                console.log(error);
	                alert(errorMessage);
	                $('#quickstart-sign-up').prop('disabled', false);
	                // [END_EXCLUDE]
	            });
	        }

	        $('#quickstart-sign-up').prop('disabled', true);
	    });
	}

	/**
	 * Handles the sign up button press.
	 */
	function handleSignUp() {
	    $('#quickstart-sign-up').on('click', function (event) {
	    	event.preventDefault();
	        var email = $('#email'),
	        	password = $('#password'),
	        	pass_confirm = $('#pass_confirm'),
	        	tos = $('#c2');

	        if (email.val().length < 4) {
	            alert('Please enter an email address.');
	            return;
	        }
	        if (password.val().length < 4) {
	            alert('Please enter a password.');
	            return;
	        }
	        if (pass_confirm.val().length < 4) {
	            alert('Please confirm password to continue.');
	            return;
	        }
	        if (password.val() != pass_confirm.val()) {
	            alert('Your password and confirmation password do not match.');
	            return;
	        }
	        if (tos.prop('checked') == false){
	        	alert('You must accept to continue.');
			    return;
			}

	        // Create user with email and pass.
	        // [START createwithemail]
	        auth.createUserWithEmailAndPassword(email.val(), password.val()).then(function(result) {
	            successAuthCallback(result);
	        }).catch(function(error) {
	            // Handle Errors here.
	            var errorCode = error.code;
	            var errorMessage = error.message;
	            console.log(error);
	            // [START_EXCLUDE]
	            if (errorCode == 'auth/weak-password') {
	                alert('The password is too weak.');
	            } else {
	                alert(errorMessage);
	            }
	            // [END_EXCLUDE]
	        });
	        // [END createwithemail]
	    });
	}

	function successAuthCallback(result) {
	    $.ajaxq ('MyQueue', {
		    url: '<?= site_url('callback') ?>',
		    type: 'post',
		    contentType: 'application/json',
		    data: JSON.stringify( result ),
		    processData: false,
		    success: function(data){
		    	if (data.isActive == false) {
		    		if (data.requireActivation == true) {
		    			var activation = '<?= site_url('activate-account') ?>';
			    		var activationUrl = activation + '?token=' + data.activationHash;
			    		auth.currentUser.sendEmailVerification({url: activationUrl}).then(function() {
					      	// Verification email sent.
					    	alert("Please activate by verify your email address.");
					      	window.location.href = "<?= site_url() ?>";
					    }).catch(function(error) {
						  	// Error occurred. Inspect error.code.
						  	console.log(error);
					    });
		    		} else {
		    			alert("Please activate by verify your email address.");
		    		}
		    	} else {
		    		window.location.href = "<?= site_url() ?>";
		    	}
		    },
		    error: function(jqXhr, textStatus, errorThrown){
				console.log('something wrong...');
		        console.log(textStatus);
		    }
		});
	}
</script>