<?php
/*
 * File: _footer.php
 * Project: echo
 * File Created: Wednesday, 19th August 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Wednesday, 2nd September 2020
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com>)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */
?>
<footer>
	<div class="footy-sec mn no-margin">
		<div class="container">
			<ul>
				<li><?= anchor(current_url(), lang('General.helpCenter'), ['title' => '']) ?></li>
				<li><?= anchor(current_url(), lang('General.about'), ['title' => '']) ?></li>
				<li><?= anchor(current_url(), lang('General.privacyPolicy'), ['title' => '']) ?></li>
				<li><?= anchor(current_url(), lang('General.communityGuidelines'), ['title' => '']) ?></li>
				<li><?= anchor(current_url(), lang('General.cookiesPolicy'), ['title' => '']) ?></li>
				<li><?= anchor(current_url(), lang('General.career'), ['title' => '']) ?></li>
				<li><?= anchor(current_url(), lang('General.language'), ['title' => '']) ?></li>
				<li><?= anchor(current_url(), lang('General.copyrightPolicy'), ['title' => '']) ?></li>
			</ul>
			<p><?= img(base_url('assets/images/copy-icon2.png'), true) ?>Copyright <?= date('Y') ?></p>
			<!--<?= img(['src' => base_url('assets/images/logo2.png'), 'class' => 'fl-rgt'], true) ?>-->
		</div>
	</div>
</footer><!--footer end-->
