<?php
/*
 * File: _navbar.php
 * Project: echo
 * File Created: Wednesday, 19th August 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Wednesday, 2nd September 2020
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com>)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */
?>
<header>
	<div class="container">
		<div class="header-data">
			<!--div class="logo">
				<?= anchor(base_url(), img(base_url('assets/images/logo.png'), true), ['title' => '']) ?>
			</div--><!--logo end-->
			<div class="search-bar">
				<form>
					<input type="text" name="search" placeholder="Search...">
					<button type="submit"><i class="la la-search"></i></button>
				</form>
			</div><!--search-bar end-->
			<nav>
				<ul>
					<li>
						<a href="<?= base_url() ?>" title="">
							<span><?= img(base_url('assets/images/icon1.png'), true) ?></span>
							Home
						</a>
					</li>
					<li>
						<a href="projects.html" title="">
							<span><?= img(base_url('assets/images/icon3.png'), true) ?></span>
							Projects
						</a>
					</li>
					<li>
						<a href="#" title="">
							<span><?= img(base_url('assets/images/icon4.png'), true) ?></span>
							Profiles
						</a>
						<ul>
							<li><?= anchor('home/user', 'User Profile', 'title=""') ?></li>
							<li><?= anchor('profile', 'My Profile', 'title=""') ?></li>
						</ul>
					</li>
					<!-- <li>
						<a href="jobs.html" title="">
							<span><?= img(base_url('assets/images/icon5.png'), true) ?></span>
							Jobs
						</a>
					</li> -->
					<li>
						<a href="#" title="" class="not-box-openm">
							<span><?= img(base_url('assets/images/icon6.png'), true) ?></span>
							Messages
						</a>
						<div class="notification-box msg" id="message">
							<div class="nt-title">
								<h4>Setting</h4>
								<a href="#" title="">Clear all</a>
							</div>
							<div class="nott-list">
								<div class="notfication-details">
									<div class="noty-user-img">
										<?= img(base_url('assets/images/resources/ny-img1.png'), true) ?>
									</div>
									<div class="notification-info">
										<h3><a href="messages.html" title="">Jassica William</a> </h3>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do.</p>
										<span>2 min ago</span>
									</div><!--notification-info -->
								</div>
								<div class="notfication-details">
									<div class="noty-user-img">
										<?= img(base_url('assets/images/resources/ny-img2.png'), true) ?>
									</div>
									<div class="notification-info">
										<h3><a href="messages.html" title="">Jassica William</a></h3>
										<p>Lorem ipsum dolor sit amet.</p>
										<span>2 min ago</span>
									</div><!--notification-info -->
								</div>
								<div class="notfication-details">
									<div class="noty-user-img">
										<?= img(base_url('assets/images/resources/ny-img3.png'), true) ?>
									</div>
									<div class="notification-info">
										<h3><a href="messages.html" title="">Jassica William</a></h3>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempo incididunt ut labore et dolore magna aliqua.</p>
										<span>2 min ago</span>
									</div><!--notification-info -->
								</div>
								<div class="view-all-nots">
									<?= anchor('home/messages', 'View All Messsages', 'title=""') ?>
								</div>
							</div><!--nott-list end-->
						</div><!--notification-box end-->
					</li>
					<li>
						<a href="#" title="" class="not-box-open">
							<span><?= img(base_url('assets/images/icon7.png'), true) ?></span>
							Notification
						</a>
						<div class="notification-box noti" id="notification">
							<div class="nt-title">
								<h4>Setting</h4>
								<a href="#" title="">Clear all</a>
							</div>
							<div class="nott-list">
								<div class="notfication-details">
									<div class="noty-user-img">
										<?= img(base_url('assets/images/resources/ny-img1.png'), true) ?>
									</div>
									<div class="notification-info">
										<h3><a href="#" title="">Jassica William</a> Comment on your project.</h3>
										<span>2 min ago</span>
									</div><!--notification-info -->
								</div>
								<div class="notfication-details">
									<div class="noty-user-img">
										<?= img(base_url('assets/images/resources/ny-img2.png'), true) ?>
									</div>
									<div class="notification-info">
										<h3><a href="#" title="">Jassica William</a> Comment on your project.</h3>
										<span>2 min ago</span>
									</div><!--notification-info -->
								</div>
								<div class="notfication-details">
									<div class="noty-user-img">
										<?= img(base_url('assets/images/resources/ny-img3.png'), true) ?>
									</div>
									<div class="notification-info">
										<h3><a href="#" title="">Jassica William</a> Comment on your project.</h3>
										<span>2 min ago</span>
									</div><!--notification-info -->
								</div>
								<div class="notfication-details">
									<div class="noty-user-img">
										<?= img(base_url('assets/images/resources/ny-img2.png'), true) ?>
									</div>
									<div class="notification-info">
										<h3><a href="#" title="">Jassica William</a> Comment on your project.</h3>
										<span>2 min ago</span>
									</div><!--notification-info -->
								</div>
								<div class="view-all-nots">
									<a href="#" title="">View All Notification</a>
								</div>
							</div><!--nott-list end-->
						</div><!--notification-box end-->
					</li>
				</ul>
			</nav><!--nav end-->
			<div class="menu-btn">
				<a href="#" title=""><i class="fa fa-bars"></i></a>
			</div><!--menu-btn end-->
			<div class="user-account">
				<div class="user-info">
					<?= img(base_url('assets/images/resources/'.$profile['avatar_image']), true) ?>
					<a href="#" title=""><?=$profile['first_name'];?></a>
					<i class="la la-sort-down"></i>
				</div>
				<div class="user-account-settingss">
					<h3>Online Status</h3>
					<ul class="on-off-status">
						<li>
							<div class="fgt-sec">
								<input type="radio" name="cc" id="c5" checked="">
								<label for="c5">
									<span></span>
								</label>
								<small>Online</small>
							</div>
						</li>
						<li>
							<div class="fgt-sec">
								<input type="radio" name="cc" id="c6">
								<label for="c6">
									<span></span>
								</label>
								<small>Offline</small>
							</div>
						</li>
					</ul>
					<h3>Setting</h3>
					<ul class="us-links">
						<li><?= anchor('profile/settings', 'Account Setting', ['title' => '']); ?></li>
						<li><a href="#" title="">Privacy</a></li>
						<li><a href="#" title="">Faqs</a></li>
						<li><a href="#" title="">Terms & Conditions</a></li>
					</ul>
					<h3 class="tc"><a href="<?= base_url('logout') ?>" title="">Logout</a></h3>
				</div><!--user-account-settingss end-->
			</div>
		</div><!--header-data end-->
	</div>
</header><!--header end-->
