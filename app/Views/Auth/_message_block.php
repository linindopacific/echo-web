<?php
/*
 * File: _message_block.php
 * Project: echo
 * File Created: Wednesday, 19th August 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Wednesday, 2nd September 2020
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com>)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */
?>
<?php if (session()->has('message')) : ?>
	<div class="alert alert-success">
		<?= session('message') ?>
	</div>
<?php endif ?>

<?php if (session()->has('error')) : ?>
	<div class="alert alert-danger">
		<?= session('error') ?>
	</div>
<?php endif ?>

<?php if (session()->has('errors')) : ?>
	<ul class="alert alert-danger">
	<?php foreach (session('errors') as $error) : ?>
		<li><?= $error ?></li>
	<?php endforeach ?>
	</ul>
<?php endif ?>
