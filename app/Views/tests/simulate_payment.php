<!doctype html>
	<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<title>Simulate Payment</title>
	</head>
	<body>
		<div class="container">
			<h1>Simulate Payment</h1>

			<?php if( session()->getFlashdata('success') ){ ?>
				<div class="alert alert-success alert-dismissible fade show" role="alert">
					<strong>Success!</strong> <?php echo session()->getFlashdata('success'); ?>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			<?php }else if( session()->getFlashdata('error') ){  ?>
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
					<strong>Error!</strong> <?php echo session()->getFlashdata('error'); ?>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			<?php }?>

			<?= form_open('payment/simulate'); ?>
				<?= form_input(['type' => 'hidden', 'name' => 'category', 'id' => 'inputCategory']); ?>
				<div class="form-group row">
					<label for="inputChannel" class="col-sm-2 col-form-label">Payment Channel</label>
					<div class="col-sm-10">
						<select class="custom-select my-1 mr-sm-2" id="inputChannel" name="channel" required>
							<option value="" data-id="" selected>Choose...</option>
							<?php for ($i=0; $i < count($data['VIRTUAL_ACCOUNT']); $i++): ?>
								<option value="<?= $data['VIRTUAL_ACCOUNT'][$i]['code'] ?>" data-id="VIRTUAL_ACCOUNT"><?= $data['VIRTUAL_ACCOUNT'][$i]['code'] ?></option>
							<?php endfor; ?>
							<?php for ($i=0; $i < count($data['RETAIL_OUTLET']); $i++): ?>
								<option value="<?= $data['RETAIL_OUTLET'][$i]['code'] ?>" data-id="RETAIL_OUTLET"><?= $data['RETAIL_OUTLET'][$i]['code'] ?></option>
							<?php endfor; ?>
						</select>
					</div>
				</div>
				<div class="form-group row">
					<label for="inputNumber" class="col-sm-2 col-form-label">Account Number</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="inputNumber" name="number" required placeholder="Number">
					</div>
				</div>
				<div class="form-group row">
					<label for="inputAmount" class="col-sm-2 col-form-label">Amount</label>
					<div class="col-sm-10">
						<input type="number" min="10000" class="form-control" id="inputAmount" name="amount" required placeholder="Amount">
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-10">
						<?= form_submit('submit', 'Pay', ['class' => 'btn btn-primary']); ?>
					</div>
				</div>
			<?= form_close(); ?>
		</div>
		<!-- Optional JavaScript -->
		<!-- jQuery first, then Popper.js, then Bootstrap JS -->
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
		<script>
			$(document).ready(function() {
				$( "#inputChannel" ).change(function() {
					var selected = $(this).find(":selected");
					$( "#inputCategory" ).val( selected.data("id") );
				});
			});
		</script>
	</body>
</html>