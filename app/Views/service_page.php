<?= $this->extend($viewLayout) ?>

<?= $this->section('pageStyles') ?>
<?= link_tag(base_url('node_modules/ekko-lightbox/dist/ekko-lightbox.css')); ?>
<?= $this->endSection() ?>

<?= $this->section('main') ?>
<div class="container">
    <div class="row pt-3 justify-content-center">
        <div id="container_row" class="row">
            <div class="col-12 order-1 mx-3">
                <div class="card shadow-sm">
                    <div class="row">
                        <aside class="col-sm-5 pr-0 border-right">
                            <article class="gallery-wrap pb-3">
                                <div class="img-big-wrap">
                                    <a href="<?= $service->images[0]; ?>" data-toggle="lightbox" data-gallery="img-gallery" data-type="image">
                                        <img src="<?= $service->images[0]; ?>" alt="" loading="lazy" class="img-fluid">
                                    </a>
                                </div> <!-- slider-product.// -->
                                <?php if (count($service->images) > 1): ?>
                                    <div class="img-small-wrap">
                                        <?php for ($i=1; $i < 5; $i++): ?>
                                            <div class="item-gallery">
                                                <a href="<?= $service->images[$i] ?>" data-toggle="lightbox" data-gallery="img-gallery" data-type="image">
                                                    <img src="<?= $service->images[$i] ?>" loading="lazy" class="img-fluid">
                                                </a>
                                            </div>
                                        <?php endfor; ?>
                                    </div> <!-- slider-nav.// -->
                                <?php endif; ?>
                                <!-- elements not showing, use data-remote -->
                                <?php if (count($service->images) > 5): ?>
                                    <?php for ($i=5; $i < count($service->images); $i++): ?>
                                        <div data-toggle="lightbox" data-gallery="img-gallery" data-type="image" data-remote="<?= $service->images[$i] ?>" loading="lazy"></div>
                                    <?php endfor; ?>
                                <?php endif; ?>
                            </article> <!-- gallery-wrap .end// -->
                        </aside>
                        <aside class="col-sm-7">
                            <article class="card-body p-3">
                                <h3 class="title mb-3">
                                    <?= $service->name; ?>
                                </h3> <!-- service name -->
                                <p class="price-detail-wrap">
                                    <span class="price h3 text-warning">
                                        Rp <?= number_format($service->price, '0', ',', '.'); ?>
                                    </span>
                                    <span>/<?= $service->price_per; ?></span>
                                </p> <!-- price-detail-wrap .// -->
                                <dl class="item-property">
                                    <dt>Description</dt>
                                    <dd><p><?= $service->card_content; ?></p></dd>
                                </dl>
                                <dl class="param param-feature">
                                    <dt>Coverage Area</dt>
                                    <dd><?= ucwords(strtolower($service->coverages)); ?></dd>
                                </dl>  <!-- item-property-hor .// -->
								<?php
									if (! $service->is_owner)
									{
										echo '<hr>';
                                        if (! $service->is_coverage)
                                        {
                                            echo form_button([
                                                'name' => 'button',
                                                'id' => 'button',
                                                'value' => 'true',
                                                'type' => 'button',
                                                'class' => 'btn btn-lg btn-outline-danger text-uppercase',
                                                'disabled' => 'disabled',
                                                'content' => 'Area not covered'
                                            ]);
                                        }
                                        else
                                        {
                                            echo anchor('#', '<i class="fas fa-calendar-day"></i> Make schedule', ['id' => 'btnOrderService', 'data-id' => $service->id, 'class' => 'btn btn-lg btn-outline-primary text-uppercase']);
                                        }
									}
								?>
                            </article> <!-- card-body.// -->
                        </aside> <!-- col.// -->
                    </div> <!-- row.// -->
                </div>
                <br>
                <div class="card">
                    <h5 class="card-header">Reviews</h5>
                    <?php foreach ($service->reviews as $row): ?>
                        <div class="card-body">
                            <h5 class="card-title"><?= $row->user->display_name ?>
                                <span class="score mr-2">
                                    <div class="score-wrap">
                                        <span class="market-stars-active" style="width:<?= ($row->rating/5)*100 ?>%">
                                            <i class="fas fa-star fa-sm" aria-hidden="true"></i>
                                            <i class="fas fa-star fa-sm" aria-hidden="true"></i>
                                            <i class="fas fa-star fa-sm" aria-hidden="true"></i>
                                            <i class="fas fa-star fa-sm" aria-hidden="true"></i>
                                            <i class="fas fa-star fa-sm" aria-hidden="true"></i>
                                        </span>
                                        <span class="market-stars-inactive">
                                            <i class="far fa-star fa-sm" aria-hidden="true"></i>
                                            <i class="far fa-star fa-sm" aria-hidden="true"></i>
                                            <i class="far fa-star fa-sm" aria-hidden="true"></i>
                                            <i class="far fa-star fa-sm" aria-hidden="true"></i>
                                            <i class="far fa-star fa-sm" aria-hidden="true"></i>
                                        </span>
                                    </div>
                                </span>
                            </h5>
                            <p class="card-text"><?= $row->content ?></p>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('pageScripts') ?>
<?= script_tag(base_url('node_modules/ekko-lightbox/dist/ekko-lightbox.min.js')); ?>
<?= $this->endSection() ?>