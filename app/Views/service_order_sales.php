<div class="row">
    <div class="col-md-3 col-sm-12">
        <div class="nav flex-column nav-pills" id="v-sales-tab" role="tablist" aria-orientation="vertical">
            <a href="<?= site_url('services/sales-orders'); ?>" class="nav-link show active" id="v-so-all-tab" data-type="0" data-toggle="pill" data-target="#v-so-all" role="tab" aria-controls="v-po-all" aria-selected="true">Semua</a>
            <a href="<?= site_url('services/sales-orders'); ?>" class="nav-link" id="v-so-unpaid-tab" data-type="1" data-toggle="pill" data-target="#v-so-unpaid" role="tab" aria-controls="v-po-unpaid" aria-selected="false">Menunggu Diproses</a>
            <a href="<?= site_url('services/sales-orders'); ?>" class="nav-link" id="v-so-process-tab" data-type="2" data-toggle="pill" data-target="#v-so-process" role="tab" aria-controls="v-po-process" aria-selected="false">Pesanan Terproses</a>
            <a href="<?= site_url('services/sales-orders'); ?>" class="nav-link" id="v-so-done-tab" data-type="3" data-toggle="pill" data-target="#v-so-done" role="tab" aria-controls="v-po-done" aria-selected="false">Pesanan Selesai</a>
        </div>
    </div>
    <div class="col-md-9 col-sm-12">
        <div class="tab-content" id="v-pills-tabContent">
            <div class="tab-pane fade show active" id="v-so-all" role="tabpanel" aria-labelledby="v-so-all-tab"></div>
            <div class="tab-pane fade" id="v-so-unpaid" role="tabpanel" aria-labelledby="v-so-unpaid-tab"></div>
            <div class="tab-pane fade" id="v-so-process" role="tabpanel" aria-labelledby="v-so-process-tab"></div>
            <div class="tab-pane fade" id="v-so-done" role="tabpanel" aria-labelledby="v-so-done-tab"></div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        // default tab
        var $this = $('#v-sales-tab a[id="v-so-all-tab"]'),
            loadurl = siteUrl + '/services/sales-orders',
            targ = $this.attr('data-target'),
            type = $this.attr('data-type'),
            data = {'type': type};

        find_services(loadurl, data, targ);

        $('#v-sales-tab a').on('shown.bs.tab', function (e) {
            e.target // newly activated tab
            e.relatedTarget // previous active tab
            var $this = $(this),
                targ = $this.attr('data-target'),
                type = $this.attr('data-type'),
                data = {'type': type};
                prevTarg = e.relatedTarget.id.slice(0,-4);

            find_services(loadurl, data, targ, prevTarg);
        });
    });
</script>