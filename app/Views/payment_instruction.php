<?= $this->extend($viewLayout) ?>

<?= $this->section('pageStyles') ?>
<?= $this->endSection() ?>

<?= $this->section('main') ?>
<div class="container">
    <div class="row pt-3 justify-content-center">
        <div class="col">
            <div id="container_row" class="row">
                <div class="col-12 order-1 mx-3">
                    <div class="card shadow-sm">
                        <div class="card-body">
                            <h5 class="card-title">Payment Instruction</h5>
                            <p class="card-text"></p>
                            <div class="card bg-light" style="background-color: #ffffd2">
                                <div class="card-body">
                                    <h2 class="text-center"><strong><?= ($data->is_paid) ? 'Pembayaran Lunas' : 'Menunggu Pembayaran'; ?></strong></h2>
                                    <p class="text-center">Mohon selesaikan pembayaran Anda sebelum tanggal <strong><?= date('d/m/Y H:i:s', $data->expired_at->getTimestamp()); ?></strong> dengan rincian sebagai berikut.</p>
                                    <hr>
                                    <div class="row">
                                        <?php if ( in_array($data->payment_category, ['VIRTUAL_ACCOUNT', 'RETAIL_OUTLET']) ) { ?>
                                        <div class="col-md-6 text-right">
                                            <img src="<?= $data->payment_channel_logo; ?>" style="margin-right:10px; /* height:60px */">
                                        </div>
                                        <div class="col-md-6">
                                            Nomor Pembayaran:<br><span style="font-size: 18px"><strong><?= $data->payment_code; ?></strong></span><br>a.n. <strong><?= $data->order->sell_to_name; ?></strong>
                                        </div>
                                        <?php } ?>
                                    </div>
                                    <hr>
                                    <div class="row justify-content-center">
                                        <p class="text-center">Jumlah yang harus dibayar: &nbsp;</p>
                                        <p class="text-center"> <span class="font-weight-bold"><?= number_to_currency($data->amount, 'IDR', 'id'); ?></span> </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('pageScripts') ?>
<?= $this->endSection() ?>