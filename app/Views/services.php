<?= $this->extend($viewLayout) ?>

<?= $this->section('pageStyles') ?>
<?= link_tag("https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css"); ?>
<?= link_tag("https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.18/dist/css/bootstrap-select.min.css"); ?>
<?= link_tag(base_url('node_modules/ekko-lightbox/dist/ekko-lightbox.css')); ?>
<?= $this->endSection() ?>

<?= $this->section('main') ?>
<div class="container-fluid">
    <div class="row pt-3">
        <div class="col-lg-3 d-none d-lg-block my-2"></div>
        <div class="col-lg-9 col-md-12 col-sm-12 my-2">
            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                <li class="nav-item" role="presentation">
                    <a href="<?= site_url('services/mine'); ?>" class="nav-link show active" id="pills-service-tab" data-toggle="pill" data-target="#pills-service" role="tab" aria-controls="pills-service" aria-selected="true">My Services</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a href="<?= site_url('services/marketplace'); ?>" class="nav-link" id="pills-marketplace-tab" data-toggle="pill" data-target="#pills-marketplace" role="tab" aria-controls="pills-marketplace" aria-selected="false">Marketplace</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a href="<?= site_url('services/recommendation'); ?>" class="nav-link" id="pills-recommendation-tab" data-toggle="pill" data-target="#pills-recommendation" role="tab" aria-controls="pills-recommendation" aria-selected="false">Recommendations</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade show active" id="pills-service" role="tabpanel" aria-labelledby="pills-service-tab"></div>
    	<div class="tab-pane fade" id="pills-marketplace" role="tabpanel" aria-labelledby="pills-marketplace-tab"></div>
    	<div class="tab-pane fade" id="pills-recommendation" role="tabpanel" aria-labelledby="pills-recommendation-tab"></div>
	</div>
</div>
<?= $this->endSection() ?>

<?= $this->section('pageScripts') ?>
<?= script_tag("https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"); ?>
<?= script_tag("https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.18/dist/js/bootstrap-select.min.js"); ?>
<?= script_tag(base_url('node_modules/ekko-lightbox/dist/ekko-lightbox.min.js')); ?>
<script type="text/javascript">
    $(document).ready(function(e) {
        toggle_my_services();

        // default tab
        var $this = $('#pills-tab a[id="pills-service-tab"]'),
            loadurl = $this.attr('href'),
            targ = $this.attr('data-target');

        $.post(loadurl, function(data) {
            $(targ).html(data.html);
        }, 'json');

        $('#pills-tab a').on('shown.bs.tab', function (e) {
            e.target // newly activated tab
            e.relatedTarget // previous active tab
            var $this = $(this),
                loadurl = $this.attr('href'),
                targ = $this.attr('data-target'),
                prevTarg = e.relatedTarget.id.slice(0,-4);

            $.ajax ({
                url: loadurl,
                method: 'post',
                dataType: 'json',
                beforeSend: function() {
                    $('#' + prevTarg).html('');
                }
            })
            .done(function(data) {
                $(targ).html(data.html);
            });
        });

        // $('#pills-tab a').on('click', function (e) {
        //     e.preventDefault();
        //     //$(this).tab('show');

        //     $(".service-category").click(function() {
        //         var id = $(this).attr('data-id'),
        //             name = $(this).text()
        //             active_tab = $(this).attr("href"),
        //             current_li = '#service-category-' . id,
        //             selector = active_tab + ' ' + current_li;

        //         console.log(id, name);
        //         $('#service-category-10').append('<li class="list-group-item border-0 py-1">New list ' + name + '</li>');
        //         //$('#collapseEvents').collapse('toggle');
        //     });

        //     console.log(
        //         $(this).attr("href").replace("#", "")
        //     )
        // })
    });
</script>
<?= $this->endSection() ?>