<?= $this->extend($viewLayout) ?>

<?= $this->section('pageStyles') ?>
<?= link_tag(base_url('assets/css/style_profile.css')); ?>
<?= link_tag(base_url('assets/css/style_connection.css')); ?>
<?= $this->endSection() ?>

<?= $this->section('main') ?>
<div class="container-fluid" style="width:90%;">
	<div class="row pt-3">
		<div class="col-lg-12 col-md-12 col-sm-12 my-2 gedf-main">
			<div class="card social-timeline-card">
				<div class="card-body">
					Showing results...
				</div>
			</div>
			<br>
			<div class="card social-timeline-card">
				<div class="card-body">
					<?php foreach($results as $profile): ?>
						<div class="people-nearby">
							<div class="nearby-user">
								<div class="row">
									<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 profile-photo">
										<img src="<?= $profile->display_picture; ?>" alt="user" class="profile-photo-lg">
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
										<h5><?= anchor(site_url('profile/' . $profile->username),$profile->display_name, ['class' => 'profile-link']); ?></h5>
										<p style="margin-bottom: 0;"><?= $profile->job; ?></p>
										<p class="text-muted" style="font-size: smaller;"><?= ($profile->joined == '') ?: 'Joined '.$profile->joined ; ?></p>
									</div>
									<div class="col-md-3 col-sm-3">
										<div class="row justify-content-center">
											<?php if($profile->connected) { ?>
												<button class="btn btn-primary pull-left mr-2 mb-1 float-right act-profile" data-act="private-message" data-id="<?= $profile->id; ?>" data-toggle="tooltip" data-placement="top" title="Message">
													<i class="fab fa-facebook-messenger"></i> Message
												</button>
											<?php } else { ?>
												<?php if (!empty($profile->connection)) { ?>
													<?php if ($profile->connection->status === '0') { ?>
														<?php if ($profile->connection->action_user_id === user_id()) { ?>
															<button class="btn btn-primary pull-left float-right mr-2 act-profile" data-act="relation-cancel" data-id="<?= $profile->id; ?>">
																Cancel Request
															</button>
														<?php } else { ?>
															<div>
																<button class="btn btn-primary pull-left float-right mr-2" type="button" id="gedf-drop12" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																	<i class="fas fa-user-check"></i> Respond
																</button>
																<div class="dropdown-menu dropdown-menu-right" aria-labelledby="gedf-drop12">
																	<a class="dropdown-item act-profile" href="#" data-act="relation-approve" data-id="<?= $profile->id; ?>">Confirm</a>
																	<a class="dropdown-item act-profile" href="#" data-act="relation-reject" data-id="<?= $profile->id; ?>">Delete Request</a>
																</div>
															</div>
														<?php } ?>
													<?php } elseif ($profile->connection->status === '2') { ?>
														<button class="btn btn-primary pull-left float-right mr-2 act-profile" data-act="relation-invite" data-id="<?= $profile->id; ?>">
															<i class="fas fa-user-plus"></i> Connect
														</button>
													<?php } ?>
												<?php } else { ?>
													<button class="btn btn-primary pull-left float-right mr-2 act-profile" data-act="relation-invite" data-id="<?= $profile->id; ?>">
														<i class="fas fa-user-plus"></i> Connect
													</button>
												<?php } ?>
											<?php } ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?= $this->endSection() ?>

<?= $this->section('pageScripts') ?>
<script type="text/javascript">
	toggle_profile();
</script>
<?= $this->endSection() ?>