<li class="media">
    <div class="mr-2">
        <img class="rounded-circle picture-timeline" src="<?= $commentData->profile->display_picture; ?>" alt="">
    </div>
    <div style="width:100%;">
        <div class="comment-box1">
            <div width="80%">
                <div class="h5 m-0 text-blue" style="margin-bottom: 0px;"><?= anchor($commentData->profile->url, $commentData->profile->display_name, []); ?></div>
                <h8><small class="text-muted"><i class="far fa-clock"></i> <?= $commentData->since; ?></small></h8>
            </div>
            <div class="well well-lg">
                <div class="card-text">
                    <article><?= $commentData->content; ?></article>
                </div>
            </div>
        </div>
    </div>
</li>