<div class="row comment-box-card">
    <div class="mr-2" style="margin-left: 12px;">
        <img class="rounded-circle picture-timeline" src="<?= user_profile()->display_picture; ?>" alt="">
    </div>
    <div style="width: 86%">
        <div style="width:100%;">
            <textarea class="comment-box-input act-comment" data-post="<?= $postId; ?>" data-parent="0" placeholder="Add comment... "></textarea>
        </div>
    </div>
</div>

<script type="text/javascript">
    toggle_post_comment();
</script>