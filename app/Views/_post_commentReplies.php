<div class="card-comment-body">
<?php for ($i=0; $i < count($commentsData); $i++):?>
    <li class="media">
        <div class="mr-2">
            <img class="rounded-circle picture-timeline" src="<?= $commentsData[$i]->profile->display_picture; ?>" alt="">
        </div>
        <div style="width:100%;">
            <div class="comment-box1">
                <div width="80%">
                    <div class="h5 m-0 text-blue" style="margin-bottom: 0px;"><?= anchor($commentsData[$i]->profile->url, $commentsData[$i]->profile->display_name, []); ?></div>
                    <h8><small class="text-muted"><i class="far fa-clock"></i> <?= $commentsData[$i]->since; ?></small></h8>
                </div>
                <div class="well well-lg">
                    <div class="card-text">
                        <article><?= $commentsData[$i]->content; ?></article>
                    </div>
                </div>
            </div>
        </div>
    </li>
<?php endfor; ?>
</div>