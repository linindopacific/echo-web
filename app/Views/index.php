<?php $faker = \Faker\Factory::create(); ?>

<?= $this->extend($viewLayout) ?>

<?= $this->section('pageStyles') ?>
<?= link_tag('https://cdn.jsdelivr.net/npm/placeholder-loading/dist/css/placeholder-loading.min.css'); ?>
<?= link_tag("https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css"); ?>
<?= link_tag("https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.18/dist/css/bootstrap-select.min.css"); ?>
<?= link_tag(base_url('assets/css/style_comment.css')); ?>
<?= $this->endSection() ?>

<?= $this->section('main') ?>
<div class="container-fluid">
    <div class="row justify-content-center">
        <aside class="col-lg-3 col-md-10 col-sm-10 my-2 ml-0 px-2">
            <div id="container_row" class="row">
                <div class="col">
                    <div class="card shadow-sm">
                        <ul class="list-group list-group-flush my-3">
                            <li class="list-group-item">
                                <div class="col ml-0 px-0 py-2 text-truncate">
                                    <img src="<?= user_profile()->display_picture; ?>" width="30" height="30" class="rounded-circle" alt="">
                                    <span class="hidden-xs ml-2 font-weight-bold"><?= user_profile()->display_name; ?></span>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <ul class="list-unstyled font-weight-bold">
                                    <li class="list-group-item p-1 border-0 text-truncate">
                                        <?= anchor(route_to("profile"), "<i class=\"material-icons md-18 mr-2\">person_outline</i>My Profile", ["class" => "text-muted", "title" => "My Profile"]) ?>
                                    </li>
                                    <li class="list-group-item p-1 border-0 text-truncate">
                                        <?= anchor(route_to("services"), "<i class=\"material-icons md-18 mr-2\">construction</i>My Services", ["class" => "text-muted", "title" => "My Services"]) ?>
                                    </li>
                                </ul>
                            </li>
                            <li class="list-group-item">
                                <div id="accordion_1">
                                    <button class="btn btn-link px-0 font-weight-bold text-muted" data-toggle="collapse" data-target="#echoPoints" aria-expanded="true" aria-controls="echoPoints">
                                        <i class="material-icons md-18 mr-2">keyboard_arrow_down</i>Echo Points:</span>
                                        <span class="text-primary">350</span>
                                    </button>
                                    <div id="echoPoints" class="collapse show px-4" aria-labelledby="headingOne" data-parent="#accordion_1">
                                        <p class="px-2 m-0">Points Summary</p>
                                        <ul style="list-style-type: '-'" class="pl-3 pr-2 text-muted">
                                            <li class="pl-1">Transactions: <span class="font-weight-bold text-primary">287</span></li>
                                            <li class="pl-1">Recommendations:  <span class="font-weight-bold text-primary">63</span></li>
                                            <li class="pl-1">Friend Referals: <span class="font-weight-bold text-primary">0</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                            <?php if (! empty($pinnedService)): ?>
                            <li class="list-group-item">
                                <div id="accordion_2">
                                    <button class="btn btn-link px-0 font-weight-bold text-muted" data-toggle="collapse" data-target="#pinnedService" aria-expanded="true" aria-controls="pinnedService">
                                        <i class="material-icons md-18 mr-2">keyboard_arrow_down</i>Service:</span>
                                        <span class="text-primary"><?= $pinnedService->name; ?></span>
                                    </button>
                                    <div id="pinnedService" class="collapse show px-4 text-muted" aria-labelledby="headingOne" data-parent="#accordion_2">
                                        <ul class="list-unstyled">
                                            <li class="list-group-item p-1 border-0">
                                                <span class="market-revenue mr-2"><i class="fas fa-wallet fa-sm" aria-hidden="true"></i></span><?= number_to_currency($pinnedService->net_amount, 'IDR', 'id') ?>
                                            </li>
                                            <li class="list-group-item p-1 border-0">
                                                <span class="market-complete mr-1"><i class="fas fa-check-circle fa-sm" aria-hidden="true"></i></span> <?= $pinnedService->completes ?> Completed
                                            </li>
                                            <li class="list-group-item p-1 border-0">
                                                <span class="score mr-2"> <i class="far fa-list-alt fa-sm"></i>
                                                    <div class="score-wrap">
                                                        <span class="market-stars-active" style="width:<?= ($pinnedService->ratings/5)*100 ?>%">
                                                            <i class="fas fa-star fa-sm" aria-hidden="true"></i>
                                                            <i class="fas fa-star fa-sm" aria-hidden="true"></i>
                                                            <i class="fas fa-star fa-sm" aria-hidden="true"></i>
                                                            <i class="fas fa-star fa-sm" aria-hidden="true"></i>
                                                            <i class="fas fa-star fa-sm" aria-hidden="true"></i>
                                                        </span>
                                                        <span class="market-stars-inactive">
                                                            <i class="far fa-star fa-sm" aria-hidden="true"></i>
                                                            <i class="far fa-star fa-sm" aria-hidden="true"></i>
                                                            <i class="far fa-star fa-sm" aria-hidden="true"></i>
                                                            <i class="far fa-star fa-sm" aria-hidden="true"></i>
                                                            <i class="far fa-star fa-sm" aria-hidden="true"></i>
                                                        </span>
                                                    </div>
                                                </span>
                                            </li>
                                            <li class="list-group-item p-1 border-0">
                                                <span class="market-since mr-1"><i class="fas fa-calendar fa-sm" aria-hidden="true"></i></span> Since <?= $pinnedService->since ?>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="container_row" class="row">
                <div class="col">
                    <div class="card shadow-sm">
                        <div class="card-body">
                            <h5 class="card-title">Left sidebar</h5>
                            <p class="card-text"></p>
                        </div>
                    </div>
                </div>
            </div>
        </aside>
        <aside class="col-lg-5 col-md-10 col-sm-10 my-2 mx-1 px-2">
            <!-- share start -->
            <div id="container_row" class="row">
                <div class="col">
                    <div class="card shadow-sm">
                        <div class="card-body">
                            <h6 class="card-title">Share your gig story / experience / recommendation / DIY advice</h6>
                            <div class="row justify-content-start">
                                <div class="col-8 ml-0">
                                    <p class="card-text">Add video, photos (before & after) or thumbs up for an excellent service.</p>
                                </div>
                                <div class="col">
                                    <button type="button" class="btn btn-primary post-modal"><i class="fas fa-bullhorn"></i> <span class="align-middle"> Create Post</span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- share end -->
            <div id="timeline_post"></div>
            <div id="no_more_data"></div>
            <div class="backTop"><i class="fas fa-arrow-circle-up fa-2x"></i></div>
        </aside>
        <aside class="col-lg-3 col-md-10 col-sm-10 my-2 mr-0 px-2">
            <div id="container_row" class="row">
                <div class="col">
                    <div class="card shadow-sm">
                        <div class="card-body">
                            <h5 class="card-title">Right sidebar 1</h5>
                            <p class="card-text"></p>
                        </div>
                    </div>
                </div>
            </div>
            <div id="container_row" class="row">
                <div class="col">
                    <div class="card shadow-sm">
                        <div class="card-body">
                            <h5 class="card-title">Right sidebar 2</h5>
                            <p class="card-text"></p>
                        </div>
                    </div>
                </div>
            </div>
        </aside>
    </div>
</div>

<div class="modal fade" id="postModal" tabindex="-1" role="dialog" aria-labelledby="postModalLabel" aria-hidden="true"></div>
<?= $this->endSection() ?>

<?= $this->section('pageScripts') ?>
<?= script_tag("https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"); ?>
<?= script_tag("https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.18/dist/js/bootstrap-select.min.js"); ?>
<script type="text/javascript">
    var action = 'inactive',
        uname = '',
        lId = '',
        no_more_data = false,
        is_empty = true;

    if(action == 'inactive')
    {
        make_skeleton_post();
        setTimeout(function(){
            load_timeline_data(uname);
        }, 3000);
    }

    $(window).scroll(function(){
        if(lId != '' && no_more_data != true)
        {
            if($(window).scrollTop() + $(window).height() > $("#timeline_post").height() && action == 'inactive' && $.ajaxq.isRunning('MyQueue') == false)
            {
                action = 'active';
                make_skeleton_post();
                setTimeout(function(){
                    load_timeline_data(uname, lId);
                }, 5000);
            }
        }
    });

    function load_timeline_data(uname, page='') {
        $.ajaxq ('MyQueue', {
            url: '<?= site_url('post/timeline') ?>',
            type: 'post',
            data: {uname: uname, page: page},
            dataType: 'json',
            beforeSend: function() {
            },
            complete:function() {
                $('.tmp_container').remove();
            },
            success:function(response) {
                if (response.is_empty) {
                    $('#timeline_post').append(response.html);
                }

                if (response.no_more_data) {
                    $('#no_more_data').html('<div id="container_row" class="row"> \
                                                <div class="col"> \
                                                    <div style="text-align: center;" class="alert alert-info">No more data found</div> \
                                                </div> \
                                            </div>');
                    no_more_data = response.no_more_data;
                } else {
                    $('#timeline_post').append(response.html);
                    lId = response.lId;
                }
                action = 'inactive';
            },
            error: function(xhr, textStatus, errorThrown) {
                var errors = xhr.responseJSON;
                if (xhr.status == 401) {
                    alert("Error: " + errorThrown);
                    window.location.replace(siteUrl);
                } else {
                    alert("Error: " + errors.message);
                }
            }
        });
    }
</script>
<?= $this->endSection() ?>