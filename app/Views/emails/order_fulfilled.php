<?= $this->extend('App\Views\emails\layout') ?>

<?= $this->section('main') ?>
<tr>
    <td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;">
        <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
            <tr>
                <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">
                    <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Hi <?= esc($customerName) ?>,</p>
                    <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Berikut ini adalah informasi pemesanan Anda terselesaikan:</p>
                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                        <tr>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top; width: 35%">Permintaan Layanan</td>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">:</td>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"><?= esc($itemName) ?></td>
                        </tr>
                        <tr>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top; width: 35%">Penyedia Layanan</td>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">:</td>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"><?= esc($sellerName) ?></td>
                        </tr>
                        <tr>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top; width: 35%">Waktu Pengerjaan</td>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">:</td>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"><?= esc($fulfilledAt) ?></td>
                        </tr>
                    </table>
                    <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;"></p>
                    <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Info: Mohon melakukan konfirmasi pengerjaan.</p>
                    <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Rincian Pesanan:</p>
                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                        <tr>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top; width: 35%">Nomor Pesanan</td>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">:</td>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"><?= esc($externalId) ?></td>
                        </tr>
                    </table>
                    <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;"></p>
                    <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Semoga informasi ini bermanfaat bagi Anda. <br> Terima kasih.</p>
                </td>
            </tr>
        </table>
    </td>
</tr>
<?= $this->endSection() ?>