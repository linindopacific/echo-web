<?= $this->extend('App\Views\emails\layout') ?>

<?= $this->section('main') ?>
<tr>
    <td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;">
        <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
            <tr>
                <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">
                    <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Hi <?= esc($customerName) ?>,</p>
                    <p style="font-family: sans-serif; font-size: 16px; font-weight: bold; margin: 0; Margin-bottom: 15px;">Pembayaran terverifikasi dan pesanan telah diteruskan ke penyedia</p>
                    <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;"></p>
                    <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Terima kasih, ya! Berikut detail pembayaranmu:</p>
                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                        <tr>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top; width: 35%">Jumlah Pembayaran</td>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">:</td>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"><?= esc(number_to_currency($paymentAmount, 'IDR', 'id')) ?></td>
                        </tr>
                        <tr>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top; width: 35%">Kanal Pembayaran</td>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">:</td>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"><?= esc($paymentChannel) ?></td>
                        </tr>
                        <tr>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top; width: 35%">Waktu Pembayaran</td>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">:</td>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"><?= esc($transactionAt) ?></td>
                        </tr>
                    </table>
                    <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;"></p>
                    <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Info: Jangan menginformasikan bukti dan data pembayaran kepada pihak manapun kecuali <?= esc(env('APP_NAME')) ?></p>
                    <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Rincian Pesanan:</p>
                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                        <tr>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top; width: 35%">Nomor Pesanan</td>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">:</td>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"><?= esc($externalId) ?></td>
                        </tr>
                        <tr>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top; width: 35%">Penyedia Layanan</td>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">:</td>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"><?= esc($sellerName) ?></td>
                        </tr>
                        <tr>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top; width: 35%">Permintaan Layanan</td>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">:</td>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"><?= esc($itemName) ?></td>
                        </tr>
                    </table>
                    <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;"></p>
                    <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Alamat Permintaan:</p>
                    <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">
                        <?= esc($customerName); ?>
                        <br>
                        <?= esc($customerFullAddress); ?>
                        <br>
                        <?= esc($customerPhone); ?>
                    </p>
                    <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Terima kasih.</p>
                </td>
            </tr>
        </table>
    </td>
</tr>
<?= $this->endSection() ?>