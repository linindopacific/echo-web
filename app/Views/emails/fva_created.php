<?= $this->extend('App\Views\emails\layout') ?>

<?= $this->section('main') ?>
<tr>
    <td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;">
        <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
            <tr>
                <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">
                    <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Hi <?= esc($customerName) ?>,</p>
                    <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Berikut ini adalah informasi transaksi yang ingin Anda lakukan:</p>
                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                        <tr>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top; width: 35%">Tujuan Pembayaran</td>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">:</td>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"><?= esc($itemName) ?></td>
                        </tr>
                        <tr>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top; width: 35%">Tanggal</td>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">:</td>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"><?= esc($createdAt) ?></td>
                        </tr>
                        <tr>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top; width: 35%">Status Pembayaran</td>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">:</td>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">PROCESS</td>
                        </tr>
                    </table>
                    <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;"></p>
                    <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Info: Mohon melakukan pembayaran paling lambat <?= esc($expiredAt) ?></p>
                    <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Rincian Pesanan:</p>
                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                        <tr>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top; width: 35%">Kanal Pembayaran</td>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">:</td>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"><?= esc($paymentChannel) ?></td>
                        </tr>
                        <tr>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top; width: 35%">Kode Bayar</td>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">:</td>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"><?= esc($paymentCode) ?></td>
                        </tr>
                        <tr>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top; width: 35%">Jumlah Pembayaran</td>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">:</td>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"><?= esc(number_to_currency($paymentAmount, 'IDR', 'id')) ?></td>
                        </tr>
                        <tr>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top; width: 35%">Nomor Pesanan</td>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">:</td>
                            <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;"><?= esc($externalId) ?></td>
                        </tr>
                    </table>
                    <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;"></p>
                    <?php if ($paymentCategory === 'VIRTUAL_ACCOUNT'): ?>
                        <?php
                            $channelStr = strtolower($paymentChannel);
                            $langName = "Payment.va.$channelStr";

                            if ( $channelStr === 'mandiri' )
                            {
                                $_code = substr($paymentCode, 0, 5);

                                $opt1 = lang($langName.'.internal.'.$_code.'.atm');
                                $opt2 = lang($langName.'.internal.'.$_code.'.ibanking');
                                $opt3 = lang($langName.'.internal.'.$_code.'.mbanking');
                            }
                            else
                            {
                                $opt1 = lang($langName.'.internal.atm');
                                $opt2 = lang($langName.'.internal.ibanking');
                                $opt3 = lang($langName.'.internal.mbanking');
                            }
                        ?>
                        <?php if (count($opt1) > 0): ?>
                            <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Cara pembayaran melalui ATM:</p>
                            <ol>
                                <?php for ($i=0; $i < count($opt1); $i++): ?>
                                    <?= '<li>'.$opt1[$i].'</li>' ?>
                                <?php endfor; ?>
                            </ol>
                        <?php endif; ?>
                        <?php if (count($opt2) > 0): ?>
                            <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Cara pembayaran melalui Internet Banking:</p>
                            <ol>
                                <?php for ($i=0; $i < count($opt2); $i++): ?>
                                    <?= '<li>'.$opt2[$i].'</li>' ?>
                                <?php endfor; ?>
                            </ol>
                        <?php endif; ?>
                        <?php if (count($opt3) > 0): ?>
                            <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Cara pembayaran melalui Mobile Banking:</p>
                            <ol>
                                <?php for ($i=0; $i < count($opt3); $i++): ?>
                                    <?= '<li>'.$opt3[$i].'</li>' ?>
                                <?php endfor; ?>
                            </ol>
                        <?php endif; ?>
                    <?php endif; ?>
                    <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">Semoga informasi ini bermanfaat bagi Anda. <br> Terima kasih.</p>
                </td>
            </tr>
        </table>
    </td>
</tr>
<?= $this->endSection() ?>