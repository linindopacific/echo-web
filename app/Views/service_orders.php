<?= $this->extend($viewLayout) ?>

<?= $this->section('pageStyles') ?>
<style>
    .rating {
        float:left;
    }

    /* :not(:checked) is a filter, so that browsers that don’t support :checked don’t
    follow these rules. Every browser that supports :checked also supports :not(), so
    it doesn’t make the test unnecessarily selective */
    .rating:not(:checked) > input {
        position:absolute;
        top:-9999px;
        clip:rect(0,0,0,0);
    }

    .rating:not(:checked) > label {
        float:right;
        width:1em;
        padding:0 .1em;
        overflow:hidden;
        white-space:nowrap;
        cursor:pointer;
        font-size:200%;
        line-height:1.2;
        color:#ddd;
        text-shadow:1px 1px #bbb, 2px 2px #666, .1em .1em .2em rgba(0,0,0,.5);
    }

    .rating:not(:checked) > label:before {
        content: '★ ';
    }

    .rating > input:checked ~ label {
        color: #f70;
        text-shadow:1px 1px #c60, 2px 2px #940, .1em .1em .2em rgba(0,0,0,.5);
    }

    .rating:not(:checked) > label:hover,
    .rating:not(:checked) > label:hover ~ label {
        color: gold;
        text-shadow:1px 1px goldenrod, 2px 2px #B57340, .1em .1em .2em rgba(0,0,0,.5);
    }

    .rating > input:checked + label:hover,
    .rating > input:checked + label:hover ~ label,
    .rating > input:checked ~ label:hover,
    .rating > input:checked ~ label:hover ~ label,
    .rating > label:hover ~ input:checked ~ label {
        color: #ea0;
        text-shadow:1px 1px goldenrod, 2px 2px #B57340, .1em .1em .2em rgba(0,0,0,.5);
    }

    .rating > label:active {
        position:relative;
        top:2px;
        left:2px;
    }
</style>
<?= $this->endSection() ?>

<?= $this->section('main') ?>
<div class="container-fluid">
    <div class="row pt-3">
        <div class="col-lg-3 d-none d-lg-block my-2"></div>
        <div class="col-lg-9 col-md-12 col-sm-12 my-2">
            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                <li class="nav-item" role="presentation">
                    <a href="<?= site_url('services/orders'); ?>" class="nav-link show active" id="pills-purchases-tab" data-id="purchases" data-toggle="pill" data-target="#pills-purchases" role="tab" aria-controls="pills-purchases" aria-selected="true">Pembelian</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a href="<?= site_url('services/orders'); ?>" class="nav-link" id="pills-sales-tab" data-id="sales" data-toggle="pill" data-target="#pills-sales" role="tab" aria-controls="pills-sales" aria-selected="false">Penjualan</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade show active" id="pills-purchases" role="tabpanel" aria-labelledby="pills-purchases-tab"></div>
    	<div class="tab-pane fade" id="pills-sales" role="tabpanel" aria-labelledby="pills-sales-tab"></div>
	</div>
</div>

<div class="modal fade" id="serviceOrderModal" tabindex="-1" role="dialog" aria-labelledby="serviceOrderModalLabel" aria-hidden="true"></div>
<div class="modal fade" id="serviceReviewModal" tabindex="-1" role="dialog" aria-labelledby="serviceReviewModalLabel" aria-hidden="true"></div>
<?= $this->endSection() ?>

<?= $this->section('pageScripts') ?>
<script type="text/javascript">
    $(document).ready(function() {
        // default tab
        var $this = $('#pills-tab a[id="pills-purchases-tab"]'),
            loadurl = siteUrl + '/services/orders',
            targ = $this.attr('data-target');

        $.ajaxq ('MyQueue', {
            url: loadurl,
            headers: {'X-Requested-With': 'XMLHttpRequest'},
            type: 'POST',
            data: {'id': $this.attr('data-id')},
            dataType: 'json',
            beforeSend: function() {},
            success: function(response) {
                $(targ).html(response.html);
            },
            error: function(xhr, textStatus, errorThrown) {
                var res = xhr.responseJSON;
                if (xhr.status == 401) {
                    alert("Error: " + errorThrown)
                    window.location.replace(siteUrl);
                } else {
                    alert("Error: " + res.error);
                }
            }
        });

        $('#pills-tab a').on('shown.bs.tab', function (e) {
            e.target // newly activated tab
            e.relatedTarget // previous active tab
            var $this = $(this),
                targ = $this.attr('data-target'),
                prevTarg = e.relatedTarget.id.slice(0,-4);

            $.ajaxq ('MyQueue', {
                url: loadurl,
                headers: {'X-Requested-With': 'XMLHttpRequest'},
                type: 'POST',
                data: {'id': $this.attr('data-id')},
                dataType: 'json',
                beforeSend: function() {
                    $('#' + prevTarg).html('');
                },
                success: function(response) {
                    $(targ).html(response.html);
                },
                error: function(xhr, textStatus, errorThrown) {
                    var res = xhr.responseJSON;
                    if (xhr.status == 401) {
                        alert("Error: " + errorThrown);
                        window.location.replace(siteUrl);
                    } else {
                        alert("Error: " + res.error);
                    }
                }
            });
        });
    });

    function toogle_order_list()
    {
        $(".order-list-group a").on("click", function (e) {
            e.preventDefault();
            var id = $(this).attr("data-id"),
                source = $(this).parent().attr("data-id");

            $.ajaxq ("MyQueue", {
                url: siteUrl + "/services/order-detail",
                type: "get",
                data: {id: id, source: source},
                dataType: "json",
                success: function(response) {
                    $("#serviceOrderModal").html(response.html);
                    $("#serviceOrderModal").modal({
                        //backdrop: "static",
                        keyboard: false,
                        show: true
                    });
                },
                error: function(xhr, textStatus, errorThrown) {
                    var res = xhr.responseJSON;
                    if (xhr.status == 401) {
                        alert("Error: " + errorThrown)
                        window.location.replace(siteUrl);
                    } else {
                        alert("Error: " + res.error);
                    }
                }
            });
        });
    }
</script>
<?= $this->endSection() ?>