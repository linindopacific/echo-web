<div class="row">
    <div class="col-md-3 col-sm-12">
        <div class="nav flex-column nav-pills" id="v-purchases-tab" role="tablist" aria-orientation="vertical">
            <a href="<?= site_url('services/purchase-orders'); ?>" class="nav-link show active" id="v-po-all-tab" data-type="0" data-toggle="pill" data-target="#v-po-all" role="tab" aria-controls="v-po-all" aria-selected="true">Semua</a>
            <a href="<?= site_url('services/purchase-orders'); ?>" class="nav-link" id="v-po-unpaid-tab" data-type="1" data-toggle="pill" data-target="#v-po-unpaid" role="tab" aria-controls="v-po-unpaid" aria-selected="false">Menunggu Dibayar</a>
            <a href="<?= site_url('services/purchase-orders'); ?>" class="nav-link" id="v-po-process-tab" data-type="2" data-toggle="pill" data-target="#v-po-process" role="tab" aria-controls="v-po-process" aria-selected="false">Pesanan Diproses</a>
            <a href="<?= site_url('services/purchase-orders'); ?>" class="nav-link" id="v-po-done-tab" data-type="3" data-toggle="pill" data-target="#v-po-done" role="tab" aria-controls="v-po-done" aria-selected="false">Pesanan Selesai</a>
        </div>
    </div>
    <div class="col-md-9 col-sm-12">
        <div class="tab-content" id="v-pills-tabContent">
            <div class="tab-pane fade show active" id="v-po-all" role="tabpanel" aria-labelledby="v-po-all-tab"></div>
            <div class="tab-pane fade" id="v-po-unpaid" role="tabpanel" aria-labelledby="v-po-unpaid-tab"></div>
            <div class="tab-pane fade" id="v-po-process" role="tabpanel" aria-labelledby="v-po-process-tab"></div>
            <div class="tab-pane fade" id="v-po-done" role="tabpanel" aria-labelledby="v-po-done-tab"></div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        // default tab
        var $this = $('#v-purchases-tab a[id="v-po-all-tab"]'),
            loadurl = siteUrl + '/services/purchase-orders',
            targ = $this.attr('data-target'),
            type = $this.attr('data-type'),
            data = {'type': type};

        find_services(loadurl, data, targ);

        $('#v-purchases-tab a').on('shown.bs.tab', function (e) {
            e.target // newly activated tab
            e.relatedTarget // previous active tab
            var $this = $(this),
                targ = $this.attr('data-target'),
                type = $this.attr('data-type'),
                data = {'type': type};
                prevTarg = e.relatedTarget.id.slice(0,-4);

            find_services(loadurl, data, targ, prevTarg);
        });
    });
</script>