<div class="card my-0" style="width: 25rem; border: 0px">
	<ul class="list-group list-group-flush">
        <?php foreach ($data as $row): ?>
		<li class="list-group-item">
			<div class="d-flex">
                <div class="mr-2">
                    <img class="rounded-circle picture-timeline" src="<?= $row['displayPicture']; ?>" alt="">
                </div>
                <div class="ml-2">
                    <div class="m-0"><?= $row['displayName'] . ' ' . lang('Activity.' . $row['actionType'])?><?= ($row['description'] <> '') ? ' "' . $row['description'] . '"' : '' ?></div>
                    <h7><small class="text-muted"> <?= $row['since'] ?></small></h7>
                </div>
            </div>
		</li>
        <?php endforeach; ?>
		<li class="list-group-item list-group-item-dark" style="text-align: center;">View All</li>
	</ul>
</div>