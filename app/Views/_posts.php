<div class="card shadow-sm">
    <div class="card-header">
        <div class="d-flex justify-content-between align-items-center">
            <div class="d-flex justify-content-between align-items-center">
                <div class="mr-2">
                    <img class="rounded-circle picture-timeline" src="<?= $postData->profile->display_picture; ?>" alt="">
                </div>
                <div class="ml-2">
                    <div class="h5 m-0 text-blue"><?= anchor($postData->profile->url, $postData->profile->display_name, []); ?></div>
                    <h7><small class="text-muted"><i class="far fa-clock"></i> <?= $postData->since; ?></small></h7>
                </div>
            </div>
            <div class="dropdown">
                <button class="btn" type="button" id="gedf-drop" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-ellipsis-h"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="gedf-drop">
                    <?php if ($postData->user_id === user_id()) { ?>
                    <a id="post_edit" class="dropdown-item post-modal" href="#" data-id="<?= $postData->id; ?>">Edit</a>
                    <a id="post_delete" class="dropdown-item act-post" href="#" data-id="<?= $postData->id; ?>" data-act="delete">Delete</a>
                    <?php } else { ?>
                    <a id="post_save" class="dropdown-item act-post" href="#" data-id="<?= $postData->id; ?>" data-act="save">Save</a>
                    <a id="post_hide" class="dropdown-item act-post" href="#" data-id="<?= $postData->id; ?>" data-act="hide">Hide</a>
                    <a id="post_report" class="dropdown-item act-post" href="#" data-id="<?= $postData->id; ?>" data-act="report">Report</a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="card-text"><article><?= $postData->content ?></article></div>
    </div>
    <div class="card-footer">
        <a id="post_reaction" href="#" class="card-link act-post" data-id="<?= $postData->id; ?>" data-act="like"><span id="icon" style=""><i class="<?= $postData->isReacted() ? 'fas' : 'far'; ?> fa-thumbs-up"></i></span> Like</a>
        <a id="post_comment" href="#" class="card-link act-post" data-id="<?= $postData->id; ?>" data-act="get-comment"><span id="icon" style=""><i class="far fa-comment"></i></span> Comment</a>
        <!--a id="post_share" href="#" class="card-link act-post" data-id="<?= $postData->id; ?>" data-act="share"><span id="icon" style=""><i class="far fa-share-square"></i></span> Share</a-->
        <div id="post_commentBox"></div>
    </div>

    <!--div class="card-comment-body">
        <li class="media">
            <div class="mr-2">
               <img class="rounded-circle picture-timeline" src="https://s3.amazonaws.com/uifaces/faces/twitter/dancounsell/128.jpg" alt="">
            </div>
            <div style="width:100%;">
              <div class="comment-box1">
                <div width="80%">
                    <div class="h5 m-0 text-blue" style="margin-bottom: 0px;">Marco</div>
                    <h8><small class="text-muted"><i class="far fa-clock"></i> 1 minutes</small></h8>
                </div>
                <div class="well well-lg">
                    <div class="card-text">
                        <article>test comment 1 2 3 aku dua kdajdj rojwodjwj ahaeh</article>
                    </div>
                </div>
              </div>
              <ul class="comment-link">
                <a id="post_reaction" href="#" class="card-comment act-post" data-id="<?= $postData->id; ?>" data-act="reaction"><span id="icon" style=""><i class="<?= $postData->isReacted() ? 'fas' : 'far'; ?> fa-thumbs-up"></i></span> Like</a>
                <a class="card-comment act-post" href="#" id="post_reply"><span class="fa fa-reply"></span> Reply</a>
                <a class="card-comment act-post" id="post_comment" data-toggle="collapse" href="#replyOne"><span class="fa fa-comment"></span> 2 Comments</a>
                <a class="card-comment act-post" style="float: right" id="gedf-drop" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-ellipsis-h"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="gedf-drop">
                    <?php if ($postData->user_id === user_id()) { ?>
                    <a id="post_delete" class="dropdown-item act-post" href="#" data-id="<?= $postData->id; ?>" data-act="delete">Delete</a>
                    <?php } else { ?>
                    <a id="post_save" class="dropdown-item act-post" href="#" data-id="<?= $postData->id; ?>" data-act="save">Save</a>
                    <a id="post_hide" class="dropdown-item act-post" href="#" data-id="<?= $postData->id; ?>" data-act="hide">Hide</a>
                    <a id="post_report" class="dropdown-item act-post" href="#" data-id="<?= $postData->id; ?>" data-act="report">Report</a>
                    <?php } ?>
                </div>
              </ul>
              <div class="collapse" id="replyOne">
                <ul class="media-list" style='padding-inline-start: 0px; '>
                    <li class="media media-replied">

                        <div class="mr-2">
                           <img class="rounded-circle picture-timeline" src="https://s3.amazonaws.com/uifaces/faces/twitter/ManikRathee/128.jpg" alt="">
                        </div>
                        <div style="width:100%;">
                          <div class="comment-box1">
                            <div>
                                <div class="h5 m-0 text-blue" style="margin-bottom: 0px;">Mitue</div>
                                <h8><small class="text-muted"><i class="far fa-clock"></i> 1 minutes</small></h8>

                            </div>

                            <div class="well well-lg">
                                <div class="card-text">
                                    <article>Nice job Maria. hahahahahahahahahahahahahahahah</article>
                                </div>
                            </div>
                          </div>
                          <ul class="comment-link">
                            <a id="post_reaction" href="#" class="card-comment act-post" data-id="<?= $postData->id; ?>" data-act="reaction"><span id="icon" style=""><i class="<?= $postData->isReacted() ? 'fas' : 'far'; ?> fa-thumbs-up"></i></span> Like</a>
                            <a class="card-comment act-post" href="#" id="post_reply"><span class="fa fa-reply"></span> Reply</a>
                            <a class="card-comment act-post" style="float: right" id="gedf-drop" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="gedf-drop">
                                <?php if ($postData->user_id === user_id()) { ?>
                                <a id="post_delete" class="dropdown-item act-post" href="#" data-id="<?= $postData->id; ?>" data-act="delete">Delete</a>
                                <?php } else { ?>
                                <a id="post_save" class="dropdown-item act-post" href="#" data-id="<?= $postData->id; ?>" data-act="save">Save</a>
                                <a id="post_hide" class="dropdown-item act-post" href="#" data-id="<?= $postData->id; ?>" data-act="hide">Hide</a>
                                <a id="post_report" class="dropdown-item act-post" href="#" data-id="<?= $postData->id; ?>" data-act="report">Report</a>
                                <?php } ?>
                            </div>
                          </ul>
                          <div class="media media-replied reply" >
                            <div class="mr-2">
                              <img class="rounded-circle picture-timeline" style="margin-left: 0.5rem;" src="<?= $postData->display_picture; ?>" alt="">
                            </div>
                              <div style="width:86%;">
                                <textarea id="txtArea" class="comment-box-input" placeholder="Add comment ... "></textarea>
                              </div>
                          </div>
                    </li>
                    <li class="media media-replied" id="replied">
                        <div class="mr-2">
                           <img class="rounded-circle picture-timeline" src="https://pbs.twimg.com/profile_images/442656111636668417/Q_9oP8iZ.jpeg" alt="">
                        </div>
                        <div style="width:100%;">
                          <div class="comment-box1">
                            <div>
                                <div class="h5 m-0 text-blue" style="margin-bottom: 0px;">Morinaga</div>
                                <h8><small class="text-muted"><i class="far fa-clock"></i> 1 minutes</small></h8>
                            </div>
                            <div class="well well-lg">
                                <div class="card-text">
                                    <article>Thank you alllll hahahahahahahahahahahahahahahah</article>
                                </div>
                            </div>
                          </div>
                          <ul class="comment-link">
                            <a id="post_reaction" href="#" class="card-comment act-post" data-id="<?= $postData->id; ?>" data-act="reaction"><span id="icon" style=""><i class="<?= $postData->isReacted() ? 'fas' : 'far'; ?> fa-thumbs-up"></i></span> Like</a>
                            <a class="card-comment act-post" href="#" id="post_reply"><span class="fa fa-reply"></span> Reply</a>
                            <a class="card-comment act-post" style="float: right" id="gedf-drop" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="gedf-drop">
                                <?php if ($postData->user_id === user_id()) { ?>
                                <a id="post_delete" class="dropdown-item act-post" href="#" data-id="<?= $postData->id; ?>" data-act="delete">Delete</a>
                                <?php } else { ?>
                                <a id="post_save" class="dropdown-item act-post" href="#" data-id="<?= $postData->id; ?>" data-act="save">Save</a>
                                <a id="post_hide" class="dropdown-item act-post" href="#" data-id="<?= $postData->id; ?>" data-act="hide">Hide</a>
                                <a id="post_report" class="dropdown-item act-post" href="#" data-id="<?= $postData->id; ?>" data-act="report">Report</a>
                                <?php } ?>
                            </div>
                          </ul>
                    </li>
                    <div class="media media-replied reply" >
                      <div class="mr-2" sty>
                        <img class="rounded-circle picture-timeline" src="<?= $postData->display_picture; ?>" alt="">
                      </div>
                        <div style="width:88%;">
                          <textarea id="txtArea" class="comment-box-input" placeholder="Add comment ... "></textarea>
                        </div>
                    </div>
                </ul>
              </div>
            </div>
        </li>
    </div-->

</div>