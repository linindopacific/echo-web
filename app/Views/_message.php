<?php if ($type === 'out') { ?>
    <div class="msg outgoing_msg">
        <div class="sent_msg">
            <p><?= $content ?></p>
            <span class="time_date"><?= $timeString . ' | ' . $dateString ?></span>
        </div>
    </div>
<?php } else if ($type === 'in') { ?>
    <div class="msg incoming_msg">
        <div class="incoming_msg_img"> <img src="<?= $profile->display_picture; ?>" alt="<?= $profile->display_name; ?>" class="rounded-circle"> </div>
        <div class="received_msg">
            <div class="received_withd_msg">
                <p><?= $content ?></p>
                <span class="time_date"><?= $timeString . ' | ' . $dateString ?></span>
            </div>
        </div>
    </div>
<?php } ?>