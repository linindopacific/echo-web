<div id="container_row" class="row post_<?= $postData->id ?>">
    <div class="col">
        <div class="card shadow-sm">
            <div class="card-header">
                <div class="d-flex justify-content-between align-items-center">
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="mr-2">
                            <img class="rounded-circle picture-timeline" src="<?= $postData->profile->display_picture; ?>" alt="">
                        </div>
                        <div class="ml-2">
                            <div class="h5 m-0 text-blue"><?= anchor($postData->profile->url, $postData->profile->display_name, []); ?></div>
                            <h7><small class="text-muted"><i class="far fa-clock"></i> <?= $postData->since; ?></small></h7>
                        </div>
                    </div>
                    <div class="dropdown">
                        <button class="btn" type="button" id="gedf-drop" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-h"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="gedf-drop">
                            <?php if ($postData->user_id === user_id()) { ?>
                            <a id="post_edit" class="dropdown-item post-modal" href="#" data-id="<?= $postData->id; ?>">Edit</a>
                            <a id="post_delete" class="dropdown-item act-post" href="#" data-id="<?= $postData->id; ?>" data-act="delete">Delete</a>
                            <?php } else { ?>
                            <a id="post_save" class="dropdown-item act-post" href="#" data-id="<?= $postData->id; ?>" data-act="save">Save</a>
                            <a id="post_hide" class="dropdown-item act-post" href="#" data-id="<?= $postData->id; ?>" data-act="hide">Hide</a>
                            <a id="post_report" class="dropdown-item act-post" href="#" data-id="<?= $postData->id; ?>" data-act="report">Report</a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="card-text"><article><?= $postData->content; ?></article></div>
            </div>
            <div class="card-footer">
                <a id="post_reaction" href="#" class="card-link act-post" data-id="<?= $postData->id; ?>" data-act="like"><span id="icon" style=""><i class="far fa-thumbs-up"></i></span> Like</a>
                <a id="post_comment" href="#" class="card-link act-post" data-id="<?= $postData->id; ?>" data-act="get-comment"><span id="icon" style=""><i class="far fa-comment"></i></span> Comment</a>
                <!--a id="post_share" href="#" class="card-link act-post" data-id="<?= $postData->id; ?>" data-act="share"><span id="icon" style=""><i class="far fa-share-square"></i></span> Share</a-->
                <div id="post_commentBox"></div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    toggle_post();
</script>