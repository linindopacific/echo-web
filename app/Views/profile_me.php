<?php $faker = \Faker\Factory::create(); ?>

<div class="row">
    <div class="col-lg-3 order-1">
        <div class="card">
            <div class="card-body">
                <div class="h7"><img src="https://img.icons8.com/pastel-glyph/64/000000/worldwide-location--v1.png" style="height: 25px;"/><strong>  <?= $profile->city .', '. $profile->region; ?> </strong></div><br>
                <div class="h7"><strong>About :</strong> <?= $profile->about; ?>
                </div>
            </div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item">
                    <div class="h6 text-muted">Followers</div>
                    <div class="h5"><?= $faker->numberBetween($min=10, $max=500) ?></div>
                </li>
                <li class="list-group-item">
                    <div class="h6 text-muted">Following</div>
                    <div class="h5"><?= $faker->numberBetween($min=10, $max=500) ?></div>
                </li>
            </ul>
        </div>
        <div class="card">
            <div class="card-body">
                <h3 class="card-title">Latest Photos</h3>
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class=""></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1" class=""></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2" class="active"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item">
                            <img src="https://bootdey.com/img/Content/avatar/avatar2.png" class="d-block w-100" alt="...">
                        </div>
                        <div class="carousel-item">
                            <img src="https://bootdey.com/img/Content/avatar/avatar2.png" class="d-block w-100" alt="...">
                        </div>
                        <div class="carousel-item active">
                            <img src="https://bootdey.com/img/Content/avatar/avatar2.png" class="d-block w-100" alt="...">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 order-2 gedf-main">
        <div id="timeline_post"></div>
        <div id="no_more_data"></div>
    </div>
    <?php if($isMe): ?>
    <div class="col-lg-3 order-3">
        <div class="card social-timeline-card">
            <div class="card-body">
                <h5 class="card-title">People you may know</h5>
                <ul class="friend-list">
                    <?php $maxLoop = (count($mayYouKnow) > 10) ? 10 : count($mayYouKnow); ?>
                    <?php for ($i=0; $i < $maxLoop; $i++): ?>
                    <li>
                        <div class="left">
                            <img src="<?= $mayYouKnow[$i]->display_picture ?>" alt="">
                        </div>
                        <div class="right">
                            <h3><?= anchor(site_url('profile/' . $mayYouKnow[$i]->username),$mayYouKnow[$i]->display_name, ['class' => 'profile-link']); ?></h3>
                            <p><?= $mayYouKnow[$i]->total_connections ?> Friends</p>
                        </div>
                    </li>
                    <?php endfor; ?>
                </ul>
            </div>
            <!-- Post /////-->
        </div>
    </div>
    <?php endif; ?>
</div>
<script type="text/javascript">
    var action = 'inactive',
        uname = '<?= $profile->username; ?>',
        lId = '',
        no_more_data = false,
        is_empty = true;

    if(action == 'inactive')
    {
        make_skeleton_post();
        setTimeout(function(){
            load_timeline_data(uname);
        }, 3000);
    }

    $(window).scroll(function(){
        if(lId != '' && no_more_data != true)
        {
            if($(window).scrollTop() + $(window).height() > $("#timeline_post").height() && action == 'inactive' && $.ajaxq.isRunning('MyQueue') == false)
            {
                action = 'active';
                make_skeleton_post();
                setTimeout(function(){
                    load_timeline_data(uname, lId);
                }, 5000);
            }
        }
    });

    function load_timeline_data(uname, page='') {

        $.ajaxq ('MyQueue', {
            url: '<?= site_url('post/timeline') ?>',
            type: 'post',
            data: {uname: uname, page: page},
            dataType: 'json',
            beforeSend: function() {
            },
            complete:function() {
                $('.tmp_container').remove();
            },
            success:function(response) {
                if (response.is_empty) {
                    $('#timeline_post').append(response.html);
                }

                if (response.no_more_data) {
                    $('#no_more_data').html('<div id="container_row" class="row"> \
                                                <div class="col"> \
                                                    <div style="text-align: center;" class="alert alert-info">No more data found</div> \
                                                </div> \
                                            </div>');
                    no_more_data = response.no_more_data;
                } else {
                    $('#timeline_post').append(response.html);
                    lId = response.lId;
                }
            },
            error: function(xhr, textStatus, errorThrown) {
                var errors = xhr.responseJSON;
                if (xhr.status == 401) {
                    alert("Error: " + errorThrown);
                    window.location.replace(siteUrl);
                } else {
                    alert("Error: " + errors.message);
                }
            }
        });
    }
</script>