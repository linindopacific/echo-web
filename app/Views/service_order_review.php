<div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Ulas Pesanan</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?= form_open(route_to('order/review'), ['id' => 'fm_add_review']); ?>
        <div class="modal-body">
            <div class="form-group">
                <fieldset class="rating">
                    <input type="radio" id="star5" name="rating" value="5" /><label for="star5">5 stars</label>
                    <input type="radio" id="star4" name="rating" value="4" /><label for="star4">4 stars</label>
                    <input type="radio" id="star3" name="rating" value="3" /><label for="star3">3 stars</label>
                    <input type="radio" id="star2" name="rating" value="2" /><label for="star2">2 stars</label>
                    <input type="radio" id="star1" name="rating" value="1" /><label for="star1">1 star</label>
                </fieldset>
            </div>
            <div class="form-group">
                <textarea class="form-control" id="review-content" name="content"></textarea>
            </div>
            <div class="form-group text-right">
                <span id="maxContentReview"></span>
            </div>
        </div>
        <div class="modal-footer">
            <button id="share-review" type="button" class="btn btn-primary">
                <i class="fas fa-share-square"></i> Post</span>
            </button>
        </div>
        <?= form_close(); ?>
    </div>
</div>

<script>
    toggle_review();
</script>