<div class="row">
    <div class="col-md-10 m-2">
        <div class="list-group order-list-group" data-id="purchases">
            <?php foreach ($services as $row): ?>
                <a href="#" class="list-group-item list-group-item-action flex-column align-items-start" data-id="<?= $row->id; ?>">
                    <div class="d-flex w-100 justify-content-between">
                        <h5 class="mb-1"><?= $row->order_no; ?></h5>
                        <small class="text-muted"><?= $row->created_at->humanize(); ?></small>
                    </div>
                    <p class="mb-1"><?= $row->item; ?></p>
                </a>
            <?php endforeach; ?>
        </div>
        <!-- end row -->
        <?php $pager->setPath('services/purchase-orders', 'purchase-orders'); ?>
        <?= $pager->makeLinks($page, $perPage, $total, 'echo_orders', 0); ?>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        toogle_order_list();

        $('.pagination .page-item a').on('click', function (e) {
            e.preventDefault();
            var active_tab = $('#v-purchases-tab .active'),
                loadurl = $(this).attr('data-target'),
                targ = active_tab.attr('data-target'),
                data = {'type': active_tab.attr('data-type')};

            find_services(loadurl, data, targ);
        });
    });
</script>
