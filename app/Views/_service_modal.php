<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="serviceModalLabel"><?= (! $isNew) ? 'Update Service' : 'New Service'; ?></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div id="formError" class="alert alert-danger" role="alert" style="display: none;"></div>
            <?= form_open(route_to('services/store'), ['id' => $formId]); ?>
            <?= (! $isNew) ? form_hidden('service_id', $service->id) : ''; ?>
                <div class="form-group">
                    <label for="service-name" class="col-form-label"><?= lang('Rules.service_name') ?>:</label>
                    <input type="text" class="form-control" id="service-name" placeholder="Name of your service" name="service_name" value="<?= (! $isNew) ? $service->name : ''; ?>">
                </div>
                <div class="form-group">
                    <label for="service-category" class="col-form-label"><?= lang('Rules.service_category') ?>:</label>
                    <select id="service-category" class="form-control selectpicker" name="service_category" data-live-search="true" data-max-options="1" title="Category..." data-size="10" multiple>
                        <?php
                            foreach ($service_category as $r1):
                                if (! empty($r1['subcategory'])) {
                                    echo '<optgroup label="' . $r1['name'] . '">';
                                    foreach ($r1['subcategory'] as $r2) {
                                        $selected = (! $isNew) ? (($r2['id'] === $service->category_id) ? 'selected' : '') : '';
                                        echo '<option value="' . $r2['id'] . '" ' . $selected . '>' . $r2['name'] . '</option>';
                                    }
                                    echo '</optgroup>';
                                } else {
                                    $selected = (! $isNew) ? (($r1['id'] === $service->category_id) ? 'selected' : '') : '';
                                    echo '<option value="' . $r1['id'] . '" ' . $selected . '>' . $r1['name'] . '</option>';
                                }
                            endforeach;
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="service-coverage" class="col-form-label"><?= lang('Rules.service_coverage') ?>:</label>
                    <select id="service-coverage" class="form-control selectpicker" name="service_coverage[]" data-live-search="true" data-selected-text-format="count > 4" title="Coverage..." data-size="10" multiple>
                        <?php
                            $optgroup='';
                            foreach ($province_regency as $key1 => $value1):
                                foreach ($province_regency[$key1] as $key2 => $value2):
                                    if ($optgroup != $key1) {
                                        if ($optgroup != '') echo '</optgroup>';
                                        echo '<optgroup label="' . $key1 . '">';
                                    } else {
                                        $selected = (! $isNew) ? ((in_array($key2, $service->CoverageIds)) ? 'selected' : '') : '';
                                        echo '<option value="' . $key2 . '" ' . $selected . '>' . $value2 . '</option>';
                                    }
                                    $optgroup = $key1;
                                endforeach;
                            endforeach;
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="service-description" class="col-form-label"><?= lang('Rules.service_description') ?>:</label>
                    <textarea class="form-control" id="service-description" name="service_description"><?= (! $isNew) ? $service->description : ''; ?></textarea>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <!--label for="service-price">Price</label-->
                        <input type="number" min="10000" class="form-control" id="service-price" placeholder="<?= lang('Rules.service_price') ?>" name="service_price" value="<?= (! $isNew) ? $service->price : ''; ?>">
                    </div>
                    <div class="form-group col-md-2">
                        <!--label for="service-pricePer">Price Per</label-->
                        <select id="service-pricePer" class="form-control selectpicker" name="service_pricePer">
                            <option disabled value="">Price Per...</option>
                            <option value="hour" <?= (! $isNew) ? (($service->price_per === 'hour') ? 'selected' : '') : ''; ?>>Per Hour</option>
                            <option value="day" <?= (! $isNew) ? (($service->price_per === 'day') ? 'selected' : '') : ''; ?>>Per Day</option>
                            <option value="event" <?= (! $isNew) ? (($service->price_per === 'event') ? 'selected' : '') : ''; ?>>Per Event</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="checked" id="service-pinned" name="service_pinned" <?= (! $isNew) ? (($service->is_pinned) ? 'checked' : '') : '' ?>>
                        <label class="form-check-label" for="service-pinned">
                            Create Shortcut
                        </label>
                        <small id="pinnedHelp" class="form-text text-muted">This service will be pinned on your homepage. (Only one service will be pinned).</small>
                    </div>
                </div>
            <?= form_close(); ?>
        </div>
        <div class="modal-footer">
            <button id="save-service" type="button" class="btn btn-secondary">
                <i class="fas fa-save"></i> Save to Draft
            </button>
            <button id="publish-service" type="button" class="btn btn-primary">
                <i class="fas fa-paper-plane"></i> Publish
            </button>
        </div>
    </div>
</div>