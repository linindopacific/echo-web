<?php foreach ($posts as $postData): ?>
    <div id="container_row" class="row post_<?= $postData->id ?>">
        <div class="col">
            <?= view('_posts', ['postData' => $postData]) ?>
        </div>
    </div>
<?php endforeach; ?>

<script type="text/javascript">
    toggle_post();
</script>