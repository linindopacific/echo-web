<?php
$_id = 0;
foreach ($data as $row):
    if ($row->user_id === user_id())
    {
?>
        <div class="msg outgoing_msg">
            <div class="sent_msg">
                <p><?= $row->content; ?></p>
                <span class="time_date"><?= $row->time_string . ' | ' . $row->date_string; ?></span>
            </div>
        </div>
<?php
    }
    else
    {
?>
        <div class="msg incoming_msg">
            <div class="incoming_msg_img"> <img src="<?= $row->sender_picture; ?>" alt="<?= $row->sender_name; ?>" class="rounded-circle" style="visibility: <?= ($_id <> $row->user_id) ? 'visible' : 'hidden'; ?>;"> </div>
            <div class="received_msg">
                <div class="received_withd_msg">
                    <p><?= $row->content; ?></p>
                    <span class="time_date"><?= $row->time_string . ' | ' . $row->date_string; ?></span>
                </div>
            </div>
        </div>
<?php
    }

    $_id = $row->user_id;
endforeach;
?>