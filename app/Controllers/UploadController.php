<?php namespace App\Controllers;

/*
 * File: UploadController.php
 * Project: echo
 * File Created: Thursday, 5th November 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Tuesday, 16th November 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use App\Entities\Upload;
use App\Models\UploadModel;
use CodeIgniter\Files\File;
use CodeIgniter\HTTP\Response;
use CodeIgniter\HTTP\URI;

class UploadController extends BaseController
{
    public function attemptDelete()
    {
        if ( ! $this->request->isAJAX() ) {
			exit("No direct script access allowed");
        }

        if ( empty(user()) ) {
			return $this->response->setStatusCode(Response::HTTP_UNAUTHORIZED)->setJSON(["message" => "session_expired"]);
        }

        $src = $this->request->getPost("src");
        $uri = new URI($src);

        $currentUri = $uri->getScheme() . "://" . $uri->getHost();
        $status = Response::HTTP_BAD_REQUEST;
        $message = "Gagal menghapus file";
        if ($currentUri === base_url()) {
            $paths = explode("/", $uri->getPath());
            $fileName = array_pop($paths); // pop the last element and set to filename
            array_shift($paths); $filePath = implode("/", $paths); // pop the first element and set to filepath

            $uploadModel = new UploadModel();
            $_getData = $uploadModel->where("file_name", $fileName)->where("file_path", $filePath)->where("user_id", user_id())->first();
            if (! empty($_getData))
            {
                $fileId = $_getData->id;
                if ( $uploadModel->delete($fileId) )
                {
                    if( unlink($filePath . "/" . $fileName) )
                    {
                        $status = Response::HTTP_OK;
                        $message = "Berhasil menghapus file";
                    }
                    else
                    {
                        $status = Response::HTTP_BAD_REQUEST;
                        $message = "Gagal menghapus file";
                        log_message("error", $message);
                    }
                }
                else
                {
                    $status = Response::HTTP_BAD_REQUEST;
                    $message = "Gagal menghapus file";
                    log_message("error", $message);
                }
            }
        }

        log_message("error", $currentUri . " " . base_url());

        return $this->response->setStatusCode($status)->setJSON(["message" => $message]);
    }

    public function attemptStore()
    {
        if ( ! $this->request->isAJAX() ) {
			exit("No direct script access allowed");
        }

        if ( empty(user()) ) {
			return $this->response->setStatusCode(Response::HTTP_UNAUTHORIZED)->setJSON(["message" => "session_expired"]);
        }

        $status = Response::HTTP_BAD_REQUEST;
        $message = "[ERROR]";
        if($imagefile = $this->request->getFiles())
        {
            $img = $imagefile["image"];
            if ($img->isValid() && ! $img->hasMoved())
            {
                $uploadModel = new UploadModel();
				$originalName = $img->getClientName();
				$newName = $img->getRandomName();
                $filePath = "assets/files/uploads/images";
                $img->move(WRITEPATH."uploads", $originalName);
                if ($img->hasMoved()) // berhasil di move
                {
                    try
					{
                        \Config\Services::image()
							->withFile(WRITEPATH."uploads/".$originalName)
                            ->save($filePath."/".$newName);

						// get file properties
						$info = \Config\Services::image()
							->withFile($filePath . "/" . $newName)
							->getFile()
							->getProperties(true);

						$resizedFile = new File($filePath."/".$newName);

						$upload = new Upload();
						$upload->file_name = $newName;
						$upload->file_type = $resizedFile->getMimeType();
						$upload->file_path = $filePath;
						$upload->raw_name = explode(".", $newName)[0];
						$upload->orig_name = $originalName;
						$upload->file_ext = $resizedFile->guessExtension();;
						$upload->is_image = TRUE;
						$upload->file_size = $resizedFile->getSizeByUnit();
						$upload->image_width = $info["width"];
						$upload->image_height = $info["height"];
						$upload->image_type = $resizedFile->getMimeType();
						$upload->user_id = user_id();
						if ($uploadModel->save($upload))
						{
                            $status = Response::HTTP_OK;
                            $message = base_url($filePath . "/" . $newName);
						}
						else
						{
                            $status = Response::HTTP_BAD_REQUEST;
                            $message = "Gagal menyimpan file ke dalam database";
                            log_message("error", $message, $uploadModel->errors());
						}
					}
					catch (\CodeIgniter\Images\Exceptions\ImageException $e)
					{
                        $status = Response::HTTP_BAD_REQUEST;
                        $message = "".$e->getMessage();
                        log_message("error", $message);
                    }
                }
                else
                {
                    $status = Response::HTTP_BAD_REQUEST;
                    $message = "Gagal mengunggah file";
                    log_message("error", $message);
                }
            }
        }

        return $this->response->setStatusCode($status)->setJSON(["message" => $message]);
    }
}