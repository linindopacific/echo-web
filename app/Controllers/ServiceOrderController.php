<?php namespace App\Controllers;

/*
 * File: ServiceOrderController.php
 * Project: echo
 * File Created: Wednesday, 25th November 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 23rd August 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use App\Entities\ServiceOrder;
use App\Entities\ServiceInvoice;
use App\Libraries\XenditLibrary;
use App\Models\ServiceModel;
use App\Models\ServiceOrderModel;
use App\Models\ServiceInvoiceModel;
use CodeIgniter\HTTP\URI;

class ServiceOrderController extends BaseController
{
    use \CodeIgniter\API\ResponseTrait;

    public function orderLookup()
    {
        if ( empty(user()) ) {
			return $this->failUnauthorized();
        }

        $serviceOrderId = $this->request->getGet('id');
        $source = $this->request->getGet('source');

        if ( is_null($serviceOrderId) )
        {
            return $this->fail('Invalid order.', 404, 'INVALID_ORDER_ID');
        }
        else
        {
            $serviceOrderModel = new ServiceOrderModel();
            $serviceOrder = $serviceOrderModel->find($serviceOrderId);

            if (! empty($serviceOrder))
            {
                if ($source === 'purchases')
                {
                    return $this->respond(['html' => view('service_order_purchases_content_detail', ['id' => $serviceOrderId, 'data' => $serviceOrder])]);
                }
                elseif ($source === 'sales')
                {
                    return $this->respond(['html' => view('service_order_sales_content_detail', ['id' => $serviceOrderId, 'data' => $serviceOrder])]);
                }
                else
                {
                    return $this->fail('Invalid source type.', 400, 'INVALID_SOURCE_TYPE');
                }
            }
            else
            {
                return $this->fail('Invalid order.', 404, 'INVALID_ORDER_ID');
            }
        }

    }

    public function serviceLookup()
    {
        $serviceId = $this->request->getGet('id');

        if ( is_null($serviceId) )
        {
            $redirectURL = session('redirect_url') ?? site_url('services');
			unset($_SESSION['redirect_url']);

			return redirect()->to($redirectURL);
        }

        $serviceModel = new ServiceModel();
        $service = $serviceModel->is_ready($serviceId);

        if (! empty($service))
        {
            if ($service->is_owner)
            {
                log_message('error', 'owned_service');
                throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
            }

            if ($service->is_owner_profile_complete === FALSE)
            {
                log_message('error', 'empty_profile');
                throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
            }

            return view('service_checkout', [
				'title' => 'Checkout',
				'active_service' => 'active',
				'viewLayout' => 'App\Views\Template\layout',
				'service' => $service
			]);
        }
        else
        {
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }
    }

    public function attemptOrder()
    {
        $agent = $this->request->getUserAgent();
        $isMobile = $agent->isMobile();

        $serviceModel = new ServiceModel();
        $serviceOrderModel = new ServiceOrderModel();

        $orderNo = $serviceOrderModel->create_no_series();
        $paymentChannel = $this->request->getPost('payment_channel');
        $paymentCategory = $this->request->getPost('payment_category');
        $totalAmount = $this->request->getPost('total_amount');
        $itemId = $this->request->getPost('item_id');
        $itemName = $this->request->getPost('item_name');
        $itemAmount = $this->request->getPost('item_amount');
        $requestDate = $this->request->getPost('request_datetime');
        $marketFee = 20;

        $customerFullName = user()->display_name;
        $customerPhoneNumber = user_profile()->phone_number;
        $customerAddress = $this->request->getPost('address');
        $customerCity = $this->request->getPost('city');

        $service = $serviceModel->find($itemId);
        $description = $service->description;
        $sellerFullName = $service->owner;
        $sellerPhoneNumber = $service->owner_contact;
        $sellerAddress = $service->owner_address;
        $sellerCity = $service->owner_city;
        $itemCategory = $service->category;

        $data = [
            'order_no' => $orderNo,
            'buy_from_name' => $sellerFullName,
            'buy_from_address' => $sellerAddress,
            'buy_from_city' => $sellerCity,
            'buy_from_phone_no' => $sellerPhoneNumber,
            'sell_to_name' => $customerFullName,
            'sell_to_address' => $customerAddress,
            'sell_to_city' => $customerCity,
            'sell_to_phone_no' => $customerPhoneNumber,
            'request_date' => $requestDate,
            'service_id' => $itemId,
            'item' => $itemName,
            'description' => $description,
            'quantity' => 1,
            'unit_price' => $itemAmount,
            'amount' => $itemAmount,
            'market_fee' => $marketFee,
            'user_id' => user_id()
        ];
        $validation =  \Config\Services::validation();
		$rules = $serviceOrderModel->getValidationRules();
		$validationResult = $validation->setRules($rules)->run($data);
		if (! $validationResult)
		{
			$validationErrors = $validation->getErrors();
			log_message('error', 'User {uid} createOrder on Service {id} ' . implode(' ', $validationErrors), ['uid' => user_id(), 'id' => $itemId]);
            session()->setFlashData('error', implode(' ', $validationErrors));

            $uri = new URI(site_url('services/order'));
            $redirectURL = $uri->setQuery("id=$itemId");
            return redirect()->to($redirectURL);
		}
        else
        {
            $xendit = new XenditLibrary();
            $params = [
                'order_no' => $orderNo,
                'payment_channel' => $paymentChannel,
                'customer_name' => $customerFullName,
                'customer_phone_no' => $customerPhoneNumber,
                'total_amount' => $totalAmount,
                'description' => 'Payment ' . $itemName,
                'service_id' => $itemId,
                'service_name' => $itemName,
                'service_category' => $itemCategory
            ];

            $response = $xendit->inquiry($paymentCategory, $params);

            if ( $response['status'] == false )
            {
                log_message('error', '[Xendit] {message} - User {id} gagal membuat pesanan dengan metode pembayaran {channel} dari {ip_address} {params}', ['message' => $response['message'], 'id' => user_id(), 'channel' => $params['payment_channel'], 'ip_address' => $this->request->getIPAddress(), 'params' => print_r($params, true)]);

                session()->setFlashData('error', 'Gagal membuat pesanan dengan metode pembayaran tersebut.');

                $uri = new URI(site_url('services/order'));
                $redirectURL = $uri->setQuery("id=$itemId");
                return redirect()->to($redirectURL);
            }
            else
            {
                $responseData = $response['data'];
                $serviceOrder = new ServiceOrder($data);

                if (! $serviceOrderModel->save($serviceOrder))
                {
                    $errors = $serviceOrderModel->errors();
                    log_message('error', implode(' ', $errors));
                    session()->setFlashData('error', implode(' ', $errors));

                    $uri = new URI(site_url('services/order'));
                    $redirectURL = $uri->setQuery("id=$itemId");
                    return redirect()->to($redirectURL);
                }
                else
                {
                    $orderId = $serviceOrderModel->getInsertID();

                    $serviceInvoiceModel = new ServiceInvoiceModel();
                    $serviceInvoice = new ServiceInvoice();
                    $serviceInvoice->order_id = $orderId;
                    $serviceInvoice->xnd_id = $responseData['id'];
                    $serviceInvoice->external_id = $orderNo;
                    $serviceInvoice->amount = $responseData['amount'];
                    $serviceInvoice->payment_category = $paymentCategory;
                    $serviceInvoice->payment_channel = $paymentChannel;
                    $serviceInvoice->payment_code = $responseData['payment_code'] ?? NULL;
                    $serviceInvoice->qr_string = $responseData['qr_string'] ?? NULL;
                    $serviceInvoice->expired_at = $responseData['expired_at'];

                    if (! $serviceInvoiceModel->save($serviceInvoice))
                    {
                        log_message('error', implode(' ', $serviceInvoiceModel->errors()));

                        // cancel fva if exists
                        $serviceOrderModel->delete($orderId);
                        $xendit = new XenditLibrary();
                        $xendit->setToExpire($responseData);

                        $uri = new URI(site_url('services/order'));
                        $redirectURL = $uri->setQuery("id=$itemId");
                        return redirect()->to($redirectURL);
                    }
                    else
                    {
                        $is_redirect_required = $responseData['is_redirect_required'];
                        if ($is_redirect_required)
                        {
                            $uri = ( $isMobile ) ? new URI($responseData['mobile_web_checkout_url']) : new URI($responseData['desktop_web_checkout_url']);
                            return redirect()->to($uri);
                        }
                        else
                        {
                            $invoiceId = $serviceInvoiceModel->getInsertID();

                            $uri = new URI(site_url('services/payment'));
                            $redirectURL = $uri->setQuery("id=$invoiceId");

                            return redirect()->to($redirectURL);
                        }
                    }
                }
            }
        }
    }

    public function ordersLookup()
    {
        return view('service_orders', [
			'title' => 'Daftar Pesanan',
			'active_orders' => 'active',
			'viewLayout' => 'App\Views\Template\layout',
		]);
    }

    public function orders()
    {
		if ( empty(user()) ) {
            return $this->failUnauthorized();
        }

        $source = $this->request->getPost('id');

        if ($source === 'purchases')
        {
            return $this->respond(['html' => view('service_order_purchases')]);
        }
        elseif ($source === 'sales')
        {
            return $this->respond(['html' => view('service_order_sales')]);
        }
        else
        {
            return $this->fail('Invalid source type.', 400, 'INVALID_SOURCE_TYPE');
        }
    }

    public function purchaseOrders()
    {
		if ( empty(user()) ) {
			return $this->failUnauthorized();
        }

        $page = $this->request->getGet('page');
        $type = $this->request->getPost('type');

        if (! $this->validate([
            'page' => 'permit_empty|integer',
            'type' => 'required|string'
        ]))
        {
            return $this->failValidationErrors($this->validator->getErrors());
        }

        // Available Service Order perPage
		$services_page = $page ?? 1;
		$services_perPage = 8;
		$offset = ($services_page == 1) ? 0 : (($services_page-1) * $services_perPage);

        $serviceOrderModel = new ServiceOrderModel();
		$serviceOrders = $serviceOrderModel->getOrders($type, $services_perPage, $offset);
		$serviceOrders_total = $serviceOrderModel->countOrders($type);

        $pager = service('pager');

		return $this->respond(['html' => view('service_order_purchases_content', [
            'services' => $serviceOrders,
            'type' => $type,
			'page' => $services_page,
			'perPage' => $services_perPage,
			'total' => $serviceOrders_total,
			'pager' => $pager
		])]);
    }

    public function salesOrders()
    {
		if ( empty(user()) ) {
            return $this->failUnauthorized();
        }

        $page = $this->request->getGet('page');
        $type = $this->request->getPost('type');

        if (! $this->validate([
            'page' => 'permit_empty|integer',
            'type' => 'required|string'
        ]))
        {
            return $this->failValidationErrors($this->validator->getErrors());
        }

        // Available Service Order perPage
		$services_page = $page ?? 1;
		$services_perPage = 8;
		$offset = ($services_page == 1) ? 0 : (($services_page-1) * $services_perPage);

        $serviceOrderModel = new ServiceOrderModel();
		$serviceOrders = $serviceOrderModel->getSales($type, $services_perPage, $offset);
		$serviceOrders_total = $serviceOrderModel->countSales($type);

        $pager = service('pager');

		return $this->respond(['html' => view('service_order_sales_content', [
            'services' => $serviceOrders,
            'type' => $type,
			'page' => $services_page,
			'perPage' => $services_perPage,
			'total' => $serviceOrders_total,
			'pager' => $pager
		])]);
    }
}