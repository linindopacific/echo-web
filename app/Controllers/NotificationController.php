<?php namespace App\Controllers;

/*
 * File: NotificationController.php
 * Project: echo
 * File Created: Monday, 5th April 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Thursday, 22nd April 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use App\Models\NotificationsModel;
use CodeIgniter\HTTP\Response;

class NotificationController extends BaseController
{
    public function getNotification()
    {
        if ( ! $this->request->isAJAX() ) {
			exit('No direct script access allowed');
        }

        if ( empty(user()) ) {
            return $this->response->setStatusCode(Response::HTTP_UNAUTHORIZED)->setJSON(['message' => 'session_expired']);
        }

        $notificationModel = new NotificationsModel();
		$notification = $notificationModel->fetchNotification();

        return $this->response->setStatusCode(Response::HTTP_OK)->setJSON(['data' => view('_notifications', ['data' => $notification])]);
    }
}