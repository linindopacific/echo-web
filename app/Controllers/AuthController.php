<?php namespace App\Controllers;

/*
 * File: AuthController.php
 * Project: echo
 * File Created: Wednesday, 19th August 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Friday, 16th July 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use App\Entities\UserProfile;
use App\Models\UserModel;
use App\Models\UserProfileModel;
use CodeIgniter\Events\Events;
use Myth\Auth\Controllers\AuthController as MythAuthController;

class AuthController extends MythAuthController
{
	public function __construct()
	{
		parent::__construct();

		helper(['html', 'form']);

		service('language')->setLocale('id');
	}

	private function currentAgent($request) : string
	{
		$agent = $request->getUserAgent();
		if ($agent->isBrowser())
		{
			$currentAgent = $agent->getBrowser().' '.$agent->getVersion();
		}
		elseif ($agent->isRobot())
		{
			$currentAgent = $this->agent->robot();
		}
		elseif ($agent->isMobile())
		{
			$currentAgent = $agent->getMobile();
		}
		else
		{
			$currentAgent = 'Unidentified User Agent';
		}

		return $currentAgent;
	}

	/**
	 * AJAX endpoint for successful Firebase login.
	 * Handles registration (if necessary) and logging in.
	 */
	public function callback()
	{
		if ($this->request->isAJAX()) {

			log_message('debug', 'Initiating Firebase login request');

			// Parse the data
			$data = $this->request->getJSON();
			if (! $result = $data)
			{
				log_message('error', 'Invalid JSON data in Firebase request');
				return;
			}

			$entity = $data->user;
			unset($data);

			log_message('debug', 'Firebase response {uid} - {name} - {email} - {phone_no} - {last_login} ', ['uid' => $entity->uid, 'name' => $entity->displayName, 'email' => $entity->email, 'phone_no' => $entity->phoneNumber, 'last_login' => $entity->lastLoginAt]);

			// Verify required fields
			// Myth:Auth currently requires all these - this may change in the future
			if (empty($entity->email) || empty($entity->uid))
			{
				log_message('error', 'Required fields missing from result: ' . json_encode($entity));
				return;
			}

			// Convert it to Myth:Auth format
			$displayName = $entity->displayName ?? '';
			$row = [
				'email' => $entity->email,
				'username' => $entity->uid,
				'password_hash' => bin2hex(random_bytes(16)), // this will prevent local logins
				'display_name' => $displayName,
				'created_at' => date('Y-m-d H:i:s', $entity->createdAt / 1000),
			];

			$firebase_emailVerified = $entity->emailVerified;
			unset($entity);

			// Try to match a user
			$users = model(UserModel::class);
			$profiles = model(UserProfileModel::class);
			$user = $users
				->where('email', $row['email'])
				->orWhere('username', $row['username'])
				->first();

			$requireActivation = false;
			$activationHash = null;
			// If no user was found then register a new one
			if (empty($user))
			{
				$user = new User($row);

				if ($firebase_emailVerified) {
					$user->activate();
				} else {
					$this->config->requireActivation !== false ? $user->generateActivateHash() : $user->activate();
				}

				// Ensure default group gets assigned if set
		        if (! empty($this->config->defaultUserGroup)) {
		            $users = $users->withGroup($this->config->defaultUserGroup);
		        }

		        // insert user
				if (! $id = $users->insert($user))
				{
					log_message("error", "User {email} unable to registered: " . implode(". ", $users->errors()), ["email" => $user->email]);
					return;
				}
				else
				{
					$user = $users->find($id);
					if ($this->config->requireActivation !== false)
					{
						if ( !$firebase_emailVerified ) {
							$requireActivation = true;
							$activationHash = $user->activate_hash;
						}
					}
					log_message("info", "User {email} has been successfully registered from {ip_address} with {user_agent}", ["email" => $user->email, "ip_address" => $this->request->getIPAddress(), "user_agent" => $this->currentAgent($this->request)]);

					// create profile
					$profile = new UserProfile(['first_name' => $displayName,'user_id' => $id, 'created_at' => $row['created_at']]);
					if ( !$profiles->insert($profile) )
					{
						log_message("error", "User profile {email} unable to updated: " . implode(". ", $profiles->errors()), ["email" => $user->email]);
						return;
					}
					else
					{
						log_message("info", "User profile {email} has been updated from {ip_address} with {user_agent}", ["email" => $user->email, "ip_address" => $this->request->getIPAddress(), "user_agent" => $this->currentAgent($this->request)]);
					}
				}
			}
			// Otherwise update with the latest info from Firebase
			{
				if ($firebase_emailVerified) {
					$user->activate();
				}
				$user->password_hash =  bin2hex(random_bytes(16));

				if ( !$users->save($user) )
				{
					log_message("error", "User {email} unable to updated: " . implode(". ", $users->errors()), ["email" => $user->email]);
				}
			}

			$isActivated = $user->isActivated();
			if ( $isActivated )
			{
				// Log the user in
				$auth = service('authentication');
				if ( $auth->login($user, true) )
				{
					log_message('info', 'User {email} has been successfully logged in from {ip_address} with {user_agent}', ["email" => $user->email, "ip_address" => $this->request->getIPAddress(), "user_agent" => $this->currentAgent($this->request)]);

					// Check for a new user
					if (! empty($result->additionalUserInfo->isNewUser))
					{
						// Trigger the event
						Events::trigger('firebase_new_user', $result->user);
					}
				}
				else
				{
					log_message('info', 'Login failed for user {email} - {message}', ["email" => $user->email, "message" => $auth->error() ?? lang('Auth.badAttempt')]);
				}
			}
			else
			{
				log_message('info', 'Login failed for user {email} - {message}', ["email" => $user->email, "message" => lang('Auth.notActivated')]);
				$user = null;
			}

			return $this->response->setJSON(['requireActivation' => $requireActivation, 'activationHash' => $activationHash, 'isActive' => $isActivated]);
		}
	}

	//--------------------------------------------------------------------
	// Login/out
	//--------------------------------------------------------------------

	/**
	 * Displays the login form, or redirects
	 * the user to their destination/home if
	 * they are already logged in.
	 */
	public function login()
	{
		// No need to show a login form if the user
		// is already logged in.
		if ($this->auth->check())
		{
			$redirectURL = session('redirect_url') ?? site_url('/');
			unset($_SESSION['redirect_url']);

			return redirect()->to($redirectURL);
		}

        // Set a return URL if none is specified
        $_SESSION['redirect_url'] = session('redirect_url') ?? previous_url() ?? site_url('/');

		return view($this->config->views['login'], [
			'bodyClass'  => 'sign-in',
			'viewLayout' => $this->config->viewLayout,
			'validFields' => $this->config->validFields,
			'allowRemembering' => $this->config->allowRemembering,
			'allowRegistration' => $this->config->allowRegistration,
			'activeResetter' => $this->config->activeResetter
		]);
	}

	//--------------------------------------------------------------------
	// Register
	//--------------------------------------------------------------------

	/**
	 * Displays the user registration page.
	 */
	public function register()
	{
        // check if already logged in.
		if ($this->auth->check())
		{
			return redirect()->back();
		}

        // Check if registration is allowed
		if (! $this->config->allowRegistration)
		{
			return redirect()->back()->withInput()->with('error', lang('Auth.registerDisabled'));
		}

		return view($this->config->views['register'], [
			'bodyClass'  => 'sign-in',
			'viewLayout' => $this->config->viewLayout
		]);
	}

	/**
	 * Attempt to register a new user.
	 */
	public function attemptRegister()
	{
		// Check if registration is allowed
		if (! $this->config->allowRegistration)
		{
			return redirect()->back()->withInput()->with('error', lang('Auth.registerDisabled'));
		}

		$users = model(UserModel::class);

		// Validate here first, since some things,
		// like the password, can only be validated properly here.
		$rules = [
			//'username'  	=> 'required|alpha_numeric_space|min_length[3]|is_unique[users.username]',
			'username'  	=> 'required',
			'email'			=> 'required|valid_email|is_unique[users.email]',
			'password'	 	=> 'required|strong_password',
			'pass_confirm' 	=> 'required|matches[password]',
			'cc'			=> 'required'
		];

		// Error messages samples
		$messages = [
	        'username' => [
	            'required' => 'All accounts must have usernames provided',
	        ],
	        'email' => [
	        	'is_unique' => 'The email has already been registered',
	        ],
	    ];

		if (! $this->validate($rules, $messages))
		{
			return redirect()->back()->withInput()->with('errors', service('validation')->getErrors());
		}

		// Save the user
		$allowedPostFields = array_merge(['password'], $this->config->validFields, $this->config->personalFields);
		$user = new User($this->request->getPost($allowedPostFields));

		$this->config->requireActivation !== false ? $user->generateActivateHash() : $user->activate();

		// Ensure default group gets assigned if set
        if (! empty($this->config->defaultUserGroup)) {
            $users = $users->withGroup($this->config->defaultUserGroup);
        }

		if (! $users->save($user))
		{
			return redirect()->back()->withInput()->with('errors', $users->errors());
		}
		else
		{
			log_message("info", "User {email} has been successfully registered from {ip_address} with {user_agent}", ["email" => $this->request->getPost('email'), "ip_address" => $this->request->getIPAddress(), "user_agent" => $this->currentAgent($this->request)]);
		}

		if ($this->config->requireActivation !== false)
		{
			$activator = service('activator');
			$sent = $activator->send($user);

			if (! $sent)
			{
				return redirect()->back()->withInput()->with('error', $activator->error() ?? lang('Auth.unknownError'));
			}

			// Success!
			return redirect()->route('login')->with('message', lang('Auth.activationSuccess'));
		}

		// Success!
		return redirect()->route('login')->with('message', lang('Auth.registerSuccess'));
	}

	//--------------------------------------------------------------------
	// Forgot Password
	//--------------------------------------------------------------------

	/**
	 * Displays the forgot password form.
	 */
	public function forgotPassword()
	{
		if ($this->config->activeResetter === false)
		{
			return redirect()->route('login')->with('error', lang('Auth.forgotDisabled'));
		}

		return view($this->config->views['forgot'], [
			'bodyClass'  => 'sign-in',
			'viewLayout' => $this->config->viewLayout,
			'allowRegistration' => $this->config->allowRegistration
		]);
	}

	/**
	 * Displays the Reset Password form.
	 */
	public function resetPassword()
	{
		if ($this->config->activeResetter === false)
		{
			return redirect()->route('login')->with('error', lang('Auth.forgotDisabled'));
		}

		$token = $this->request->getGet('token');

		return view($this->config->views['reset'], [
			'bodyClass'  => 'sign-in',
			'viewLayout' => $this->config->viewLayout,
			'allowRegistration' => $this->config->allowRegistration,
			'token'  => $token,
		]);
	}

	/**
	 * Verifies the code with the email and saves the new password,
	 * if they all pass validation.
	 *
	 * @return mixed
	 */
	public function attemptReset()
	{
		if ($this->config->activeResetter === false)
		{
			return redirect()->route('login')->with('error', lang('Auth.forgotDisabled'));
		}

		$users = model(UserModel::class);

		// First things first - log the reset attempt.
		$users->logResetAttempt(
			$this->request->getPost('email'),
			$this->request->getPost('token'),
			$this->request->getIPAddress(),
			(string)$this->request->getUserAgent()
		);

		$rules = [
			'token'		=> 'required',
			'email'		=> 'required|valid_email',
			'password'	 => 'required|strong_password',
			'pass_confirm' => 'required|matches[password]',
		];

		if (! $this->validate($rules))
		{
			return redirect()->back()->withInput()->with('errors', $users->errors());
		}

		$user = $users->where('email', $this->request->getPost('email'))
					  ->where('reset_hash', $this->request->getPost('token'))
					  ->first();

		if (is_null($user))
		{
			log_message("info", "An unregistered user {email} has tried to change the password from {ip_address}", ["email" => $this->request->getPost('email'), "ip_address" => $this->request->getIPAddress()]);
			return redirect()->back()->with('error', lang('Auth.forgotNoUser'));
		}

        // Reset token still valid?
        if (! empty($user->reset_expires) && time() > $user->reset_expires->getTimestamp())
        {
			log_message("info", "User {email} has tried to change the password using an expired token from {ip_address}", ["email" => $this->request->getPost('email'), "ip_address" => $this->request->getIPAddress()]);
            return redirect()->back()->withInput()->with('error', lang('Auth.resetTokenExpired'));
        }

		// Success! Save the new password, and cleanup the reset hash.
		$user->password 		= $this->request->getPost('password');
		$user->reset_hash 		= null;
		$user->reset_at 		= date('Y-m-d H:i:s');
		$user->reset_expires    = null;
        $user->force_pass_reset = false;
		if ( $users->save($user) )
		{
			log_message("info", "User {email} successfully changed password from {ip_address}", ["email" => $user->email, "ip_address" => $this->request->getIPAddress()]);
		}

		return redirect()->route('login')->with('message', lang('Auth.resetSuccess'));
	}

	/**
	 * Activate account.
	 *
	 * @return mixed
	 */
	public function activateAccount()
	{
		$users = model(UserModel::class);

		// First things first - log the activation attempt.
		$users->logActivationAttempt(
			$this->request->getGet('token'),
			$this->request->getIPAddress(),
			$this->currentAgent($this->request)
		);

		$throttler = service('throttler');

		if ($throttler->check($this->request->getIPAddress(), 2, MINUTE) === false)
        {
			return service('response')->setStatusCode(429)->setBody(lang('Auth.tooManyRequests', [$throttler->getTokentime()]));
        }

		$user = $users->where('activate_hash', $this->request->getGet('token'))
					  ->where('active', 0)
					  ->first();

		if (is_null($user))
		{
			return redirect()->route('login')->with('error', lang('Auth.activationNoUser'));
		}

		$user->activate();
		if ( $users->save($user) )
		{
			log_message("info", "User {email} has been activated from {ip_address}", ["email" => $user->email, "ip_address" => $this->request->getIPAddress()]);
		}

		return redirect()->route('login')->with('message', lang('Auth.registerSuccess'));
	}
}
