<?php namespace App\Controllers;

/*
 * File: ServiceReviewController.php
 * Project: echo
 * File Created: Wednesday, 17th March 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Wednesday, 31st March 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use App\Entities\ServiceReview;
use App\Models\ServiceFulfillmentModel;
use App\Models\ServiceOrderModel;
use App\Models\ServiceReviewModel;
use CodeIgniter\HTTP\Response;

class ServiceReviewController extends BaseController
{
    public function orderReview()
    {
        if ( !$this->request->isAJAX() ) {
			exit('No direct script access allowed');
		}

        if ( empty(user()) ) {
			return $this->response->setStatusCode(Response::HTTP_UNAUTHORIZED)->setJSON(['message' => 'session_expired']);
        }

        return $this->response->setStatusCode(Response::HTTP_OK)->setJSON(['data' => view('service_order_review')]);
    }

    public function attemptReview()
    {
        if ( !$this->request->isAJAX() ) {
			exit('No direct script access allowed');
		}

        if ( empty(user()) ) {
			return $this->response->setStatusCode(Response::HTTP_UNAUTHORIZED)->setJSON(['message' => 'session_expired']);
        }

        $orderId = $this->request->getGet('id');
        // 1st check order user_id
        $serviceOrderModel = new ServiceOrderModel();
        $serviceOrder = $serviceOrderModel->is_order_belong_to_me($orderId);
        if ( !$serviceOrder ) {
            return $this->response->setStatusCode(Response::HTTP_UNAUTHORIZED)->setJSON(['message' => 'unauthorized']);
        }

        $serviceOrder = $serviceOrderModel->find($orderId);
        if ( $serviceOrder->has_review ) {
            return $this->response->setStatusCode(Response::HTTP_BAD_REQUEST)->setJSON(['message' => 'not_allowed']);
        }

        $data = [
            'content' => $this->request->getPost('content'),
            'rating' => $this->request->getPost('rating')
        ];
        $validation =  \Config\Services::validation();
        $serviceReviewModel = new ServiceReviewModel();
		$rules = $serviceReviewModel->getValidationRules(['except' => ['fulfillment_id', 'user_id']]);
		$validationResult = $validation->setRules($rules)->run($data);
		if ( !$validationResult )
		{
            return $this->response->setStatusCode(Response::HTTP_BAD_REQUEST)->setJSON(['message' => implode(' ', $validation->getErrors())]);
		}
        else
        {
            $serviceFulfillmentModel = new ServiceFulfillmentModel();
            $serviceFulfillment = $serviceFulfillmentModel->findByOrderId($orderId);

            if ( empty($serviceFulfillment) )
            {
                return $this->response->setStatusCode(Response::HTTP_NOT_FOUND)->setJSON(['message' => 'not_found']);
            }

            $fulfillmentId = $serviceFulfillment->id;
            $data['fulfillment_id'] = $fulfillmentId;
            $data['user_id'] = user_id();

            $serviceReview = new ServiceReview($data);
            if ( !$serviceReviewModel->save($serviceReview) )
            {
                return $this->response->setStatusCode(Response::HTTP_BAD_REQUEST)->setJSON(['message' => implode(' ', $serviceReviewModel->errors())]);
            }

            return $this->response->setStatusCode(Response::HTTP_OK)->setJSON([]);
        }
    }

    public function orderReviews()
    {
        if ( !$this->request->isAJAX() ) {
			exit('No direct script access allowed');
		}

        if ( empty(user()) ) {
			return $this->response->setStatusCode(Response::HTTP_UNAUTHORIZED)->setJSON(['message' => 'session_expired']);
        }


    }
}