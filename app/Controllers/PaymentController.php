<?php namespace App\Controllers;

/*
 * File: PaymentController.php
 * Project: echo
 * File Created: Tuesday, 25th May 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Thursday, 8th July 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use App\Models\ServiceInvoiceModel;
use CodeIgniter\HTTP\Response;
use Xendit\Xendit;

class PaymentController extends BaseController
{
    public function test()
    {
        $serviceInvoiceModel = new ServiceInvoiceModel();
        $serviceInvoice = $serviceInvoiceModel->find(1);

        dd($serviceInvoice->expired_at);

        Xendit::setApiKey(env('XND_SECRET_KEY'));

        $id = '60b7223812573e4719efd6fb';
        $getVA = \Xendit\VirtualAccounts::retrieve($id);
        dd($getVA);
    }

    public function testMail()
    {
        $serviceInvoiceModel = new ServiceInvoiceModel();
        $serviceInvoice = $serviceInvoiceModel->find(33);

        if ( ! empty($serviceInvoice) )
        {
            $customerEmail = $serviceInvoice->customer_email;
            $sellerEmail = $serviceInvoice->seller_email;
            if ( $customerEmail <> '' || $sellerEmail <> '' )
            {
                //$emailSubject = env('APP_NAME') . ' - Checkout Pesanan dengan ' . $serviceInvoice->payment_channel . ' Berhasil';
                ///$emailSubject = env('APP_NAME') . ' - Informasi Pembayaran';
                $emailSubject = env('APP_NAME') . ' - Informasi Pemesanan';
                $kirim = $this->kirim_surel($emailSubject, 'emails/order_created', [
                    'itemName' => $serviceInvoice->order->item,
                    'sellerName' => $serviceInvoice->order->buy_from_name,
                    'customerName' => $serviceInvoice->order->sell_to_name,
                    'customerFullAddress' => $serviceInvoice->order->sell_to_full_address,
                    'customerPhone' => $serviceInvoice->order->sell_to_phone_no,
                    'requestDate' => date('d/m/Y H:i:s', $serviceInvoice->order->request_date->getTimestamp()),
                    'createdAt' => date('d/m/Y H:i:s', $serviceInvoice->created_at->getTimestamp()),
                    'expiredAt' => date('d/m/Y H:i:s', $serviceInvoice->expired_at->getTimestamp()),
                    'transactionAt' => date('d/m/Y H:i:s', $serviceInvoice->transaction_at->getTimestamp()),
                    'paymentCategory' => $serviceInvoice->payment_category,
                    'paymentChannel' => $serviceInvoice->payment_channel,
                    'paymentCode' => $serviceInvoice->payment_code,
                    'paymentAmount' => $serviceInvoice->amount,
                    'externalId' => $serviceInvoice->external_id
                ], $sellerEmail);

                if ( $kirim )
                {
                    echo "Message sent";
                }
                else
                {
                    echo "Message could not be sent.";
                }
            }
        }
    }

    public function instruction()
    {
        $invoiceId = $_GET['id'] ?? '';

        $serviceInvoiceModel = new ServiceInvoiceModel();
        $serviceInvoice = $serviceInvoiceModel->find($invoiceId);

        if ( ! empty($serviceInvoice) )
        {
            if ( $serviceInvoice->is_owner && !$serviceInvoice->is_paid )
            {
                if ( $serviceInvoice->is_expired ) // comment for test
                {
                    return view('payment_instruction', [
                        'title' => 'Payment Instruction',
                        'active_service' => 'active',
                        'viewLayout' => 'App\Views\Template\layout',
                        'data' => $serviceInvoice
                    ]);
                }
            }
        }

        return redirect()->to(base_url());
    }

    public function getChannels()
    {
        if ( ! $this->request->isAJAX() ) {
			//exit('No direct script access allowed');
        }

        Xendit::setApiKey(env('XND_SECRET_KEY'));
        $requestor = new \Xendit\ApiRequestor;
        $channels = $requestor->request('GET', 'payment_channels');

        $data = [];
        foreach ($channels as $channel)
        {
            if ( $channel['currency'] === 'IDR' && $channel['is_enabled'] )
            {
                $data[$channel['channel_category']][] = [
                    'code' => $channel['channel_code'],
                    'name' => $channel['name'],
                    'image' => base_url('assets/images/gateway/xnd/logo-'.strtolower($channel['channel_code'].'.png')),
                    'is_livemode' => $channel['is_livemode']
                ];
            }
        }

        return $this->response->setStatusCode(Response::HTTP_OK)->setJSON(['data' => view('_payment_channels', ['data' => $data])]);
    }

    private function verify_callback($request)
    {
        if ( !$request->hasHeader('X-Callback-Token') )
        {
            return $this->response->setStatusCode(Response::HTTP_BAD_REQUEST);
        }

        if ((string)$request->header('X-Callback-Token') <> 'X-Callback-Token: ' . env('XND_CALLBACK_TOKEN'))
        {
            return $this->response->setStatusCode(Response::HTTP_BAD_REQUEST);
        }
    }

    private function kirim_surel(string $subject, string $viewLayout, array $params, string $addressEmail, string $addressName = '')
    {
        //Create a new PHPMailer instance
        $mail = service('phpmailer');
        $mail->ClearAllRecipients();
        $mail->addAddress($addressEmail, $addressName);
        $mail->Subject = $subject;
        $mail->msgHTML(view($viewLayout, $params));
        if (!$mail->send()) {
            log_message("error", "Message could not be sent. Mailer Error: {error}", ['error' => $mail->ErrorInfo]);
            return false;
        } else {
            return true;
        }
    }

    public function ro_paid()
    {
        $request = $this->request;

        // verify callback token
        $this->verify_callback($request);

        $data = $request->getJSON();
        if ( is_null($data) )
        {
            return $this->response->setStatusCode(Response::HTTP_BAD_REQUEST);
        }
        else
        {
            $serviceInvoiceModel = new ServiceInvoiceModel();
            $serviceInvoice = $serviceInvoiceModel->findByExternalId($data->external_id);

            if ( $serviceInvoice <> false )
            {
                $invoiceId = (int) $serviceInvoice->id;
                $update = $serviceInvoiceModel->update_transaction($invoiceId, $data->payment_id, $data->transaction_timestamp);
                if ( $update == true )
                {
                    $serviceInvoice = $serviceInvoiceModel->find($invoiceId);

                    // send email ke admin dan customer-seller
                    $sellerEmail = $serviceInvoice->seller_email;
                    $sellerName = $serviceInvoice->order->buy_from_name;
                    $customerEmail = $serviceInvoice->customer_email;
                    $customerName = $serviceInvoice->order->sell_to_name;

                    if ( $customerEmail <> '' )
                    {
                        $emailSubject1 = env('APP_NAME') . ' - Checkout Pesanan dengan ' . $serviceInvoice->payment_channel . ' Berhasil';
                        $kirim1 = $this->kirim_surel($emailSubject1, 'emails/inv_paid', [
                            'itemName' => $serviceInvoice->order->item,
                            'sellerName' => $sellerName,
                            'customerName' => $customerName,
                            'customerFullAddress' => $serviceInvoice->order->sell_to_full_address,
                            'customerPhone' => $serviceInvoice->order->sell_to_phone_no,
                            'transactionAt' => date('d/m/Y H:i:s', $serviceInvoice->transaction_at->getTimestamp()),
                            'paymentChannel' => $serviceInvoice->payment_channel,
                            'paymentAmount' => $serviceInvoice->amount,
                            'externalId' => $serviceInvoice->external_id
                        ], $customerEmail, $customerName);

                        $emailSubject2 = env('APP_NAME') . ' - Informasi Pemesanan';
                        $kirim2 = $this->kirim_surel($emailSubject2, 'emails/order_created', [
                            'itemName' => $serviceInvoice->order->item,
                            'sellerName' => $sellerName,
                            'customerName' => $customerName,
                            'customerFullAddress' => $serviceInvoice->order->sell_to_full_address,
                            'customerPhone' => $serviceInvoice->order->sell_to_phone_no,
                            'requestDate' => date('d/m/Y H:i:s', $serviceInvoice->order->request_date->getTimestamp()),
                            'createdAt' => date('d/m/Y H:i:s', $serviceInvoice->created_at->getTimestamp()),
                            'externalId' => $serviceInvoice->external_id
                        ], $sellerEmail, $sellerName);

                        if ( $kirim1 && $kirim2 )
                        {
                            return $this->response->setStatusCode(Response::HTTP_OK);
                        }
                        else
                        {
                            return $this->response->setStatusCode(Response::HTTP_BAD_REQUEST);
                        }
                    }
                }
            }

            return $this->response->setStatusCode(Response::HTTP_BAD_REQUEST);
        }
    }

    public function fva_paid()
    {
        $request = $this->request;

        // verify callback token
        $this->verify_callback($request);

        $data = $request->getJSON();
        if ( is_null($data) )
        {
            return $this->response->setStatusCode(Response::HTTP_BAD_REQUEST);
        }
        else
        {
            $serviceInvoiceModel = new ServiceInvoiceModel();
            $serviceInvoice = $serviceInvoiceModel->findByExternalId($data->external_id);

            if ( $serviceInvoice <> false )
            {
                $invoiceId = (int) $serviceInvoice->id;
                $update = $serviceInvoiceModel->update_transaction($invoiceId, $data->payment_id, $data->transaction_timestamp);
                if ( $update == true )
                {
                    $serviceInvoice = $serviceInvoiceModel->find($invoiceId);
                    // send email ke admin dan customer-seller
                    $sellerEmail = $serviceInvoice->seller_email;
                    $sellerName = $serviceInvoice->order->buy_from_name;
                    $customerEmail = $serviceInvoice->customer_email;
                    $customerName = $serviceInvoice->order->sell_to_name;

                    if ( $customerEmail <> '' )
                    {
                        $emailSubject1 = env('APP_NAME') . ' - Checkout Pesanan dengan ' . $serviceInvoice->payment_channel . ' Virtual Account Berhasil';
                        $kirim1 = $this->kirim_surel($emailSubject1, 'emails/inv_paid', [
                            'itemName' => $serviceInvoice->order->item,
                            'sellerName' => $sellerName,
                            'customerName' => $customerName,
                            'customerFullAddress' => $serviceInvoice->order->sell_to_full_address,
                            'customerPhone' => $serviceInvoice->order->sell_to_phone_no,
                            'transactionAt' => date('d/m/Y H:i:s', $serviceInvoice->transaction_at->getTimestamp()),
                            'paymentChannel' => $serviceInvoice->payment_channel,
                            'paymentAmount' => $serviceInvoice->amount,
                            'externalId' => $serviceInvoice->external_id
                        ], $customerEmail, $customerName);

                        $emailSubject2 = env('APP_NAME') . ' - Informasi Pemesanan';
                        $kirim2 = $this->kirim_surel($emailSubject2, 'emails/order_created', [
                            'itemName' => $serviceInvoice->order->item,
                            'sellerName' => $sellerName,
                            'customerName' => $customerName,
                            'customerFullAddress' => $serviceInvoice->order->sell_to_full_address,
                            'customerPhone' => $serviceInvoice->order->sell_to_phone_no,
                            'requestDate' => date('d/m/Y H:i:s', $serviceInvoice->order->request_date->getTimestamp()),
                            'createdAt' => date('d/m/Y H:i:s', $serviceInvoice->created_at->getTimestamp()),
                            'externalId' => $serviceInvoice->external_id
                        ], $sellerEmail, $sellerName);

                        if ( $kirim1 && $kirim2 )
                        {
                            return $this->response->setStatusCode(Response::HTTP_OK);
                        }
                        else
                        {
                            return $this->response->setStatusCode(Response::HTTP_BAD_REQUEST);
                        }
                    }
                }
            }

            return $this->response->setStatusCode(Response::HTTP_BAD_REQUEST);
        }
    }

    public function fva_created()
    {
        $request = $this->request;

        // verify callback token
        $this->verify_callback($request);

        $data = $request->getJSON();
        if ( is_null($data) )
        {
            return $this->response->setStatusCode(Response::HTTP_BAD_REQUEST);
        }
        else
        {
            // send email ke admin dan customer
            $serviceInvoiceModel = new ServiceInvoiceModel();
            $serviceInvoice = $serviceInvoiceModel->findByExternalId($data->external_id);

            if ( $serviceInvoice <> false )
            {
                $customerEmail = $serviceInvoice->customer_email;
                $customerName = $serviceInvoice->order->sell_to_name;
                if ( $customerEmail <> '' )
                {
                    $emailSubject = env('APP_NAME') . ' - Informasi Pembayaran';
                    $kirim = $this->kirim_surel($emailSubject, 'emails/fva_created', [
                        'itemName' => $serviceInvoice->order->item,
                        'customerName' => $customerName,
                        'createdAt' => date('d/m/Y H:i:s', $serviceInvoice->created_at->getTimestamp()),
                        'expiredAt' => date('d/m/Y H:i:s', $serviceInvoice->expired_at->getTimestamp()),
                        'paymentCategory' => $serviceInvoice->payment_category,
                        'paymentChannel' => $serviceInvoice->payment_channel,
                        'paymentCode' => $serviceInvoice->payment_code,
                        'paymentAmount' => $serviceInvoice->amount,
                        'externalId' => $serviceInvoice->external_id
                    ], $customerEmail, $customerName);

                    if ( $kirim )
                    {
                        return $this->response->setStatusCode(Response::HTTP_OK);
                    }
                    else
                    {
                        return $this->response->setStatusCode(Response::HTTP_BAD_REQUEST);
                    }
                }
            }

            return $this->response->setStatusCode(Response::HTTP_BAD_REQUEST);
        }
    }

    public function ewallet_status()
    {
        $request = $this->request;

        // verify callback token
        $this->verify_callback($request);

        $data = $request->getJSON();
        if ( is_null($data) )
        {
            return $this->response->setStatusCode(Response::HTTP_BAD_REQUEST);
        }
        else
        {
            $data = $data->data;

            if ( $data->status == "SUCCEEDED" )
            {
                $serviceInvoiceModel = new ServiceInvoiceModel();
                $serviceInvoice = $serviceInvoiceModel->findByExternalId($data->reference_id);

                if ( $serviceInvoice <> false )
                {
                    $invoiceId = (int) $serviceInvoice->id;
                    $update = $serviceInvoiceModel->update_transaction($invoiceId, '', $data->updated);
                    if ( $update == true )
                    {
                        $serviceInvoice = $serviceInvoiceModel->find($invoiceId);

                        // send email ke admin dan customer-seller
                        $sellerEmail = $serviceInvoice->seller_email;
                        $sellerName = $serviceInvoice->order->buy_from_name;
                        $customerEmail = $serviceInvoice->customer_email;
                        $customerName = $serviceInvoice->order->sell_to_name;

                        if ( $customerEmail <> '' )
                        {
                            $emailSubject1 = env('APP_NAME') . ' - Checkout Pesanan dengan ' . $serviceInvoice->payment_channel . ' Berhasil';
                            $kirim1 = $this->kirim_surel($emailSubject1, 'emails/inv_paid', [
                                'itemName' => $serviceInvoice->order->item,
                                'sellerName' => $sellerName,
                                'customerName' => $customerName,
                                'customerFullAddress' => $serviceInvoice->order->sell_to_full_address,
                                'customerPhone' => $serviceInvoice->order->sell_to_phone_no,
                                'transactionAt' => date('d/m/Y H:i:s', $serviceInvoice->transaction_at->getTimestamp()),
                                'paymentChannel' => $serviceInvoice->payment_channel,
                                'paymentAmount' => $serviceInvoice->amount,
                                'externalId' => $serviceInvoice->external_id
                            ], $customerEmail, $customerName);

                            $emailSubject2 = env('APP_NAME') . ' - Informasi Pemesanan';
                            $kirim2 = $this->kirim_surel($emailSubject2, 'emails/order_created', [
                                'itemName' => $serviceInvoice->order->item,
                                'sellerName' => $sellerName,
                                'customerName' => $customerName,
                                'customerFullAddress' => $serviceInvoice->order->sell_to_full_address,
                                'customerPhone' => $serviceInvoice->order->sell_to_phone_no,
                                'requestDate' => date('d/m/Y H:i:s', $serviceInvoice->order->request_date->getTimestamp()),
                                'createdAt' => date('d/m/Y H:i:s', $serviceInvoice->created_at->getTimestamp()),
                                'externalId' => $serviceInvoice->external_id
                            ], $sellerEmail, $sellerName);

                            if ( $kirim1 && $kirim2 )
                            {
                                return $this->response->setStatusCode(Response::HTTP_OK);
                            }
                            else
                            {
                                return $this->response->setStatusCode(Response::HTTP_BAD_REQUEST);
                            }
                        }
                    }
                }
            }

            return $this->response->setStatusCode(Response::HTTP_BAD_REQUEST);
        }
    }

    public function simulate()
    {
        Xendit::setApiKey(env('XND_SECRET_KEY'));
        $requestor = new \Xendit\ApiRequestor;
        $channels = $requestor->request('GET', 'payment_channels');

        $data = [];
        foreach ($channels as $channel)
        {
            if ( $channel['currency'] === 'IDR' && $channel['is_enabled'] )
            {
                $data[$channel['channel_category']][] = [
                    'code' => $channel['channel_code'],
                    'name' => $channel['name'],
                    'image' => base_url('assets/images/gateway/xnd/logo-'.strtolower($channel['channel_code'].'.png')),
                    'is_livemode' => $channel['is_livemode']
                ];
            }
        }

        return view('tests/simulate_payment', ['data' => $data]);
    }

    public function attemptSimulate()
    {
        $category = $this->request->getPost('category');
        $channel = $this->request->getPost('channel');
        $accountNo = $this->request->getPost('number');
        $amount = $this->request->getPost('amount');

        if ( $category === 'VIRTUAL_ACCOUNT' )
        {
            $body = [
                'transfer_amount' => $amount,
                'bank_account_number' => $accountNo,
                'bank_code' => $channel
            ];

            $url = 'https://api.xendit.co/pool_virtual_accounts/simulate_payment';
        }
        elseif ( $category === 'RETAIL_OUTLET' )
        {
            $body = [
                'retail_outlet_name' => $channel,
                'payment_code' => $accountNo,
                'transfer_amount' => $amount
            ];

            $url = 'https://api.xendit.co/fixed_payment_code/simulate_payment';
        }

        $client = \Config\Services::curlrequest();
        $response = $client->request('POST', $url, [
            'headers' => [
                'Authorization' => 'Basic ' . base64_encode( $_ENV['XND_SECRET_KEY'] . ':' )
            ],
            'json' => $body,
            'http_errors' => false
        ]);

        $code   = $response->getStatusCode();
        $body = $response->getBody();
        if (strpos($response->getHeader('content-type'), 'application/json') !== false)
        {
            $body = json_decode($body);
        }
        $message = $body->message;

        $status = ( $code == 200 ) ? 'success' : 'error';
        return redirect()->to('payment/simulate')->with($status, $message);
    }
}