<?php namespace App\Controllers;

/*
 * File: RegionController.php
 * Project: echo
 * File Created: Wednesday, 2nd September 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Friday, 30th April 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use App\Models\RegProvinceModel;
use App\Models\RegRegencyModel;

class RegionController extends BaseController
{
	public function __construct()
	{
		//
	}

	public function getProvince()
	{
		if (!$this->request->isAJAX()) {
			exit('No direct script access allowed');
		}

		$provinceModel = new RegProvinceModel();

		$provinces = $provinceModel->getProvinces();

		return $this->response->setJSON($provinces);
	}

	public function getRegency()
	{
		if (!$this->request->isAJAX()) {
			exit('No direct script access allowed');
		}

		$provinceModel = new RegProvinceModel();
		$regencyModel = new RegRegencyModel();

		$province = $provinceModel->getProvincesByName($this->request->getGet('province'));
		$regencies = $regencyModel->getProvinceRegencies($province->id);

		return $this->response->setJSON($regencies);
	}
}