<?php namespace App\Controllers;

/*
 * File: AccountController.php
 * Project: echo
 * File Created: Wednesday, 19th August 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Tuesday, 24th August 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use App\Entities\Upload;
use App\Entities\UserProfile;
use App\Models\RegRegencyModel;
use App\Models\UploadModel;
use App\Models\UserModel;
use App\Models\UserProfileModel;

class AccountController extends BaseController
{
	public function index()
	{
		if (! $this->auth->check())
		{
			return redirect()->to( base_url('login') );
		}

		return view('Account/index', [
			'viewLayout' => 'App\Views\Template\layout'
		]);
	}

	public function settings()
	{
		if (! $this->auth->check())
		{
			return redirect()->to( base_url('login') );
		}

		return view('account_settings', [
			'title' => 'Account Settings',
			'active_account_settings' => 'active',
			'viewLayout' => 'App\Views\Template\layout'
		]);
	}

	public function attemptSettings()
	{
		$userProfileModel = new UserProfileModel();
		$profile = $userProfileModel->find(user_profile()->id);

		$data = $this->request->getPost();
		$imagefile = $this->request->getFile('avatar');
		if ($imagefile != NULL)
		{
			if ($imagefile->isValid()) // jika ada file yang diupload
			{
				$uploadModel = new UploadModel();
				$originalName = $imagefile->getClientName();
				$newName = $imagefile->getRandomName();
				$filePath = 'assets/files/uploads/avatar';
				$imagefile->move(WRITEPATH.'uploads', $originalName);
				if ($imagefile->hasMoved()) // berhasil di move, insert upload table and update user profile avatar
				{
					try
					{
						// resize
						\Config\Services::image()
							->withFile(WRITEPATH.'uploads/'.$originalName)
							->resize(300, 200, true)
							->save($filePath.'/'.$newName);

						// get file properties
						$info = \Config\Services::image()
							->withFile($filePath . '/' . $newName)
							->getFile()
							->getProperties(true);

						$resizedFile = new \CodeIgniter\Files\File($filePath.'/'.$newName);

						$upload = new Upload();
						$upload->file_name = $newName;
						$upload->file_type = $resizedFile->getMimeType();
						$upload->file_path = $filePath;
						$upload->raw_name = explode('.', $newName)[0];
						$upload->orig_name = $originalName;
						$upload->file_ext = $resizedFile->guessExtension();;
						$upload->is_image = TRUE;
						$upload->file_size = $resizedFile->getSizeByUnit();
						$upload->image_width = $info['width'];
						$upload->image_height = $info['height'];
						$upload->image_type = $resizedFile->getMimeType();
						$upload->user_id = user_id();
						if ($uploadModel->save($upload))
						{
							$id = $uploadModel->getInsertID();
							if ($id <> 0) {
								$data['display_picture'] = $id;
							} else {
								log_message('error', 'Gagal memperbarui gambar profil');
							}
						}
						else
						{
							log_message('error', 'Gagal menyimpan file ke dalam database ' . implode(' ', $uploadModel->errors()));
						}
					}
					catch (\CodeIgniter\Images\Exceptions\ImageException $e)
					{
						log_message('error', ''.$e->getMessage());
					}
				}
				else // gagal upload beri pesan error
				{
					log_message('error', 'Gagal mengunggah file');
				}
			}
			else
			{
				log_message('error', $imagefile->getErrorString());
			}
		}

		if (empty($profile)) {
			$profile = new UserProfile(['user_id' => user_profile()->user_id]);
		}

		$profile->fill($data);

		$hasChanged = false;
		foreach ($data as $key => $value) {
			if ($key != 'csrf_test_name') {
				if ($profile->hasChanged($key)) {
					if ($key === 'region') {
						// check is city is in that region
						$regencyModel = new RegRegencyModel();
						$check = $regencyModel->checkRegencyProvince($profile->city, $profile->region);
						if ( !$check ) $profile->city = NULL; // reset city if region has changed
					}
					$hasChanged = true;
				}
			}
		}

		if (! $hasChanged)
		{
			$sess_message = 'No data has been changed.';
		}
		else
		{
			if (! $userProfileModel->save($profile))
			{
				return redirect()->back()->withInput()->with('errors', $userProfileModel->errors());
			}
			else
			{
				// update display_name
				$userModel = new UserModel();
				$user = $userModel->find(user_profile()->user_id);

				$first_name   = $this->request->getPost('first_name') ?? "";
				$middle_name  = $this->request->getPost('middle_name') ? " " . $this->request->getPost('middle_name') . " " : " ";
				$last_name 	  = $this->request->getPost('last_name') ?? "";
				$display_name = $first_name . $middle_name . $last_name;
				if ($user->display_name != $display_name) {
					$user->display_name = $display_name;
					if ($userModel->save($user)) {
						log_message("info", "User {email} displayName has been updated from {ip_address}", ["email" => $user->email, "ip_address" => $this->request->getIPAddress()]);
					}
				}

				log_message("info", "User {email} profile has been updated from {ip_address}", ["email" => $user->email, "ip_address" => $this->request->getIPAddress()]);
				$sess_message = 'Account has been updated.';
				$this->session->setFlashdata('success', $sess_message);
			}
		}

		$redirectURL = session('redirect_url') ?? site_url('account/settings');
		unset($_SESSION['redirect_url']);

		return redirect()->to($redirectURL)->withCookies()->with('message', $sess_message);
	}
}