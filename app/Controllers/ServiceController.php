<?php namespace App\Controllers;

/*
 * File: ServiceController.php
 * Project: echo
 * File Created: Monday, 31st August 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 15th November 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use App\Entities\Service;
use App\Models\RegProvinceModel;
use App\Models\RegRegencyModel;
use App\Models\ServiceCategoryModel;
use App\Models\ServiceModel;
use App\Models\UserProfileModel;

class ServiceController extends BaseController
{
	use \CodeIgniter\API\ResponseTrait;

	public function index()
	{
		return view('services', [
			'title' => 'Services',
			'active_service' => 'active',
			'viewLayout' => 'App\Views\Template\layout',
		]);
	}

	public function serviceLookup()
	{
		$serviceModel = new ServiceModel();

		$uri = service('uri');
		if ($uri->getSegment(2) === '') {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		} else {
			$service = $serviceModel->find($uri->getSegment(2));
		}

		if (! empty($service))
		{
			return view('service_page', [
				'title' => $service->name,
				'active_service' => 'active',
				'viewLayout' => 'App\Views\Template\layout',
				'service' => $service
			]);
		}
		else
		{
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}
	}

	public function store()
	{
		if ( empty(user()) ) {
			return $this->failUnauthorized();
        }

		$userProfileModel = new UserProfileModel();
		$profile = $userProfileModel->getProfile();
		if (! $profile->is_complete) {
			return $this->fail('Empty profile, please complete your profile.', 400, 'EMPTY_PROFILE');
		}

		$provinceModel = new RegProvinceModel();
		$regencyModel = new RegRegencyModel();
		$serviceCategoryModel = new ServiceCategoryModel();

		// Province - Regency
		$province_regency = [];
		$_provinces = $provinceModel->getProvinces();
		foreach ($_provinces as $r1) {
			$regencies = $regencyModel->getProvinceRegencies($r1->id);
			foreach ($regencies as $r2) {
				$province_regency[$r1->name][$r2->id] = $r2->name;
			}
		}

		// Service Category
		$service_category = [];
		$_service_category = $serviceCategoryModel->getCategories();
		foreach ($_service_category as $r1) {
			$service_category[$r1->id] = [
				'id' => $r1->id,
				'name' => $r1->name
			];

			$_service_subcategory = $serviceCategoryModel->getCategories($r1->id);
			foreach ($_service_subcategory as $r2) {
				$service_category[$r2->parent_id]['subcategory'][] = [
					'id' => $r2->id,
					'name' => $r2->name
				];
			}
		}

		$id = $this->request->getGet('id');
		if ( is_null($id) )
		{
			// create
			return $this->respond(['html' => view('_service_modal', [
				'isNew' => true,
				'formId' => 'fm_add_service',
				'province_regency' => $province_regency,
				'service_category' => $service_category
			])]);
		}
		else
		{
			// update
			$serviceModel = new ServiceModel();
			$service = $serviceModel->find($id);

			return $this->respond(['html' => view('_service_modal', [
				'isNew' => false,
				'formId' => 'fm_update_service',
				'service' => $service,
				'province_regency' => $province_regency,
				'service_category' => $service_category
			])]);
		}
	}

	public function attemptStore()
	{
		if ( empty(user()) ) {
			return $this->failUnauthorized();
        }

		$serviceModel = new ServiceModel();
		$service_id = $this->request->getPost('service_id');
		$isPinned = (!is_null($this->request->getPost('service_pinned')) && $this->request->getPost('service_pinned') == 'checked') ? true : false;
		$coverages = $this->request->getPost('service_coverage');

		$data = [
			'name' => $this->request->getPost('service_name'),
			'description' => $this->request->getPost('service_description'),
			'coverage' => $coverages,
			'category_id' => $this->request->getPost('service_category'),
			'price' => $this->request->getPost('service_price'),
			'price_per' => $this->request->getPost('service_pricePer'),
			'active' => ($this->request->getPost('type') === 'save') ? false : true,
			'pinned' => $isPinned
		];

		$validation =  \Config\Services::validation();
		$rules = $serviceModel->getValidationRules(['except' => ['user_id']]);
		$validationResult = $validation->setRules($rules)->run($data);
		if (! $validationResult)
		{
			log_message('error', 'User {uid} createService ' . implode(' ', $validation->getErrors()), ['uid' => user_id()]);
			return $this->fail($validation->getErrors());
		}
		else
		{
			unset($data['coverage']);
			if ( is_null($service_id) )
			{
				// new service
				$data['user_id'] = user_id();
				$service = new Service($data);

				$serviceModel->skipValidation(true); // because we already check it
				if (! $serviceModel->save($service))
				{
					log_message('error', 'User {uid} createService ' . implode(' ', $serviceModel->errors()), ['uid' => user_id()]);
					return $this->fail($serviceModel->errors());
				}
				else
				{
					return $this->respond(['status' => 'OK']);
				}
			}
			else
			{
				// update current service
				$service = $serviceModel->find($service_id);

				if ( $service->user_id === user_id() )
				{
					$service->fill($data);
					if ( $service->hasChanged() )
					{
						if (! $serviceModel->save($service))
						{
							log_message('error', 'User {uid} updateService '. implode(' ', $serviceModel->errors()), ['uid' => user_id()]);
							return $this->fail($serviceModel->errors());
						}
						else
						{
							return $this->respond(['status' => 'OK']);
						}
					}
					else
					{
						// manually update coverages if header service doesnt changed
						if ($coverages != $service->CoverageIds)
						{
							$serviceModel->updateCoverages([
								'id' => [
									$service_id
								],
								'data' => [
									'updated_at' => date('Y-m-d H:i:s')
								],
								'result' => true
							]);

							return $this->respond(['status' => 'OK']);
						}
						else
						{
							return $this->fail(lang('Rules.no_changes'));
						}
					}
				}
				else
				{
					log_message('error', 'Service action unauthorized');
					return $this->fail(lang('Rules.unauthorized'));
				}
			}
		}
	}

	public function mine()
	{
		if ( empty(user()) ) {
			return $this->failUnauthorized();
		}

		return $this->respond(['html' => view('services_mine')]);
	}

	public function mine_published()
	{
		$serviceModel = new ServiceModel();
		$services = $serviceModel->published();

		return $this->respond(view('services_mine_published', [
			'my_published_services' => $services
		]));
	}

	public function mine_draft()
	{
		$serviceModel = new ServiceModel();
		$services = $serviceModel->draft();

		return $this->respond(view('services_mine_draft', [
			'my_drafted_services' => $services
		]));
	}

	public function mine_recommended()
	{
		return $this->respond(view('services_mine_recommended'));
	}

	public function marketplace()
	{
		if ( empty(user()) ) {
			return $this->failUnauthorized();
		}

		$userProfileModel = new UserProfileModel();
		$profile = $userProfileModel->getProfile();
		if (! $profile->is_complete) {
			return $this->fail('Empty profile, please complete your profile.', 400, 'EMPTY_PROFILE');
		}

		$provinceModel = new RegProvinceModel();
		$regencyModel = new RegRegencyModel();
		$serviceCategoryModel = new ServiceCategoryModel();
		$serviceModel = new ServiceModel();

		// Province - Regency
		$province_regency = [];
		$_provinces = $provinceModel->getProvinces();
		foreach ($_provinces as $r1) {
			$regencies = $regencyModel->getProvinceRegencies($r1->id);
			foreach ($regencies as $r2) {
				$province_regency[$r1->name][$r2->id] = $r2->name;
			}
		}

		// Service Category
		$service_category = [];
		$_service_category = $serviceCategoryModel->getCategories();
		foreach ($_service_category as $r1) {
			$service_category[$r1->id] = [
				'id' => $r1->id,
				'name' => $r1->name
			];

			$_service_subcategory = $serviceCategoryModel->getCategories($r1->id);
			foreach ($_service_subcategory as $r2) {
				$service_category[$r2->parent_id]['subcategory'][] = [
					'id' => $r2->id,
					'name' => $r2->name
				];
			}
		}

		// check filter
		$filters = [];
		$page = $this->request->getGet('page');
		$categories = $this->request->getPost('categories');
		$locations = $this->request->getPost('locations');
		$stars = $this->request->getPost('stars');
		$pricePers = $this->request->getPost('pricePers');

		if (! is_null($categories)) {
			$filters['category'] = $categories;
		}
		if (! is_null($locations)) {
			$filters['coverage'] = $locations;
		}
		if (! is_null($stars)) {
			$filters['star'] = $stars;
		}
		if (! is_null($pricePers)) {
			$filters['price_per'] = $pricePers;
		}

		// Available Service
		$services_page = $page ?? 1;
		$services_perPage = 8;
		$offset = ($services_page == 1) ? 0 : (($services_page-1) * $services_perPage);

		$services = $serviceModel->getMarketplaces($filters, $services_perPage, $offset);
		$services_total = $serviceModel->countMarketplaces($filters);

		$pager = service('pager');

		return $this->respond(['html' => view('services_marketplace', [
			'province_regency' => $province_regency,
			'services' => $services,
			'page' => $services_page,
			'perPage' => $services_perPage,
			'total' => $services_total,
			'pager' => $pager,
			'service_category' => $service_category,
			'numberLib' => new \App\Libraries\NumberLibrary,
			'filter_categories' => $categories,
			'filter_locations' => $locations,
			'filter_stars' => $stars,
			'filter_pricePers' => $pricePers
		])]);
	}

	public function recommendation()
	{
		if ( empty(user()) ) {
			return $this->failUnauthorized();
		}

		$userProfileModel = new UserProfileModel();
		$profile = $userProfileModel->getProfile();
		if (! $profile->is_complete) {
			return $this->fail('Empty profile, please complete your profile.', 400, 'EMPTY_PROFILE');
		}

		$provinceModel = new RegProvinceModel();
		$regencyModel = new RegRegencyModel();
		$serviceCategoryModel = new ServiceCategoryModel();
		$serviceModel = new ServiceModel();

		// Province - Regency
		$province_regency = [];
		$_provinces = $provinceModel->getProvinces();
		foreach ($_provinces as $r1) {
			$regencies = $regencyModel->getProvinceRegencies($r1->id);
			foreach ($regencies as $r2) {
				$province_regency[$r1->name][$r2->id] = $r2->name;
			}
		}

		// Service Category
		$service_category = [];
		$_service_category = $serviceCategoryModel->getCategories();
		foreach ($_service_category as $r1) {
			$service_category[$r1->id] = [
				'id' => $r1->id,
				'name' => $r1->name
			];

			$_service_subcategory = $serviceCategoryModel->getCategories($r1->id);
			foreach ($_service_subcategory as $r2) {
				$service_category[$r2->parent_id]['subcategory'][] = [
					'id' => $r2->id,
					'name' => $r2->name
				];
			}
		}

		// check filter
		$filters = [];
		$page = $this->request->getGet('page');
		$categories = $this->request->getPost('categories');
		$locations = $this->request->getPost('locations');
		$stars = $this->request->getPost('stars');
		$pricePers = $this->request->getPost('pricePers');

		if (! is_null($categories)) {
			$filters['category'] = $categories;
		}
		if (! is_null($locations)) {
			$filters['coverage'] = $locations;
		}
		if (! is_null($stars)) {
			$filters['star'] = $stars;
		}
		if (! is_null($pricePers)) {
			$filters['price_per'] = $pricePers;
		}

		// Recomendation Service -> must be change later, its just sample data
		$services_page = $page ?? 1;
		$services_perPage = 3;
		$offset = ($services_page == 1) ? 0 : (($services_page-1) * $services_perPage);

		$services = $serviceModel->getRecommendations($filters, $services_perPage, $offset);
		$services_total = $serviceModel->countRecommendations($filters);

		$pager = service('pager');

		return $this->respond(['html' => view('services_recommendation', [
			'province_regency' => $province_regency,
			'services' => $services,
			'page' => $services_page,
			'perPage' => $services_perPage,
			'total' => $services_total,
			'pager' => $pager,
			'service_category' => $service_category,
			'numberLib' => new \App\Libraries\NumberLibrary,
			'filter_categories' => $categories,
			'filter_locations' => $locations,
			'filter_stars' => $stars,
			'filter_pricePers' => $pricePers
		])]);
	}
}