<?php

namespace App\Controllers;

use App\Models\ServiceOrderModel;
use CodeIgniter\Controller;
use CodeIgniter\I18n\Time;

class ToolsController extends Controller
{
	private function kirim_surel(string $subject, string $viewLayout, array $params, string $addressEmail, string $addressName = '')
    {
        //Create a new PHPMailer instance
        $mail = service('phpmailer');
        $mail->ClearAllRecipients();
        $mail->addAddress($addressEmail, $addressName);
        $mail->Subject = $subject;
        $mail->msgHTML(view($viewLayout, $params));
        if (!$mail->send()) {
            log_message("error", "Message could not be sent. Mailer Error: {error}", ['error' => $mail->ErrorInfo]);
            return false;
        } else {
            return true;
        }
    }

	public function ordersCreate()
	{
		helper("html");
		$myTime = Time::today();

		$serviceOrderModel = new ServiceOrderModel();
		$serviceOrders = $serviceOrderModel->getSales(1);

		foreach ($serviceOrders as $service)
		{
			$requestDate = $service->request_date;
			$diff = $requestDate->difference($myTime);
			if ($diff->getDays() > 1)
			{
				// send email ke customer-seller
				$sellerEmail = $service->sell_to_email_address;
				$sellerName = $service->buy_from_name;
				$customerName = $service->sell_to_name;

				$emailSubject = env('APP_NAME') . ' - Informasi Pemesanan';
				$this->kirim_surel($emailSubject, 'emails/order_created', [
					'itemName' => $service->item,
					'sellerName' => $sellerName,
					'customerName' => $customerName,
					'customerFullAddress' => $service->invoice->order->sell_to_full_address,
					'customerPhone' => $service->sell_to_phone_no,
					'requestDate' => date('d/m/Y H:i:s', $requestDate->getTimeStamp()),
					'createdAt' => date('d/m/Y H:i:s', $service->created_at->getTimestamp()),
					'externalId' => $service->order_no
				], $sellerEmail, $sellerName);
			}
		}
	}

	public function ordersFulfilled()
	{
		helper("html");
		$myTime = Time::today();

		$serviceOrderModel = new ServiceOrderModel();
		$serviceOrders = $serviceOrderModel->getOrders(3);

		foreach ($serviceOrders as $service)
		{
			$fulfilledAt = $service->fulfill->arrived_at;
			$diff = $fulfilledAt->difference($myTime);
			if ($diff->getDays() > 1)
			{
				$customerName = $service->sell_to_name;
				$customerEmail = $service->sell_to_email_address;

				$emailSubject = env('APP_NAME') . ' - Informasi Pengerjaan Layanan';
				$this->kirim_surel($emailSubject, 'emails/order_fulfilled', [
					'itemName' => $service->item,
					'customerName' => $customerName,
					'sellerName' => $service->buy_from_name,
					'fulfilledAt' => date('d/m/Y H:i:s', $fulfilledAt->getTimestamp()),
					'externalId' => $service->order_no
				], $customerEmail, $customerName);
			}
		}
	}

}