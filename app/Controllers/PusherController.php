<?php namespace App\Controllers;

/*
 * File: PusherController.php
 * Project: echo
 * File Created: Thursday, 22nd April 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Wednesday, 19th May 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\HTTP\Response;

class PusherController extends BaseController
{
    public function beamsAuth()
    {
        $beamsClient = service('pusher_beam');

        $userID = user_id();
        $userIDInQueryParam = $this->request->getGet('user_id');

        if ($userID != $userIDInQueryParam) {
            return $this->response->setStatusCode(Response::HTTP_UNAUTHORIZED)->setJSON('Inconsistent request');
        } else {
            $beamsToken = $beamsClient->generateToken($userID);
            return $this->response->setStatusCode(Response::HTTP_OK)->setJSON($beamsToken);
        }
    }

    public function auth()
    {
        if ( !$this->auth->check() )
        {
            return $this->response->setStatusCode(Response::HTTP_UNAUTHORIZED)->setJSON(['message' => 'session_expired']);
        }

        $channelName = $this->request->getPost('channel_name');
        $channelType = explode('-', $channelName);
        $socketId = $this->request->getPost('socket_id');

        $channelClient = service('pusher_channel');
        if ($channelType[0] == 'presence')
        {
            $auth = $channelClient->presenceAuth($channelName, $socketId, user_id(), ['username' => user()->username, 'fullname' => user_profile()->displayName]);
        }
        elseif ($channelType[0] == 'private')
        {
            $auth = $channelClient->socketAuth($channelName, $socketId);
        }

        return $this->response->setStatusCode(Response::HTTP_OK)->setJSON($auth);
    }

    public function test()
    {
        $beamsClient = service('pusher_beam');
        $beamsClient->publishToUsers(
            array("3"),
            array(
                "fcm" => array(
                    "notification" => array(
                        "title" => "Hi!",
                        "body" => "This is my first Push Notification!"
                    )
                ),
                "apns" => array(
                    "aps" => array(
                        "alert" => array(
                            "title" => "Hi!",
                            "body" => "This is my first Push Notification!"
                        )
                    )
                ),
                "web" => array(
                    "notification" => array(
                        "title" => "Hi!",
                        "body" => "This is my first Push Notification!"
                    )
                )
            )
        );
    }
}