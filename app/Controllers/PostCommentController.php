<?php namespace App\Controllers;

/*
 * File: PostCommentController.php
 * Project: echo
 * File Created: Friday, 18th December 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Tuesday, 20th April 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use App\Entities\PostComment;
use App\Models\PostCommentModel;
use CodeIgniter\HTTP\Response;

class PostCommentController extends BaseController
{
    public function attemptStore()
    {
        if ( ! $this->request->isAJAX() ) {
			exit('No direct script access allowed');
        }

        if ( empty(user()) ) {
			return $this->response->setStatusCode(Response::HTTP_UNAUTHORIZED)->setJSON(['message' => 'session_expired']);
        }

        $post_id = $this->request->getPost('post_id');
        $parent_id = $this->request->getPost('parent_id');
        $content = $this->request->getPost('content');

        $postCommentModel = new PostCommentModel();
        $postComment = new PostComment();
        if ( $parent_id <> 0 ) {
            $postComment->parent_id = $parent_id;
        }
        $postComment->content = $content;
        $postComment->post_id = $post_id;
        $postComment->user_id = user_id();
        if ( ! $postCommentModel->save($postComment) )
        {
            log_message('error', 'User {uid} status-comment on {id} ' . implode(' ', $postComment->errors()), ['uid' => user_id(), 'id' => $post_id]);
            return $this->response->setStatusCode(Response::HTTP_BAD_REQUEST)->setJSON(['message' => implode(' ', $postCommentModel->errors())]);
        }
        else
        {
            $id = $postCommentModel->getInsertID();
            $postComment = $postCommentModel->find($id);
            return $this->response->setStatusCode(Response::HTTP_OK)->setJSON(['data' => view('_post_comment', ['commentData' => $postComment])]);
        }
    }
}