<?php namespace App\Controllers;

/*
 * File: ServiceFulfillmentController.php
 * Project: echo
 * File Created: Wednesday, 3rd March 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 23rd August 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use App\Entities\ServiceFulfillment;
use App\Models\ServiceFulfillmentModel;
use App\Models\ServiceOrderModel;
use CodeIgniter\I18n\Time;

class ServiceFulfillmentController extends BaseController
{
    use \CodeIgniter\API\ResponseTrait;

    public function attemptFulfill()
    {
        if ( empty(user()) ) {
            return $this->failUnauthorized();
        }

        $orderId = $this->request->getPost('order_id');

        $serviceFulfillmentModel = new ServiceFulfillmentModel();
        $rules = $serviceFulfillmentModel->getValidationRules(['only' => ['order_id']]);
		if (! $this->validate($rules))
        {
            return $this->failValidationErrors($this->validator->getErrors());
        }
        else
        {
            $serviceOrderModel = new ServiceOrderModel();
            $serviceOrder = $serviceOrderModel->find($orderId);
            if ( empty($serviceOrder) )
            {
                return $this->fail('Invalid order.', 404, 'INVALID_ORDER');
            }
            if ( $serviceOrder->has_fulfilled ) {
                return $this->fail('Already fulfilled', 400, 'ALREADY_FULFILLED');
            }

            $serviceFulfillment = new ServiceFulfillment();
            $serviceFulfillment->arrived_at = Time::now()->toDateTimeString();
            $serviceFulfillment->order_id = $orderId;
            $serviceFulfillment->completed = false;
            $serviceFulfillment->user_id = user_id();
            if (! $serviceFulfillmentModel->save($serviceFulfillment))
            {
                return $this->fail($serviceFulfillmentModel->errors(), 400, 'FAIL_FULFILLED');
            }

            $customerName = $serviceFulfillment->order->sell_to_name;
            $customerEmail = $serviceFulfillment->order->sell_to_email_address;
            $emailSubject = env('APP_NAME') . ' - Informasi Pengerjaan Layanan';
            $this->kirim_surel($emailSubject, 'emails/order_fulfilled', [
                'itemName' => $serviceFulfillment->order->item,
                'customerName' => $customerName,
                'sellerName' => $serviceFulfillment->order->buy_from_name,
                'fulfilledAt' => date('d/m/Y H:i:s', $serviceFulfillment->arrived_at->getTimestamp()),
                'externalId' => $serviceFulfillment->order->order_no
            ], $customerEmail, $customerName);

            return $this->respond(['status' => 'OK']);
        }
    }

    public function attemptConfirm()
    {
        if ( empty(user()) ) {
			return $this->failUnauthorized();
        }

        $orderId = $this->request->getPost('order_id');

        $serviceFulfillmentModel = new ServiceFulfillmentModel();
		$rules = $serviceFulfillmentModel->getValidationRules(['only' => ['order_id']]);
		if (! $this->validate($rules))
        {
            return $this->failValidationErrors($this->validator->getErrors());
        }
        else
        {
            $serviceFulfillment = $serviceFulfillmentModel->findByOrderId($orderId);
            if ( empty($serviceFulfillment) )
            {
                return $this->fail('Not yet fulfilled.', 400, 'NOT_FULFILLED');
            }

            if ( $serviceFulfillment->completed )
            {
                return $this->fail('Already completed.', 400, 'ALREADY_COMPLETED_FULFILLED');
            }

            $serviceFulfillment->completed = true;
            if (! $serviceFulfillmentModel->save($serviceFulfillment))
            {
                return $this->fail($serviceFulfillmentModel->errors(), 400, 'FAIL_COMPLETED_FULFILLED');
            }

            return $this->respond(['status' => 'OK']);
        }
    }

    private function kirim_surel(string $subject, string $viewLayout, array $params, string $addressEmail, string $addressName = '')
    {
        //Create a new PHPMailer instance
        $mail = service('phpmailer');
        $mail->ClearAllRecipients();
        $mail->addAddress($addressEmail, $addressName);
        $mail->Subject = $subject;
        $mail->msgHTML(view($viewLayout, $params));
        if (! $mail->send()) {
            log_message("error", "Message could not be sent. Mailer Error: {error}", ['error' => $mail->ErrorInfo]);
            return false;
        } else {
            return true;
        }
    }
}