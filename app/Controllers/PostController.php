<?php namespace App\Controllers;

/*
 * File: PostController.php
 * Project: echo
 * File Created: Monday, 26th October 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 15th November 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use App\Entities\Post;
use App\Entities\PostReaction;
use App\Models\PostModel;
use App\Models\PostReactionModel;
use App\Models\PostVisibilityModel;
use App\Models\ReactionModel;
use CodeIgniter\HTTP\Response;

class PostController extends BaseController
{
    public function store()
    {
        if ( ! $this->request->isAJAX() ) {
			exit('No direct script access allowed');
        }

        if ( empty(user()) ) {
            return $this->response->setStatusCode(Response::HTTP_UNAUTHORIZED)->setJSON(['message' => 'session_expired']);
        }

        $postVisibilityModel = new PostVisibilityModel();
		$postVisibility = $postVisibilityModel->asArray()->findAll();

        $id = $this->request->getGet('id');
		if ( is_null($id) )
		{
			// create
			return $this->response->setStatusCode(Response::HTTP_OK)->setJSON(['data' => view('_post_modal', [
				'isNew' => true,
                'formId' => 'fm_add_post',
                'postVisibility' => $postVisibility
			])]);
		}
		else
		{
			// update
			$postModel = new PostModel();
			$post = $postModel->find($id);

			return $this->response->setStatusCode(Response::HTTP_OK)->setJSON(['data' => view('_post_modal', [
				'isNew' => false,
				'formId' => 'fm_update_post',
                'post' => $post,
                'postVisibility' => $postVisibility
			])]);
		}
    }

    public function attemptStore()
	{
		if ( ! $this->request->isAJAX() ) {
			exit('No direct script access allowed');
        }

        if ( empty(user()) ) {
			return $this->response->setStatusCode(Response::HTTP_UNAUTHORIZED)->setJSON(['message' => 'session_expired']);
        }

        $postModel = new PostModel();
        $post_id = $this->request->getPost('id');

        $validation =  \Config\Services::validation();
        $rules = $postModel->getValidationRules(['except' => ['user_id']]);
        $validation->setRules($rules);
        $validationResult = $validation->withRequest($this->request)->run();
        if (! $validationResult)
		{
			$validationErrors = $validation->getErrors();
            log_message('error', 'User {uid} status-create ' . implode(' ', $validationErrors), ['uid' => user_id()]);
            return $this->response->setStatusCode(Response::HTTP_BAD_REQUEST)->setJSON(['message' => implode(' ', $validationErrors)]);
		}
		else
		{
            if ( is_null($post_id) )
			{
				// new post
                $data = $this->request->getPost();
                $data['user_id'] = user_id();
                $post = new Post($data);
                if (! $postModel->save($post))
                {
                    log_message('error', 'User {uid} status-create ' . implode(' ', $postModel->errors()), ['uid' => user_id()]);
                    return $this->response->setStatusCode(Response::HTTP_BAD_REQUEST)->setJSON(['message' => implode(' ', $postModel->errors())]);
                }
                else
                {
                    $id = $postModel->getInsertID();
                    return $this->response->setStatusCode(Response::HTTP_OK)->setJSON(['html' => view('_post', ['postData' => $postModel->find($id)])]);
                }
            }
            else
            {
                // update current post
                $post = $postModel->find($post_id);
                if ( $post->user_id === user_id() )
				{
                    $post->content = $this->request->getPost('content');
                    $post->visibility = $this->request->getPost('visibility');

                    if ( $post->hasChanged() )
					{
						if (! $postModel->save($post))
						{
							log_message('error', 'User {uid} status-update ' . implode(' ', $postModel->errors()), ['uid' => user_id()]);
							return $this->response->setStatusCode(Response::HTTP_BAD_REQUEST)->setJSON(['message' => implode(' ', $postModel->errors())]);
                        }
                        else
                        {
                            return $this->response->setStatusCode(Response::HTTP_OK)->setJSON(['html' => $post->content]);
                        }
                    }
                    else
                    {
                        return $this->response->setStatusCode(Response::HTTP_BAD_REQUEST)->setJSON(['message' => lang('Rules.no_changes')]);
                    }
                }
                else
                {
                    log_message('error', 'Post action unauthorized');
					return $this->response->setStatusCode(Response::HTTP_BAD_REQUEST)->setJSON(['message' => lang('Rules.unauthorized')]);
                }
            }
        }
    }

    public function attemptAction()
	{
		if ( ! $this->request->isAJAX() ) {
			exit('No direct script access allowed');
        }

        if ( empty(user()) ) {
			return $this->response->setStatusCode(Response::HTTP_UNAUTHORIZED)->setJSON(['message' => 'session_expired']);
		}

        $postId = $this->request->getPost('post_id');
        $actType = $this->request->getPost('act_type');

        $postModel = new PostModel();
        $post = $postModel->find($postId);

        if ( ! empty($post) ) // if post is exists
        {
            if ($actType === 'delete')
            {
                // is his or her own post
                if ( $post->is_owner )
                {
                    if ( ! $postModel->delete($postId) )
                    {
                        log_message('error', 'User {uid} status-delete ' . implode(' ', $postModel->errors()), ['uid' => user_id()]);
                        return $this->response->setStatusCode(Response::HTTP_BAD_REQUEST)->setJSON(['message' => lang('Post.errorDelete')]);
                    }
                    else
                    {
                        return $this->response->setStatusCode(Response::HTTP_OK)->setJSON(['data' => lang('Post.successDelete')]);
                    }
                }
                else
                {
                    return $this->response->setStatusCode(Response::HTTP_BAD_REQUEST)->setJSON(['message' => lang('Post.unableToDelete')]);
                }
            }
            elseif ($actType === 'save')
            {
                # code...
                return $this->response->setStatusCode(Response::HTTP_OK)->setJSON(['data' => '']);
            }
            elseif ($actType === 'hide')
            {
                # code...
                return $this->response->setStatusCode(Response::HTTP_OK)->setJSON(['data' => '']);
            }
            elseif ($actType === 'report')
            {
                # code...
                return $this->response->setStatusCode(Response::HTTP_OK)->setJSON(['data' => '']);
            }
            elseif ($actType === 'get-comment')
            {
                $postComments = $post->getComments();
                return $this->response->setStatusCode(Response::HTTP_OK)->setJSON(['data' => [
                    'commentBox' => view('_post_commentBox', ['postId' => $postId]),
                    'commentReplies' => view('_post_commentReplies', ['commentsData' => $postComments])
                ]]);
            }
            elseif ($actType === 'share')
            {
                # code...
                return $this->response->setStatusCode(Response::HTTP_OK)->setJSON(['data' => '']);
            }
            else
            {
                $reactionModel = new ReactionModel();
                $reaction = $reactionModel->where('name', $actType)->first();
                if ( !empty($reaction) )
                {
                    $reactionId = $reaction->id;
                    $reactionStyle = $reaction->style;

                    $postReactionModel = new PostReactionModel();
                    $postReaction = $postReactionModel->getReactionOf($postId, $reactionId);
                    if ( empty($postReaction) )
                    {
                        // create the reaction
                        $validation =  \Config\Services::validation();
                        $rules = $postReactionModel->getValidationRules(['except' => ['user_id']]);
                        $validationResult = $validation->setRules($rules)->run([
                            'post_id' => $postId,
                            'reaction_id' => $reactionId
                        ]);
                        if ( !$validationResult )
                        {
                            $validationErrors = $validation->getErrors();
                            log_message('error', 'User {uid} status-react ' . implode(' ', $validationErrors), ['uid' => user_id()]);
                            return $this->response->setStatusCode(Response::HTTP_BAD_REQUEST)->setJSON(['message' => implode(' ', $validationErrors)]);
                        }
                        else
                        {
                            $postReaction = new PostReaction();
                            $postReaction->post_id = $postId;
                            $postReaction->reaction_id = $reactionId;
                            $postReaction->user_id = user_id();

                            if ( !$postReactionModel->save($postReaction) )
                            {
                                log_message('error', 'User {uid} status-react ' . implode(' ', $postReactionModel->errors()), ['uid' => user_id()]);
                                return $this->response->setStatusCode(Response::HTTP_BAD_REQUEST)->setJSON(['message' => implode(' ', $postReactionModel->errors())]);
                            }
                            else
                            {
                                return $this->response->setStatusCode(Response::HTTP_OK)->setJSON(['data' => '<span id="icon" style=""><i class="fas '.$reactionStyle.'"></i></span>']);
                            }
                        }
                    }
                    else
                    {
                        // undo that reaction
                        if ( !$postReactionModel->deleteReactionOf($postId, $reactionId) )
                        {
                            log_message('error', 'User {uid} status-react ' . implode(' ', $postReactionModel->errors()), ['uid' => user_id()]);
                            return $this->response->setStatusCode(Response::HTTP_BAD_REQUEST)->setJSON(['message' => implode(' ', $postReactionModel->errors())]);
                        }
                        else
                        {
                            return $this->response->setStatusCode(Response::HTTP_OK)->setJSON(['data' => '<span id="icon" style=""><i class="far '.$reactionStyle.'"></i></span>']);
                        }
                    }
                }
            }
        }
    }

    public function getTimeline()
	{
		if ( ! $this->request->isAJAX() ) {
			exit('No direct script access allowed');
		}

        if ( empty(user()) ) {
			return $this->response->setStatusCode(Response::HTTP_UNAUTHORIZED)->setJSON(['message' => 'session_expired']);
		}

        $postModel = new PostModel();
        $uname = $this->request->getPost('uname');
        $page = ($this->request->getPost('page') == '') ? 1 : $this->request->getPost('page')+1;
		$perPage = 8;
        $offset = ($page == 1) ? 0 : (($page-1) * $perPage);
        $defaultView = ($uname == '') ? '_post_timeline_home' : '_post_timeline_profile';

        try {
            $getPosts = $postModel->getPostTimeline($uname, $perPage, $offset);
            $no_more_data = (count($getPosts)>0) ? False : True;
            $is_empty = ($page == 1 && count($getPosts) == 0) ? true : false;

            return $this->response->setStatusCode(Response::HTTP_OK)->setJSON([
                'html' => view($defaultView, ['posts' => $getPosts]),
                'lId' => $page,
                'no_more_data' => $no_more_data,
                'is_empty' => $is_empty
            ]);
        } catch (\Exception $e) {
            log_message('error', $e->getMessage());
            return $this->response->setStatusCode(Response::HTTP_BAD_REQUEST)->setJSON(['message' => 'Fetch data error.']);
        }

	}
}