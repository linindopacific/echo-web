<?php namespace App\Controllers;

/*
 * File: HomeController.php
 * Project: echo
 * File Created: Wednesday, 19th August 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Thursday, 19th November 2020
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

class HomeController extends BaseController
{
	public function __construct()
	{
		//
	}

	public function index()
	{
		$serviceModel = new \App\Models\ServiceModel();

		return view('index', [
			'bodyClass' => 'body_home',
			'active_home' => 'active',
			'viewLayout' => 'App\Views\Template\layout',
			'pinnedService' => $serviceModel->pinned()
		]);
	}
}