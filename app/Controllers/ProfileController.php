<?php namespace App\Controllers;

/*
 * File: ProfileController.php
 * Project: echo
 * File Created: Monday, 31st August 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Tuesday, 18th May 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use App\Entities\UserRelation;
use App\Models\UserModel;
use App\Models\UserProfileModel;
use App\Models\UserRelationModel;
use CodeIgniter\HTTP\Response;

class ProfileController extends BaseController
{
	public function __construct()
	{
		//
	}

	public function userLookup()
	{
		$uri = service('uri');

		$userProfileModel = new UserProfileModel();
		$isMe = FALSE;

		if ($uri->getSegment(2) === '') {
			$profile = $userProfileModel->getProfile();
			$isMe = TRUE;
		} else {
			$profile = $userProfileModel->getProfileByUsername($uri->getSegment(2));
			$isMe = ($uri->getSegment(2) === user()->username) ? TRUE : FALSE;
		}

		if (!empty($profile))
		{
			return view('profile', [
				'bodyClass' => 'body_home',
				'active_profile' => 'active',
				'viewLayout' => 'App\Views\Template\layout',
				'profile' => $profile,
				'isMe'	=> $isMe,
				'title' => $profile->display_name
			]);
		}
		else
		{
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}
	}

	public function ajax_search()
	{
		if (!$this->request->isAJAX()) {
			exit('No direct script access allowed');
		}

		$phrase = $this->request->getPost('phrase');

		$response = [];
		if ($phrase != '') {
			$userProfileModel = new UserProfileModel();
			$users = $userProfileModel->searchProfiles($phrase);

			foreach ($users as $row) {
				$response[] = [
					'text' => $row->display_name,
					'image' => $row->display_picture,
					'link' => site_url('profile/' . $row->username)
				];
			}
		}

		return $this->response->setJSON($response);
	}


	public function search()
	{
		$phrase = $this->request->getPost('phrase');

		$results = [];
		if ($phrase != '') {
			$userProfileModel = new UserProfileModel();
			$results = $userProfileModel->searchProfiles($phrase);
		}

		return view('profile_results', [
			'viewLayout' => 'App\Views\Template\layout',
			'results' => $results
		]);
	}

	public function me()
	{
		if (!$this->request->isAJAX()) {
			exit('No direct script access allowed');
		}

		if (empty(user())) {
			return $this->response->setStatusCode(Response::HTTP_UNAUTHORIZED)->setJSON(['message' => 'session_expired']);
		}

		$userProfileModel = new UserProfileModel();
		$userRelationModel = new UserRelationModel();
		$profile = $userProfileModel->getProfileByUsername($this->request->getPost('uid'));
		$isMe = ($this->request->getPost('uid') === user()->username) ? TRUE : FALSE;
		$userSecondLevel = [];

		foreach ($userRelationModel->getSecondLevelConnectionList() as $row) {
			if ( $userRelationModel->hasConnectedWith($row->user_one_id) === false ) {
				$userSecondLevel[] = $userProfileModel->getProfile($row->user_one_id);
			} else {
				$userSecondLevel[] = $userProfileModel->getProfile($row->user_two_id);
			}
		}

		return $this->response->setStatusCode(Response::HTTP_OK)->setJSON(['data' => view('profile_me', [
			'profile' => $profile,
			'isMe'	=> $isMe,
			'mayYouKnow' => $userSecondLevel
		])]);
	}

	public function about()
	{
		if (!$this->request->isAJAX()) {
			exit('No direct script access allowed');
		}

		if (empty(user())) {
			return $this->response->setStatusCode(Response::HTTP_UNAUTHORIZED)->setJSON(['message' => 'session_expired']);
		}

		$userProfileModel = new UserProfileModel();
		$profile = $userProfileModel->getProfileByUsername($this->request->getPost('uid'));

		return $this->response->setStatusCode(Response::HTTP_OK)->setJSON(['data' => view('profile_about', [
			'profile' => $profile
		])]);
	}

	public function connectionStatus()
	{
		if (!$this->request->isAJAX()) {
			exit('No direct script access allowed');
		}

		if (empty(user())) {
			return $this->response->setStatusCode(Response::HTTP_UNAUTHORIZED)->setJSON(['message' => 'session_expired']);
		}

		$userName = '';
		if ($this->request->hasHeader('Referer'))
		{
			$uri = new \CodeIgniter\HTTP\URI($this->request->getHeaderLine('Referer'));
			$userName = $uri->setSilent()->getSegment(2);
		}

		if ($userName === '')
			return $this->response->setStatusCode(Response::HTTP_OK)->setJSON(['data' => '']);

		$userProfileModel = new UserProfileModel();
		$profile = $userProfileModel->getProfileByUsername($userName);
		$isMe = ($userName === user()->username) ? TRUE : FALSE;

		return $this->response->setStatusCode(Response::HTTP_OK)->setJSON(['data' => view('profile_connection_status', [
			'profile' => $profile,
			'isMe'	=> $isMe,
		])]);
	}

	public function connection()
	{
		if (!$this->request->isAJAX()) {
			exit('No direct script access allowed');
		}

		if (empty(user())) {
			return $this->response->setStatusCode(Response::HTTP_UNAUTHORIZED)->setJSON(['message' => 'session_expired']);
		}

		$userProfileModel = new UserProfileModel();
		$userRelationModel = new UserRelationModel();
		$profile = $userProfileModel->getProfileByUsername($this->request->getPost('uid'));
		$userRelations = [];
		$pendingRequest = [];

		foreach ($userRelationModel->getConnectionList($profile->user_id) as $row) {
			if ( $row->user_one_id === $profile->user_id ) {
				$userRelations[] = $userProfileModel->getProfile($row->user_two_id);
			} elseif ( $row->user_two_id === $profile->user_id ) {
				$userRelations[] = $userProfileModel->getProfile($row->user_one_id);
			}
		}
		foreach ($userRelationModel->getRequestConnectionList($profile->user_id) as $row) {
			$pendingRequest[] = $userProfileModel->getProfile($row->action_user_id);
		}

		return $this->response->setStatusCode(Response::HTTP_OK)->setJSON(['data' => view('profile_connection', [
			'profile' => $profile,
			'relations' => $userRelations,
			'pending' => $pendingRequest
		])]);
	}

	public function attemptAction()
	{
		if (!$this->request->isAJAX()) {
			exit('No direct script access allowed');
        }

        if (empty(user())) {
			return $this->response->setStatusCode(Response::HTTP_UNAUTHORIZED)->setJSON(['message' => 'session_expired']);
		}

		$request = $this->request;
		$uid = (int) $request->getPost('uid');
        $actType = $request->getPost('act_type');

		if ($actType == 'private-message')
		{
			$userModel = new UserModel();
			$user = $userModel->find($uid);
			if ( !empty($user) )
			{
				$url_message = site_url('messages/t/'.$user->username);
				return $this->response->setStatusCode(Response::HTTP_OK)->setJSON(['data' => $url_message]);
			}
			else
			{
				return $this->response->setJSON(['status' => 'error', 'message' => 'not_found']);
			}
		}
		else
		{
			$userRelationModel = new UserRelationModel();
			if ($actType === 'relation-invite')
			{
				$relation = $userRelationModel->statusConnectionWith($uid);
				if ( !empty($relation) )
				{
					// already sent a request first
					return $this->response->setStatusCode(Response::HTTP_BAD_REQUEST)->setJSON(['message' => 'Unable to add']);
				}
				else
				{
					$relation = new UserRelation();
					$relation->user_one_id = user_id();
					$relation->user_two_id = $uid;
					$relation->status = 0;
					$relation->action_user_id = user_id();

					if ( !$userRelationModel->save($relation) )
					{
						log_message('error', 'User {uid} relation-invite {uid2} ' . implode(' ', $userRelationModel->errors()), ['uid' => user_id(), 'uid2' => $uid]);
						return $this->response->setStatusCode(Response::HTTP_BAD_REQUEST)->setJSON(['message' => 'Unable to add']);
					}
					else
					{
						return $this->response->setStatusCode(Response::HTTP_OK)->setJSON(['data' => null]);
					}
				}
			}
			elseif ($actType === 'relation-remove')
			{
				$relation = $userRelationModel->statusConnectionWith($uid);
				if ( !empty($relation) )
				{
					if ( !$userRelationModel->delete($relation->id) )
					{
						log_message('error', 'User {uid} relation-remove {uid2} ' . implode(' ', $userRelationModel->errors()), ['uid' => user_id(), 'uid2' => $uid]);
						return $this->response->setStatusCode(Response::HTTP_BAD_REQUEST)->setJSON(['message' => 'Unable to update']);
					}
					else
					{
						return $this->response->setStatusCode(Response::HTTP_OK)->setJSON(['data' => null]);
					}
				}
				else
				{
					return $this->response->setStatusCode(Response::HTTP_NOT_FOUND)->setJSON(['message' => 'Not Found']);
				}
			}
			elseif ($actType === 'relation-cancel')
			{
				$relation = $userRelationModel->pendingRequest(user_id(), $uid);
				if ( !empty($relation) )
				{
					if ( !$userRelationModel->delete($relation->id) )
					{
						log_message('error', 'User {uid} relation-invite {uid2} ' . implode(' ', $userRelationModel->errors()), ['uid' => user_id(), 'uid2' => $uid]);
						return $this->response->setStatusCode(Response::HTTP_BAD_REQUEST)->setJSON(['message' => 'Unable to add']);
					}
					else
					{
						return $this->response->setStatusCode(Response::HTTP_OK)->setJSON(['data' => null]);
					}
				}
				else
				{
					return $this->response->setStatusCode(Response::HTTP_NOT_FOUND)->setJSON(['message' => 'Not Found']);
				}
			}
			elseif ($actType === 'relation-reject')
			{
				$relation = $userRelationModel->pendingRequest($uid, user_id());
				if ( !empty($relation) )
				{
					if ( !$userRelationModel->delete($relation->id) )
					{
						log_message('error', 'User {uid} relation-reject {uid2} ' . implode(' ', $userRelationModel->errors()), ['uid' => user_id(), 'uid2' => $uid]);
						return $this->response->setStatusCode(Response::HTTP_BAD_REQUEST)->setJSON(['message' => 'Unable to update']);
					}
					else
					{
						return $this->response->setStatusCode(Response::HTTP_OK)->setJSON(['data' => null]);
					}
				}
				else
				{
					return $this->response->setStatusCode(Response::HTTP_NOT_FOUND)->setJSON(['message' => 'Not Found']);
				}
			}
			elseif ($actType === 'relation-approve')
			{
				$relation = $userRelationModel->pendingRequest($uid, user_id());
				if ( !empty($relation) )
				{
					$relation->status = 1;
					$relation->action_user_id = user_id();
					if ( !$userRelationModel->save($relation) )
					{
						log_message('error', 'User {uid} relation-approve {uid2} ' . implode(' ', $userRelationModel->errors()), ['uid' => user_id(), 'uid2' => $uid]);
						return $this->response->setStatusCode(Response::HTTP_BAD_REQUEST)->setJSON(['message' => 'Unable to update']);
					}
					else
					{
						return $this->response->setStatusCode(Response::HTTP_OK)->setJSON(['data' => null]);
					}
				}
				else
				{
					return $this->response->setStatusCode(Response::HTTP_NOT_FOUND)->setJSON(['message' => 'Not Found']);
				}
			}
			else
			{
				die(var_dump($actType));
			}
		}
    }
}