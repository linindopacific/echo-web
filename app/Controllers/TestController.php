<?php namespace App\Controllers;

/*
 * File: TestController.php
 * Project: echo
 * File Created: Friday, 6th November 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

class TestController extends BaseController
{
    public function myIP()
	{
		$ipstack = service('ipstack');

		dd( $ipstack);
	}

	public function test()
	{
		$model = new \App\Models\ServiceModel();
		$get = $model->find(122);

		dd($get->reviews);
    }
}