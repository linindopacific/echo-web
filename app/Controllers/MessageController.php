<?php namespace App\Controllers;

/*
 * File: MessageController.php
 * Project: echo
 * File Created: Friday, 27th November 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Wednesday, 16th December 2020
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use App\Entities\MessageReply;
use App\Models\MessageModel;
use App\Models\MessageReplyModel;
use App\Models\UserProfileModel;
use CodeIgniter\I18n\Time;

class MessageController extends BaseController
{
    public function threadLookup()
    {
        $uri = service('uri');

        if ($uri->getSegment(3) === '') {
            die('Error occured.');
        } else {
            $uname = $uri->getSegment(3);

            $userProfileModel = new UserProfileModel();
            $profile = $userProfileModel->getProfileByUsername($uname);
        }

        return view('messaging', [
            'title' => 'Messaging',
            'viewLayout' => 'App\Views\Template\layout',
            'profile' => $profile
        ]);
    }

    public function attemptStore()
    {
        if (!$this->request->isAJAX()) {
			exit('No direct script access allowed');
        }

        $request = $this->request;

        $type = $request->getPost('type');
        $fromId = $request->getPost('from');
        $toId = $request->getPost('to');
        $content = $request->getPost('content');
        $myTime = Time::now();

        $timeString = date('H:i', $myTime->getTimestamp());
        $dateString = date('M d', $myTime->getTimestamp());

        $profile = [];

        if ($type === 'out') // save to db
        {
            // Determining If Logged In
            if (! $this->auth->check() )
            {
                $this->session->set('redirect_url', current_url() );
                return $this->response->setJSON(['status' => 'error', 'message' => 'session_expired']);
            }

            // find conversation id, if not exist create new
            $messageModel = new MessageModel();
            $messageId = $messageModel->createConversation($fromId, $toId);

            $messageReplyModel = new MessageReplyModel();
            $messageReply = new MessageReply();
            $messageReply->content = $content;
            $messageReply->messages_id = $messageId;
            $messageReply->user_id = $fromId;
            if ( !$messageReplyModel->save($messageReply) )
            {
                return $this->response->setJSON(['status' => 'error', 'message' => implode(' ', $messageReplyModel->errors())]);
            }
        }
        else
        {
            // get profile picture the other side
            $userProfileModel = new UserProfileModel();
            $profile = $userProfileModel->getProfile($fromId);
        }

        return $this->response->setJSON(['status' => 'success', 'data' => view('_message', ['profile' => $profile, 'type' => $type, 'content' => $content, 'dateString' => $dateString, 'timeString' => $timeString])]);
    }

    public function getThread()
    {
        if (!$this->request->isAJAX()) {
			exit('No direct script access allowed');
        }

        // Determining If Logged In
        if (! $this->auth->check() )
        {
            $this->session->set('redirect_url', current_url() );
            return $this->response->setJSON(['status' => 'error', 'message' => 'session_expired']);
        }

        $request = $this->request;

        $toId = $request->getPost('to');

        $messageModel = new MessageModel();
        $message = $messageModel->hasConversation(user_id(), $toId);

        $messages = [];

        if ( !empty($message) )
        {
            $messageReplyModel = new MessageReplyModel();
            $messages = $messageReplyModel->getConversations($message->id);
        }

        return $this->response->setJSON(['status' => 'success', 'data' => view('_messages', ['data' => $messages])]);
    }
}