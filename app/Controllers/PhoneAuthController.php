<?php namespace App\Controllers;

/*
 * File: PhoneAuthController.php
 * Project: echo
 * File Created: Friday, 16th July 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Friday, 16th July 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use App\Controllers\BaseController;
use App\Entities\PhoneAuth;
use App\Models\PhoneAuthModel;
use CodeIgniter\HTTP\Response;
use CodeIgniter\I18n\Time;

class PhoneAuthController extends BaseController
{
	public function checkLogin()
	{
		// throttle check
		$phoneNumber = $this->request->getPost('phone_number');

		$phoneAuthModel = new PhoneAuthModel();
		if ( $phoneAuthModel->hold_request($phoneNumber) )
		{
			return $this->response->setStatusCode(Response::HTTP_BAD_REQUEST)->setJSON(['message' => 'Please wait']);
		}

		return $this->response->setJSON(['status' => 'OK']);
	}

	public function attemptLogin()
	{
		$data = $this->request->getPost();
		$data['created_at'] = Time::now()->toDateTimeString();

		$phoneAuth = new PhoneAuth();
		$phoneAuth->fill($data);

		$phoneAuthModel = new PhoneAuthModel();
		if ( !$phoneAuthModel->save($phoneAuth) )
		{
			return $this->response->setStatusCode(Response::HTTP_BAD_REQUEST)->setJSON(['message' => implode(' ', $phoneAuthModel->errors)]);
		}

		return $this->response->setJSON(['status' => 'OK']);
	}
}
