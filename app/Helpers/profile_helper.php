<?php

/*
 * File: profile_helper.php
 * Project: echo
 * File Created: Monday, 12th October 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Tuesday, 13th October 2020
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use Config\Database;
use Config\Services;

if (! function_exists('user_profile'))
{
	function user_profile()
	{
        $db = Database::connect();
        if ( !$db->tableExists('user_profile') ) {
            return NULL;
        }

        $authenticate = Services::authentication();
        if ( !$authenticate->check() ) {
            return NULL;
        }

        $userProfileModel = new App\Models\UserProfileModel();
        return $userProfileModel->getProfile($authenticate->id());
	}
}