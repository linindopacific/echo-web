<?php namespace App\Models;

/*
 * File: RegProvinceModel.php
 * Project: echo
 * File Created: Friday, 28th August 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 28th June 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use App\Entities\RegProvince;
use CodeIgniter\Model;

class RegProvinceModel extends Model
{
	protected $table = TBL_REG_PROVINCES;

	protected $primaryKey = 'id';

	protected $returnType = RegProvince::class;

	public function getProvinces($id = false)
	{
		if ($id == false) {
			return $this->findAll();
		}

		return $this->where('id', $id)->findAll();
	}

	public function getProvincesByName($name)
	{
		return $this->where('name', $name)->first();
	}
}