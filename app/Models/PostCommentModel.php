<?php namespace App\Models;

/*
 * File: PostCommentModel.php
 * Project: echo
 * File Created: Friday, 18th December 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 28th June 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use App\Entities\PostComment;
use CodeIgniter\Model;

class PostCommentModel extends Model
{
    protected $table = TBL_POST_COMMENTS;

	protected $primaryKey = 'id';

    protected $returnType = PostComment::class;

    protected $useSoftDeletes = true;

	protected $allowedFields = [
		'content', 'parent_id', 'post_id', 'user_id'
	];

	protected $useTimestamps = true;

	protected $afterInsert = ['addToActivity'];

	protected $afterUpdate = ['addToActivity'];

	protected $afterDelete = ['addToActivity'];

	/**
	 * get notifier userId
	 *
	 * @param integer $commentId
	 * @return array
	 */
	private function getNotifierIds(int $commentId) : array
	{
		$userIds = [];

		$comment = $this->find($commentId);
		if ( ! empty($comment) )
		{
			// owner post
			$postId = $comment->post_id;
			$postModel = new PostModel();
			$post = $postModel->find($postId);
			if ( ! empty($post) )
			{
				$userId = $post->user_id;
				if ( $userId <> user_id() )
				{
					array_push($userIds, $userId);
				}
			}

			// another commenter
			$commenter = $this->select('user_id')->where('post_id', $postId)->findAll();
			foreach ( $commenter as $row )
			{
				$userId = $row->user_id;
				if ( $userId <> user_id() )
				{
					array_push($userIds, $userId);
				}
			}
		}

		return array_unique($userIds);
	}

	protected function addToActivity($data)
    {
		if ( is_null($data['id']) )
			return;

		if ( isset($data['data']['created_at']) )
		{
			// create
			$activity = service('activity');
			$objectId = $activity->saveObject('comment-create', $data['id'], user_id());

			// get notifier
			$notifierIds = $this->getNotifierIds($data['id']);
			$activity->notifyUsers($objectId, $notifierIds);

		}
		elseif ( isset($data['data']['updated_at']) )
		{
			// update
			if ( $data['result'] == true)
			{
				// afterUpdate return the array of pk
				$arr_length = count($data['id']);
				for ($i=0; $i < $arr_length; $i++) {
					$activity = service('activity');
					$objectId = $activity->saveObject('comment-update', $data['id'][$i], user_id());

					// get notifier
					$notifierIds = $this->getNotifierIds($data['id'][$i]);
					$activity->notifyUsers($objectId, $notifierIds);
				}
			}
		}
		elseif ( is_null($data['data']) )
		{
			// delete
			if ( $data['result'] == true)
			{
				$arr_length = count($data['id']);
				for ($i=0; $i < $arr_length; $i++) {
					$activity = service('activity');
					$activity->saveObject('comment-delete', $data['id'][$i], user_id());
				}
			}
		}
    }
}