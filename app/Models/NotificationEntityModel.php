<?php namespace App\Models;

/*
 * File: NotificationEntityModel.php
 * Project: echo
 * File Created: Monday, 19th April 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Thursday, 22nd April 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use App\Entities\NotificationEntity;
use CodeIgniter\Model;

class NotificationEntityModel extends Model
{
    protected $table = TBL_NOTIFICATION_ENTITY;

	protected $primaryKey = 'id';

    protected $returnType = NotificationEntity::class;

    protected $useSoftDeletes = true;

	protected $allowedFields = [
		'name', 'reference_table'
	];

	protected $useTimestamps = true;

	protected $validationRules = [
		'name' => [
			'rules' => 'required|alpha_dash|max_length[50]|is_unique[notification_entity.name,id,{id}]',
			'errors' => []
		],
        'reference_table' => [
			'rules' => 'required|string|max_length[100]',
			'errors' => []
		]
	];

	/**
	 * Get Entity Type Id
	 *
	 * @param integer $id
	 * @return object
	 */
	public function get_entity_type(int $id) : object
	{
		$entity = $this->find($id);
		if ( ! empty($entity) )
		{
			return $entity;
		}

		return false;
	}
}