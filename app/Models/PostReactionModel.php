<?php namespace App\Models;

/*
 * File: PostReactionModel.php
 * Project: echo
 * File Created: Monday, 2nd November 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 28th June 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use App\Entities\PostReaction;
use CodeIgniter\Model;

class PostReactionModel extends Model
{
    protected $table = TBL_POST_REACTIONS;

	protected $returnType = PostReaction::class;

    protected $useSoftDeletes = true;

	protected $allowedFields = [
		'post_id', 'reaction_id', 'user_id'
	];

	protected $useTimestamps = true;

	protected $validationRules = [
		'post_id' => [
			'rules' => 'required|integer',
			'errors' => []
		],
        'reaction_id' => [
			'rules' => 'required|integer',
			'errors' => []
		],
		'user_id' => [
			'rules' => 'required|integer',
			'errors' => []
		]
	];

	protected $afterInsert = ['addToActivity'];

	/**
	 * Get reaction
	 *
	 * @param integer $postId
	 * @param integer $reactionId
	 * @return void
	 */
	public function getReactionOf(int $postId, int $reactionId)
	{
		return $this->where('post_id', $postId)
					->where('reaction_id', $reactionId)
					->where('user_id', user_id())
					->where('deleted_at', null)
					->first();
	}

	/**
	 * Delete reaction
	 *
	 * @param integer $postId
	 * @param integer $reactionId
	 * @return boolean
	 */
	public function deleteReactionOf(int $postId, int $reactionId) : bool
	{
		$delete = $this->where('post_id', $postId)
					   ->where('reaction_id', $reactionId)
					   ->where('user_id', user_id())
					   ->where('deleted_at', null)
					   ->delete();

		if ( $delete )
		{
			$this->addToActivity([
				'result' => true,
				'data' => [
					'post_id' => $postId,
					'reaction_id' => $reactionId
				]
			]);
			return true;
		}

		return false;
	}

	/**
	 * Get notifier userId
	 *
	 * @param integer $postId
	 * @return array
	 */
	private function getNotifierIds(int $postId) : array
	{
		$userIds = [];

		// owner post
		$postModel = new PostModel();
		$post = $postModel->find($postId);
		if ( ! empty($post) )
		{
			$userId = $post->user_id;
			if ( $userId <> user_id() )
			{
				array_push($userIds, $userId);
			}
		}

		return array_unique($userIds);
	}

	protected function addToActivity($data)
    {
		if ( isset($data['data']['created_at']) )
		{
			// create
			$postId = $data['data']['post_id'];
			$reactionId = $data['data']['reaction_id'];
			$reactionModel = new \App\Models\ReactionModel();
			$reaction = $reactionModel->find($reactionId);
			$reactionName = $reaction->name;

			$activity = service('activity');
			$objectId = $activity->saveObject('status-react-' . $reactionName, $postId, user_id());

			// get notifier
			$notifierIds = $this->getNotifierIds($postId);
			$activity->notifyUsers($objectId, $notifierIds);
		}
		else
		{
			// delete
			if ( $data['result'] == true)
			{
				$postId = $data['data']['post_id'];
				$reactionId = $data['data']['reaction_id'];
				$reactionModel = new \App\Models\ReactionModel();
				$reaction = $reactionModel->find($reactionId);
				$reactionName = $reaction->name;

				// update
				$activity = service('activity');
				$activity->saveObject('status-react', $postId, user_id());
			}
		}
    }
}