<?php namespace App\Models;

/*
 * File: UserRelationModel.php
 * Project: echo
 * File Created: Thursday, 26th November 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Wednesday, 19th May 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use App\Entities\UserRelation;
use CodeIgniter\Model;

class UserRelationModel extends Model
{
    protected $table = TBL_USER_RELATION;

	protected $primaryKey = 'id';

	protected $useAutoIncrement = true;

	protected $returnType = UserRelation::class;

	protected $allowedFields = [
        'user_one_id', 'user_two_id', 'status', 'action_user_id'
	];

	protected $useTimestamps = true;

	protected $validationRules = [
		'user_one_id' => [
			'rules' => 'required|integer',
			'errors' => []
		],
        'user_two_id' => [
			'rules' => 'required|integer',
			'errors' => []
		],
		'status' => [
			'rules' => 'required|integer|max_length[3]',
			'errors' => []
		],
		'action_user_id' => [
			'rules' => 'required|integer',
			'errors' => []
		]
	];

	protected $afterInsert = ['addToActivity'];

	protected $afterUpdate = ['addToActivity'];

	protected $afterDelete = ['addToActivity'];

	/**
	* @param int $user_one_id
	* @param int $user_two_id
	*
	* @return array|object|null The resulting row of data, or null
	*/
	public function pendingRequest($user_one_id, $user_two_id)
	{
		return $this->where('user_one_id', $user_one_id)
					->where('user_two_id', $user_two_id)
					->where('status', 0)
					->where('action_user_id', $user_one_id)
					->where('deleted_at', null)
					->first();
	}

	public function hasConnectedWith(int $user_id) : bool
	{
		$check = $this->where('user_one_id', user_id())
					  ->where('user_two_id', $user_id)
					  ->where('status', 1)
					  ->where('deleted_at', null)
					  ->orWhere('user_one_id', $user_id)
					  ->where('user_two_id', user_id())
					  ->where('status', 1)
					  ->where('deleted_at', null)
					  ->first();

		return ( !empty($check) ) ? true : false;
	}

	public function statusConnectionWith(int $user_id)
	{
		$check = $this->where('user_one_id', user_id())
					  ->where('user_two_id', $user_id)
					  ->where('deleted_at', null)
					  ->orWhere('user_one_id', $user_id)
					  ->where('user_two_id', user_id())
					  ->where('deleted_at', null)
					  ->first();

		return ( !empty($check) ) ? $check : null;
	}

	public function getConnectionList(int $user_id)
	{
		return $this->where('user_one_id', $user_id)
					->where('status', 1)
					->where('deleted_at', null)
					->orWhere('user_two_id', $user_id)
					->where('status', 1)
					->where('deleted_at', null)
					->findAll();
	}

	public function getSecondLevelConnectionList()
	{
		$sql = "SELECT * FROM user_relation
				WHERE `status` = ? AND user_one_id IN (select CASE WHEN a.user_one_id = ? THEN a.user_two_id ELSE a.user_one_id END AS user_id from user_relation a WHERE a.`status` = ? AND a.user_one_id = ? OR a.`status` = ? AND a.user_two_id = ?)
				AND user_two_id <> ?
				OR `status` = ? AND user_two_id IN (select CASE WHEN a.user_one_id = ? THEN a.user_two_id ELSE a.user_one_id END AS user_id from user_relation a WHERE a.`status` = ? AND a.user_one_id = ? OR a.`status` = ? AND a.user_two_id = ?)
				AND user_one_id <> ?";

		$query = $this->query($sql, [1, user_id(), 1, user_id(), 1, user_id(), user_id(), 1, user_id(), 1, user_id(), 1, user_id(), user_id()]);

		return $query->getResult($this->returnType);
	}

	public function getRequestConnectionList(int $user_id)
	{
		return $this->where('user_two_id', $user_id)
					->where('status', 0)
					->where('action_user_id !=', $user_id)
					->where('deleted_at', null)
					->findAll();
	}

	protected function addToActivity($data)
    {
		if ( is_null($data['id']) )
			return;

		if ( isset($data['data']['created_at']) )
		{
			// create
			$activity = service('activity');
			$objectId = $activity->saveObject('relation-invite', $data['id'], user_id());
			$notifierId = $data['data']['user_two_id'];
			$activity->notifyUsers($objectId, [$notifierId]);

			$pusher = service('pusher_channel');
			$channelName = 'private-user' . $notifierId;
			$pusher->trigger($channelName, 'relation', 'relation-invite');
		}
		elseif ( isset($data['data']['updated_at']) )
		{
			// update
			if ( $data['result'] == true)
			{
				// afterUpdate return the array of pk
				$arr_length = count($data['id']);
				for ($i=0; $i < $arr_length; $i++) {
					$activity = service('activity');
					if ( $data['data']['status'] == 1 )
					{
						$objectId = $activity->saveObject('relation-approve', $data['id'][$i], user_id());

						// get notifier
						$check = $this->find($data['id'][$i]);
						$notifierId = $check->user_one_id;
						$activity->notifyUsers($objectId, [$notifierId]);

						$pusher = service('pusher_channel');
						$channelName = 'private-user' . $notifierId;
						$pusher->trigger($channelName, 'relation', 'relation-approve');
					}
					elseif ( $data['data']['status'] == 3 )
					{
						$activity->saveObject('relation-ban', $data['id'][$i], user_id());
					}
				}
			}
		}
		elseif ( is_null($data['data']) )
		{
			// delete
			if ( $data['result'] == true)
			{
				$actType = $_POST['act_type']; // relation-cancel, relation-reject, relation-remove

				$arr_length = count($data['id']);
				for ($i=0; $i < $arr_length; $i++) {
					$activity = service('activity');
					$activity->saveObject($actType, $data['id'][$i], user_id());
				}
			}
		}
    }
}