<?php namespace App\Models;

/*
 * File: MessageModel.php
 * Project: echo
 * File Created: Thursday, 10th December 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 28th June 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use App\Entities\Message;
use CodeIgniter\Model;

class MessageModel extends Model
{
    protected $table = TBL_MESSAGES;

	protected $primaryKey = 'id';

    protected $returnType = Message::class;

	protected $allowedFields = [
		'user_one_id', 'user_two_id'
	];

	public function hasConversation(int $user_id, int $another_user_id)
	{
		return $this->where('user_one_id', $user_id)
					->where('user_two_id', $another_user_id)
					->orWhere('user_two_id', $user_id)
					->where('user_one_id', $another_user_id)
					->first();
	}

	public function createConversation(int $user_id, int $another_user_id) : int
	{
		$check = $this->hasConversation($user_id, $another_user_id);

		if ( !empty($check) )
		{
			return $check->id;
		}
		else
		{
			if ($user_id < $another_user_id)
			{
				$firstId = $user_id;
				$secondId = $another_user_id;
			}
			else
			{
				$firstId = $another_user_id;
				$secondId = $user_id;
			}

			$this->insert(['user_one_id' => $firstId, 'user_two_id' => $secondId]);
			return $this->insertID();
		}
	}
}