<?php namespace App\Models;

/*
 * File: UserProfileModel.php
 * Project: echo
 * File Created: Thursday, 27th August 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 28th June 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use App\Entities\UserProfile;
use CodeIgniter\Model;

class UserProfileModel extends Model
{
	protected $table = TBL_USER_PROFILES;

	protected $primaryKey = 'id';

	protected $useAutoIncrement = true;

	protected $returnType = UserProfile::class;

	protected $useSoftDeletes = true;

	protected $allowedFields = [
		'user_id', 'first_name', 'middle_name', 'last_name', 'gender', 'address_1', 'address_2', 'city', 'region', 'zip_code', 'phone_number', 'birthdate', 'display_picture', 'visibility', 'job', 'hobby', 'industry', 'about'
	];

	protected $useTimestamps = true;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'first_name' => 'alpha_space|max_length[50]|permit_empty',
		'middle_name' => 'alpha_space|max_length[50]|permit_empty',
		'last_name' => 'alpha_space|max_length[50]|permit_empty',
		'gender' => 'alpha|in_list[male,female,other]|permit_empty',
		'address_1' => 'string|permit_empty',
		'address_2' => 'string|permit_empty',
		'region' => 'string|max_length[255]|permit_empty',
		'city' => 'string|max_length[255]|permit_empty',
		'zip_code' => 'string|max_length[10]|permit_empty',
		'phone_number' => 'string|max_length[20]|permit_empty',
		'birthdate' => 'valid_date|permit_empty',
		'display_picture' => 'integer|permit_empty',
		'visibility' => 'in_list[0,1]|permit_empty',
		'job' => 'string|max_length[255]|permit_empty',
		'hobby' => 'string|max_length[255]|permit_empty',
		'industry' => 'string|max_length[255]|permit_empty',
		'about' => 'string|max_length[255]|permit_empty',
	];

	protected $validationMessages = [];

	protected $skipValidation = false;

	protected $afterInsert = ['addToActivity'];

	protected $afterUpdate = ['addToActivity'];

	protected $afterDelete = ['addToActivity'];

	public function getProfile($id = false)
	{
		$id = ($id === false) ? user_id() : $id;

		$this->select($this->table . '.id');
		$this->select($this->table . '.first_name');
		$this->select($this->table . '.middle_name');
		$this->select($this->table . '.last_name');
		$this->select($this->table . '.gender');
		$this->select($this->table . '.address_1');
		$this->select($this->table . '.address_2');
		$this->select($this->table . '.city');
		$this->select($this->table . '.region');
		$this->select($this->table . '.zip_code');
		$this->select($this->table . '.phone_number');
		$this->select($this->table . '.birthdate');
		$this->select($this->table . '.job');
		$this->select($this->table . '.hobby');
		$this->select($this->table . '.industry');
		$this->select($this->table . '.about');
		$this->select($this->table . '.display_picture');
		$this->select($this->table . '.visibility');
		$this->select($this->table . '.user_id');
		$this->select($this->table . '.created_at');
		$this->select($this->table . '.updated_at');
		$this->select($this->table . '.deleted_at');
		$this->select(TBL_USERS . '.username');
		$this->select(TBL_USERS . '.email');
		$this->select(TBL_USERS . '.display_name');
		$this->select(TBL_USERS . '.active');

		return $this->join(TBL_USERS, TBL_USERS . '.id = ' . $this->table .  '.user_id')
					->where($this->table . '.user_id', $id)
					->first();
	}

	public function getProfileByUsername(string $username)
	{
		return $this->join(TBL_USERS, TBL_USERS . '.id = ' . $this->table .  '.user_id')
					->where(TBL_USERS . '.active', True)
					->where(TBL_USERS . '.username', $username)
					->first();
	}

	public function getProfileByEmail(string $email)
	{
		return $this->join(TBL_USERS, TBL_USERS . '.id = ' . $this->table .  '.user_id')
					->where(TBL_USERS . '.active', True)
					->where(TBL_USERS . '.email', $email)
					->first();
	}

	public function searchProfiles(string $phrase)
	{
		return $this->join(TBL_USERS, TBL_USERS . '.id = ' . $this->table .  '.user_id')
					->where($this->table . '.visibility', True)
					->where(TBL_USERS . '.active', True)
					->where(TBL_USERS . '.id !=', user_id())
					->like(TBL_USERS . '.display_name', $phrase)
					->orderBy(TBL_USERS . '.display_name', 'ASC')
					->findAll();
	}

	protected function addToActivity($data)
    {
		if ( is_null($data['id']) )
			return;

		if ( isset($data['data']['created_at']) )
		{
			// create
			$activity = service('activity');
			$activity->saveObject('profile-create', $data['id'], user_id());
		}
		elseif ( isset($data['data']['updated_at']) )
		{
			// update
			if ( $data['result'] == true)
			{
				// afterUpdate return the array of pk
				$arr_length = count($data['id']);
				for ($i=0; $i < $arr_length; $i++) {
					$activity = service('activity');
					$activity->saveObject('profile-update', $data['id'][$i], user_id());
				}
			}
		}
    }
}