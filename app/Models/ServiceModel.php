<?php namespace App\Models;

/*
 * File: ServiceModel.php
 * Project: echo
 * File Created: Thursday, 3rd September 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Thursday, 18th November 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use App\Entities\Service;
use CodeIgniter\Model;

class ServiceModel extends Model
{
	protected $table = TBL_SERVICES;

	protected $primaryKey = 'id';

	protected $returnType = Service::class;

	protected $useSoftDeletes = true;

	protected $allowedFields = [
		'name', 'description', 'category_id', 'price', 'price_per', 'featured', 'pinned', 'active', 'user_id'
	];

	protected $useTimestamps = true;

	protected $validationRules = [
		'name' => [
			'label' => 'Rules.service_name',
			'rules' => 'required|string|max_length[100]',
			'errors' => []
		],
        'description' => [
			'label' => 'Rules.service_description',
			'rules' => 'required|string|max_length[65535]',
			'errors' => []
		],
		'coverage' => [
			'label' => 'Rules.service_coverage',
			'rules' => 'required',
			'errors' => []
		],
		'category_id' => [
			'label' => 'Rules.service_category',
			'rules' => 'required|integer',
			'errors' => []
		],
		'price' => [
			'label' => 'Rules.service_price',
			'rules' => 'required|numeric|greater_than[0]',
			'errors' => []
		],
		'price_per' => [
			'label' => 'Rules.service_price_per',
			'rules' => 'required|alpha|in_list[hour,day,event]',
			'errors' => []
		],
        'featured' => [
			'label' => 'Rules.service_featured',
			'rules' => 'permit_empty',
			'errors' => []
		],
		'pinned' => [
			'label' => 'Rules.service_pinned',
			'rules' => 'permit_empty',
			'errors' => []
		],
        'active' => [
			'label' => 'Rules.service_active',
			'rules' => 'permit_empty',
			'errors' => []
		],
		'user_id' => [
			'rules' => 'required|integer',
			'errors' => []
		]
	];

	protected $afterInsert = ['addToActivity'];

	protected $afterUpdate = ['addToActivity'];

	protected $afterDelete = ['addToActivity'];

	private function marketplaces(array $filters = [])
	{
		$this->distinct();
		$this->select($this->table . '.id');
		$this->select($this->table . '.name');
		$this->select($this->table . '.description');
		$this->select($this->table . '.category_id');
		$this->select($this->table . '.price');
		$this->select($this->table . '.price_per');
		$this->select($this->table . '.featured');
		$this->select($this->table . '.active');
		$this->select($this->table . '.user_id');
		$this->select($this->table . '.created_at');
		$this->select($this->table . '.updated_at');
		$this->select($this->table . '.deleted_at');

		// check filter category
		if ( array_key_exists('category', $filters) ) {
			$this->whereIn($this->table . '.category_id', $filters['category']);
		}
		// check filter coverage location
		if ( array_key_exists('coverage', $filters) ) {
			$this->join(TBL_SERVICE_COVERAGES, TBL_SERVICE_COVERAGES . '.service_id = ' . $this->table .  '.id')
				 ->where(TBL_SERVICE_COVERAGES . '.deleted_at', NULL)
				 ->whereIn(TBL_SERVICE_COVERAGES . '.regency_id', $filters['coverage']);
		}
		// check filter stars
		if ( array_key_exists('star', $filters) ) {
			$this->whereIn('(
				SELECT COALESCE(floor(AVG(rating)),0) FROM ' . TBL_SERVICE_REVIEWS . ' AS b WHERE b.deleted_at IS NULL
				AND EXISTS (
					SELECT * FROM ' . TBL_SERVICE_FULFILLMENTS . ' c WHERE c.id = b.fulfillment_id AND c.deleted_at IS NULL
					AND c.order_id IN (SELECT id FROM ' . TBL_SERVICE_ORDERS . ' d WHERE d.service_id = ' . $this->table . '.id)
				)
			)', $filters['star']);
		}
		// check filter price per
		if ( array_key_exists('price_per', $filters) ) {
			$this->whereIn($this->table . '.price_per', $filters['price_per']);
		}

		return $this->join(TBL_USERS, TBL_USERS . '.id = ' . $this->table .  '.user_id')
					->where($this->table . '.active', true);
	}

	public function countMarketplaces(array $filters) : int
	{
		return $this->marketplaces($filters)->countAllResults();
	}

	public function getMarketplaces(array $filters, int $limit=0, int $offset=0)
	{
		return $this->marketplaces($filters)->findAll($limit, $offset);
	}

	private function recommendations(array $filters = [])
	{
		return $this->marketplaces($filters)->orderBy('name', 'RANDOM'); // sementara gunakan ini
	}

	public function countRecommendations(array $filters) : int
	{
		return $this->recommendations($filters)->countAllResults();
	}

	public function getRecommendations(array $filters, int $limit=0, int $offset=0)
	{
		return $this->recommendations($filters)->findAll($limit, $offset);
	}

	public function draft()
	{
		return $this->where('active', false)
					->where('user_id', user_id())
					->orderBy('pinned', 'DESC')
					->orderBy('name', 'ASC')
					->findAll();
	}

	public function published()
	{
		return $this->where('active', true)
					->where('user_id', user_id())
					->orderBy('pinned', 'DESC')
					->orderBy('name', 'ASC')
					->findAll();
	}

	public function pinned()
	{
		return $this->where('active', true)
					->where('user_id', user_id())
					->where('pinned', true)
					->first();
	}

	private function clear_pinned(int $id)
	{
		return $this->set('pinned', false)
			 		->where('pinned', true)
			 		->where('active', true)
					->where('user_id', user_id())
					->where('id <>', $id)
					->update();
	}

	public function is_ready(int $id)
	{
		return $this->where('id', $id)
					->where('active', true)
					->first();
	}

	protected function addToActivity($data)
    {
		if ( is_null($data['id']) )
			return;

		if ( isset($data['data']['created_at']) )
		{
			// create
			$activity = service('activity');
			$activity->saveObject('service-create', $data['id'], user_id());

			// is pinned
			if ($data['data']['pinned'] == 1) {
				$this->clear_pinned($data['id']);
			}
		}
		elseif ( isset($data['data']['updated_at']) )
		{
			// update
			if ( $data['result'] == true)
			{
				//if ( is_array($data['id']) ) die(var_dump( count($data['id']) ));

				// afterUpdate return the array of pk
				$arr_length = count($data['id']);
				for ($i=0; $i < $arr_length; $i++) {
					$activity = service('activity');
					$activity->saveObject('service-update', $data['id'][$i], user_id());

					// is pinned
					if ( isset($data['data']['pinned']) ) {
						if ($data['data']['pinned'] == 1) {
							$this->clear_pinned($data['id'][$i]);
						}
					}
				}
			}
		}
		elseif ( is_null($data['data']) )
		{
			// delete
			if ( $data['result'] == true)
			{
				$activity = service('activity');
				$activity->saveObject('service-delete', $data['id'], user_id());
			}
		}

		if ( !is_null($data) ) $this->updateCoverages($data);
    }

	function updateCoverages($data)
    {
		$serviceCoverageModel = new ServiceCoverageModel();
		$regencyIds = $_POST['service_coverage'];

		if ( isset($data['data']['created_at']) )
		{
			for ($i=0; $i < count($regencyIds); $i++)
			{
				$regencyId = $regencyIds[$i];
				$serviceCoverageModel->addCoverage($regencyId, $data['id']);
			}
		}
		elseif ( isset($data['data']['updated_at']) )
		{
			// update
			if ( $data['result'] == true)
			{
				// afterUpdate return the array of pk
				for ($i=0; $i < count($data['id']); $i++)
				{
					$serviceId = $data['id'][$i];
					$serviceCoverageModel->removeCoverage($regencyIds, $serviceId);

					for ($j=0; $j < count($regencyIds); $j++)
					{
						$regencyId = $regencyIds[$j];
						$serviceCoverageModel->addCoverage($regencyId, $serviceId);
					}
				}
			}
		}
    }
}