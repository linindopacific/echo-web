<?php namespace App\Models;

/*
 * File: MessageReplyModel.php
 * Project: echo
 * File Created: Thursday, 10th December 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 28th June 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use App\Entities\MessageReply;
use CodeIgniter\Model;

class MessageReplyModel extends Model
{
    protected $table = TBL_MESSAGE_REPLIES;

	protected $primaryKey = 'id';

    protected $returnType = MessageReply::class;

    protected $useSoftDeletes = true;

	protected $allowedFields = [
		'content', 'messages_id', 'user_id'
	];

	protected $useTimestamps = true;

	public function getConversations(int $message_id)
	{
		return $this->where('messages_id', $message_id)->findAll();
	}
}