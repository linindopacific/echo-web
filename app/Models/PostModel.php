<?php namespace App\Models;

/*
 * File: PostModel.php
 * Project: echo
 * File Created: Monday, 26th October 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 28th June 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use App\Entities\Post;
use CodeIgniter\Model;

class PostModel extends Model
{
    protected $table = TBL_POSTS;

	protected $primaryKey = 'id';

    protected $returnType = Post::class;

    protected $useSoftDeletes = true;

	protected $allowedFields = [
		'content', 'visibility', 'user_id'
	];

	protected $useTimestamps = true;

	protected $validationRules = [
		'content' => [
			'label' => 'Rules.post_content',
			'rules' => 'required|string|max_length[65535]',
			'errors' => []
		],
        'visibility' => [
			'label' => 'Rules.post_visibility',
			'rules' => 'required|integer',
			'errors' => []
		],
		'user_id' => [
			'rules' => 'required|integer',
			'errors' => []
		]
	];

	protected $afterInsert = ['addToActivity'];

	protected $afterUpdate = ['addToActivity'];

	protected $afterDelete = ['addToActivity'];

	public function getPostTimeline(string $uname, int $limit=0, int $offset=0)
	{
		if ($uname === '')
		{
			$sql = "SELECT * FROM " . $this->table . " a ";
			$sql.= "WHERE a.deleted_at IS NULL AND a.user_id = ? " ;
			$sql.= "OR a.deleted_at IS NULL AND a.visibility = (SELECT id FROM " . TBL_POST_VISIBILITIES . " WHERE name = ?) ";
			$sql.= "OR a.deleted_at IS NULL AND a.visibility = (SELECT id FROM " . TBL_POST_VISIBILITIES . " WHERE name = ?) AND EXISTS (SELECT * FROM " . TBL_USER_RELATION . " WHERE status = ? AND user_one_id = a.user_id AND user_two_id = ? OR status = ? AND user_one_id = ? AND user_two_id = a.user_id) ";
			$sql.= "ORDER BY a.created_at DESC ";
			$sql.= "LIMIT $limit OFFSET $offset";

			$query = $this->query($sql, [user_id(), 'public', 'connection', 1, user_id(), 1, user_id()]);
		}
		else
		{
			if (user()->username === $uname)
			{
				$sql = "SELECT * FROM " . $this->table . " a ";
				$sql.= "WHERE a.user_id = ? AND a.deleted_at IS NULL ";
				$sql.= "ORDER BY a.created_at DESC ";
				$sql.= "LIMIT $limit OFFSET $offset";

				$query = $this->query($sql, [user_id()]);
			}
			else
			{
				$userModel = new \App\Models\UserModel();
				$user = $userModel->where('username', $uname)->first();

				$sql = "SELECT * FROM " . $this->table . " a ";
				$sql.= "WHERE a.user_id = ? AND a.deleted_at IS NULL AND a.visibility = (SELECT id FROM " . TBL_POST_VISIBILITIES . " WHERE name = ?) ";
				$sql.= "OR a.user_id = ? AND a.deleted_at IS NULL AND a.visibility = (SELECT id FROM " . TBL_POST_VISIBILITIES . " WHERE name = ?) AND EXISTS (SELECT * FROM " . TBL_USER_RELATION . " WHERE status = ? AND user_one_id = a.user_id AND user_two_id = ? OR status = ? AND user_one_id = ? AND user_two_id = a.user_id) ";
				$sql.= "ORDER BY a.created_at DESC ";
				$sql.= "LIMIT $limit OFFSET $offset";

				$query = $this->query($sql, [$user->id, 'public', $user->id, 'connection', 1, user_id(), 1, user_id()]);
			}
		}

		return $query->getResult($this->returnType);
	}

	protected function addToActivity($data)
    {
		if ( is_null($data['id']) )
			return;

		if ( isset($data['data']['created_at']) )
		{
			// create
			$activity = service('activity');
			$activity->saveObject('status-create', $data['id'], user_id());
		}
		elseif ( isset($data['data']['updated_at']) )
		{
			// update
			if ( $data['result'] == true)
			{
				// afterUpdate return the array of pk
				$arr_length = count($data['id']);
				for ($i=0; $i < $arr_length; $i++) {
					$activity = service('activity');
					$activity->saveObject('status-update', $data['id'][$i], user_id());
				}
			}
		}
		elseif ( is_null($data['data']) )
		{
			// delete
			if ( $data['result'] == true)
			{
				$arr_length = count($data['id']);
				for ($i=0; $i < $arr_length; $i++) {
					$activity = service('activity');
					$activity->saveObject('status-delete', $data['id'][$i], user_id());
				}
			}
		}
    }
}