<?php namespace App\Models;

/*
 * File: ServiceCategoryModel.php
 * Project: echo
 * File Created: Friday, 28th August 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 28th June 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use App\Entities\ServiceCategory;
use CodeIgniter\Model;

class ServiceCategoryModel extends Model
{
	protected $table = TBL_SERVICE_CATEGORIES;

	protected $primaryKey = 'id';

	protected $returnType = ServiceCategory::class;

	protected $useSoftDeletes = true;

	protected $allowedFields = [
		'name', 'parent_id'
	];

	protected $useTimestamps = true;

	public function getCategories($parent = false)
	{
		if ($parent == false) {
			return $this->where('parent_id', NULL)->orderBy('name', 'ASC')->findAll();
		}

		return $this->where('parent_id', $parent)->orderBy('name', 'ASC')->findAll();
	}
}