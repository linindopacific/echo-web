<?php namespace App\Models;

/*
 * File: NotificationChangeModel.php
 * Project: echo
 * File Created: Monday, 21st December 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 28th June 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use App\Entities\NotificationChange;
use CodeIgniter\Model;

class NotificationChangeModel extends Model
{
    protected $table = TBL_NOTIFICATION_CHANGE;

	protected $primaryKey = 'id';

    protected $returnType = NotificationChange::class;

    protected $useSoftDeletes = true;

	protected $allowedFields = [
		'notification_object_id', 'actor_id'
	];

	protected $useTimestamps = true;

	protected $validationRules = [
		'notification_object_id' => [
			'rules' => 'required|integer|max_length[20]',
			'errors' => []
		],
        'actor_id' => [
			'rules' => 'required|integer|max_length[10]',
			'errors' => []
		]
	];

	/**
	 * Get Actor
	 *
	 * @param integer $objectId
	 * @return array
	 */
	public function get_actor(int $objectId) : array
	{
		$actor = [
			'displayName' => '',
			'displayPicture' => 'https://ui-avatars.com/api/?name=User&bold=true'
		];

		$change = $this->where('notification_object_id', $objectId)->first();
		if ( ! empty($change) )
		{
			$actorId = $change->actor_id;
			$userProfileModel = new UserProfileModel();
			$userProfile = $userProfileModel->getProfile($actorId);
			$actor['displayName'] = $userProfile->displayName;
			$actor['displayPicture'] = $userProfile->displayPicture;
		}

		return $actor;
	}
}