<?php namespace App\Models;

/*
 * File: NotificationObjectModel.php
 * Project: echo
 * File Created: Monday, 21st December 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Tuesday, 27th April 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use App\Entities\NotificationObject;
use CodeIgniter\Model;

class NotificationObjectModel extends Model
{
    protected $table = TBL_NOTIFICATION_OBJECT;

	protected $primaryKey = 'id';

    protected $returnType = NotificationObject::class;

    protected $useSoftDeletes = false;

	protected $allowedFields = [
		'entity_type_id', 'entity_id', 'status'
	];

	protected $useTimestamps = true;

	protected $validationRules = [
		'entity_type_id' => [
			'rules' => 'required|integer|max_length[20]',
			'errors' => []
		],
        'entity_id' => [
			'rules' => 'required|integer|max_length[20]',
			'errors' => []
		],
		'status' => [
			'rules' => 'required|integer',
			'errors' => []
		]
	];

	/**
	 * Get object from activity
	 *
	 * @param integer $type
	 * @param integer $id
	 * @param integer $userId
	 * @return array
	 */
	public function get_object_from_activity(int $type, int $id, int $userId = 0)
	{
		$this->select($this->table . '.id');
		$this->select($this->table . '.entity_type_id');
		$this->select($this->table . '.entity_id');
		$this->select($this->table . '.status');

		$this->join(TBL_NOTIFICATION_CHANGE, TBL_NOTIFICATION_CHANGE . '.notification_object_id = ' . $this->table .  '.id', 'left')
			->where($this->table . '.entity_type_id', $type)
			->where($this->table . '.entity_id', $id)
			->where($this->table . '.deleted_at', null);

		if ( $userId <> 0 )
			$this->where(TBL_NOTIFICATION_CHANGE . '.actor_id', $userId);

		return $this->findAll();
	}
}