<?php namespace App\Models;

/*
 * File: PhoneAuthModel.php
 * Project: echo
 * File Created: Friday, 16th July 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Friday, 16th July 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use App\Entities\PhoneAuth;
use CodeIgniter\I18n\Time;
use CodeIgniter\Model;

class PhoneAuthModel extends Model
{
	protected $table = TBL_AUTH_PHONE_VERIFY;

	protected $returnType = PhoneAuth::class;

	protected $useSoftDeletes = true;

	protected $allowedFields = ['code', 'session_info', 'phone_number', 'created_at'];

	public function hold_request(string $phoneNo)
	{
		$auth = $this->where('code', null)
					 ->where('phone_number', $phoneNo)
					 ->orderBy('created_at', 'desc')
					 ->first();

		if ( !empty($auth) )
		{
			// hold for 10 minutes for new request
			$current = Time::now();
			$time = Time::parse($auth->created_at);

			$diff = $time->difference($current);
			if ( $diff->getMinutes() < 10 )
			{
				return true;
			}
		}

		return false;
	}
}
