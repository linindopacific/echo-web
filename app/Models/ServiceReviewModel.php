<?php namespace App\Models;

/*
 * File: ServiceReviewModel.php
 * Project: echo
 * File Created: Wednesday, 17th March 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Wednesday, 17th November 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use App\Entities\ServiceReview;
use CodeIgniter\Model;

class ServiceReviewModel extends Model
{
    protected $table = TBL_SERVICE_REVIEWS;

	protected $primaryKey = 'id';

	protected $returnType = ServiceReview::class;

	protected $useSoftDeletes = true;

	protected $allowedFields = [
        'fulfillment_id', 'content', 'rating', 'user_id'
	];

	protected $useTimestamps = true;

    protected $validationRules = [
		'fulfillment_id' => [
			'label' => 'Rules.fulfillment_id',
			'rules' => 'required|integer',
			'errors' => []
		],
        'content' => [
			'label' => 'Rules.review_content',
			'rules' => 'permit_empty|string',
			'errors' => []
		],
        'rating' => [
			'label' => 'Rules.review_rating',
			'rules' => 'required|integer|max_length[3]',
			'errors' => []
		],
		'user_id' => [
			'rules' => 'required|integer',
			'errors' => []
		]
	];

	public function get_reviews(int $serviceId)
	{
		$sql = "SELECT * FROM " . TBL_SERVICE_REVIEWS . " as a ";
		$sql.= "WHERE a.deleted_at IS NULL ";
		$sql.= "AND EXISTS (SELECT * FROM " . TBL_SERVICE_FULFILLMENTS . " b WHERE b.id = a.fulfillment_id AND b.deleted_at IS NULL ";
		$sql.= "AND b.order_id IN (SELECT id FROM " . TBL_SERVICE_ORDERS . " c WHERE c.service_id = ?) ";
		$sql.= ") ";
		$sql.= "ORDER BY a.created_at DESC ";

		$query = $this->query($sql, [$serviceId]);

		return $query->getResult($this->returnType);
	}

	public function count_ratings(int $serviceId) : float
	{
		$sql = "SELECT AVG(rating) as value FROM " . TBL_SERVICE_REVIEWS . " as a ";
		$sql.= "WHERE a.deleted_at IS NULL ";
		$sql.= "AND EXISTS (SELECT * FROM " . TBL_SERVICE_FULFILLMENTS . " b WHERE b.id = a.fulfillment_id AND b.deleted_at IS NULL ";
		$sql.= "AND b.order_id IN (SELECT id FROM " . TBL_SERVICE_ORDERS . " c WHERE c.service_id = ?) ";
		$sql.= ") ";

		$query = $this->query($sql, [$serviceId]);

		$rating = $query->getRow()->value;

		return (float) $rating;
	}
}