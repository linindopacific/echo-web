<?php namespace App\Models;

/*
 * File: ServiceFulfillmentModel.php
 * Project: echo
 * File Created: Monday, 18th January 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Friday, 20th August 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use App\Entities\ServiceFulfillment;
use CodeIgniter\Model;

class ServiceFulfillmentModel extends Model
{
    protected $table = TBL_SERVICE_FULFILLMENTS;

    protected $primaryKey = 'id';

    protected $returnType = ServiceFulfillment::class;

	protected $useSoftDeletes = true;

	protected $allowedFields = [
		'arrived_at', 'order_id', 'remarks', 'completed', 'user_id'
	];

	protected $useTimestamps = true;

	protected $validationRules = [
		'arrived_at' => [
			'rules' => 'required|valid_date',
			'errors' => []
		],
		'order_id' => [
			'rules' => 'required|integer',
			'errors' => []
		],
		'remarks' => [
			'rules' => 'permit_empty|string|max_length[50]',
			'errors' => []
		],
		'completed' => [
			'rules' => 'permit_empty',
			'errors' => []
		],
		'user_id' => [
			'rules' => 'required|integer',
			'errors' => []
		]
	];

	protected $afterInsert = [
		'addToActivity'
	];

	protected $afterUpdate = [
		'addToActivity'
	];

	public function findByOrderId(int $orderId)
	{
		return $this->where('order_id', $orderId)->first();
	}

	protected function addToActivity($data)
    {
		if ( is_null($data['id']) )
			return;

		if ( isset($data['data']['created_at']) )
		{
			// create
			$activity = service('activity');
			$objectId = $activity->saveObject('fulfillment-create', $data['id'], user_id());

			// get notifier
			$check = $this->find($data['id']);
			$notifierId = $check->order->user_id;
			$activity->notifyUsers($objectId, [$notifierId]);

			$pusher = service('pusher_channel');
			$channelName = 'private-user' . $notifierId;
			$pusher->trigger($channelName, 'fulfillment', 'fulfillment-create');
		}
		elseif ( isset($data['data']['updated_at']) )
		{
			// update
			if ( $data['result'] == true)
			{
				// afterUpdate return the array of pk
				$arr_length = count($data['id']);
				for ($i=0; $i < $arr_length; $i++) {
					if ( isset($data['data']['completed']) ) {
						if ($data['data']['completed'] === true) {
							$activity = service('activity');
							$objectId = $activity->saveObject('fulfillment-complete', $data['id'][$i], user_id());

							// get notifier
							$check = $this->find($data['id'][$i]);
							$notifierId = $check->service->user_id;
							$activity->notifyUsers($objectId, [$notifierId]);

							$pusher = service('pusher_channel');
							$channelName = 'private-user' . $notifierId;
							$pusher->trigger($channelName, 'fulfillment', 'fulfillment-complete');
						}
					}
				}
			}
		}
    }
}