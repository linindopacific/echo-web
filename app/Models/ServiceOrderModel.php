<?php namespace App\Models;

/*
 * File: ServiceOrderModel.php
 * Project: echo
 * File Created: Wednesday, 25th November 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Wednesday, 17th November 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use App\Entities\ServiceOrder;
use CodeIgniter\Model;

class ServiceOrderModel extends Model
{
    protected $table = TBL_SERVICE_ORDERS;

	protected $primaryKey = 'id';

	protected $returnType = ServiceOrder::class;

	protected $useSoftDeletes = true;

	protected $allowedFields = [
        'order_no', 'buy_from_name', 'buy_from_address', 'buy_from_city', 'buy_from_zip_code', 'buy_from_phone_no', 'sell_to_name', 'sell_to_address', 'sell_to_city', 'sell_to_zip_code', 'sell_to_phone_no', 'request_date',
        'service_id', 'item', 'description', 'quantity', 'unit_price', 'unit_discount', 'amount', 'market_fee', 'remarks', 'referal_code', 'fulfilled', 'status', 'user_id'
	];

	protected $useTimestamps = true;

	protected $validationRules = [
		'order_no' => [
			'label' => 'Rules.order_no',
			'rules' => 'required|string|max_length[50]|is_unique['.TBL_SERVICE_ORDERS.'.order_no,id,{id}]',
			'errors' => []
		],
		'buy_from_name' => [
			'label' => 'Rules.order_provider_name',
			'rules' => 'required|alpha_space|max_length[255]',
			'errors' => []
		],
		'buy_from_address' => [
			'label' => 'Rules.order_provider_address',
			'rules' => 'required|string',
			'errors' => []
		],
		'buy_from_city' => [
			'label' => 'Rules.order_provider_city',
			'rules' => 'required|string',
			'errors' => []
		],
		'buy_from_zip_code' => [
			'label' => 'Rules.order_provider_zipcode',
			'rules' => 'permit_empty|numeric',
			'errors' => []
		],
		'buy_from_phone_no' => [
			'label' => 'Rules.order_provider_phone',
			'rules' => 'required|numeric|max_length[20]',
			'errors' => []
		],
		'sell_to_name' => [
			'label' => 'Rules.order_customer_name',
			'rules' => 'required|alpha_space|max_length[255]',
			'errors' => []
		],
		'sell_to_address' => [
			'label' => 'Rules.order_customer_address',
			'rules' => 'required|string',
			'errors' => []
		],
		'sell_to_city' => [
			'label' => 'Rules.order_customer_city',
			'rules' => 'required|string',
			'errors' => []
		],
		'sell_to_zip_code' => [
			'label' => 'Rules.order_customer_zipcode',
			'rules' => 'permit_empty|numeric',
			'errors' => []
		],
		'sell_to_phone_no' => [
			'label' => 'Rules.order_customer_phone',
			'rules' => 'required|numeric|max_length[20]',
			'errors' => []
		],
		'request_date' => [
			'label' => 'Rules.order_request_date',
			'rules' => 'required|valid_date',
			'errors' => []
		],
		'service_id' => [
			'label' => 'Rules.service_id',
			'rules' => 'required|integer',
			'errors' => []
		],
		'item' => [
			'label' => 'Rules.order_item',
			'rules' => 'required|string|max_length[100]',
			'errors' => []
		],
		'description' => [
			'label' => 'Rules.order_description',
			'rules' => 'permit_empty|string',
			'errors' => []
		],
		'quantity' => [
			'label' => 'Rules.order_quantity',
			'rules' => 'required|numeric',
			'errors' => []
		],
		'unit_price' => [
			'label' => 'Rules.order_unit_price',
			'rules' => 'required|decimal',
			'errors' => []
		],
		'unit_discount' => [
			'label' => 'Rules.order_unit_discount',
			'rules' => 'permit_empty|decimal',
			'errors' => []
		],
		'amount' => [
			'label' => 'Rules.order_amount',
			'rules' => 'required|decimal',
			'errors' => []
		],
		'market_fee' => [
			'label' => 'Rules.order_fees',
			'rules' => 'required|decimal',
			'errors' => []
		],
		'remarks' => [
			'label' => 'Rules.order_remarks',
			'rules' => 'permit_empty|string',
			'errors' => []
		],
		'referal_code' => [
			'label' => 'Rules.order_referal_code',
			'rules' => 'permit_empty|alpha_numeric',
			'errors' => []
		],
		'fulfilled' => [
			'label' => 'Rules.order_fulfilled',
			'rules' => 'permit_empty',
			'errors' => []
		],
		'status' => [
			'label' => 'Rules.order_status',
			'rules' => 'permit_empty|integer',
			'errors' => []
		],
		'user_id' => [
			'rules' => 'required|integer',
			'errors' => []
		]
	];

	protected $afterInsert = ['addToActivity'];

	protected $afterUpdate = ['addToActivity'];

	public function create_no_series()
	{
		// SVO/20201230/000000001
		$currentDate = date("Ymd");
		$query = $this->query("SELECT * FROM ".$this->table." WHERE MID(order_no, 5, 8) = ? ORDER BY RIGHT(order_no, 9) DESC LIMIT 1", [$currentDate]);
		$row = $query->getRow();
		if ( isset($row) )
		{
			$order_no = $row->order_no;
			$number = (int)substr($order_no, -9);
			$number = $number+1;
			return 'SVO' . '/' . $currentDate . '/' . sprintf("%09d", $number);
		}
		else
		{
			return 'SVO' . '/' . $currentDate . '/' . sprintf("%09d", 1);
		}
	}

	private function orders(string $type)
	{
		$this->select($this->table . '.id');
		$this->select($this->table . '.order_no');
		$this->select($this->table . '.buy_from_name');
		$this->select($this->table . '.buy_from_address');
		$this->select($this->table . '.buy_from_zip_code');
		$this->select($this->table . '.buy_from_phone_no');
		$this->select($this->table . '.sell_to_name');
		$this->select($this->table . '.sell_to_address');
		$this->select($this->table . '.sell_to_zip_code');
		$this->select($this->table . '.sell_to_phone_no');
		$this->select($this->table . '.request_date');
		$this->select($this->table . '.service_id');
		$this->select($this->table . '.item');
		$this->select($this->table . '.description');
		$this->select($this->table . '.quantity');
		$this->select($this->table . '.unit_price');
		$this->select($this->table . '.unit_discount');
		$this->select($this->table . '.amount');
		$this->select($this->table . '.market_fee');
		$this->select($this->table . '.remarks');
		$this->select($this->table . '.referal_code');
		$this->select($this->table . '.fulfilled');
		$this->select($this->table . '.status');
		$this->select($this->table . '.user_id');
		$this->select($this->table . '.created_at');
		$this->select($this->table . '.updated_at');
		$this->select($this->table . '.deleted_at');

		if ( $type == '0' ) // semua
		{
			// all
		}
		elseif ( $type == '1' ) { // menunggu dibayar
			$this->join(TBL_SERVICE_INVOICES_XND, TBL_SERVICE_INVOICES_XND . '.order_id = ' . $this->table .  '.id')
				 ->where(TBL_SERVICE_INVOICES_XND . '.transaction_at', NULL)
				 ->where(TBL_SERVICE_INVOICES_XND . '.expired_at >=', date('Y-m-d H:i:s'));
		}
		elseif ( $type == '2' ) { // diproses
			$this->join(TBL_SERVICE_INVOICES_XND, TBL_SERVICE_INVOICES_XND . '.order_id = ' . $this->table .  '.id')
				 ->where(TBL_SERVICE_INVOICES_XND . '.transaction_at !=', NULL);
			$this->whereNotIn($this->table . '.id', function(\CodeIgniter\Database\BaseBuilder $builder) {
				return $builder->select('order_id')->from(TBL_SERVICE_FULFILLMENTS)->where('deleted_at', NULL);
			});
		}
		elseif ( $type == '3' ) { // selesai
			$this->whereIn($this->table . '.id', function(\CodeIgniter\Database\BaseBuilder $builder) {
				return $builder->select('order_id')->from(TBL_SERVICE_FULFILLMENTS)->where('deleted_at', NULL);
			});
		}

		$this->whereIn($this->table . '.id', function(\CodeIgniter\Database\BaseBuilder $builder) {
			return $builder->select('order_id')->from(TBL_SERVICE_INVOICES_XND)->where('deleted_at', NULL);
		});

		if (! is_cli())
		{
			// cant use session on cli
			$this->where($this->table . '.user_id', user_id());
		}

		$sort = ($type == '0' || $type == '3') ? 'DESC' : 'ASC';
		return $this->orderBy($this->table . '.created_at', $sort);
	}

	public function getOrders(string $type, int $limit=0, int $offset=0)
	{
		return $this->orders($type)->findAll($limit, $offset);
	}

	public function countOrders(string $type) : int
	{
		return $this->orders($type)->countAllResults();
	}

	private function sales(string $type)
	{
		$this->select($this->table . '.id');
		$this->select($this->table . '.order_no');
		$this->select($this->table . '.buy_from_name');
		$this->select($this->table . '.buy_from_address');
		$this->select($this->table . '.buy_from_zip_code');
		$this->select($this->table . '.buy_from_phone_no');
		$this->select($this->table . '.sell_to_name');
		$this->select($this->table . '.sell_to_address');
		$this->select($this->table . '.sell_to_zip_code');
		$this->select($this->table . '.sell_to_phone_no');
		$this->select($this->table . '.request_date');
		$this->select($this->table . '.service_id');
		$this->select($this->table . '.item');
		$this->select($this->table . '.description');
		$this->select($this->table . '.quantity');
		$this->select($this->table . '.unit_price');
		$this->select($this->table . '.unit_discount');
		$this->select($this->table . '.amount');
		$this->select($this->table . '.market_fee');
		$this->select($this->table . '.remarks');
		$this->select($this->table . '.referal_code');
		$this->select($this->table . '.fulfilled');
		$this->select($this->table . '.status');
		$this->select($this->table . '.user_id');
		$this->select($this->table . '.created_at');
		$this->select($this->table . '.updated_at');
		$this->select($this->table . '.deleted_at');

		if ($type == '0') // semua
		{
			// all
		}
		elseif ($type == '1') { // menunggu diproses
			$this->whereNotIn($this->table . '.id', function(\CodeIgniter\Database\BaseBuilder $builder) {
				return $builder->select('order_id')->from(TBL_SERVICE_FULFILLMENTS)->where('deleted_at', NULL);
			});
		}
		elseif ($type == '2') { // diproses
			$this->whereIn($this->table . '.id', function(\CodeIgniter\Database\BaseBuilder $builder) {
				return $builder->select('order_id')->from(TBL_SERVICE_FULFILLMENTS)->where('completed', false)->where('deleted_at', NULL);
			});
		}
		elseif ($type == '3') { // selesai, diselesaikan customer utk bisa masuk saldo
			$this->whereIn($this->table . '.id', function(\CodeIgniter\Database\BaseBuilder $builder) {
				return $builder->select('order_id')->from(TBL_SERVICE_FULFILLMENTS)->where('completed', true)->where('deleted_at', NULL);
			});
		}

		$this->whereIn($this->table . '.id', function(\CodeIgniter\Database\BaseBuilder $builder) {
			return $builder->select('order_id')->from(TBL_SERVICE_INVOICES_XND)->where('transaction_at !=', NULL)->where('deleted_at', NULL);
		});

		if (! is_cli())
		{
			// cant use session on cli
			$this->whereIn($this->table . '.service_id', function(\CodeIgniter\Database\BaseBuilder $builder) {
				return $builder->select('id')->from(TBL_SERVICES)->where('user_id', user_id())->where('deleted_at', NULL);
			});
		}

		$sort = ($type == '0' || $type == '3') ? 'DESC' : 'ASC';
		return $this->orderBy($this->table . '.created_at', $sort);
	}

	public function getSales(string $type, int $limit=0, int $offset=0)
	{
		return $this->sales($type)->findAll($limit, $offset);
	}

	public function countSales(string $type) : int
	{
		return $this->sales($type)->countAllResults();
	}

	public function is_order_belong_to_me(int $orderId) : bool
	{
		$isExists = $this->where('id', $orderId)->where('user_id', user_id())->first();

		return (! empty($isExists)) ? true : false;
	}

	public function get_earnings(int $userId = NULL)
	{
		$sql = "SELECT * FROM " . TBL_SERVICE_ORDERS . " a ";
		$sql.= "WHERE a.deleted_at IS NULL ";
		if ($userId)
		{
			$sql.= "AND EXISTS (SELECT * FROM " . TBL_SERVICES . " b WHERE b.id = a.service_id AND b.user_id = " . $userId . ") ";
		}
		$sql.= "AND EXISTS (SELECT * FROM " . TBL_SERVICE_INVOICES_XND . " c WHERE c.transaction_at IS NOT NULL AND c.order_id = a.id AND c.deleted_at IS NULL) ";
		$sql.= "AND EXISTS (SELECT * FROM " . TBL_SERVICE_FULFILLMENTS . " d WHERE d.completed = 1 AND d.order_id = a.id AND d.deleted_at IS NULL) ";

		$query = $this->query($sql);

		return $query->getResult();
	}

	public function count_completes(int $serviceId) : int
	{
		$this->whereIn($this->table . '.id', function(\CodeIgniter\Database\BaseBuilder $builder) {
			return $builder->select('order_id')->from(TBL_SERVICE_FULFILLMENTS)->where('completed', true)->where('deleted_at', NULL);
		});

		$this->whereIn($this->table . '.id', function(\CodeIgniter\Database\BaseBuilder $builder) {
			return $builder->select('order_id')->from(TBL_SERVICE_INVOICES_XND)->where('transaction_at !=', NULL)->where('deleted_at', NULL);
		});

		$this->where($this->table . ".service_id", $serviceId);

		return $this->countAllResults();
	}

	protected function addToActivity($data)
    {
		if ( is_null($data['id']) )
			return;

		if ( isset($data['data']['created_at']) )
		{
			// create
			$activity = service('activity');
			$activity->saveObject('order-create', $data['id'], user_id());
		}
		elseif ( isset($data['data']['updated_at']) )
		{
			// update
			if ( $data['result'] == true)
			{
				// afterUpdate return the array of pk
				$arr_length = count($data['id']);
				for ($i=0; $i < $arr_length; $i++) {
					$activity = service('activity');
					$activity->saveObject('order-update', $data['id'][$i], user_id());
				}
			}
		}
    }
}