<?php namespace App\Models;

/*
 * File: UploadModel.php
 * Project: echo
 * File Created: Thursday, 8th October 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 28th June 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use App\Entities\Upload;
Use CodeIgniter\Model;

class UploadModel extends Model
{
    protected $table = TBL_UPLOADS;

	protected $primaryKey = 'id';

	protected $useAutoIncrement = true;

	protected $returnType = Upload::class;

	protected $useSoftDeletes = true;

	protected $allowedFields = [
		'file_name', 'file_type', 'file_path', 'raw_name', 'orig_name', 'file_ext', 'file_size', 'is_image', 'image_width', 'image_height', 'image_type', 'user_id'
	];

	protected $useTimestamps = true;

	protected $validationRules = [
        'user_id' => 'required|integer',
        'file_name' => 'required|string|max_length[100]',
        'file_type' => 'required|string|max_length[20]',
        'file_path' => 'required|string|max_length[200]',
        'raw_name' => 'required|string',
        'orig_name' => 'required|string',
        'file_ext' => 'required|alpha|max_length[10]',
        'file_size' => 'required|decimal|max_length[10]',
        'is_image' => 'required|integer|max_length[1]',
        'image_width' => 'decimal|max_length[10]|permit_empty',
        'image_height' => 'decimal|max_length[10]|permit_empty',
        'image_type' => 'string|max_length[20]|permit_empty'
	];

	protected $validationMessages = [];

	protected $skipValidation = false;
}