<?php namespace App\Models;

/*
 * File: ServiceInvoiceModel.php
 * Project: echo
 * File Created: Wednesday, 30th December 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Thursday, 8th July 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use App\Entities\ServiceInvoice;
use CodeIgniter\I18n\Time;
use CodeIgniter\Model;

class ServiceInvoiceModel extends Model
{
    protected $table = TBL_SERVICE_INVOICES_XND;

	protected $primaryKey = 'id';

	protected $returnType = ServiceInvoice::class;

	protected $useSoftDeletes = true;

	protected $allowedFields = [
        'order_id', 'xnd_id', 'external_id', 'amount', 'payment_category', 'payment_channel', 'payment_code', 'qr_string',
		'payment_id', 'expired_at', 'transaction_at'
	];

	protected $useTimestamps = true;

	protected $validationRules = [
		'order_id' => [
			'label' => 'Rules.order_id',
			'rules' => 'required|integer',
			'errors' => []
		],
		'xnd_id' => [
			'label' => 'Rules.xnd.id',
			'rules' => 'required|string|max_length[255]',
			'errors' => []
		],
        'external_id' => [
			'label' => 'Rules.xnd.external_id',
			'rules' => 'required|string|max_length[100]',
			'errors' => []
		],
		'amount' => [
			'label' => 'Rules.xnd.amount',
			'rules' => 'required|numeric|greater_than[0]',
			'errors' => []
		],
		'payment_category' => [
			'label' => 'Rules.xnd.category',
			'rules' => 'required|string|max_length[20]',
			'errors' => []
		],
		'payment_channel' => [
			'label' => 'Rules.xnd.channel',
			'rules' => 'required|string|max_length[50]',
			'errors' => []
		],
		'payment_code' => [
			'label' => 'Rules.xnd.code',
			'rules' => 'permit_empty|string|max_length[65535]',
			'errors' => []
		],
		'qr_string' => [
			'label' => 'Rules.xnd.qr_string',
			'rules' => 'permit_empty|string|max_length[65535]',
			'errors' => []
		],
		'payment_id' => [
			'label' => 'Rules.xnd.payment_id',
			'rules' => 'permit_empty|string|max_length[255]',
			'errors' => []
		],
		'expired_at' => [
			'label' => 'Rules.xnd.expired_at',
			'rules' => 'required',
			'errors' => []
		],
		'transaction_at' => [
			'label' => 'Rules.xnd.transaction_at',
			'rules' => 'permit_empty',
			'errors' => []
		],
	];

	protected $afterUpdate = ['addToActivity'];

	public function findByExternalId(string $externalId)
	{
		$externalId = str_replace('-', '/', $externalId);
		$serviceInvoice = $this->where(['external_id' => $externalId])->first();

		return ( !empty($serviceInvoice) ) ? $serviceInvoice : false;
	}

	public function update_transaction(int $id, string $paymentId, string $transactionAt)
	{
		$serviceInvoice = $this->find($id);

		if ( !empty($serviceInvoice) )
		{
			if ( $serviceInvoice->is_paid == false )
			{
				$time = Time::parse($transactionAt, 'UTC');
				$time2 = $time->setTimezone('Asia/Jakarta');

				if ( $time2->isBefore($serviceInvoice->expired_at) )
				{
					$serviceInvoice->payment_id = $paymentId;
					$serviceInvoice->transaction_at = $time2->toDateTimeString();
					if ( !$this->save($serviceInvoice) )
					{
						log_message('error', implode(' ', $this->error()));
						return false;
					}
					else
					{
						return true;
					}
				}
			}
		}
		else
		{
			return false;
		}
	}

	protected function addToActivity($data)
    {
		if ( is_null($data['id']) )
			return;

		if ( isset($data['data']['updated_at']) )
		{
			// update
			if ( $data['result'] == true)
			{
				// afterUpdate return the array of pk
				$arr_length = count($data['id']);
				for ($i=0; $i < $arr_length; $i++) {
					$check = $this->find($data['id'][$i]);
					$actorId = $check->order->user_id;

					$activity = service('activity');
					$objectId = $activity->saveObject('payment-create', $data['id'][$i], $actorId);

					// get notifier
					$check = $this->find($data['id'][$i]);
					$notifierId = $check->service->user_id;
					$activity->notifyUsers($objectId, [$notifierId]);

					$pusher = service('pusher_channel');
					$channelName = 'private-user' . $notifierId;
					$pusher->trigger($channelName, 'payment', 'payment-create');
				}
			}
		}
    }
}