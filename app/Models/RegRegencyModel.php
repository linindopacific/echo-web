<?php namespace App\Models;

/*
 * File: RegRegencyModel.php
 * Project: echo
 * File Created: Friday, 28th August 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 28th June 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use App\Entities\RegRegency;
use CodeIgniter\Model;

class RegRegencyModel extends Model
{
	protected $table = TBL_REG_REGENCIES;

	protected $primaryKey = 'id';

	protected $returnType = RegRegency::class;

	public function getRegencies($id = false)
	{
		if ($id == false) {
			return $this->findAll();
		}

		return $this->where('id', $id)->findAll();
	}

	public function getProvinceRegencies($id)
	{
		return $this->where('province_id', $id)->findAll();
	}

	public function checkRegencyProvince(string $regencyName, string $provinceName)
	{
		$query = $this->where('name', $regencyName)
					  ->where('province_id', function(\CodeIgniter\Database\BaseBuilder $builder) use($provinceName) {
							return $builder->select('id')->from(TBL_REG_PROVINCES)->where('name', $provinceName);
						})
					  ->first();

		return (!empty($query)) ? true : false;
	}
}