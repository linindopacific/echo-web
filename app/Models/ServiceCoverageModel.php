<?php namespace App\Models;

/*
 * File: ServiceCoverageModel.php
 * Project: echo
 * File Created: Monday, 9th November 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Tuesday, 4th May 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use App\Entities\ServiceCoverage;
use CodeIgniter\Model;

class ServiceCoverageModel extends Model
{
    protected $table = TBL_SERVICE_COVERAGES;

    protected $primaryKey = 'id';

    protected $returnType = ServiceCoverage::class;

	protected $useSoftDeletes = true;

	protected $allowedFields = [
		'regency_id', 'service_id'
	];

	protected $useTimestamps = true;

	protected $validationRules = [
		'regency_id' => [
			'rules' => 'required|alpha_numeric|max_length[5]',
			'errors' => []
		],
        'service_id' => [
			'rules' => 'required|integer',
			'errors' => []
		],
	];

	protected $afterInsert = ['addToActivity'];

	protected $afterUpdate = ['addToActivity'];

	protected $afterDelete = ['addToActivity'];

	/**
	 * Add coverage of service
	 *
	 * @param string $regencyId
	 * @param integer $serviceId
	 * @return void
	 */
	public function addCoverage(string $regencyId, int $serviceId)
	{
		$postData = [
			'regency_id' => $regencyId,
			'service_id' => $serviceId
		];

		$validation =  \Config\Services::validation();
		$rules = $this->getValidationRules();
		$validationResult = $validation->setRules($rules)->run($postData);
		if ( !$validationResult )
		{
			$validationErrors = $validation->getErrors();
			log_message('error', 'User {uid} coverages-add on service {id} ' . implode(" ", $validationErrors), ['uid' => user_id(), 'id' => $serviceId]);
		}
		else
		{
			$serviceCoverage = $this->where('service_id', $serviceId)->where('regency_id', $regencyId)->first();
			if ( empty($serviceCoverage) )
			{
				$serviceCoverage = new ServiceCoverage($postData);
				if ( !$this->save($serviceCoverage) )
				{
					log_message('error', 'User {uid} coverages-add on service {id} ' . implode(" ", $this->errors()), ['uid' => user_id(), 'id' => $serviceId]);
				}
			}
		}
	}

	/**
	 * Remove coverage of service where not in
	 *
	 * @param array $regencyIds
	 * @param integer $serviceId
	 * @return void
	 */
	public function removeCoverage(array $regencyIds, int $serviceId)
	{
		$results = $this->asArray()->select('id')->where('service_id', $serviceId)->whereNotIn('regency_id', $regencyIds)->findAll();
		foreach ($results as $row)
		{
			if ( !$this->delete($row['id']) )
			{
				log_message('error', 'User {uid} coverages-remove {regencyId} on service {id} ' . implode(" ", $this->errors()), ['uid' => user_id(), 'regencyId' => $row['id'], 'id' => $serviceId]);
			}
		}
	}

	protected function addToActivity($data)
    {
		if ( is_null($data['id']) )
			return;

		if ( isset($data['data']['created_at']) )
		{
			// create
			$activity = service('activity');
			$activity->saveObject('coverages-add', $data['id'], user_id());
		}
		elseif ( isset($data['data']['updated_at']) )
		{
			// update
			if ( $data['result'] == true)
			{
				// afterUpdate return the array of pk
				$arr_length = count($data['id']);
				for ($i=0; $i < $arr_length; $i++) {
					$activity = service('activity');
					$activity->saveObject('coverages-add', $data['id'][$i], user_id());
				}
			}
		}
		elseif ( is_null($data['data']) )
		{
			// delete
			if ( $data['result'] == true)
			{
				$arr_length = count($data['id']);
				for ($i=0; $i < $arr_length; $i++) {
					$activity = service('activity');
					$activity->saveObject('coverages-remove', $data['id'][$i], user_id());
				}
			}
		}
    }
}