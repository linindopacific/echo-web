<?php namespace App\Models;

/*
 * File: NotificationsModel.php
 * Project: echo
 * File Created: Monday, 21st December 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Thursday, 19th August 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use App\Entities\Notifications;
use CodeIgniter\I18n\Time;
use CodeIgniter\Model;

class NotificationsModel extends Model
{
    protected $table = TBL_NOTIFICATIONS;

	protected $primaryKey = 'id';

    protected $returnType = Notifications::class;

    protected $useSoftDeletes = true;

	protected $allowedFields = [
		'notification_object_id', 'notifier_id', 'status'
	];

	protected $useTimestamps = true;

	protected $validationRules = [
		'notification_object_id' => [
			'rules' => 'required|integer|max_length[20]',
			'errors' => []
		],
        'notifier_id' => [
			'rules' => 'required|integer|max_length[10]',
			'errors' => []
		]
	];

	/**
	 * Fetch notifications
	 *
	 * @param integer $limit
	 * @param integer $offset
	 * @return array
	 */
	public function fetchNotification(int $limit=6, int $offset=0) : array
	{
		$builder = $this->builder();

		$builder->select('n_o.id as notification_object_id, n_o.entity_type_id as entity_type_id, n_o.entity_id, notifications.notifier_id, n_o.created_at');
		$builder->join(TBL_NOTIFICATION_OBJECT . ' as n_o', 'n_o.id = notifications.notification_object_id', 'left');
		$builder->where('notifications.notifier_id', user_id());
		$builder->where('notifications.deleted_at', null);
		$builder->limit($limit, $offset);
		$builder->orderBy('created_at', 'DESC');
		$query = $builder->get();

		$results = $query->getResultArray();

		$data = [];
		for ($i=0; $i < count($results); $i++)
		{
			/*
			// grouped
			$data[$entity_type_id][] = [
				'entity_id' => $results[$i]['entity_id'],
				'notification_object_id' => $results[$i]['notification_object_id'],
				'created_at' => $results[$i]['created_at'],
				'since' => $myTime->humanize()
			];
			// {{actor_user_name}} {{action_type}} post. {{post_description}}
			*/

			$objectId = $results[$i]['notification_object_id'];
			$entityTypeId = $results[$i]['entity_type_id'];
			$entityId = $results[$i]['entity_id'];
			$myTime = Time::parse($results[$i]['created_at']);

			// get entityType
			$notificationEntityModel = new NotificationEntityModel();
			$notificationEntity = $notificationEntityModel->get_entity_type($entityTypeId);
			if ( $notificationEntity != false )
			{
				$entityName = $notificationEntity->name;
				$table = $notificationEntity->reference_table;

				// get actor
				$notificationChangeModel = new NotificationChangeModel();
				$actor = $notificationChangeModel->get_actor($objectId);

				// get entity description
				$description = $this->getDescription($table, $entityId);

				$data[] = [
					'displayName' => $actor['displayName'],
					'displayPicture' => $actor['displayPicture'],
					'actionType' => $entityName,
					'description' => $description,
					'since' => $myTime->humanize()
				];
			}
		}

		return $data;
	}

	/**
	 * Fetch notification for pusher beams description
	 *
	 * @param integer $id
	 * @return void
	 */
	public function beamsNotification(int $id)
	{
		$notification = $this->find($id);
		if ( empty($notification) )
			return;

		$objectId = $notification->notification_object_id;
		$notifierId = (string)$notification->notifier_id;

		$notificationObjectModel = new NotificationObjectModel();
		$notificationObject = $notificationObjectModel->find($notification->notification_object_id);
		if ( empty($notificationObject) )
			return;

		$entityTypeId = $notificationObject->entity_type_id;
		$entityId = $notificationObject->entity_id;

		// get entityType
		$notificationEntityModel = new NotificationEntityModel();
		$notificationEntity = $notificationEntityModel->get_entity_type($entityTypeId);
		if ( $notificationEntity != false )
		{
			$entityName = $notificationEntity->name;
			$table = $notificationEntity->reference_table;

			// get actor
			$notificationChangeModel = new NotificationChangeModel();
			$actor = $notificationChangeModel->get_actor($objectId);

			// get entity description
			$description = $this->getDescription($table, $entityId);

			$pusherBody = $actor['displayName'] . ' ' . lang('Activity.' . $entityName) . ' ' . $description;
			$beamsClient = service('pusher_beam');
			$beamsClient->publishToUsers(
				array($notifierId),
				array(
					"fcm" => array(
						"notification" => array(
							"title" => getenv('APP_NAME'),
							"body" => $pusherBody
						)
					),
					"apns" => array(
						"aps" => array(
							"alert" => array(
								"title" => getenv('APP_NAME'),
								"body" => $pusherBody
							)
						)
					),
					"web" => array(
						"notification" => array(
							"title" => getenv('APP_NAME'),
							"body" => $pusherBody
						)
					)
				)
			);
		}

		return;
	}

	/**
	 * Get Description
	 *
	 * @param string $table
	 * @param integer $id
	 * @return string
	 */
	private function getDescription(string $table, int $id) : string
	{
		switch ($table) {
			case TBL_POSTS:
				$postModel = new PostModel();
				$post = $postModel->find($id);
				$description = (! empty($post)) ? strip_tags($post->content) : '';
				break;
			case TBL_POST_COMMENTS:
				$postCommentModel = new PostCommentModel();
				$postComment = $postCommentModel->find($id);
				$description = (! empty($postComment)) ? strip_tags($postComment->content) : '';
				break;
			case TBL_POST_REACTIONS:
				$postModel = new PostModel();
				$post = $postModel->find($id);
				$description = (! empty($post)) ? strip_tags($post->content) : '';
				break;
			case TBL_SERVICE_INVOICES_XND:
				$invoiceModel = new ServiceInvoiceModel();
				$invoice = $invoiceModel->find($id);
				$description = (! empty($invoice)) ? strip_tags($invoice->order->item) : '';
				break;
			case TBL_SERVICE_FULFILLMENTS:
				$fulfillmentModel = new ServiceFulfillmentModel();
				$fulfillment = $fulfillmentModel->find($id);
				$description = (! empty($fulfillment)) ? strip_tags($fulfillment->order->item) : '';
				break;
			default:
				$description = '';
				break;
		}

		return $description;
	}

}