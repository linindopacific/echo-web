<?php namespace App\Models;

/*
 * File: LoginModel.php
 * Project: echo
 * File Created: Wednesday, 19th August 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Friday, 30th April 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

class LoginModel extends \Myth\Auth\Models\LoginModel
{
    protected $afterInsert = ['addLoginDetail'];

    public function addLoginDetail()
    {
        $id = $this->getInsertID();

        $ipstack = service('ipstack');
        if ($ipstack['status'] == 'success')
        {
            $dt = $ipstack['data'];
            $db = \Config\Database::connect();
            $builder = $db->table(TBL_AUTH_LOGINS_IP);
            $builder->insert([
                'country_code' => $dt['country_code'],
                'country_name' => $dt['country_name'],
                'region_name' => $dt['region_name'],
                'city_name' => $dt['city'],
                'latitude' => $dt['latitude'],
                'longitude' => $dt['longitude'],
                'calling_code' => $dt['location']['calling_code'],
                'language_code' => $dt['location']['languages'][0]['code'],
                'login_id' => $id
            ]);
        }
    }
}
