<?php namespace App\Models;

/*
 * File: ReactionModel.php
 * Project: echo
 * File Created: Monday, 2nd November 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 28th June 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use App\Entities\Reaction;
use CodeIgniter\Model;

class ReactionModel extends Model
{
    protected $table = TBL_REACTIONS;

	protected $primaryKey = 'id';

	protected $returnType = Reaction::class;

    protected $useSoftDeletes = true;

	protected $allowedFields = [
		'name', 'style',
	];

	protected $useTimestamps = true;

	protected $validationRules = [
        'name' => 'required|alpha|max_length[10]|is_unique[reactions.name,id,{id}]',
        'style' => 'string|max_length[50]'
	];
}