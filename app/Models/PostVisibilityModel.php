<?php namespace App\Models;

/*
 * File: PostVisibilityModel.php
 * Project: echo
 * File Created: Monday, 26th October 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 28th June 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use App\Entities\PostVisibility;
use CodeIgniter\Model;

class PostVisibilityModel extends Model
{
    protected $table = TBL_POST_VISIBILITIES;

	protected $primaryKey = 'id';

    protected $returnType = PostVisibility::class;

    protected $useSoftDeletes = true;

	protected $allowedFields = [
		'name', 'description',
	];

	protected $useTimestamps = true;

	protected $validationRules = [
        'name' => 'required|alpha|max_length[10]|is_unique[post_visibilities.name,id,{id}]',
        'description' => 'alpha_space|max_length[20]|permit_empty'
	];

	protected $validationMessages = [];

	protected $skipValidation = false;
}