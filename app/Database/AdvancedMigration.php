<?php namespace App\Database;

/*
 * File: AdvancedMigration.php
 * Project: echo
 * File Created: Monday, 26th October 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Wednesday, 25th November 2020
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Exceptions\DatabaseException;
use CodeIgniter\Database\Migration;

abstract class AdvancedMigration extends Migration
{
    /**
     * Perform a migration step.
     */
    abstract public function up();

    /**
     * Perform a migration step.
     */
    abstract public function down();


    /**
     * @param string $tableName
     * @param string $fieldName
     * @param string $relatedTableName
     * @param string $relatedTableField
     * @param string $onUpdate
     * @param string $onDelete
     * @param string $indexMethod
     * @return bool
     */
    protected function addNewForeignKey(
        string $tableName = '',
        string $fieldName = '',
        string $relatedTableName = '',
        string $relatedTableField = '',
        string $onUpdate = '',
        string $onDelete = '',
        string $indexMethod = 'btree'
    ): bool
    {
        if (!$this->db->simpleQuery('ALTER TABLE `'.$tableName.'` ADD CONSTRAINT `'.$tableName.'_'.$fieldName.'_foreign` FOREIGN KEY (`'.$fieldName.'`) REFERENCES `'.$relatedTableName.'` (`'.$relatedTableField.'`) ON DELETE '.strtoupper($onDelete).' ON UPDATE '.strtoupper($onUpdate).';'))
        {
            $error = $this->db->error();

            throw new DatabaseException('An error occured in "AdvancedMigration" (1).\nCode: '.$error['code'].'\nMessage: '.$error['message']);
        }

        if (empty($error))
        {
            if (!$this->db->simpleQuery('ALTER TABLE `'.$tableName.'` ADD INDEX `'.$tableName.'_'.$fieldName.'_foreign`(`'.$fieldName.'`) USING '.strtoupper($indexMethod).';'))
            {
                $error = $this->db->error();

                throw new DatabaseException('An error occured in "AdvancedMigration" (2).\nCode: '.$error['code'].'\nMessage: '.$error['message']);
            }
        }

        return true;
    }

    /**
     * @param string $tableName
     * @param string $fieldName
     * @param string $indexMethod
     * @return bool
     */
    protected function addIndex(
        string $tableName = '',
        string $fieldName = '',
        string $indexMethod = 'btree'
    ): bool
    {
        if ( strtolower($indexMethod) != 'spatial' )
        {
            $sql = 'ALTER TABLE `'.$tableName.'` ADD INDEX `'.$tableName.'_'.$fieldName.'_foreign`(`'.$fieldName.'`) USING '.strtoupper($indexMethod).';';
        }
        else
        {
            $sql = 'ALTER TABLE `'.$tableName.'` ADD SPATIAL INDEX `'.$tableName.'_'.$fieldName.'_foreign`(`'.$fieldName.'`);';
        }

        if (!$this->db->simpleQuery($sql))
        {
            $error = $this->db->error();

            throw new DatabaseException('An error occured in "AdvancedMigration" (2).\nCode: '.$error['code'].'\nMessage: '.$error['message']);
        }

        return true;
    }

    /**
     * @param string $tableName
     * @param string $foreignKeyName
     * @return bool
     */
    protected function dropIndex(
        string $tableName = '',
        string $foreignKeyName = ''
    ): bool
    {
        if (!$this->db->simpleQuery('ALTER TABLE `'.$tableName.'` DROP INDEX `'.$foreignKeyName.'`;'))
        {
            $error = $this->db->error();

            throw new DatabaseException('An error occured in "AdvancedMigration" (2).\nCode: '.$error['code'].'\nMessage: '.$error['message']);
        }

        return true;
    }

    /**
     * @param string $tableName
     * @param string $foreignKeyName
     * @return bool
     */
    protected function dropForeignKey(
        string $tableName = '',
        string $foreignKeyName = ''
    ): bool
    {
        if (!$this->db->simpleQuery('ALTER TABLE `'.$tableName.'` DROP FOREIGN KEY `'.$foreignKeyName.'`;'))
        {
            $error = $this->db->error();

            throw new DatabaseException('An error occured in "AdvancedMigration" (1).\nCode: '.$error['code'].'\nMessage: '.$error['message']);
        }

        if (empty($error))
        {
            $this->dropIndex($tableName, $foreignKeyName);
        }

        return true;
    }
}