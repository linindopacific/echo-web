<?php namespace App\Database\Seeds;

/*
 * File: FakeServiceSeeder.php
 * Project: echo
 * File Created: Thursday, 3rd September 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Friday, 13th November 2020
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Seeder;

class FakeServiceSeeder extends Seeder
{
    public function run()
    {
        $db = \Config\Database::connect();
        // get categories
        $builder = $db->table(TBL_SERVICE_CATEGORIES);
		$query   = $builder->where('deleted_at', NULL)->get();
		$categories = [];
		foreach ($query->getResult() as $row) {
			array_push($categories, $row->id);
        }

        // get coverages
		$builder = $db->table(TBL_REG_REGENCIES);
		$query = $builder->whereIn('province_id', [31, 32, 33, 34, 35, 36])->get();
		$coverages = [];
		foreach ($query->getResult() as $row) {
			array_push($coverages, $row->id);
        }

        // get user
        $builder = $db->table(TBL_USERS);
		$query   = $builder->get();
		$users = [];
		foreach ($query->getResult() as $row) {
			array_push($users, $row->id);
        }

        $faker = \Faker\Factory::create();
        for ($i = 0; $i < 120; $i++) {
			$data = [
				'name' => $faker->jobTitle,
				'description' => $faker->text($maxNbChars = 200),
				'category_id' => $faker->randomElement($categories),
                'price' => $faker->numberBetween(100000, 10000000),
                'price_per' => $faker->randomElement(['hour', 'day', 'event']),
                'featured' => $faker->numberBetween($min = 0, $max = 1),
                'active' => $faker->numberBetween($min = 0, $max = 1),
                'user_id' => $faker->randomElement($users),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ];

            $this->db->table(TBL_SERVICES)->insert($data);
            $id = $this->db->insertID();

            $this->db->table(TBL_SERVICE_COVERAGES)->insert([
                'regency_id' => $faker->randomElement($coverages),
                'service_id' => $id,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        }
    }
}