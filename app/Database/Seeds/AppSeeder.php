<?php namespace App\Database\Seeds;

/*
 * File: AppSeeder.php
 * Project: echo
 * File Created: Friday, 4th September 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Thursday, 15th April 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Seeder;

class AppSeeder extends Seeder
{
    public function run()
    {
        $this->call('App\Database\Seeds\AuthGroupSeeder');
        $this->call('App\Database\Seeds\PostVisibilitySeeder');
        $this->call('App\Database\Seeds\RegProvinceSeeder');
        $this->call('App\Database\Seeds\RegRegencySeeder');
        $this->call('App\Database\Seeds\RegDistrictSeeder');
        $this->call('App\Database\Seeds\RegVillageSeeder');
        $this->call('App\Database\Seeds\ReactionSeeder');
        $this->call('App\Database\Seeds\NotificationEntitySeeder');
    }
}