<?php namespace App\Database\Seeds;

/*
 * File: FakeServiceCategorySeeder.php
 * Project: echo
 * File Created: Friday, 28th August 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Friday, 13th November 2020
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Seeder;

class FakeServiceCategorySeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'name' => 'Home Improvement',
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Fashion & Style',
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Household',
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Sport & Education',
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Travel & Automotive',
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Electronic Repair',
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Health Care',
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Financial Services',
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Other',
                'created_at' => date('Y-m-d H:i:s')
            ],
        ];

        $this->db->table(TBL_SERVICE_CATEGORIES)->insertBatch($data);

        // Event with child
        $this->db->table(TBL_SERVICE_CATEGORIES)->insert(['name' => 'Events', 'created_at' => date('Y-m-d H:i:s')]);
        $eventId = $this->db->insertID();
        $data = [
            [
                'name' => 'Event Organizer',
                'parent_id' => $eventId,
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Wedding Organizer',
                'parent_id' => $eventId,
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Photographer',
                'parent_id' => $eventId,
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Videoman',
                'parent_id' => $eventId,
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Decor Designer',
                'parent_id' => $eventId,
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Security',
                'parent_id' => $eventId,
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Choreographer',
                'parent_id' => $eventId,
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Singer / Dancer',
                'parent_id' => $eventId,
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Entertainer / Performer',
                'parent_id' => $eventId,
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Bartender / Barista',
                'parent_id' => $eventId,
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Waiter',
                'parent_id' => $eventId,
                'created_at' => date('Y-m-d H:i:s')
            ],
        ];
        $this->db->table(TBL_SERVICE_CATEGORIES)->insertBatch($data);
    }
}