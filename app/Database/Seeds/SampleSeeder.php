<?php namespace App\Database\Seeds;

/*
 * File: SampleSeeder.php
 * Project: echo
 * File Created: Friday, 4th September 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 26th October 2020
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Seeder;

class SampleSeeder extends Seeder
{
    public function run()
    {
        $this->call('App\Database\Seeds\FakeUserSeeder');
        $this->call('App\Database\Seeds\FakeServiceCategorySeeder');
        $this->call('App\Database\Seeds\FakeServiceSeeder');
        //$this->call('App\Database\Seeds\FakeServiceOrderSeeder');
        //$this->call('App\Database\Seeds\FakeServiceReviewSeeder');
    }
}