<?php namespace App\Database\Seeds;

/*
 * File: AuthGroupSeeder.php
 * Project: echo
 * File Created: Wednesday, 19th August 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Friday, 13th November 2020
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Seeder;

class AuthGroupSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
            	'name' => 'admin',
            	'description' => 'Administrator'
            ],
            [
            	'name' => 'guests',
            	'description' => 'Guest User'
            ],
        ];

        // Using Query Builder
        $this->db->table(TBL_AUTH_GROUPS)->insertBatch($data);
    }
}