<?php namespace App\Database\Seeds;

/*
 * File: NotificationentitySeeder.php
 * Project: echo
 * File Created: Thursday, 15th April 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Thursday, 8th July 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Seeder;

class NotificationEntitySeeder extends Seeder
{
	public function run()
	{
		$data = [
            [
                'name' => 'status-create',
                'reference_table' => 'posts',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'status-update',
                'reference_table' => 'posts',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
			],
			[
                'name' => 'status-delete',
                'reference_table' => 'posts',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'comment-create',
                'reference_table' => 'post_comments',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'comment-update',
                'reference_table' => 'post_comments',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'comment-delete',
                'reference_table' => 'post_comments',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
			[
                'name' => 'service-create',
                'reference_table' => 'services',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
			[
                'name' => 'service-update',
                'reference_table' => 'services',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
			[
                'name' => 'service-delete',
                'reference_table' => 'services',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
			[
                'name' => 'coverages-add',
                'reference_table' => 'service_coverages',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
			[
                'name' => 'category-create',
                'reference_table' => 'service_categories',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
			[
                'name' => 'category-update',
                'reference_table' => 'service_categories',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
			[
                'name' => 'category-delete',
                'reference_table' => 'service_categories',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
			[
                'name' => 'order-create',
                'reference_table' => 'service_orders',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
			[
                'name' => 'order-update',
                'reference_table' => 'service_orders',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
			[
                'name' => 'payment-create',
                'reference_table' => 'service_invoices_xnd',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
			[
                'name' => 'fulfillment-create',
                'reference_table' => 'service_fulfillment',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
			[
                'name' => 'fulfillment-complete',
                'reference_table' => 'service_fulfillment',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
			[
                'name' => 'review-create',
                'reference_table' => 'service_reviews',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
			[
                'name' => 'review-update',
                'reference_table' => 'service_reviews',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
			[
                'name' => 'user-update',
                'reference_table' => 'users',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
			[
                'name' => 'profile-create',
                'reference_table' => 'user_profile',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
			[
                'name' => 'profile-update',
                'reference_table' => 'user_profile',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
			[
                'name' => 'relation-invite',
                'reference_table' => 'user_relation',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
			[
                'name' => 'relation-reject',
                'reference_table' => 'user_relation',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
			[
                'name' => 'relation-approve',
                'reference_table' => 'user_relation',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
			[
                'name' => 'relation-ban',
                'reference_table' => 'user_relation',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
			[
                'name' => 'status-react',
                'reference_table' => 'post_reation',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
			[
                'name' => 'status-react-like',
                'reference_table' => 'post_reation',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'relation-remove',
                'reference_table' => 'user_relation',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'relation-cancel',
                'reference_table' => 'user_relation',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'coverages-remove',
                'reference_table' => 'service_coverages',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
        ];

        // Using Query Builder
        $this->db->table(TBL_NOTIFICATION_ENTITY)->insertBatch($data);
	}
}
