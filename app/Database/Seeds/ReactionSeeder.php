<?php namespace App\Database\Seeds;

/*
 * File: ReactionSeeder.php
 * Project: echo
 * File Created: Monday, 2nd November 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Friday, 13th November 2020
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Seeder;

class ReactionSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
            	'id' => 1,
                'name' => 'like',
                'style' => 'fa-thumbs-up',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
            	'id' => 2,
                'name' => 'love',
                'style' => 'fa-heart',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
            	'id' => 3,
                'name' => 'care',
                'style' => 'fa-kiss-wink-heart',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
            	'id' => 4,
                'name' => 'haha',
                'style' => 'fa-laugh-squint',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
            	'id' => 5,
                'name' => 'wow',
                'style' => 'fa-surprise',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
            	'id' => 6,
                'name' => 'sad',
                'style' => 'fa-sad-cry',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
            	'id' => 7,
                'name' => 'angry',
                'style' => 'fa-angry',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
        ];

        // Using Query Builder
        $this->db->table(TBL_REACTIONS)->insertBatch($data);
    }
}