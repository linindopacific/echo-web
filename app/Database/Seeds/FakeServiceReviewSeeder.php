<?php namespace App\Database\Seeds;

/*
 * File: FakeServiceReviewSeeder.php
 * Project: echo
 * File Created: Thursday, 3rd September 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Friday, 13th November 2020
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Seeder;

class FakeServiceReviewSeeder extends Seeder
{
    public function run()
    {
        $db = \Config\Database::connect();
        // get services
        $builder = $db->table(TBL_SERVICES);
		$query   = $builder->where('deleted_at', NULL)->where('active', 1)->get();
		$services = [];
		foreach ($query->getResult() as $row) {
			array_push($services, $row->id);
        }

        // get user
        $builder = $db->table(TBL_USERS);
		$query   = $builder->get();
		$users = [];
		foreach ($query->getResult() as $row) {
			array_push($users, $row->id);
        }

        $faker = \Faker\Factory::create();
        $data = [];
        for ($i = 0; $i < 150; $i++) {
			$data[] = [
				'service_id' => $faker->randomElement($services),
				'content' => $faker->text($maxNbChars = 200),
                'rating' => $faker->numberBetween($min = 1, $max = 5),
                'user_id' => $faker->randomElement($users),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
			];
        }

        $this->db->table(TBL_SERVICE_REVIEWS)->insertBatch($data);
    }
}