<?php namespace App\Database\Seeds;

/*
 * File: FakeUserSeeder.php
 * Project: echo
 * File Created: Thursday, 3rd September 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Friday, 13th November 2020
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Seeder;

class FakeUserSeeder extends Seeder
{
    public function run()
    {
        $faker = \Faker\Factory::create('id_ID');
        for ($i = 0; $i < 14; $i++) {
            $email = $faker->safeEmail;
            $gender = $faker->randomElement(['male', 'female']);
            $firstName = $faker->firstName($gender);
            $lastName = $faker->lastName($gender);
            $displayName = $firstName . ' ' . $lastName;

            // user
			$data = [
                'email' => $email,
                'username' => explode("@", $email)[0],
                'display_name' => $displayName,
                'password_hash' => bin2hex(random_bytes(16)),
                'active' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ];
            $this->db->table(TBL_USERS)->insert($data);
            $userId = $this->db->insertID();

            // user profile
            $data = [
                'first_name' => $firstName,
                'last_name' => $lastName,
                'gender' => $gender,
                'user_id' => $userId,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ];
            $this->db->table(TBL_USER_PROFILES)->insert($data);
        }
    }
}