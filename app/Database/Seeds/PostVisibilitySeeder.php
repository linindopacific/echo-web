<?php namespace App\Database\Seeds;

/*
 * File: PostVisibilitySeeder.php
 * Project: echo
 * File Created: Monday, 26th October 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Friday, 13th November 2020
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Seeder;

class PostVisibilitySeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
            	'id' => 1,
                'name' => 'connection',
                'description' => 'Connections',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'deleted_at' => null
            ],
            [
            	'id' => 2,
                'name' => 'public',
                'description' => 'Public',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'deleted_at' => null
            ],
            [
            	'id' => 3,
                'name' => 'private',
                'description' => 'Only Me',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
                'deleted_at' => date('Y-m-d H:i:s')
            ],
        ];

        // Using Query Builder
        $this->db->table(TBL_POST_VISIBILITIES)->insertBatch($data);
    }
}