<?php namespace App\Database\Seeds;

/*
 * File: FakeServiceOrderSeeder.php
 * Project: echo
 * File Created: Friday, 4th September 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Friday, 13th November 2020
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Seeder;

class FakeServiceOrderSeeder extends Seeder
{
    public function run()
    {
        $db = \Config\Database::connect();
        // get services
        $builder = $db->table(TBL_SERVICES);
		$query   = $builder->where('deleted_at', NULL)->where('active', 1)->get();
		$services = [];
		foreach ($query->getResult() as $row) {
			array_push($services, $row->id);
        }

        // get user -customer
        $builder = $db->table(TBL_USERS);
		$query   = $builder->get();
		$users = [];
		foreach ($query->getResult() as $row) {
			array_push($users, $row->id);
        }


    }
}