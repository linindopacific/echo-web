<?php namespace App\Database\Migrations;

/*
 * File: 2020-09-04-062801_CreateServiceOrder.php
 * Project: echo
 * File Created: Friday, 4th September 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Friday, 13th November 2020
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Migration;
use Spatie\DbDumper\Databases\MySql;
use Spatie\DbDumper\Compressors\GzipCompressor;

class CreateServiceOrder extends Migration
{
	public function up()
	{
		$this->db->disableForeignKeyChecks();

		$this->forge->addField([
			'id' => [
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => true,
				'autoincrement' => true
			],
			'document_no' => [
				'type' => 'TEXT'
			],
			'request_date' => [
				'type' => 'datetime'
			],
			'service_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => true
			],
			'service_name' => [
				'type' => 'VARCHAR',
				'constraint' => 100
			],
			'service_description' => [
				'type' => 'TEXT'
			],
			'quantity' => [
				'type' => 'INT',
				'default' => 1
			],
			'unit_price' => [
				'type' => 'DECIMAL',
			],
			'remarks' => [
				'type' => 'VARCHAR',
				'constraint' => 50,
				'null' => true
			],
			'referal_code' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null' => true
			],
			'invoiced' => [
				'type' => 'BIT',
				'default' => 0
			],
			'fulfilled' => [
				'type' => 'BIT',
				'default' => 0
			],
			'reviewed' => [
				'type' => 'BIT',
				'default' => 0
			],
			'user_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => true
			],
			'created_at' => [
				'type' => 'datetime',
				'null' => true
			],
	        'updated_at' => [
	          	'type' => 'datetime',
	          	'null' => true
	        ],
	        'deleted_at' => [
	          	'type' => 'datetime',
	          	'null' => true
	        ]

		]);
		$this->forge->addPrimaryKey('id');
		$this->forge->addForeignKey('service_id', TBL_SERVICES, 'id', false, false);
		$this->forge->addForeignKey('user_id', TBL_USERS, 'id', 'CASCADE', false);
		$this->forge->createTable(TBL_SERVICE_ORDERS, TRUE, ['ENGINE' => 'InnoDB']);

		$this->db->enableForeignKeyChecks();
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$dumpFile = TBL_SERVICE_ORDERS . '_' . time() . '.sql.gz';
		MySql::create()
			->setDbName($_ENV['database.default.database'])
			->setUserName($_ENV['database.default.username'])
			->setPassword($_ENV['database.default.password'])
			->includeTables([TBL_SERVICE_ORDERS])
			->useCompressor(new GzipCompressor())
			->dumpToFile(WRITEPATH . 'dbdump/' . $dumpFile);

		$this->forge->dropTable(TBL_SERVICE_ORDERS, TRUE);
	}
}