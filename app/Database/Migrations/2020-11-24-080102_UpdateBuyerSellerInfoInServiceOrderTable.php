<?php namespace App\Database\Migrations;

/*
 * File: 2020-11-24-080102_UpdateBuyerSellerInfoInServiceOrderTable.php
 * Project: echo
 * File Created: Tuesday, 24th November 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Wednesday, 25th November 2020
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Migration;
use Spatie\DbDumper\Databases\MySql;
use Spatie\DbDumper\Compressors\GzipCompressor;

class UpdateBuyerSellerInfoInServiceOrderTable extends Migration
{
	private function back_up()
	{
		$dumpFile = TBL_SERVICE_ORDERS . '_' . time() . '.sql.gz';
		MySql::create()
			->setDbName($_ENV['database.default.database'])
			->setUserName($_ENV['database.default.username'])
			->setPassword($_ENV['database.default.password'])
			->includeTables([TBL_SERVICE_ORDERS])
			->useCompressor(new GzipCompressor())
			->dumpToFile(WRITEPATH . 'dbdump/' . $dumpFile);

	}

	public function up()
	{
		$this->back_up();

		$this->db->disableForeignKeyChecks();

		// add columns
		$columns = [
			'buy_from_name' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
				'after' => 'document_no'
			],
			'buy_from_address' => [
				'type' => 'TEXT',
				'after' => 'buy_from_name'
			],
			'buy_from_zip_code' => [
				'type' => 'VARCHAR',
				'constraint' => 10,
				'null' => true,
				'after' => 'buy_from_address'
			],
			'buy_from_phone_no' =>[
				'type' => 'VARCHAR',
				'constraint' => 20,
				'after' => 'buy_from_zip_code'
			],
			'sell_to_name' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
				'after' => 'buy_from_phone_no'
			],
			'sell_to_address' => [
				'type' => 'TEXT',
				'after' => 'sell_to_name'
			],
			'sell_to_zip_code' => [
				'type' => 'VARCHAR',
				'constraint' => 10,
				'null' => true,
				'after' => 'sell_to_address'
			],
			'sell_to_phone_no' =>[
				'type' => 'VARCHAR',
				'constraint' => 20,
				'after' => 'sell_to_zip_code'
			],
			'lat' =>[
				'type' => 'DECIMAL(10,8)',
				'after' => 'sell_to_phone_no'
			],
			'lng' =>[
				'type' => 'DECIMAL(11,8)',
				'after' => 'lat'
			],
			'unit_discount' =>[
				'type' => 'DECIMAL',
				'after' => 'unit_price'
			],
			'amount' =>[
				'type' => 'DECIMAL',
				'after' => 'unit_discount'
			],
			'market_fee' =>[
				'type' => 'DECIMAL',
				'after' => 'amount'
			],
			'status' => [
				'type' => 'TINYINT',
				'default' => 0,
				'after' => 'fulfilled'
			],
		];
		$this->forge->addColumn(TBL_SERVICE_ORDERS, $columns);

		// modify columns
		$columns = [
			'document_no' => [
				'name' => 'order_no',
				'type' => 'VARCHAR',
				'constraint' => 50
			],
			'service_name' => [
				'name' => 'item',
				'type' => 'VARCHAR',
				'constraint' => 100
			],
			'service_description' => [
				'name' => 'description',
				'type' => 'TEXT'
			],
		];
		$this->forge->modifyColumn(TBL_SERVICE_ORDERS, $columns);

		// drop columns
		$this->forge->dropColumn(TBL_SERVICE_ORDERS, 'invoiced');
		$this->forge->dropColumn(TBL_SERVICE_ORDERS, 'reviewed');

		$this->db->enableForeignKeyChecks();
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->back_up();

		$this->forge->dropColumn(TBL_SERVICE_ORDERS, 'buy_from_name');
		$this->forge->dropColumn(TBL_SERVICE_ORDERS, 'buy_from_address');
		$this->forge->dropColumn(TBL_SERVICE_ORDERS, 'buy_from_zip_code');
		$this->forge->dropColumn(TBL_SERVICE_ORDERS, 'buy_from_phone_no');
		$this->forge->dropColumn(TBL_SERVICE_ORDERS, 'sell_to_name');
		$this->forge->dropColumn(TBL_SERVICE_ORDERS, 'sell_to_address');
		$this->forge->dropColumn(TBL_SERVICE_ORDERS, 'sell_to_zip_code');
		$this->forge->dropColumn(TBL_SERVICE_ORDERS, 'sell_to_phone_no');
		$this->forge->dropColumn(TBL_SERVICE_ORDERS, 'lat');
		$this->forge->dropColumn(TBL_SERVICE_ORDERS, 'lng');
		$this->forge->dropColumn(TBL_SERVICE_ORDERS, 'unit_discount');
		$this->forge->dropColumn(TBL_SERVICE_ORDERS, 'amount');
		$this->forge->dropColumn(TBL_SERVICE_ORDERS, 'market_fee');
		$this->forge->dropColumn(TBL_SERVICE_ORDERS, 'status');
	}
}
