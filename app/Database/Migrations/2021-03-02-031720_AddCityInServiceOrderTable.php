<?php namespace App\Database\Migrations;

/*
 * File: 2021-03-02-031720_AddCityInServiceOrderTable.php
 * Project: echo
 * File Created: Tuesday, 2nd March 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Tuesday, 8th June 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Migration;
use Spatie\DbDumper\Databases\MySql;
use Spatie\DbDumper\Compressors\GzipCompressor;

class AddCityInServiceOrderTable extends Migration
{
	private function back_up()
	{
		$dumpFile = TBL_SERVICE_ORDERS . '_' . time() . '.sql.gz';
		MySql::create()
			->setDbName($_ENV['database.default.database'])
			->setUserName($_ENV['database.default.username'])
			->setPassword($_ENV['database.default.password'])
			->includeTables([TBL_SERVICE_ORDERS])
			->useCompressor(new GzipCompressor())
			->dumpToFile(WRITEPATH . 'dbdump/' . $dumpFile);
	}

	public function up()
	{
		$columns = [
			'buy_from_city' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
				'after' => 'buy_from_address'
			],
			'sell_to_city' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
				'after' => 'sell_to_address'
			],
		];
		$this->forge->addColumn(TBL_SERVICE_ORDERS, $columns);
	}

	public function down()
	{
		$this->back_up();

		$this->forge->dropColumn(TBL_SERVICE_ORDERS, ['buy_from_city', 'sell_to_city']);
	}
}
