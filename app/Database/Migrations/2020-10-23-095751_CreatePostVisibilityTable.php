<?php namespace App\Database\Migrations;

/*
 * File: 2020-10-23-095751_CreatePostVisibilityTable.php
 * Project: echo
 * File Created: Friday, 23rd October 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Friday, 13th November 2020
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Migration;
use Spatie\DbDumper\Databases\MySql;
use Spatie\DbDumper\Compressors\GzipCompressor;

class CreatePostVisibilityTable extends Migration
{
	public function up()
	{
		$this->db->disableForeignKeyChecks();

		$this->forge->addField([
			'id' => [
              	'type' => 'INT',
              	'constraint' => 11,
              	'unsigned' => true,
              	'auto_increment' => true
            ],
            'name' => [
            	'type' => 'VARCHAR',
              	'constraint' => 10,
            ],
            'description' => [
				'type' => 'VARCHAR',
				'constraint' => 20,
				'null' => true
            ],
			'created_at' => [
				'type' => 'datetime',
				'null' => true
			],
            'updated_at' => [
              	'type' => 'datetime',
              	'null' => true
            ],
            'deleted_at' => [
              	'type' => 'datetime',
              	'null' => true
            ]
		]);
		$this->forge->addPrimaryKey('id');
		$this->forge->createTable(TBL_POST_VISIBILITIES, TRUE, ['ENGINE' => 'InnoDB']);

		$this->db->enableForeignKeyChecks();
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$dumpFile = TBL_POST_VISIBILITIES . '_' . time() . '.sql.gz';
        MySql::create()
            ->setDbName($_ENV['database.default.database'])
            ->setUserName($_ENV['database.default.username'])
            ->setPassword($_ENV['database.default.password'])
            ->includeTables([TBL_POST_VISIBILITIES])
            ->useCompressor(new GzipCompressor())
            ->dumpToFile(WRITEPATH . 'dbdump/' . $dumpFile);

		$this->forge->dropTable(TBL_POST_VISIBILITIES, TRUE);
	}
}
