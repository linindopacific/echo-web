<?php namespace App\Database\Migrations;

/*
 * File: 2020-08-27-041217_CreateUploadsTable.php
 * Project: echo
 * File Created: Thursday, 27th August 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Friday, 13th November 2020
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Migration;
use Spatie\DbDumper\Databases\MySql;
use Spatie\DbDumper\Compressors\GzipCompressor;

class CreateUploadsTable extends Migration
{
	public function up()
	{
		$this->db->disableForeignKeyChecks();

		$this->forge->addField([
            'id' => [
            	'type' => 'INT',
            	'constraint' => 11,
            	'unsigned' => true,
            	'auto_increment' => true
            ],
            'file_name' => [
				'type' => 'VARCHAR',
				'constraint' => 100
			],
			'file_type' => [
				'type' => 'VARCHAR',
				'constraint' => 20
			],
			'file_path' => [
				'type' => 'VARCHAR',
				'constraint' => 200
			],
			'raw_name' => [
				'type' => 'TEXT'
			],
			'orig_name' => [
				'type' => 'TEXT'
			],
			'file_ext' => [
				'type' => 'VARCHAR',
				'constraint' => 10
			],
			'file_size' => [
				'type' => 'DECIMAL',
				'precision' => 10,
				'scale' => 2
			],
			'is_image' => [
				'type' => 'BIT',
			],
			'image_width' => [
				'type' => 'DECIMAL',
				'precision' => 10,
				'scale' => 2,
				'null' => true
			],
			'image_height' => [
				'type' => 'DECIMAL',
				'precision' => 10,
				'scale' => 2,
				'null' => true
			],
			'image_type' => [
				'type' => 'VARCHAR',
				'constraint' => 20,
				'null' => true
			],
			'user_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => true
			],
			'created_at' => [
				'type' => 'datetime',
				'null' => true
			],
            'updated_at' => [
            	'type' => 'datetime',
            	'null' => true
            ],
            'deleted_at' => [
            	'type' => 'datetime',
            	'null' => true
            ]

        ]);
		$this->forge->addPrimaryKey('id');
		$this->forge->addForeignKey('user_id', 'users', 'id', 'CASCADE', false);
		$this->forge->createTable(TBL_UPLOADS, TRUE, ['ENGINE' => 'InnoDB']);

		$this->db->enableForeignKeyChecks();
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$dumpFile = TBL_UPLOADS . '_' . time() . '.sql.gz';
		MySql::create()
			->setDbName($_ENV['database.default.database'])
			->setUserName($_ENV['database.default.username'])
			->setPassword($_ENV['database.default.password'])
			->includeTables([TBL_UPLOADS])
			->useCompressor(new GzipCompressor())
			->dumpToFile(WRITEPATH . 'dbdump/' . $dumpFile);

		$this->forge->dropTable(TBL_UPLOADS, TRUE);
	}
}