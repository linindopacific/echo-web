<?php namespace App\Database\Migrations;

/*
 * File: 2020-09-04-063839_CreateServiceReview.php
 * Project: echo
 * File Created: Friday, 4th September 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Friday, 13th November 2020
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Migration;
use Spatie\DbDumper\Databases\MySql;
use Spatie\DbDumper\Compressors\GzipCompressor;

class CreateServiceReview extends Migration
{
	public function up()
	{
		$this->db->disableForeignKeyChecks();

		$this->forge->addField([
			'id' => [
            	'type' => 'INT',
            	'constraint' => 11,
            	'unsigned' => true,
            	'auto_increment' => true
			],
			'fulfillment_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => true
			],
			'content' => [
				'type' => 'TEXT',
				'null' => true
			],
			'rating' => [
				'type' => 'TINYINT',
				'constraint' => 1,
				'unsigned' => true
			],
			'user_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => true
			],
			'created_at' => [
				'type' => 'datetime',
				'null' => true
			],
	        'updated_at' => [
	          	'type' => 'datetime',
	          	'null' => true
	        ],
	        'deleted_at' => [
	          	'type' => 'datetime',
	          	'null' => true
	        ]
		]);
		$this->forge->addPrimaryKey('id');
		$this->forge->addForeignKey('fulfillment_id', TBL_SERVICE_FULFILLMENTS, 'id', false, false);
		$this->forge->addForeignKey('user_id', TBL_USERS, 'id', 'CASCADE', false);
		$this->forge->createTable(TBL_SERVICE_REVIEWS, TRUE, ['ENGINE' => 'InnoDB']);

		$this->db->enableForeignKeyChecks();
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$dumpFile = TBL_SERVICE_REVIEWS . '_' . time() . '.sql.gz';
		MySql::create()
			->setDbName($_ENV['database.default.database'])
			->setUserName($_ENV['database.default.username'])
			->setPassword($_ENV['database.default.password'])
			->includeTables([TBL_SERVICE_REVIEWS])
			->useCompressor(new GzipCompressor())
			->dumpToFile(WRITEPATH . 'dbdump/' . $dumpFile);

		$this->forge->dropTable(TBL_SERVICE_REVIEWS, TRUE);
	}
}