<?php namespace App\Database\Migrations;

/*
 * File: 2020-10-12-103013_UpdateUserProfileJobHobbyIndustryAbout.php
 * Project: echo
 * File Created: Monday, 12th October 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Friday, 13th November 2020
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Migration;
use Spatie\DbDumper\Databases\MySql;
use Spatie\DbDumper\Compressors\GzipCompressor;

class UpdateUserProfileJobHobbyIndustryAbout extends Migration
{
	public function up()
	{
		$this->db->disableForeignKeyChecks();

		$column = [
		    'job' => [
				'type' => 'varchar',
				'constraint' => 255,
				'after' => 'birthdate'
			],
			'hobby' => [
				'type' => 'varchar',
				'constraint' => 255,
				'after' => 'job'
			],
			'industry' => [
				'type' => 'varchar',
				'constraint' => 255,
				'after' => 'hobby'
			],
			'about' => [
				'type' => 'varchar',
				'constraint' => 255,
				'after' => 'industry'
			],
		];
		$this->forge->addColumn(TBL_USER_PROFILES, $column);

		$this->db->disableForeignKeyChecks();
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$dumpFile = TBL_USER_PROFILES . '_' . time() . '.sql.gz';
		MySql::create()
			->setDbName($_ENV['database.default.database'])
			->setUserName($_ENV['database.default.username'])
			->setPassword($_ENV['database.default.password'])
			->includeTables([TBL_USER_PROFILES])
			->useCompressor(new GzipCompressor())
			->dumpToFile(WRITEPATH . 'dbdump/' . $dumpFile);

		$this->forge->dropColumn(TBL_USER_PROFILES, 'display_name');
	}
}