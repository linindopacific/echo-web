<?php namespace App\Database\Migrations;

/*
 * File: 2020-12-30-065534_UpdateIDIncrementInServiceTables.php
 * Project: echo
 * File Created: Wednesday, 30th December 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Wednesday, 30th December 2020
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Migration;

class UpdateIDIncrementInServiceTables extends Migration
{
	public function up()
	{
		$this->db->disableForeignKeyChecks();

		$this->forge->modifyColumn(TBL_SERVICE_ORDERS, [
			'id' => ['name' => 'id', 'type' => 'BIGINT', 'unsigned' => true, 'auto_increment' => true]
		]);
		$this->forge->modifyColumn(TBL_SERVICE_FULFILLMENTS, [
			'id' => ['name' => 'id', 'type' => 'BIGINT', 'unsigned' => true, 'auto_increment' => true]
		]);
		$this->forge->modifyColumn(TBL_SERVICE_REVIEWS, [
			'id' => ['name' => 'id', 'type' => 'BIGINT', 'unsigned' => true, 'auto_increment' => true]
		]);
		$this->forge->modifyColumn(TBL_SERVICE_INVOICES, [
			'id' => ['name' => 'id', 'type' => 'BIGINT', 'unsigned' => true, 'auto_increment' => true]
		]);
		$this->forge->modifyColumn(TBL_SERVICE_TRANSACTIONS, [
			'id' => ['name' => 'id', 'type' => 'BIGINT', 'unsigned' => true, 'auto_increment' => true]
		]);

		$this->db->enableForeignKeyChecks();
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->db->disableForeignKeyChecks();

		$this->forge->modifyColumn(TBL_SERVICE_ORDERS, [
			'id' => ['name' => 'id', 'type' => 'BIGINT', 'unsigned' => true]
		]);
		$this->forge->modifyColumn(TBL_SERVICE_FULFILLMENTS, [
			'id' => ['name' => 'id', 'type' => 'BIGINT', 'unsigned' => true]
		]);
		$this->forge->modifyColumn(TBL_SERVICE_REVIEWS, [
			'id' => ['name' => 'id', 'type' => 'BIGINT', 'unsigned' => true]
		]);
		$this->forge->modifyColumn(TBL_SERVICE_INVOICES, [
			'id' => ['name' => 'id', 'type' => 'BIGINT', 'unsigned' => true]
		]);
		$this->forge->modifyColumn(TBL_SERVICE_TRANSACTIONS, [
			'id' => ['name' => 'id', 'type' => 'BIGINT', 'unsigned' => true]
		]);

		$this->db->enableForeignKeyChecks();
	}
}
