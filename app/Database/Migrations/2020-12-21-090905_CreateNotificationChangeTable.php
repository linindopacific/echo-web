<?php namespace App\Database\Migrations;

/*
 * File: 2020-12-21-090905_CreateNotificationChangeTable.php
 * Project: echo
 * File Created: Monday, 21st December 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Tuesday, 8th June 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Migration;
use Spatie\DbDumper\Databases\MySql;
use Spatie\DbDumper\Compressors\GzipCompressor;

class CreateNotificationChangeTable extends Migration
{
	public function up()
	{
		$this->db->disableForeignKeyChecks();

		$this->forge->addField([
			'id' => [
            	'type' => 'BIGINT',
            	'unsigned' => true,
            	'auto_increment' => true
			],
			'notification_object_id' => [
            	'type' => 'BIGINT',
            	'unsigned' => true,
            	'null' => false
			],
			'actor_id' => [
				'type' => 'INT',
				'constraint' => 10,
            	'unsigned' => true,
            	'null' => false
			],
			'created_at' => [
				'type' => 'datetime',
				'null' => true
			],
	        'updated_at' => [
	          	'type' => 'datetime',
	          	'null' => true
	        ],
	        'deleted_at' => [
	          	'type' => 'datetime',
	          	'null' => true
	        ]
		]);
		$this->forge->addPrimaryKey('id');
		$this->forge->addForeignKey('notification_object_id', TBL_NOTIFICATION_OBJECT, 'id', 'CASCADE', 'CASCADE');
		$this->forge->addForeignKey('actor_id', TBL_USERS, 'id', 'NO ACTION', 'NO ACTION');
		$this->forge->createTable(TBL_NOTIFICATION_CHANGE, TRUE, ['ENGINE' => 'InnoDB']);

		$this->db->enableForeignKeyChecks();
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$dumpFile = TBL_NOTIFICATION_CHANGE . '_' . time() . '.sql.gz';
		MySql::create()
			->setDbName($_ENV['database.default.database'])
			->setUserName($_ENV['database.default.username'])
			->setPassword($_ENV['database.default.password'])
			->includeTables([TBL_NOTIFICATION_CHANGE])
			->useCompressor(new GzipCompressor())
			->dumpToFile(WRITEPATH . 'dbdump/' . $dumpFile);

		$this->forge->dropTable(TBL_NOTIFICATION_CHANGE, TRUE);
	}
}
