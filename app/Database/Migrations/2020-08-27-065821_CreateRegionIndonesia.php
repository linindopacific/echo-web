<?php namespace App\Database\Migrations;

/*
 * File: 2020-08-27-065821_CreateRegionIndonesia.php
 * Project: echo
 * File Created: Thursday, 27th August 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Tuesday, 8th June 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Migration;
use Spatie\DbDumper\Databases\MySql;
use Spatie\DbDumper\Compressors\GzipCompressor;

class CreateRegionIndonesia extends Migration
{
	public function up()
	{
		$this->db->disableForeignKeyChecks();

		// provinces
		$this->forge->addField([
			'id' => [
            	'type' => 'CHAR',
            	'constraint' => 2
            ],
            'name' => [
            	'type' => 'VARCHAR',
            	'constraint' => 255
            ]
		]);
		$this->forge->addPrimaryKey('id');
		$this->forge->createTable(TBL_REG_PROVINCES, TRUE, ['ENGINE' => 'InnoDB']);

		// regencies
		$this->forge->addField([
			'id' => [
            	'type' => 'CHAR',
            	'constraint' => 5
            ],
            'province_id' => [
            	'type' => 'CHAR',
            	'constraint' => 2
            ],
            'name' => [
            	'type' => 'VARCHAR',
            	'constraint' => 255
            ]
		]);
		$this->forge->addPrimaryKey('id');
		$this->forge->addKey('province_id');
		$this->forge->addForeignKey('province_id', TBL_REG_PROVINCES, 'id', false, false);
		$this->forge->createTable(TBL_REG_REGENCIES, TRUE, ['ENGINE' => 'InnoDB']);

		// districts
		$this->forge->addField([
			'id' => [
            	'type' => 'CHAR',
            	'constraint' => 8
            ],
            'regency_id' => [
            	'type' => 'CHAR',
            	'constraint' => 5
            ],
            'name' => [
            	'type' => 'VARCHAR',
            	'constraint' => 255
            ]
		]);
		$this->forge->addPrimaryKey('id');
		$this->forge->addKey('regency_id');
		$this->forge->addForeignKey('regency_id', TBL_REG_REGENCIES, 'id', false, false);
		$this->forge->createTable(TBL_REG_DISTRICTS, TRUE, ['ENGINE' => 'InnoDB']);

		// villages
		$this->forge->addField([
			'id' => [
            	'type' => 'CHAR',
            	'constraint' => 13
            ],
            'district_id' => [
            	'type' => 'CHAR',
            	'constraint' => 8
            ],
            'name' => [
            	'type' => 'VARCHAR',
            	'constraint' => 255
            ]
		]);
		$this->forge->addPrimaryKey('id');
		$this->forge->addKey('district_id');
		$this->forge->addForeignKey('district_id', TBL_REG_DISTRICTS, 'id', false, false);
		$this->forge->createTable(TBL_REG_VILLAGES, TRUE, ['ENGINE' => 'InnoDB']);

		$this->db->enableForeignKeyChecks();
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$dumpFile = 'region' . '_' . time() . '.sql.gz';
		MySql::create()
			->setDbName($_ENV['database.default.database'])
			->setUserName($_ENV['database.default.username'])
			->setPassword($_ENV['database.default.password'])
			->includeTables([TBL_REG_PROVINCES, TBL_REG_REGENCIES, TBL_REG_DISTRICTS, TBL_REG_VILLAGES])
			->useCompressor(new GzipCompressor())
			->dumpToFile(WRITEPATH . 'dbdump/' . $dumpFile);

		$this->forge->dropTable(TBL_REG_PROVINCES, TRUE);
		$this->forge->dropTable(TBL_REG_REGENCIES, TRUE);
		$this->forge->dropTable(TBL_REG_DISTRICTS, TRUE);
		$this->forge->dropTable(TBL_REG_VILLAGES, TRUE);
	}
}