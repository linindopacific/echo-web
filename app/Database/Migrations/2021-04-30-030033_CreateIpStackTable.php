<?php namespace App\Database\Migrations;

/*
 * File: 2021-04-30-030033_CreateIpStackTable.php
 * Project: echo
 * File Created: Friday, 30th April 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Tuesday, 8th June 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Migration;
use Spatie\DbDumper\Databases\MySql;
use Spatie\DbDumper\Compressors\GzipCompressor;

class CreateIpStackTable extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id' => [
            	'type' => 'BIGINT',
            	'unsigned' => true,
            	'auto_increment' => true
			],
			'country_code' => [
            	'type' => 'CHAR',
				'constraint' => 2,
            	'null' => true
			],
			'country_name' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
            	'null' => true
			],
			'region_name' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
            	'null' => true
			],
			'city_name' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
            	'null' => true
			],
			'latitude' => [
				'type' => 'DECIMAL',
				'constraint' => '38,20',
            	'null' => true
			],
			'longitude' => [
				'type' => 'DECIMAL',
				'constraint' => '38,20',
            	'null' => true
			],
			'calling_code' => [
				'type' => 'VARCHAR',
				'constraint' => 10,
            	'null' => true
			],
			'language_code' => [
				'type' => 'CHAR',
				'constraint' => 2,
            	'null' => true
			],
			'login_id' => [
            	'type' => 'INT',
            	'unsigned' => true,
            	'constraint' => 10
			]
		]);
		$this->forge->addPrimaryKey('id');
		$this->forge->addForeignKey('login_id', TBL_AUTH_LOGINS, 'id', 'CASCADE', 'CASCADE');
		$this->forge->createTable(TBL_AUTH_LOGINS_IP, TRUE, ['ENGINE' => 'InnoDB']);
	}

	public function down()
	{
		$dumpFile = TBL_AUTH_LOGINS_IP . '_' . time() . '.sql.gz';
		MySql::create()
			->setDbName($_ENV['database.default.database'])
			->setUserName($_ENV['database.default.username'])
			->setPassword($_ENV['database.default.password'])
			->includeTables([TBL_AUTH_LOGINS_IP])
			->useCompressor(new GzipCompressor())
			->dumpToFile(WRITEPATH . 'dbdump/' . $dumpFile);

		$this->forge->dropTable(TBL_AUTH_LOGINS_IP);
	}
}
