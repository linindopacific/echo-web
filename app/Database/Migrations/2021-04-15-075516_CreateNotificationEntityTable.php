<?php namespace App\Database\Migrations;

/*
 * File: 2021-04-15-075516_CreateNotificationEntityTable.php
 * Project: echo
 * File Created: Thursday, 15th April 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Tuesday, 8th June 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Migration;
use Spatie\DbDumper\Databases\MySql;
use Spatie\DbDumper\Compressors\GzipCompressor;

class CreateNotificationEntityTable extends Migration
{
	public function up()
	{
		$this->db->disableForeignKeyChecks();

		$this->forge->addField([
			'id' => [
            	'type' => 'BIGINT',
            	'unsigned' => true,
            	'auto_increment' => true
			],
			'name' => [
            	'type' => 'VARCHAR',
				'constraint' => 50,
				'unique' => true,
            	'null' => false
			],
			'reference_table' => [
				'type' => 'VARCHAR',
				'constraint' => 100,
            	'null' => false
			],
			'created_at' => [
				'type' => 'datetime',
				'null' => true
			],
	        'updated_at' => [
	          	'type' => 'datetime',
	          	'null' => true
	        ],
	        'deleted_at' => [
	          	'type' => 'datetime',
	          	'null' => true
	        ]
		]);
		$this->forge->addPrimaryKey('id');
		$this->forge->createTable(TBL_NOTIFICATION_ENTITY, TRUE, ['ENGINE' => 'InnoDB']);

		$this->db->enableForeignKeyChecks();
	}

	public function down()
	{
		$dumpFile = TBL_NOTIFICATION_ENTITY . '_' . time() . '.sql.gz';
		MySql::create()
			->setDbName($_ENV['database.default.database'])
			->setUserName($_ENV['database.default.username'])
			->setPassword($_ENV['database.default.password'])
			->includeTables([TBL_NOTIFICATION_ENTITY])
			->useCompressor(new GzipCompressor())
			->dumpToFile(WRITEPATH . 'dbdump/' . $dumpFile);

		$this->forge->dropTable(TBL_NOTIFICATION_ENTITY, TRUE);
	}
}
