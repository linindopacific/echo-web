<?php namespace App\Database\Migrations;

/*
 * File: 2021-04-15-092236_UpdateNotificationObjectTable.php
 * Project: echo
 * File Created: Thursday, 15th April 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Thursday, 15th April 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Migration;

class UpdateNotificationObjectTable extends Migration
{
	public function up()
	{
		$this->db->disableForeignKeyChecks();

		$this->forge->modifyColumn(TBL_NOTIFICATION_OBJECT, [
			'entity_type' => [
				'name' => 'entity_type_id',
            	'type' => 'BIGINT',
            	'unsigned' => true,
            	'null' => false
			],
		]);

		$this->db->enableForeignKeyChecks();
	}

	public function down()
	{
		$this->db->disableForeignKeyChecks();

		$this->forge->modifyColumn(TBL_NOTIFICATION_OBJECT, [
			'entity_type_id' => [
				'name' => 'entity_type',
            	'type' => 'BIGINT',
            	'unsigned' => true,
            	'null' => false
			],
		]);

		$this->db->enableForeignKeyChecks();
	}
}
