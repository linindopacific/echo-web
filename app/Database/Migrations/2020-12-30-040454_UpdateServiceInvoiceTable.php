<?php namespace App\Database\Migrations;

/*
 * File: 2020-12-30-040454_UpdateServiceInvoiceTable.php
 * Project: echo
 * File Created: Wednesday, 30th December 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Tuesday, 8th June 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Migration;
use Spatie\DbDumper\Databases\MySql;
use Spatie\DbDumper\Compressors\GzipCompressor;

class UpdateServiceInvoiceTable extends Migration
{
	private function back_up()
	{
		$dumpFile = TBL_SERVICE_INVOICES . '_' . time() . '.sql.gz';
		MySql::create()
			->setDbName($_ENV['database.default.database'])
			->setUserName($_ENV['database.default.username'])
			->setPassword($_ENV['database.default.password'])
			->includeTables([TBL_SERVICE_INVOICES])
			->useCompressor(new GzipCompressor())
			->dumpToFile(WRITEPATH . 'dbdump/' . $dumpFile);
	}

	public function up()
	{
		$this->back_up();

		$this->db->disableForeignKeyChecks();

		// add columns
		$column = [
		    'reference' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null' => false,
				'after' => 'order_no'
			],
			'payment_method' => [
				'type' => 'CHAR',
				'null' => true,
				'after' => 'duedate_at'
			],
			'payment_url' => [
				'type' => 'TEXT',
				'null' => true,
				'after' => 'reference'
			],
			'va_number' => [
				'type' => 'TEXT',
				'null' => true,
				'after' => 'payment_url'
			],
			'qr_string' => [
				'type' => 'TEXT',
				'null' => true,
				'after' => 'va_number'
			],
			'charge_amount' => [
				'type' => 'DECIMAL',
				'default' => 0,
				'after' => 'amount'
			],
			'amount_including_charge' => [
				'type' => 'DECIMAL',
				'after' => 'charge_amount'
			],
		];
		$this->forge->addColumn(TBL_SERVICE_INVOICES, $column);

		// modify columns
		$columns = [
			'order_no' => [
				'name' => 'order_no',
				'type' => 'VARCHAR',
				'constraint' => 50
			],
		];
		$this->forge->modifyColumn(TBL_SERVICE_INVOICES, $columns);

		// delete columns
		$this->forge->dropColumn(TBL_SERVICE_INVOICES, ['vat_amount', 'amount_including_vat']);

		$this->db->disableForeignKeyChecks();
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->back_up();

		// delete columns
		$this->forge->dropColumn(TBL_SERVICE_INVOICES, ['reference', 'payment_method', 'payment_url', 'va_number', 'qr_string', 'charge_amount', 'amount_including_charge']);

		// modify columns
		$columns = [
			'order_no' => [
				'name' => 'order_no',
				'type' => 'TEXT'
			],
		];
		$this->forge->modifyColumn(TBL_SERVICE_INVOICES, $columns);

		// add columns
		$column = [
			'vat_amount' => [
				'type' => 'DECIMAL',
				'default' => 0
			],
			'amount_including_vat' => [
				'type' => 'DECIMAL'
			]
		];
		$this->forge->addColumn(TBL_SERVICE_INVOICES, $column);
	}
}
