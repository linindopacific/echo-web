<?php namespace App\Database\Migrations;

/*
 * File: 2020-11-26-060919_UpdateUserRelationTable.php
 * Project: echo
 * File Created: Thursday, 26th November 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Thursday, 26th November 2020
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Migration;
use Spatie\DbDumper\Databases\MySql;
use Spatie\DbDumper\Compressors\GzipCompressor;

class UpdateUserRelationTable extends Migration
{
	private function back_up()
	{
		$dumpFile = TBL_USER_RELATION . '_' . time() . '.sql.gz';
		MySql::create()
			->setDbName($_ENV['database.default.database'])
			->setUserName($_ENV['database.default.username'])
			->setPassword($_ENV['database.default.password'])
			->includeTables([TBL_USER_RELATION])
			->useCompressor(new GzipCompressor())
			->dumpToFile(WRITEPATH . 'dbdump/' . $dumpFile);
	}

	public function up()
	{
		$this->back_up();

		$this->forge->dropTable(TBL_USER_RELATION, TRUE);

		$this->forge->addField([
			'id' => [
            	'type' => 'INT',
            	'constraint' => 11,
            	'unsigned' => true,
            	'auto_increment' => true
			],
			'user_one_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => true,
				'null' => false
			],
			'user_two_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => true,
				'null' => false
			],
			'status' => [
				'type' => 'TINYINT',
				'unsigned' => true,
				'null' => false
			],
			'action_user_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => true,
				'null' => false
			],
			'created_at' => [
				'type' => 'datetime',
				'null' => true
			],
            'updated_at' => [
            	'type' => 'datetime',
            	'null' => true
            ],
            'deleted_at' => [
            	'type' => 'datetime',
            	'null' => true
            ]
		]);
		$this->forge->addPrimaryKey('id');
		$this->forge->addForeignKey('user_one_id', TBL_USERS, 'id', 'CASCADE', 'RESTRICT');
		$this->forge->addForeignKey('user_two_id', TBL_USERS, 'id', 'CASCADE', 'RESTRICT');
		$this->forge->addForeignKey('action_user_id', TBL_USERS, 'id', 'CASCADE', 'RESTRICT');
		$this->forge->addKey(['user_one_id', 'user_two_id'], FALSE, TRUE);
		$this->forge->createTable(TBL_USER_RELATION, TRUE, ['ENGINE' => 'InnoDB']);
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->back_up();

		$this->forge->dropTable(TBL_USER_RELATION, TRUE);

		$this->forge->addField([
			'with_user_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => true
			],
			'user_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => true
			],
			'status' => [
				'type' => 'ENUM',
				'constraint' => ['block','friend','follow','-'],
				'default' => '-'
			],
			'created_at' => [
				'type' => 'datetime',
				'null' => true
			],
            'updated_at' => [
            	'type' => 'datetime',
            	'null' => true
            ],
            'deleted_at' => [
            	'type' => 'datetime',
            	'null' => true
            ]
		]);
		$this->forge->addForeignKey('with_user_id', TBL_USERS, 'id', 'CASCADE', false);
		$this->forge->addForeignKey('user_id', TBL_USERS, 'id', 'CASCADE', false);
		$this->forge->createTable(TBL_USER_RELATION, TRUE, ['ENGINE' => 'InnoDB']);
	}
}