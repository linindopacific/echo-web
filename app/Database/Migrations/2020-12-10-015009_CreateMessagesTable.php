<?php namespace App\Database\Migrations;

/*
 * File: 2020-12-10-015009_CreateMessagesTable.php
 * Project: echo
 * File Created: Thursday, 10th December 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Tuesday, 8th June 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Migration;
use Spatie\DbDumper\Databases\MySql;
use Spatie\DbDumper\Compressors\GzipCompressor;

class CreateMessagesTable extends Migration
{
	public function up()
	{
		$this->db->disableForeignKeyChecks();

		$this->forge->addField([
			'id' => [
            	'type' => 'BIGINT',
            	'unsigned' => true,
            	'auto_increment' => true
			],
			'user_one_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => true,
				'null' => false
			],
			'user_two_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => true,
				'null' => false
			]
		]);
		$this->forge->addPrimaryKey('id');
		$this->forge->addForeignKey('user_one_id', TBL_USERS, 'id', 'CASCADE', 'RESTRICT');
		$this->forge->addForeignKey('user_two_id', TBL_USERS, 'id', 'CASCADE', 'RESTRICT');
		$this->forge->addKey(['user_one_id', 'user_two_id'], FALSE, TRUE);
		$this->forge->createTable(TBL_MESSAGES, TRUE, ['ENGINE' => 'InnoDB']);

		$this->forge->addField([
			'id' => [
            	'type' => 'BIGINT',
            	'unsigned' => true,
            	'auto_increment' => true
			],
			'content' => [
				'type' => 'TEXT',
				'null' => false
			],
			'messages_id' => [
            	'type' => 'BIGINT',
				'unsigned' => true,
				'null' => false
			],
			'user_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => true,
				'null' => false
			],
			'created_at' => [
				'type' => 'datetime',
				'null' => true
			],
            'updated_at' => [
            	'type' => 'datetime',
            	'null' => true
            ],
            'deleted_at' => [
            	'type' => 'datetime',
            	'null' => true
            ]
		]);
		$this->forge->addPrimaryKey('id');
		$this->forge->addForeignKey('user_id', TBL_USERS, 'id', 'CASCADE', 'RESTRICT');
		$this->forge->addForeignKey('messages_id', TBL_MESSAGES, 'id', 'CASCADE', 'RESTRICT');
		$this->forge->createTable(TBL_MESSAGE_REPLIES, TRUE, ['ENGINE' => 'InnoDB']);

		$this->db->enableForeignKeyChecks();
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$dumpFile = TBL_MESSAGES . '_' . time() . '.sql.gz';
		MySql::create()
			->setDbName($_ENV['database.default.database'])
			->setUserName($_ENV['database.default.username'])
			->setPassword($_ENV['database.default.password'])
			->includeTables([TBL_MESSAGES, TBL_MESSAGE_REPLIES])
			->useCompressor(new GzipCompressor())
			->dumpToFile(WRITEPATH . 'dbdump/' . $dumpFile);

		$this->forge->dropTable(TBL_MESSAGE_REPLIES, TRUE);
		$this->forge->dropTable(TBL_MESSAGES, TRUE);
	}
}
