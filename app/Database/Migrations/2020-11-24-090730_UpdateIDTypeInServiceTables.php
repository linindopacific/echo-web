<?php namespace App\Database\Migrations;

/*
 * File: 2020-11-24-090730_UpdateIDTypeInServiceTables.php
 * Project: echo
 * File Created: Tuesday, 24th November 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Wednesday, 25th November 2020
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use App\Database\AdvancedMigration;

class UpdateIDTypeInServiceTables extends AdvancedMigration
{
	public function up()
	{
		$this->db->disableForeignKeyChecks();

		// drop foreign keys & indexes
		$this->dropForeignKey(TBL_SERVICE_COVERAGES, 'service_coverages_service_id_foreign');
		$this->dropForeignKey(TBL_SERVICE_ORDERS, 'service_orders_service_id_foreign');
		$this->dropForeignKey(TBL_SERVICE_FULFILLMENTS, 'service_fulfillment_order_id_foreign');
		$this->dropForeignKey(TBL_SERVICE_REVIEWS, 'service_reviews_fulfillment_id_foreign');
		$this->dropForeignKey(TBL_SERVICE_INVOICES, 'service_invoices_order_id_foreign');
		$this->dropForeignKey(TBL_SERVICE_TRANSACTIONS, 'service_transactions_invoice_id_foreign');

		// modify fields to bigint
		$this->forge->modifyColumn(TBL_SERVICES, ['id' => ['name' => 'id', 'type' => 'BIGINT', 'unsigned' => true]]);
		$this->forge->modifyColumn(TBL_SERVICE_COVERAGES, ['service_id' => ['name' => 'service_id', 'type' => 'BIGINT', 'unsigned' => true]]);
		$this->forge->modifyColumn(TBL_SERVICE_ORDERS, [
			'id' => ['name' => 'id', 'type' => 'BIGINT', 'unsigned' => true],
			'service_id' => ['name' => 'service_id', 'type' => 'BIGINT', 'unsigned' => true]
		]);
		$this->forge->modifyColumn(TBL_SERVICE_FULFILLMENTS, [
			'id' => ['name' => 'id', 'type' => 'BIGINT', 'unsigned' => true],
			'order_id' => ['name' => 'order_id', 'type' => 'BIGINT', 'unsigned' => true]
		]);
		$this->forge->modifyColumn(TBL_SERVICE_REVIEWS, [
			'id' => ['name' => 'id', 'type' => 'BIGINT', 'unsigned' => true],
			'fulfillment_id' => ['name' => 'fulfillment_id', 'type' => 'BIGINT', 'unsigned' => true]
		]);
		$this->forge->modifyColumn(TBL_SERVICE_INVOICES, [
			'id' => ['name' => 'id', 'type' => 'BIGINT', 'unsigned' => true],
			'order_id' => ['name' => 'order_id', 'type' => 'BIGINT', 'unsigned' => true]
		]);
		$this->forge->modifyColumn(TBL_SERVICE_TRANSACTIONS, [
			'id' => ['name' => 'id', 'type' => 'BIGINT', 'unsigned' => true],
			'invoice_id' => ['name' => 'invoice_id', 'type' => 'BIGINT', 'unsigned' => true]
		]);

		// add foreign keys & indexes
		$this->addNewForeignKey(TBL_SERVICE_COVERAGES, 'service_id', TBL_SERVICES, 'id', 'CASCADE', 'CASCADE');
		$this->addNewForeignKey(TBL_SERVICE_ORDERS, 'service_id', TBL_SERVICES, 'id', 'NO ACTION', 'NO ACTION');
		$this->addNewForeignKey(TBL_SERVICE_FULFILLMENTS, 'order_id', TBL_SERVICE_ORDERS, 'id', 'NO ACTION', 'NO ACTION');
		$this->addNewForeignKey(TBL_SERVICE_REVIEWS, 'fulfillment_id', TBL_SERVICE_FULFILLMENTS, 'id', 'NO ACTION', 'NO ACTION');
		$this->addNewForeignKey(TBL_SERVICE_INVOICES, 'order_id', TBL_SERVICE_ORDERS, 'id', 'NO ACTION', 'NO ACTION');
		$this->addNewForeignKey(TBL_SERVICE_TRANSACTIONS, 'invoice_id', TBL_SERVICE_INVOICES, 'id', 'NO ACTION', 'NO ACTION');

		$this->db->enableForeignKeyChecks();
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->db->disableForeignKeyChecks();

		// drop foreign keys & indexes
		$this->dropForeignKey(TBL_SERVICE_COVERAGES, 'service_coverages_service_id_foreign');
		$this->dropForeignKey(TBL_SERVICE_ORDERS, 'service_orders_service_id_foreign');
		$this->dropForeignKey(TBL_SERVICE_FULFILLMENTS, 'service_fulfillment_order_id_foreign');
		$this->dropForeignKey(TBL_SERVICE_REVIEWS, 'service_reviews_fulfillment_id_foreign');
		$this->dropForeignKey(TBL_SERVICE_INVOICES, 'service_invoices_order_id_foreign');
		$this->dropForeignKey(TBL_SERVICE_TRANSACTIONS, 'service_transactions_invoice_id_foreign');

		// modify fields to int
		$this->forge->modifyColumn(TBL_SERVICES, ['id' => ['name' => 'id', 'type' => 'INT', 'constraint' => 10, 'unsigned' => true]]);
		$this->forge->modifyColumn(TBL_SERVICE_COVERAGES, ['service_id' => ['name' => 'service_id', 'type' => 'INT', 'constraint' => 10, 'unsigned' => true]]);
		$this->forge->modifyColumn(TBL_SERVICE_ORDERS, [
			'id' => ['name' => 'id', 'type' => 'INT', 'constraint' => 10, 'unsigned' => true],
			'service_id' => ['name' => 'service_id', 'type' => 'INT', 'constraint' => 10, 'unsigned' => true]
		]);
		$this->forge->modifyColumn(TBL_SERVICE_FULFILLMENTS, [
			'id' => ['name' => 'id', 'type' => 'INT', 'constraint' => 10, 'unsigned' => true],
			'order_id' => ['name' => 'order_id', 'type' => 'INT', 'constraint' => 10, 'unsigned' => true]
		]);
		$this->forge->modifyColumn(TBL_SERVICE_REVIEWS, [
			'id' => ['name' => 'id', 'type' => 'INT', 'constraint' => 10, 'unsigned' => true],
			'fulfillment_id' => ['name' => 'fulfillment_id', 'type' => 'INT', 'constraint' => 10, 'unsigned' => true]
		]);
		$this->forge->modifyColumn(TBL_SERVICE_INVOICES, [
			'id' => ['name' => 'id', 'type' => 'INT', 'constraint' => 10, 'unsigned' => true],
			'order_id' => ['name' => 'order_id', 'type' => 'INT', 'constraint' => 10, 'unsigned' => true]
		]);
		$this->forge->modifyColumn(TBL_SERVICE_TRANSACTIONS, [
			'id' => ['name' => 'id', 'type' => 'INT', 'constraint' => 10, 'unsigned' => true],
			'invoice_id' => ['name' => 'invoice_id', 'type' => 'INT', 'constraint' => 10, 'unsigned' => true]
		]);

		// add foreign keys & indexes
		$this->addNewForeignKey(TBL_SERVICE_COVERAGES, 'service_id', TBL_SERVICES, 'id', 'CASCADE', 'CASCADE');
		$this->addNewForeignKey(TBL_SERVICE_ORDERS, 'service_id', TBL_SERVICES, 'id', 'NO ACTION', 'NO ACTION');
		$this->addNewForeignKey(TBL_SERVICE_FULFILLMENTS, 'order_id', TBL_SERVICE_ORDERS, 'id', 'NO ACTION', 'NO ACTION');
		$this->addNewForeignKey(TBL_SERVICE_REVIEWS, 'fulfillment_id', TBL_SERVICE_FULFILLMENTS, 'id', 'NO ACTION', 'NO ACTION');
		$this->addNewForeignKey(TBL_SERVICE_INVOICES, 'order_id', TBL_SERVICE_ORDERS, 'id', 'NO ACTION', 'NO ACTION');
		$this->addNewForeignKey(TBL_SERVICE_TRANSACTIONS, 'invoice_id', TBL_SERVICE_INVOICES, 'id', 'NO ACTION', 'NO ACTION');

		$this->db->enableForeignKeyChecks();
	}
}
