<?php namespace App\Database\Migrations;

/*
 * File: 2020-10-23-100412_UpdateVisibilityInPostTable.php
 * Project: echo
 * File Created: Friday, 23rd October 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Wednesday, 25th November 2020
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use App\Database\AdvancedMigration;
use Spatie\DbDumper\Databases\MySql;
use Spatie\DbDumper\Compressors\GzipCompressor;

class UpdateVisibilityInPostTable extends AdvancedMigration
{
	public function up()
	{
		$this->db->disableForeignKeyChecks();

		$column = [
		    'visibility' => [
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => true,
				'default' => 1,
				'after' => 'content'
			],
		];
		$this->forge->addColumn(TBL_POSTS, $column);
		$this->addNewForeignKey(TBL_POSTS, 'visibility', TBL_POST_VISIBILITIES, 'id', 'CASCADE', 'NO ACTION');

		$this->db->disableForeignKeyChecks();
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$dumpFile = TBL_POSTS . '_' . time() . '.sql.gz';
        MySql::create()
            ->setDbName($_ENV['database.default.database'])
            ->setUserName($_ENV['database.default.username'])
            ->setPassword($_ENV['database.default.password'])
            ->includeTables([TBL_POSTS])
            ->useCompressor(new GzipCompressor())
            ->dumpToFile(WRITEPATH . 'dbdump/' . $dumpFile);

		$this->forge->dropForeignKey(TBL_POSTS, 'posts_visibility_foreign');
		$this->forge->dropColumn(TBL_POSTS, 'visibility');
	}
}
