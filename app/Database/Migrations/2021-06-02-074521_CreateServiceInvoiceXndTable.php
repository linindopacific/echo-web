<?php namespace App\Database\Migrations;

/*
 * File: 2021-06-02-074521_CreateServiceInvoiceXndTable.php
 * Project: echo
 * File Created: Wednesday, 2nd June 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Wednesday, 2nd June 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Migration;
use Spatie\DbDumper\Databases\MySql;
use Spatie\DbDumper\Compressors\GzipCompressor;

class CreateServiceInvoiceXndTable extends Migration
{
	public function up()
	{
		$this->db->disableForeignKeyChecks();

		$this->forge->addField([
			'id' => [
            	'type' => 'BIGINT',
            	'constraint' => 11,
            	'unsigned' => true,
            	'auto_increment' => true
			],
			'order_id' => [
				'type' => 'BIGINT',
				'constraint' => 11,
				'unsigned' => true
			],
			'external_id' => [
				'type' => 'VARCHAR',
				'constraint' => 100
			],
			'amount' => [
				'type' => 'DECIMAL',
			],
			'payment_category' => [
				'type' => 'VARCHAR',
				'constraint' => 20,
				'null' => false
			],
			'payment_channel' => [
				'type' => 'VARCHAR',
				'constraint' => 50,
				'null' => false
			],
			'payment_code' => [
				'type' => 'TEXT',
				'null' => true
			],
			'qr_string' => [
				'type' => 'TEXT',
				'null' => true
			],
			'payment_id' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null' => true
			],
			'expired_at' => [
				'type' => 'datetime',
				'null' => false,
			],
			'transaction_at' => [
				'type' => 'datetime',
				'null' => true
			],
			'created_at' => [
				'type' => 'datetime',
				'null' => true
			],
	        'updated_at' => [
	          	'type' => 'datetime',
	          	'null' => true
	        ],
	        'deleted_at' => [
	          	'type' => 'datetime',
	          	'null' => true
	        ]
		]);
		$this->forge->addPrimaryKey('id');
		$this->forge->addForeignKey('order_id', TBL_SERVICE_ORDERS, 'id', false, false);
		$this->forge->createTable(TBL_SERVICE_INVOICES_XND, TRUE, ['ENGINE' => 'InnoDB']);

		$this->db->enableForeignKeyChecks();
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$dumpFile = TBL_SERVICE_INVOICES_XND . '_' . time() . '.sql.gz';
		MySql::create()
			->setDbName($_ENV['database.default.database'])
			->setUserName($_ENV['database.default.username'])
			->setPassword($_ENV['database.default.password'])
			->includeTables([TBL_SERVICE_INVOICES_XND])
			->useCompressor(new GzipCompressor())
			->dumpToFile(WRITEPATH . 'dbdump/' . $dumpFile);

		$this->forge->dropTable(TBL_SERVICE_INVOICES_XND, TRUE);
	}
}
