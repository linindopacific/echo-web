<?php namespace App\Database\Migrations;

/*
 * File: 2020-08-27-063442_CreateReactionsTable.php
 * Project: echo
 * File Created: Thursday, 27th August 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Friday, 13th November 2020
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Migration;
use Spatie\DbDumper\Databases\MySql;
use Spatie\DbDumper\Compressors\GzipCompressor;

class CreateReactionsTable extends Migration
{
	public function up()
	{
		$this->db->disableForeignKeyChecks();

		$this->forge->addField([
			'id' => [
              	'type' => 'INT',
              	'constraint' => 11,
              	'unsigned' => true,
              	'auto_increment' => true
            ],
            'name' => [
            	'type' => 'VARCHAR',
              	'constraint' => 10,
            ],
            'style' => [
              	'type' => 'VARCHAR',
              	'constraint' => 50
            ],
			'created_at' => [
				'type' => 'datetime',
				'null' => true
			],
            'updated_at' => [
              	'type' => 'datetime',
              	'null' => true
            ],
            'deleted_at' => [
              	'type' => 'datetime',
              	'null' => true
            ]
		]);
		$this->forge->addPrimaryKey('id');
		$this->forge->createTable(TBL_REACTIONS, TRUE, ['ENGINE' => 'InnoDB']);

		$this->db->enableForeignKeyChecks();
	}

	//--------------------------------------------------------------------

	public function down()
	{
        $dumpFile = TBL_REACTIONS . '_' . time() . '.sql.gz';
        MySql::create()
            ->setDbName($_ENV['database.default.database'])
            ->setUserName($_ENV['database.default.username'])
            ->setPassword($_ENV['database.default.password'])
            ->includeTables([TBL_REACTIONS])
            ->useCompressor(new GzipCompressor())
            ->dumpToFile(WRITEPATH . 'dbdump/' . $dumpFile);

		$this->forge->dropTable(TBL_REACTIONS, TRUE);
	}
}