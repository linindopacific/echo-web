<?php namespace App\Database\Migrations;

/*
 * File: 2021-02-17-071055_UpdateIDIncrementInServiceTable.php
 * Project: echo
 * File Created: Wednesday, 17th February 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Wednesday, 17th February 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Migration;

class UpdateIDIncrementInServiceTable extends Migration
{
	public function up()
	{
		$this->db->disableForeignKeyChecks();

		$this->forge->modifyColumn(TBL_SERVICES, [
			'id' => ['name' => 'id', 'type' => 'BIGINT', 'unsigned' => true, 'auto_increment' => true]
		]);

		$this->db->enableForeignKeyChecks();
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->db->disableForeignKeyChecks();

		$this->forge->modifyColumn(TBL_SERVICES, [
			'id' => ['name' => 'id', 'type' => 'BIGINT', 'unsigned' => true]
		]);

		$this->db->enableForeignKeyChecks();
	}
}
