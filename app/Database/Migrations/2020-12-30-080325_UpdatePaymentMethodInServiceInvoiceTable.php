<?php namespace App\Database\Migrations;

/*
 * File: 2020-12-30-080325_UpdatePaymentMethodInServiceInvoiceTable.php
 * Project: echo
 * File Created: Wednesday, 30th December 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Tuesday, 5th January 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Migration;
use Spatie\DbDumper\Databases\MySql;
use Spatie\DbDumper\Compressors\GzipCompressor;

class UpdatePaymentMethodInServiceInvoiceTable extends Migration
{
	private function back_up()
	{
		$dumpFile = TBL_SERVICE_INVOICES . '_' . time() . '.sql.gz';
		MySql::create()
			->setDbName($_ENV['database.default.database'])
			->setUserName($_ENV['database.default.username'])
			->setPassword($_ENV['database.default.password'])
			->includeTables([TBL_SERVICE_INVOICES])
			->useCompressor(new GzipCompressor())
			->dumpToFile(WRITEPATH . 'dbdump/' . $dumpFile);
	}

	public function up()
	{
		$this->back_up();

		// modify columns
		$columns = [
			'payment_method' => [
				'name' => 'payment_method',
				'type' => 'CHAR',
				'constraint' => 2
			],
		];
		$this->forge->modifyColumn(TBL_SERVICE_INVOICES, $columns);
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->back_up();

		// modify columns
		$columns = [
			'payment_method' => [
				'name' => 'payment_method',
				'type' => 'CHAR'
			],
		];
		$this->forge->modifyColumn(TBL_SERVICE_INVOICES, $columns);
	}
}
