<?php namespace App\Database\Migrations;

/*
 * File: 2020-12-30-064634_UpdateGeometryPointInServiceOrderTable.php
 * Project: echo
 * File Created: Wednesday, 30th December 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Tuesday, 8th June 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use App\Database\AdvancedMigration;
use Spatie\DbDumper\Databases\MySql;
use Spatie\DbDumper\Compressors\GzipCompressor;

class UpdateGeometryPointInServiceOrderTable extends AdvancedMigration
{
	private function back_up()
	{
		$dumpFile = TBL_SERVICE_ORDERS . '_' . time() . '.sql.gz';
		MySql::create()
			->setDbName($_ENV['database.default.database'])
			->setUserName($_ENV['database.default.username'])
			->setPassword($_ENV['database.default.password'])
			->includeTables([TBL_SERVICE_ORDERS])
			->useCompressor(new GzipCompressor())
			->dumpToFile(WRITEPATH . 'dbdump/' . $dumpFile);
	}

	public function up()
	{
		$this->back_up();

		$this->forge->dropColumn(TBL_SERVICE_ORDERS, ['coordinates']);
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->back_up();

		$columns = [
			'coordinates' => [
				'type' => 'POINT',
				'null' => false,
				'after' => 'sell_to_phone_no'
			],
		];
		$this->forge->addColumn(TBL_SERVICE_ORDERS, $columns);
		$this->addIndex(TBL_SERVICE_ORDERS, 'coordinates', 'spatial');
	}
}
