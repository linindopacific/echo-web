<?php namespace App\Database\Migrations;

/*
 * File: 2020-08-21-022343_UpdateAuthGroupsUniqueName.php
 * Project: echo
 * File Created: Friday, 21st August 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Friday, 13th November 2020
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Migration;
use Spatie\DbDumper\Databases\MySql;
use Spatie\DbDumper\Compressors\GzipCompressor;

class UpdateAuthGroupsUniqueName extends Migration
{
	public function up()
	{
		/*
         * Groups Table
         */
		$columns = [
            'name' => [
            	'name' => 'name',
            	'type' => 'varchar',
            	'constraint' => 255,
            	'unique' => true
            ],
        ];

        $this->forge->modifyColumn(TBL_AUTH_GROUPS, $columns);
	}

	//--------------------------------------------------------------------

	public function down()
	{
        $dumpFile = TBL_AUTH_GROUPS . '_' . time() . '.sql.gz';
        MySql::create()
            ->setDbName($_ENV['database.default.database'])
            ->setUserName($_ENV['database.default.username'])
            ->setPassword($_ENV['database.default.password'])
            ->includeTables([TBL_AUTH_GROUPS])
            ->useCompressor(new GzipCompressor())
            ->dumpToFile(WRITEPATH . 'dbdump/' . $dumpFile);

		/*
         * Groups Table
         */
		$columns = [
            'name' => [
            	'name' => 'name',
            	'type' => 'varchar',
            	'constraint' => 255,
            	'unique' => false
            ],
        ];

        $this->forge->modifyColumn(TBL_AUTH_GROUPS, $columns);
	}
}