<?php namespace App\Database\Migrations;

/*
 * File: 2020-09-03-042302_CreateService.php
 * Project: echo
 * File Created: Thursday, 3rd September 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Friday, 13th November 2020
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Migration;
use Spatie\DbDumper\Databases\MySql;
use Spatie\DbDumper\Compressors\GzipCompressor;

class CreateService extends Migration
{
	public function up()
	{
		$this->db->disableForeignKeyChecks();

		$this->forge->addField([
			'id' => [
            	'type' => 'INT',
            	'constraint' => 11,
            	'unsigned' => true,
            	'auto_increment' => true
            ],
            'name' => [
            	'type' => 'VARCHAR',
            	'constraint' => 100
			],
			'description' => [
				'type' => 'TEXT'
			],
			'category_id' => [
				'type' => 'INT',
            	'constraint' => 11,
            	'unsigned' => true
			],
			'coverage' => [
				'type' => 'TEXT'
			],
			'price' => [
				'type' => 'DECIMAL',
				'default' => 0
			],
			'price_per' => [
				'type' => 'ENUM',
				'constraint' => ['hour', 'day', 'event'],
			],
			'featured' => [
				'type' => 'BIT',
				'default' => 0
			],
			'active' => [
				'type' => 'BIT',
				'default' => 0
			],
			'user_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => true
			],
			'created_at' => [
				'type' => 'datetime',
				'null' => true
			],
	        'updated_at' => [
	          	'type' => 'datetime',
	          	'null' => true
	        ],
	        'deleted_at' => [
	          	'type' => 'datetime',
	          	'null' => true
	        ]
		]);
		$this->forge->addPrimaryKey('id');
		$this->forge->addForeignKey('category_id', TBL_SERVICE_CATEGORIES, 'id', 'CASCADE', false);
		$this->forge->addForeignKey('user_id', TBL_USERS, 'id', 'CASCADE', false);
		$this->forge->createTable(TBL_SERVICES, TRUE, ['ENGINE' => 'InnoDB']);

		$this->db->enableForeignKeyChecks();
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$dumpFile = TBL_SERVICES . '_' . time() . '.sql.gz';
		MySql::create()
			->setDbName($_ENV['database.default.database'])
			->setUserName($_ENV['database.default.username'])
			->setPassword($_ENV['database.default.password'])
			->includeTables([TBL_SERVICES])
			->useCompressor(new GzipCompressor())
			->dumpToFile(WRITEPATH . 'dbdump/' . $dumpFile);

		$this->forge->dropTable(TBL_SERVICES, TRUE);
	}
}