<?php namespace App\Database\Migrations;

/*
 * File: 2021-06-08-023533_RemoveInvoiceAndTransactionTable.php
 * Project: echo
 * File Created: Tuesday, 8th June 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Tuesday, 8th June 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Migration;
use Spatie\DbDumper\Databases\MySql;
use Spatie\DbDumper\Compressors\GzipCompressor;

class RemoveInvoiceAndTransactionTable extends Migration
{
	private function backup($table)
	{
		$dumpFile = $table . '_' . time() . '.sql.gz';
		MySql::create()
			->setDbName($_ENV['database.default.database'])
			->setUserName($_ENV['database.default.username'])
			->setPassword($_ENV['database.default.password'])
			->includeTables([$table])
			->useCompressor(new GzipCompressor())
			->dumpToFile(WRITEPATH . 'dbdump/' . $dumpFile);
	}

	public function up()
	{
		$this->backup(TBL_SERVICE_TRANSACTIONS);
		$this->forge->dropTable(TBL_SERVICE_TRANSACTIONS);

		$this->backup(TBL_SERVICE_INVOICES);
		$this->forge->dropTable(TBL_SERVICE_INVOICES);
	}

	public function down()
	{
		$this->db->disableForeignKeyChecks();

		// invoices
		$this->forge->addField([
			'id' => [
            	'type' => 'BIGINT',
            	'constraint' => 11,
            	'unsigned' => true,
            	'auto_increment' => true
			],
			'order_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => true
			],
			'order_no' => [
				'type' => 'VARCHAR',
				'constraint' => 50
			],
			'reference' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null' => false
			],
			'payment_url' => [
				'type' => 'TEXT',
				'null' => true
			],
			'va_number' => [
				'type' => 'TEXT',
				'null' => true
			],
			'qr_string' => [
				'type' => 'TEXT',
				'null' => true
			],
			'duedate_at' => [
				'type' => 'datetime',
				'null' => false
			],
			'payment_method' => [
				'type' => 'CHAR',
				'constraint' => 2,
				'null' => false
			],
			'amount' => [
				'type' => 'DECIMAL',
				'null' => false
			],
			'charge_amount' => [
				'type' => 'DECIMAL',
				'default' => 0
			],
			'amount_including_charge' => [
				'type' => 'DECIMAL',
				'null' => false
			],
			'is_paid' => [
				'type' => 'BIT',
				'default' => 0
			],
			'created_at' => [
				'type' => 'datetime',
				'null' => true
			],
	        'updated_at' => [
	          	'type' => 'datetime',
	          	'null' => true
	        ],
	        'deleted_at' => [
	          	'type' => 'datetime',
	          	'null' => true
	        ]
		]);
		$this->forge->addPrimaryKey('id');
		$this->forge->addForeignKey('order_id', TBL_SERVICE_ORDERS, 'id', false, false);
		$this->forge->createTable(TBL_SERVICE_INVOICES, TRUE, ['ENGINE' => 'InnoDB']);

		// transactions
		$this->forge->addField([
			'id' => [
            	'type' => 'BIGINT',
            	'constraint' => 11,
            	'unsigned' => true,
            	'auto_increment' => true
			],
			'invoice_id' => [
				'type' => 'BIGINT',
				'constraint' => 11,
				'unsigned' => true
			],
			'payment_method' => [
				'type' => 'CHAR',
				'constraint' => 2,
				'null' => false
			],
			'amount' => [
				'type' => 'DECIMAL'
			],
			'created_at' => [
				'type' => 'datetime',
				'null' => true
			],
	        'updated_at' => [
	          	'type' => 'datetime',
	          	'null' => true
	        ],
	        'deleted_at' => [
	          	'type' => 'datetime',
	          	'null' => true
	        ]
		]);
		$this->forge->addPrimaryKey('id');
		$this->forge->addForeignKey('invoice_id', TBL_SERVICE_INVOICES, 'id', false, false);
		$this->forge->createTable(TBL_SERVICE_TRANSACTIONS, TRUE, ['ENGINE' => 'InnoDB']);

		$this->db->enableForeignKeyChecks();
	}
}
