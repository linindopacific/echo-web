<?php namespace App\Database\Migrations;

/*
 * File: 2020-08-19-065237_UpdateUserTableDisplayName.php
 * Project: echo
 * File Created: Wednesday, 19th August 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Friday, 13th November 2020
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Migration;
use Spatie\DbDumper\Databases\MySql;
use Spatie\DbDumper\Compressors\GzipCompressor;

class UpdateUserTableDisplayName extends Migration
{
	public function up()
	{
		$column = [
		    'display_name' => ['type' => 'varchar', 'constraint' => 255, 'after' => 'username']
		];

		$this->forge->addColumn(TBL_USERS, $column);
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$dumpFile = TBL_USERS . '_' . time() . '.sql.gz';
		MySql::create()
			->setDbName($_ENV['database.default.database'])
			->setUserName($_ENV['database.default.username'])
			->setPassword($_ENV['database.default.password'])
			->includeTables([TBL_USERS])
			->useCompressor(new GzipCompressor())
			->dumpToFile(WRITEPATH . 'dbdump/' . $dumpFile);

		$this->forge->dropColumn(TBL_USERS, 'display_name');
	}
}
