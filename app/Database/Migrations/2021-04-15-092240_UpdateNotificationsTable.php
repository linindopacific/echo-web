<?php namespace App\Database\Migrations;

/*
 * File: 2021-04-15-092240_UpdateNotificationsTable.php
 * Project: echo
 * File Created: Thursday, 15th April 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Thursday, 15th April 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Migration;

class UpdateNotificationsTable extends Migration
{
	public function up()
	{
		$this->db->disableForeignKeyChecks();

		$field = [
			'read_on' => [
				'type' => 'datetime',
				'null' => true,
				'after' => 'status'
			]
		];
		$this->forge->addColumn(TBL_NOTIFICATIONS, $field);

		$this->db->enableForeignKeyChecks();
	}

	public function down()
	{
		$this->db->disableForeignKeyChecks();

		$this->forge->dropColumn(TBL_NOTIFICATIONS, 'read_on');

		$this->db->enableForeignKeyChecks();
	}
}
