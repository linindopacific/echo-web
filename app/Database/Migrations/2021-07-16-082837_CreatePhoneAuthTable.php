<?php namespace App\Database\Migrations;

/*
 * File: 2021-07-16-082837_CreatePhoneAuthTable.php
 * Project: echo
 * File Created: Friday, 16th July 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Friday, 16th July 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Migration;
use Spatie\DbDumper\Databases\MySql;
use Spatie\DbDumper\Compressors\GzipCompressor;

class CreatePhoneAuthTable extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id' => [
            	'type' => 'BIGINT',
            	'unsigned' => true,
            	'auto_increment' => true
			],
			'code' => [
				'type' => 'VARCHAR',
				'constraint' => 20,
				'null' => true
			],
			'session_info' => [
            	'type' => 'TEXT',
				'null' => false
			],
			'phone_number' => [
            	'type' => 'VARCHAR',
				'constraint' => 20,
				'null' => false
			],
			'created_at' => [
				'type' => 'datetime',
				'null' => true
			],
            'updated_at' => [
            	'type' => 'datetime',
            	'null' => true
            ],
            'deleted_at' => [
            	'type' => 'datetime',
            	'null' => true
            ]
		]);
		$this->forge->addPrimaryKey('id');
		$this->forge->createTable(TBL_AUTH_PHONE_VERIFY, TRUE, ['ENGINE' => 'InnoDB']);
	}

	public function down()
	{
		$dumpFile = TBL_AUTH_PHONE_VERIFY . '_' . time() . '.sql.gz';
		MySql::create()
			->setDbName($_ENV['database.default.database'])
			->setUserName($_ENV['database.default.username'])
			->setPassword($_ENV['database.default.password'])
			->includeTables([TBL_AUTH_LOGINS_IP])
			->useCompressor(new GzipCompressor())
			->dumpToFile(WRITEPATH . 'dbdump/' . $dumpFile);

		$this->forge->dropTable(TBL_AUTH_PHONE_VERIFY);
	}
}
