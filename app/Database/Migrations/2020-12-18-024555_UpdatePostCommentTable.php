<?php namespace App\Database\Migrations;

/*
 * File: 2020-12-18-024555_UpdatePostCommentTable.php
 * Project: echo
 * File Created: Friday, 18th December 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Friday, 18th December 2020
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Migration;
use Spatie\DbDumper\Databases\MySql;
use Spatie\DbDumper\Compressors\GzipCompressor;

class UpdatePostCommentTable extends Migration
{
	public function up()
	{
		$this->db->disableForeignKeyChecks();

		$column = [
		    'content' => [
				'type' => 'TEXT',
				'null' => false,
				'after' => 'parent_id'
			],
		];
		$this->forge->addColumn(TBL_POST_COMMENTS, $column);

		$this->db->disableForeignKeyChecks();
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$dumpFile = TBL_POST_COMMENTS . '_' . time() . '.sql.gz';
		MySql::create()
			->setDbName($_ENV['database.default.database'])
			->setUserName($_ENV['database.default.username'])
			->setPassword($_ENV['database.default.password'])
			->includeTables([TBL_POST_COMMENTS])
			->useCompressor(new GzipCompressor())
			->dumpToFile(WRITEPATH . 'dbdump/' . $dumpFile);

		$this->forge->dropColumn(TBL_POST_COMMENTS, 'content');
	}
}
