<?php namespace App\Database\Migrations;

/*
 * File: 2020-11-17-025338_UpdatePinStatusInServiceTable.php
 * Project: echo
 * File Created: Tuesday, 17th November 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Tuesday, 17th November 2020
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Migration;
use Spatie\DbDumper\Databases\MySql;
use Spatie\DbDumper\Compressors\GzipCompressor;

class UpdatePinStatusInServiceTable extends Migration
{
	public function up()
	{
		$this->db->disableForeignKeyChecks();

		$column = [
			'pinned' => [
				'type' => 'BIT',
				'default' => 0,
				'after' => 'featured'
			],
		];
		$this->forge->addColumn(TBL_SERVICES, $column);

		$this->db->disableForeignKeyChecks();
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$dumpFile = TBL_SERVICES . '_' . time() . '.sql.gz';
		MySql::create()
			->setDbName($_ENV['database.default.database'])
			->setUserName($_ENV['database.default.username'])
			->setPassword($_ENV['database.default.password'])
			->includeTables([TBL_SERVICES])
			->useCompressor(new GzipCompressor())
			->dumpToFile(WRITEPATH . 'dbdump/' . $dumpFile);

		$this->forge->dropColumn(TBL_SERVICES, 'pinned');
	}
}
