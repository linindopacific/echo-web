<?php namespace App\Database\Migrations;

/*
 * File: 2020-08-27-041230_CreateUserProfileTable.php
 * Project: echo
 * File Created: Thursday, 27th August 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Friday, 13th November 2020
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Migration;
use Spatie\DbDumper\Databases\MySql;
use Spatie\DbDumper\Compressors\GzipCompressor;

class CreateUserProfileTable extends Migration
{
	public function up()
	{
		$this->db->disableForeignKeyChecks();

		$this->forge->addField([
			'id' => [
            	'type' => 'INT',
            	'constraint' => 11,
            	'unsigned' => true,
            	'auto_increment' => true
            ],
			'first_name' => [
				'type' => 'VARCHAR',
				'constraint' => 50,
				'null' => true
			],
			'middle_name' => [
				'type' => 'VARCHAR',
				'constraint' => 50,
				'null' => true
			],
			'last_name' => [
				'type' => 'VARCHAR',
				'constraint' => 50,
				'null' => true
			],
			'gender' => [
				'type' => 'ENUM',
				'constraint' => ['male', 'female', 'other'],
				'null' => true
			],
            'address_1' => [
                'type' => 'TEXT',
                'null' => true,
            ],
            'address_2' => [
                'type' => 'TEXT',
                'null' => true,
            ],
            'city' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null' => true
			],
			'region' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null' => true
			],
			'zip_code' => [
				'type' => 'VARCHAR',
				'constraint' => 10,
				'null' => true
			],
			'phone_number' => [
				'type' => 'VARCHAR',
				'constraint' => 20,
				'null' => true
			],
			'birthdate' => [
				'type' => 'DATE',
				'null' => true
			],
			'display_picture' => [
				'type' => 'INT',
				'unsigned' => true,
				'constraint' => 10,
				'null' => true
			],
			'visibility' => [
				'type' => 'BIT',
				'default' => true
			],
			'user_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => true
			],
			'created_at' => [
				'type' => 'datetime',
				'null' => true
			],
            'updated_at' => [
            	'type' => 'datetime',
            	'null' => true
            ],
            'deleted_at' => [
            	'type' => 'datetime',
            	'null' => true
            ]
        ]);
        $this->forge->addPrimaryKey('id');
		$this->forge->addForeignKey('display_picture', TBL_UPLOADS, 'id', 'CASCADE', 'SET NULL');
		$this->forge->addForeignKey('user_id', TBL_USERS, 'id', 'CASCADE', 'CASCADE');
		$this->forge->addUniqueKey('user_id');
        $this->forge->createTable(TBL_USER_PROFILES, TRUE, ['ENGINE' => 'InnoDB']);

        $this->db->enableForeignKeyChecks();
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$dumpFile = TBL_USER_PROFILES . '_' . time() . '.sql.gz';
		MySql::create()
			->setDbName($_ENV['database.default.database'])
			->setUserName($_ENV['database.default.username'])
			->setPassword($_ENV['database.default.password'])
			->includeTables([TBL_USER_PROFILES])
			->useCompressor(new GzipCompressor())
			->dumpToFile(WRITEPATH . 'dbdump/' . $dumpFile);

		$this->forge->dropTable(TBL_USER_PROFILES, TRUE);
	}
}
