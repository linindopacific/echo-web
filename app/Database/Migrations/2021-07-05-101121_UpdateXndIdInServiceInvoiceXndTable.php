<?php namespace App\Database\Migrations;

/*
 * File: 2021-07-05-101121_UpdateXndIdInServiceInvoiceXndTable.php
 * Project: echo
 * File Created: Monday, 5th July 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Tuesday, 6th July 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Migration;
use Spatie\DbDumper\Databases\MySql;
use Spatie\DbDumper\Compressors\GzipCompressor;

class UpdateXndIdInServiceInvoiceXndTable extends Migration
{
	private function back_up()
	{
		$dumpFile = TBL_SERVICE_INVOICES_XND . '_' . time() . '.sql.gz';
		MySql::create()
			->setDbName($_ENV['database.default.database'])
			->setUserName($_ENV['database.default.username'])
			->setPassword($_ENV['database.default.password'])
			->includeTables([TBL_SERVICE_INVOICES_XND])
			->useCompressor(new GzipCompressor())
			->dumpToFile(WRITEPATH . 'dbdump/' . $dumpFile);
	}

	public function up()
	{
		$this->back_up();

		$columns = [
			'xnd_id' => [
				'type' => 'varchar',
				'constraint' => 255,
				'null' => false,
				'after' => 'order_id'
			],
		];
		$this->forge->addColumn(TBL_SERVICE_INVOICES_XND, $columns);
	}

	public function down()
	{
		$this->back_up();

		$this->forge->dropColumn(TBL_SERVICE_INVOICES_XND, ['xnd_id']);
	}
}
