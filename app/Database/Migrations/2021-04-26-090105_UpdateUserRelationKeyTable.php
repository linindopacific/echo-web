<?php namespace App\Database\Migrations;

/*
 * File: 2021-04-26-090105_UpdateUserRelationKeyTable.php
 * Project: echo
 * File Created: Monday, 26th April 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Monday, 26th April 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Migration;
use Spatie\DbDumper\Databases\MySql;
use Spatie\DbDumper\Compressors\GzipCompressor;

class UpdateUserRelationKeyTable extends Migration
{
	private function back_up()
	{
		$dumpFile = TBL_USER_RELATION . '_' . time() . '.sql.gz';
		MySql::create()
			->setDbName($_ENV['database.default.database'])
			->setUserName($_ENV['database.default.username'])
			->setPassword($_ENV['database.default.password'])
			->includeTables([TBL_USER_RELATION])
			->useCompressor(new GzipCompressor())
			->dumpToFile(WRITEPATH . 'dbdump/' . $dumpFile);
	}

	private function _getData($withDeclined = true)
	{
		$db = \Config\Database::connect();
		$builder = $db->table(TBL_USER_RELATION);
		if ( !$withDeclined )
		{
			$query = $builder->where('status !=', 2)->where('deleted_at', NULL)->get();
		}
		else
		{
			$query = $builder->where('deleted_at', NULL)->get();
		}

		return $query->getResultArray();
	}

	private function _setData($data)
	{
		$db = \Config\Database::connect();
		$builder = $db->table(TBL_USER_RELATION);
		$builder->insertBatch($data);
	}

	public function up()
	{
		$this->back_up();

		$data = $this->_getData();

		$this->forge->dropTable(TBL_USER_RELATION, TRUE);

		$this->forge->addField([
			'id' => [
            	'type' => 'INT',
            	'constraint' => 11,
            	'unsigned' => true,
            	'auto_increment' => true
			],
			'user_one_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => true,
				'null' => false
			],
			'user_two_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => true,
				'null' => false
			],
			'status' => [
				'type' => 'TINYINT',
				'unsigned' => true,
				'null' => false
			],
			'action_user_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => true,
				'null' => false
			],
			'created_at' => [
				'type' => 'datetime',
				'null' => true
			],
            'updated_at' => [
            	'type' => 'datetime',
            	'null' => true
            ],
            'deleted_at' => [
            	'type' => 'datetime',
            	'null' => true
            ]
		]);
		$this->forge->addPrimaryKey('id');
		$this->forge->addForeignKey('user_one_id', TBL_USERS, 'id', 'CASCADE', 'RESTRICT');
		$this->forge->addForeignKey('user_two_id', TBL_USERS, 'id', 'CASCADE', 'RESTRICT');
		$this->forge->addForeignKey('action_user_id', TBL_USERS, 'id', 'CASCADE', 'RESTRICT');
		$this->forge->addKey(['user_one_id', 'user_two_id', 'status'], FALSE, TRUE);
		$this->forge->createTable(TBL_USER_RELATION, TRUE, ['ENGINE' => 'InnoDB']);

		$this->_setData($data);
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->back_up();

		$data = $this->_getData(false);

		$this->forge->dropTable(TBL_USER_RELATION, TRUE);

		$this->forge->addField([
			'id' => [
            	'type' => 'INT',
            	'constraint' => 11,
            	'unsigned' => true,
            	'auto_increment' => true
			],
			'user_one_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => true,
				'null' => false
			],
			'user_two_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => true,
				'null' => false
			],
			'status' => [
				'type' => 'TINYINT',
				'unsigned' => true,
				'null' => false
			],
			'action_user_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => true,
				'null' => false
			],
			'created_at' => [
				'type' => 'datetime',
				'null' => true
			],
            'updated_at' => [
            	'type' => 'datetime',
            	'null' => true
            ],
            'deleted_at' => [
            	'type' => 'datetime',
            	'null' => true
            ]
		]);
		$this->forge->addPrimaryKey('id');
		$this->forge->addForeignKey('user_one_id', TBL_USERS, 'id', 'CASCADE', 'RESTRICT');
		$this->forge->addForeignKey('user_two_id', TBL_USERS, 'id', 'CASCADE', 'RESTRICT');
		$this->forge->addForeignKey('action_user_id', TBL_USERS, 'id', 'CASCADE', 'RESTRICT');
		$this->forge->addKey(['user_one_id', 'user_two_id'], FALSE, TRUE);
		$this->forge->createTable(TBL_USER_RELATION, TRUE, ['ENGINE' => 'InnoDB']);

		$this->_setData($data);
	}
}
