<?php namespace App\Database\Migrations;

/*
 * File: 2020-11-25-022646_UpdateLatLngInServiceOrderTable.php
 * Project: echo
 * File Created: Wednesday, 25th November 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Wednesday, 25th November 2020
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use App\Database\AdvancedMigration;
use Spatie\DbDumper\Databases\MySql;
use Spatie\DbDumper\Compressors\GzipCompressor;

class UpdateLatLngInServiceOrderTable extends AdvancedMigration
{
	private function back_up()
	{
		$dumpFile = TBL_SERVICE_ORDERS . '_' . time() . '.sql.gz';
		MySql::create()
			->setDbName($_ENV['database.default.database'])
			->setUserName($_ENV['database.default.username'])
			->setPassword($_ENV['database.default.password'])
			->includeTables([TBL_SERVICE_ORDERS])
			->useCompressor(new GzipCompressor())
			->dumpToFile(WRITEPATH . 'dbdump/' . $dumpFile);

	}

	public function up()
	{
		$this->back_up();

		$this->db->disableForeignKeyChecks();

		$columns = [
			'coordinates' => [
				'type' => 'POINT',
				'null' => false,
				'after' => 'sell_to_phone_no'
			],
		];
		$this->forge->addColumn(TBL_SERVICE_ORDERS, $columns);
		$this->addIndex(TBL_SERVICE_ORDERS, 'coordinates', 'spatial');

		// drop columns
		$this->forge->dropColumn(TBL_SERVICE_ORDERS, 'lat');
		$this->forge->dropColumn(TBL_SERVICE_ORDERS, 'lng');

		$this->db->disableForeignKeyChecks();
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->back_up();

		$columns = [
			'lat' =>[
				'type' => 'DECIMAL(10,8)',
				'after' => 'sell_to_phone_no'
			],
			'lng' =>[
				'type' => 'DECIMAL(11,8)',
				'after' => 'lat'
			],
		];
		$this->forge->addColumn(TBL_SERVICE_ORDERS, $columns);

		// drop columns
		$this->dropIndex(TBL_SERVICE_ORDERS, 'service_orders_coordinates_foreign');
		$this->forge->dropColumn(TBL_SERVICE_ORDERS, 'coordinates');
	}
}