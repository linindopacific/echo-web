<?php namespace App\Database\Migrations;

/*
 * File: 2021-03-02-041024_AddPaidAtInServiceInvoiceTable.php
 * Project: echo
 * File Created: Tuesday, 2nd March 2021
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Tuesday, 8th June 2021
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2021 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Migration;
use Spatie\DbDumper\Databases\MySql;
use Spatie\DbDumper\Compressors\GzipCompressor;

class AddPaidAtInServiceInvoiceTable extends Migration
{
	private function back_up()
	{
		$dumpFile = TBL_SERVICE_INVOICES . '_' . time() . '.sql.gz';
		MySql::create()
			->setDbName($_ENV['database.default.database'])
			->setUserName($_ENV['database.default.username'])
			->setPassword($_ENV['database.default.password'])
			->includeTables([TBL_SERVICE_INVOICES])
			->useCompressor(new GzipCompressor())
			->dumpToFile(WRITEPATH . 'dbdump/' . $dumpFile);
	}

	public function up()
	{
		$columns = [
			'paid_at' => [
				'type' => 'datetime',
				'null' => true,
				'after' => 'is_paid'
			],
		];
		$this->forge->addColumn(TBL_SERVICE_INVOICES, $columns);
	}

	public function down()
	{
		$this->back_up();

		$this->forge->dropColumn(TBL_SERVICE_INVOICES, ['paid_at']);
	}
}
