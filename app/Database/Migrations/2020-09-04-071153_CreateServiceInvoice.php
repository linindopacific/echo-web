<?php namespace App\Database\Migrations;

/*
 * File: 2020-09-04-071153_CreateServiceInvoice.php
 * Project: echo
 * File Created: Friday, 4th September 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Friday, 13th November 2020
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Migration;
use Spatie\DbDumper\Databases\MySql;
use Spatie\DbDumper\Compressors\GzipCompressor;

class CreateServiceInvoice extends Migration
{
	public function up()
	{
		$this->db->disableForeignKeyChecks();

		$this->forge->addField([
			'id' => [
            	'type' => 'INT',
            	'constraint' => 11,
            	'unsigned' => true,
            	'auto_increment' => true
			],
			'order_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => true
			],
			'order_no' => [
				'type' => 'TEXT'
			],
			'duedate_at' => [
				'type' => 'datetime'
			],
			'amount' => [
				'type' => 'DECIMAL',
			],
			'vat_amount' => [
				'type' => 'DECIMAL',
				'default' => 0
			],
			'amount_including_vat' => [
				'type' => 'DECIMAL'
			],
			'is_paid' => [
				'type' => 'BIT',
				'default' => 0
			],
			'created_at' => [
				'type' => 'datetime',
				'null' => true
			],
	        'updated_at' => [
	          	'type' => 'datetime',
	          	'null' => true
	        ],
	        'deleted_at' => [
	          	'type' => 'datetime',
	          	'null' => true
	        ]
		]);
		$this->forge->addPrimaryKey('id');
		$this->forge->addForeignKey('order_id', TBL_SERVICE_ORDERS, 'id', false, false);
		$this->forge->createTable(TBL_SERVICE_INVOICES, TRUE, ['ENGINE' => 'InnoDB']);

		$this->db->enableForeignKeyChecks();
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$dumpFile = TBL_SERVICE_INVOICES . '_' . time() . '.sql.gz';
		MySql::create()
			->setDbName($_ENV['database.default.database'])
			->setUserName($_ENV['database.default.username'])
			->setPassword($_ENV['database.default.password'])
			->includeTables([TBL_SERVICE_INVOICES])
			->useCompressor(new GzipCompressor())
			->dumpToFile(WRITEPATH . 'dbdump/' . $dumpFile);

		$this->forge->dropTable(TBL_SERVICE_INVOICES, TRUE);
	}
}