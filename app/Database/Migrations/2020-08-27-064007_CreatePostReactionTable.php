<?php namespace App\Database\Migrations;

/*
 * File: 2020-08-27-064007_CreatePostReactionTable.php
 * Project: echo
 * File Created: Thursday, 27th August 2020
 * Author: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Last Modified: Friday, 13th November 2020
 * Modified By: CORPGROUP\MIS7102 (dpratama@group.linindo.com)
 * -----
 * Copyright (c) 2020 Paz Ace Indonesia, PT.
 */

use CodeIgniter\Database\Migration;
use Spatie\DbDumper\Databases\MySql;
use Spatie\DbDumper\Compressors\GzipCompressor;

class CreatePostReactionTable extends Migration
{
	public function up()
	{
		$this->db->disableForeignKeyChecks();

		$this->forge->addField([
			'post_id' => [
              	'type' => 'INT',
              	'constraint' => 11,
              	'unsigned' => true,
            ],
            'reaction_id' => [
              	'type' => 'INT',
              	'constraint' => 11,
              	'unsigned' => true,
            ],
            'user_id' => [
              	'type' => 'INT',
              	'constraint' => 11,
             	'unsigned' => true,
            ],
			'created_at' => [
				'type' => 'datetime',
				'null' => true
			],
	        'updated_at' => [
	          	'type' => 'datetime',
	          	'null' => true
	        ],
	        'deleted_at' => [
	          	'type' => 'datetime',
	          	'null' => true
	        ]
		]);
		$this->forge->addForeignKey('post_id', TBL_POSTS, 'id', 'CASCADE', false);
		$this->forge->addForeignKey('reaction_id', TBL_REACTIONS, 'id', 'CASCADE', false);
		$this->forge->addForeignKey('user_id', TBL_USERS, 'id', 'CASCADE', false);
		$this->forge->createTable(TBL_POST_REACTIONS, TRUE, ['ENGINE' => 'InnoDB']);

		$this->db->enableForeignKeyChecks();
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$dumpFile = TBL_POST_REACTIONS . '_' . time() . '.sql.gz';
		MySql::create()
			->setDbName($_ENV['database.default.database'])
			->setUserName($_ENV['database.default.username'])
			->setPassword($_ENV['database.default.password'])
			->includeTables([TBL_POST_REACTIONS])
			->useCompressor(new GzipCompressor())
			->dumpToFile(WRITEPATH . 'dbdump/' . $dumpFile);

		$this->forge->dropTable(TBL_POST_REACTIONS, TRUE);
	}
}