/*
console.log(location.hostname);
console.log(document.domain);

console.log("document.URL : "+document.URL);
console.log("document.location.href : "+document.location.href);
console.log("document.location.origin : "+document.location.origin); // this
console.log("document.location.hostname : "+document.location.hostname);
console.log("document.location.host : "+document.location.host);
console.log("document.location.pathname : "+document.location.pathname);
*/

const siteUrl = document.location.origin;

$(document).on('click', '[data-toggle="lightbox"]', function(e) {
    e.preventDefault();
    $(this).ekkoLightbox();
});

$(document).ajaxError(function( event, xhr, textStatus, thrownError ) {
    var res = xhr.responseJSON;
    if (xhr.statusCode == 401) {
        alert("Error: " + errorThrown);
        window.location.replace(siteUrl);
    } else if (res.error == 'EMPTY_PROFILE') {
        alert(res.messages.error);
        window.location.replace(siteUrl + '/account/settings');
    }
});

$('[data-toggle="tooltip"]').tooltip();

$("#s-people").easyAutocomplete({
    url: function(phrase) {
        return siteUrl + "/profile/ajax_search";
    },
    getValue: function(element) {
        return element.text;
    },
    template: {
        type: "custom",
        method: function(value, item) {
            return "<a href='" + item.link + "' style='text-decoration: none;'><img src='" + item.image + "' width='30' height='30' class='rounded-circle'/> " + value + "</a>";
        },
        fields: {
            link: "link"
        }
    },
    ajaxSettings: {
        dataType: "json",
        method: "POST",
        data: {
            dataType: "json"
        }
    },
    list: {
        hideAnimation: {
            type: "slide", //normal|slide|fade
            time: 400,
            callback: function() {}
        },
        maxNumberOfElements: 8,
        match: {
            enabled: true
        },
        showAnimation: {
            type: "fade", //normal|slide|fade
            time: 200,
            callback: function() {}
        },
        sort: {
            enabled: true
        }
    },
    preparePostData: function(data) {
        data.phrase = $("#s-people").val();
        return data;
    },
    requestDelay: 500
});

function connection_status()
{
    $.ajax ({
        url: siteUrl + "/profile/connection_status",
        headers: {"X-Requested-With": "XMLHttpRequest"},
        type: "get",
        dataType: "json",
        global: false
    })
    .done(function(response) {
        $('#connection_status').html(response.data);
    })
    .fail(function(xhr, textStatus, errorThrown) {
        var errors = xhr.responseJSON;
        if (xhr.status == 401) {
            alert("Error: " + errorThrown);
        } else {
            alert("Error: " + errors.message);
        }
    });
}

function toggle_profile()
{
    $(".act-profile").on("click", function(e) {
        e.preventDefault();
        var uid = $(this).attr("data-id"),
            act_type = $(this).attr("data-act");

        $.ajax ({
            url: siteUrl + "/profile/action",
            headers: {"X-Requested-With": "XMLHttpRequest"},
            data: {uid: uid, act_type: act_type},
            type: "post",
            dataType: "json",
            global: false
        })
        .done(function(response) {
            if (act_type === "private-message") {
                window.location.replace(response.data);
            } else {
                connection_status();
            }
        })
        .fail(function(xhr, textStatus, errorThrown) {
            var errors = xhr.responseJSON;
            if (xhr.status == 401) {
                alert("Error: " + errorThrown);
                window.location.replace(siteUrl);
            } else {
                alert("Error: " + errors.message);
            }
        })
    })
}

function toggle_post_comment()
{
    $(".act-comment").on( "keypress", function(e) {
        var keyCode = e.keyCode || e.which;
        // Enter was pressed without shift key
        if (keyCode == 13 && !e.shiftKey) {
            e.preventDefault();
            var parent_id = $(this).attr("data-parent"),
                post_id = $(this).attr("data-post"),
                content = $(this);

            if (content.val().length > 0)
            {
                $(this).attr("disabled", true);
                $.ajaxq ("MyQueue", {
                    url: siteUrl + "/comment/store",
                    headers: {"X-Requested-With": "XMLHttpRequest"},
                    data: {post_id: post_id, parent_id: parent_id, content: content.val()},
                    type: "post",
                    dataType: "json",
                    success: function(response) {
                        var selector = ".post_" + post_id + " .card .card-comment-body";
                        content.val("");
                        $(selector).prepend(response.data);
                    },
                    error: function(xhr, textStatus, errorThrown) {
                        var errors = xhr.responseJSON;
                        if (xhr.status == 401) {
                            alert("Error: " + errorThrown);
                            window.location.replace(siteUrl);
                        } else {
                            alert("Error: " + errors.message);
                        }
                    }
                });
                $(this).attr("disabled", false);
            }
        }
    });
}

function toggle_post()
{
    $(".act-post").on( "click", function(e) {
        e.preventDefault();
        var act_id = $(this).attr("id"),
            post_id = $(this).attr("data-id"),
            act_type = $(this).attr("data-act");

        var card_footer = ".post_" + post_id + " .card .card-footer";

        if (act_type === "get-comment")
        {
            if ($(card_footer + " .comment-box-card").length > 0) {
                $(card_footer + " #post_commentBox").html("");
                $(card_footer + " .comment-box-card").remove();
                var selector = ".post_" + post_id + " .card .card-comment-body";
                $(selector).remove();
                return;
            }
        }

        $.ajaxq ("MyQueue", {
            url: siteUrl + "/post/action",
            headers: {"X-Requested-With": "XMLHttpRequest"},
            data: {post_id: post_id, act_type: act_type},
            type: "post",
            dataType: "json",
            success:function(response) {
                if (act_type === "delete") {
                    var selector = ".post_" + post_id;
                    $(selector).fadeOut(300, function() { $(this).remove(); });
                } else if (act_type === "get-comment") {
                    $(card_footer + " #post_commentBox").html(response.data.commentBox);
                    var selector = ".post_" + post_id + " .card";
                    $(selector).append(response.data.commentReplies);
                } else {
                    var selector = ".post_" + post_id + " .card .card-footer #" + act_id + " #icon";
                    $(selector).html(response.data);
                }
            },
            error: function(xhr, textStatus, errorThrown) {
                var errors = xhr.responseJSON;
                if (xhr.status == 401) {
                    alert("Error: " + errorThrown);
                    window.location.replace(siteUrl);
                } else {
                    alert("Error: " + errors.message);
                }
            }
        });
    });

    $(".post-modal").on( "click", function(e) {
        e.preventDefault();
        var id = $(this).attr('data-id');

        $.ajax({
            url: siteUrl + "/post/store",
            type: 'get',
            data: {id: id},
            dataType: 'json',
            global: false
        })
        .done(function(response) {
            $('#postModal').html(response.data);

            $("#post_visibility").selectpicker();
            $('#post-content').summernote({
                dialogsInBody: true,
                placeholder: 'Share your gig story / experience / recommendation / DIY advice',
                tabsize: 2,
                spellCheck: false,
                disableGrammar: true,
                maxHeight: 80,
                toolbar: [
                    //['insert', ['link', 'picture']],
                ]
            });

            $("#share-post").on( "click", function(e) {
                e.preventDefault();
                var $form = $('#postModal form'),
                    data = $form.serialize();

                $.ajax({
                    url: $form.attr("action"),
                    data: data,
                    type: $form.attr("method"),
                    dataType: 'json',
                    global: false,
                    beforeSend: function() {
                        $("#post-content").attr("disabled", true);
                        $("#share-post").html('<i class="fas fa-spinner fa-pulse"></i> Posting');
                        $("#share-post").attr("disabled", true);
                    }
                })
                .always(function() {
                    $("#post-content").attr("disabled", false);
                    $("#share-post").html('<i class="fas fa-bullhorn"></i> <span class="align-middle"> Post</span>');
                    $("#share-post").attr("disabled", false);
                })
                .done(function(response) {
                    $('#postModal').modal('hide');
                    $('#post-content').summernote('reset');

                    if (id === undefined) {
                        $('#timeline_post').prepend(response.html);
                    } else {
                        var selector = "#timeline_post .post_" + id + " .card .card-body article";
                        $(selector).html(response.html);
                    }
                })
                .fail(function(xhr, textStatus, errorThrown) {
                    var errors = xhr.responseJSON;
                    if (xhr.status == 401) {
                        alert("Error: " + errorThrown);
                        window.location.replace(siteUrl);
                    } else {
                        alert("Error: " + errors.message);
                    }
                })

                return false;
            });

            $("#postModal").modal({
                //backdrop: "static",
                keyboard: false,
                show: true
            });
        })
        .fail(function(xhr, textStatus, errorThrown) {
            var errors = xhr.responseJSON;
            if (xhr.status == 401) {
                alert("Error: " + errorThrown);
                window.location.replace(siteUrl);
            } else {
                alert("Error: " + errors.message);
            }
        });
    });
}

function toggle_my_services()
{
    $(".service-modal").on( "click", function(e) {
        e.preventDefault();
        var id = $(this).attr('data-id');

        $.ajax({
            url: siteUrl + "/services/store",
            method: 'get',
            data: {id: id},
            dataType: 'json',
            global: false
        })
        .done(function(data){
            $('#serviceModal').html(data.html);

            $("#service-category").selectpicker();
            $("#service-coverage").selectpicker();
            $("#service-pricePer").selectpicker();
            $('#service-description').summernote({
                dialogsInBody: true,
                placeholder: 'Set description for your service here...',
                tabsize: 2,
                spellCheck: false,
                disableGrammar: true,
                height: 150,
                minHeight: null,
                maxHeight: null,
                focus: true,
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'underline', 'clear']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['table', ['table']],
                    ['insert', ['link', 'picture']],
                    //['insert', ['link', 'elfinder']],
                    ['view', ['codeview', 'help']]
                ],
                callbacks: {
                    onImageUpload: function(image) {
                        uploadImage(image[0]);
                    },
                    onMediaDelete : function(target) {
                        deleteImage(target[0].src);
                    }
                }
            });

            $("#save-service").on( "click", function(e) {
                e.preventDefault();
                var $form = $('#serviceModal form'),
                    data = $form.serializeArray();

                data.push({name: 'type', value: 'save'});

                $.ajax ({
                    url: $form.attr("action"),
                    data: data,
                    method: $form.attr("method"),
                    dataType: 'json',
                    global: false,
                    beforeSend: function() {
                        $("#formError").hide();
                        $("#formError").html('');
                        $("#service-description").attr("disabled", true);
                        $("#save-service").html('<i class="fas fa-spinner fa-pulse"></i> Saving');
                        $("#save-service").attr("disabled", true);
                        $("#publish-service").attr("disabled", true);
                    }
                })
                .always(function() {
                    $("#service-description").attr("disabled", false);
                    $("#save-service").html('<i class="fas fa-save"></i> Save to Draft');
                    $("#save-service").attr("disabled", false);
                    $("#publish-service").attr("disabled", false);
                })
                .done(function() {
                    location.reload();
                })
                .fail(function(xhr, textStatus, errorThrown) {
                    var res = xhr.responseJSON;
                    alert("Error: " + errorThrown);
                    if (xhr.status == 401) {
                        window.location.replace(siteUrl);
                    } else {
                        for (const [key, value] of Object.entries(res.messages)) {
                            $("#formError").append(value + "<br>");
                        }
                        $("#formError").show();
                    }
                })
            });

            $("#publish-service").on( "click", function(e) {
                e.preventDefault();
                var $form = $('#serviceModal form'),
                    data = $form.serializeArray();

                data.push({name: 'type', value: 'publish'});

                $.ajax ({
                    url: $form.attr("action"),
                    data: data,
                    type: $form.attr("method"),
                    dataType: 'json',
                    global: false,
                    beforeSend: function() {
                        $("#formError").hide();
                        $("#formError").html('');
                        $("#service-description").attr("disabled", true);
                        $("#save-service").attr("disabled", true);
                        $("#publish-service").html('<i class="fas fa-spinner fa-pulse"></i> Publishing');
                        $("#publish-service").attr("disabled", true);
                    }
                })
                .always(function() {
                    $("#service-description").attr("disabled", false);
                    $("#save-service").attr("disabled", false);
                    $("#publish-service").html('<i class="fas fa-paper-plane"></i> Publish');
                    $("#publish-service").attr("disabled", false);
                })
                .done(function() {
                    location.reload();
                })
                .fail(function(xhr, textStatus, errorThrown) {
                    var res = xhr.responseJSON;
                    alert("Error: " + errorThrown);
                    if (xhr.status == 401) {
                        window.location.replace(siteUrl);
                    } else {
                        for (const [key, value] of Object.entries(res.messages)) {
                            $("#formError").append(value + "<br>");
                        }
                        $("#formError").show();
                    }
                })
            });

            $("#serviceModal").modal({
                backdrop: "static",
                keyboard: false,
                show: true
            });
        });
    });
}

function find_services(url, data, target, prevTarget=null)
{
    $.ajax ({
        url: url,
        data: data,
        method: 'post',
        dataType: 'json',
        global: false,
        beforeSend: function() {
            if (prevTarget != null) {
                $('#' + prevTarget).html('');
            }
        }
    })
    .done(function(data) {
        $(target).html(data.html);
    });
}

$("#btnOrderService").on("click", function(e)
{
    e.preventDefault();
    window.location.replace(siteUrl + '/services/order?id=' + $(this).attr('data-id'));
});

$("#dropdownNotification").on("click", function (e)
{
    e.preventDefault();
    var selector = $(this).next('.dropdown-menu');
    $.ajax ({
        url: siteUrl + '/getNotification',
        dataType: 'json',
        global: false
    })
    .done(function(response) {
        selector.html(response.data);
    })
    .fail(function(xhr, textStatus, errorThrown) {
        var errors = xhr.responseJSON;
        if (xhr.status == 401) {
            alert("Error: " + errorThrown);
            window.location.replace(siteUrl);
        } else {
            alert("Error: " + errors.message);
        }
    });
})

// elfinder function
function elfinderDialog(context)  // <------------------ +context
{
    var fm = $('<div/>').dialogelfinder({
        url : 'assets/elfinder/connector.minimal.php',
        lang : 'en',
        width : 840,
        height: 450,
        destroyOnClose: true,
        useBrowserHistory: false,
        uiOptions:{
            cwd:{
                getClass:function(file){
                    if(file.name.match(/archive/i)){
                        return'archive-folder';
                    }else if(file.name.match(/attachment/i)){
                        return'attachments-folder';
                    }else if(file.name.match(/avatar\|user\|users/i)){
                        return'users-folder';
                    }else if(file.name.match(/backup/i)){
                        return'backup-folder';
                    }else if(file.name.match(/carousel\|featured/i)){
                        return'featured-folder';
                    }else if(file.name.match(/order/i)){
                        return'orders-folder';
                    }else if(file.name.match(/photo\|picture\|image/i)){
                        return'picture-folder';
                    }else if(file.name.match(/doc/i)){
                        return'document-folder';
                    }else if(file.name.match(/vid\|mov/i)){
                        return'video-folder';
                    }else{
                        return'';
                    }
                }
            }
        },
        getFileCallback : function(file, fm) {
            console.log(file);
            // $('.editor').summernote('editor.insertImage', fm.convAbsUrl(file.url)); ...before
            context.invoke('editor.insertImage', fm.convAbsUrl(file.url)); // <------------ after
        },
        commandsOptions : {
            getfile : {
                oncomplete : 'close',
                folders : false
            }
        }
    }).dialogelfinder('instance');
}

function make_skeleton_post()
{
    $('#timeline_post').append('<div class="container px-0 tmp_container"> \
                                    <div class="ph-item"> \
                                        <div class="ph-col-2"> \
                                            <div class="ph-avatar"></div> \
                                        </div> \
                                        <div> \
                                            <div class="ph-row big"> \
                                                <div class="ph-col-6"></div> \
                                                <div class="ph-col-6 empty"></div> \
                                                <div class="ph-col-2"></div> \
                                                <div class="ph-col-10 empty"></div> \
                                            </div> \
                                        </div> \
                                        <div class="ph-col-12"> \
                                            <div class="ph-picture"></div> \
                                        </div> \
                                    </div> \
                                </div>');
}

function registerSummernote(element, placeholder, max, callbackMax)
{
    $(element).summernote({
        dialogsInBody: true,
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'underline', 'clear']]
        ],
        placeholder,
        callbacks: {
            onKeydown: function(e) {
                var t = e.currentTarget.innerText;
                if (t.length >= max) {
                    //delete key
                    if (e.keyCode != 8)
                    e.preventDefault();
                    // add other keys ...
                }
            },
            onKeyup: function(e) {
                var t = e.currentTarget.innerText;
                if (typeof callbackMax == 'function') {
                    callbackMax(max - t.length);
                }
            },
            onPaste: function(e) {
                var t = e.currentTarget.innerText;
                var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                e.preventDefault();
                var all = t + bufferText;
                document.execCommand('insertText', false, all.trim().substring(0, 400));
                if (typeof callbackMax == 'function') {
                    callbackMax(max - t.length);
                }
            }
        }
    });
}

function uploadImage(image)
{
    var data = new FormData();
    data.append("image", image);

    $.ajax ({
        url: siteUrl + "/upload/store",
        cache: false,
        contentType: false,
        processData: false,
        global: false,
        data: data,
        dataType: "JSON",
        type: "POST"
    })
    .done(function(response) {
        $('#service-description').summernote("insertImage", response.message);
    })
    .fail(function(xhr, textStatus, errorThrown) {
        var errors = xhr.responseJSON;
        if (xhr.status == 401) {
            alert("Error: " + errorThrown);
            window.location.replace(siteUrl);
        } else {
            console.log("Error: " + errors.message);
        }
    });
}

function deleteImage(src)
{
    $.ajax ({
        url: siteUrl + "/upload/delete",
        cache: false,
        global: false,
        data: {src : src},
        dataType: "JSON",
        type: "POST"
    })
    .done(function(response) {
        console.log(response.status);
    })
    .fail(function(xhr, textStatus, errorThrown) {
        var errors = xhr.responseJSON;
        if (xhr.status == 401) {
            alert("Error: " + errorThrown);
            window.location.replace(siteUrl);
        } else {
            console.log("Error: " + errors.message);
        }
    });
}

function numberWithCommas(x)
{
    return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ".");
}

/* Back Scroll to Top */
$(".backTop").hide();
$(window).on("scroll", function()
{
    if ($(this).scrollTop() > 100) {
        $(".backTop").fadeIn();
    } else {
        $(".backTop").fadeOut();
    }
});

$(".backTop").on("click", function(e)
{
    $("html, body").animate({scrollTop: 0}, 500);
});

function toggle_review()
{
    $("#share-review").on( "click", function(e)
    {
        e.preventDefault();
        var $form = $('#serviceReviewModal form'),
            data = $form.serialize();

        $.ajax ({
            url: $form.attr("action"),
            data: data,
            type: $form.attr("method"),
            dataType: 'json',
            global: false,
            beforeSend: function() {
                $("#review-content").attr("disabled", true);
                $("#share-review").html('<i class="fas fa-spinner fa-pulse"></i> Posting');
                $("#share-review").attr("disabled", true);
            }
        })
        .always(function() {
            $("#review-content").attr("disabled", false);
            $("#share-review").html('<i class="fas fa-share-square"></i> Post</span>');
            $("#share-review").attr("disabled", false);
        })
        .done(function() {
            $("#serviceReviewModal").modal("hide");
        })
        .fail(function(xhr, textStatus, errorThrown) {
            var errors = xhr.responseJSON;
            if (xhr.status == 401) {
                alert("Error: " + errorThrown);
                window.location.replace(siteUrl);
            } else {
                alert("Error: " + errors.message);
            }
        });

        return false;
    });
}